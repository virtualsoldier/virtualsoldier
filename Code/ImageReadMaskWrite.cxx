#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"

int main( int argc, char ** argv )
{
  // Verify the number of parameters in the command line
  if( argc < 3 )
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile  outputImageFile " << std::endl;
    return EXIT_FAILURE;
    }

  typedef short                              PixelType;
  const   unsigned int                       Dimension = 3;
  typedef itk::Image< PixelType, Dimension > ImageType;
  typedef itk::ImageFileReader< ImageType >  ReaderType;
  typedef itk::ImageFileWriter< ImageType >  WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();
  const char * inputFilename  = argv[1];
  const char * outputFilename = argv[2];

  reader->SetFileName( inputFilename  );
  reader->Update();

  itk::ImageRegionConstIterator<ImageType> inputIt(reader->GetOutput(),
                                                   reader->GetOutput()->GetLargestPossibleRegion());
  itk::ImageRegionIterator<ImageType> outputIt(reader->GetOutput(),
                                               reader->GetOutput()->GetLargestPossibleRegion());
  while( !inputIt.IsAtEnd() )
    {
    outputIt.Set(  inputIt.Get() & 0x7fff );
    ++inputIt;
    ++outputIt;
    }

  writer->SetFileName( outputFilename );
  writer->SetInput( reader->GetOutput() );
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & err )
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
    }
  return EXIT_SUCCESS;
}
