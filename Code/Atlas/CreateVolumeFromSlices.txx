#ifndef _CreateVolumeFromSlices_txx
#define _CreateVolumeFromSlices_txx

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageSeriesReader.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkDICOMSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"

#include <itksys/SystemTools.hxx>

#include "itkNumericTraits.h"

#include "itkRegionOfInterestImageFilter.h"

#include "itkDICOMImageIO2.h"
#include "itkDICOMImageIO2Factory.h"

#include "CreateVolumeFromSlices.h"


namespace itk {
template <class TOutputImage>
bool CreateVolumeFromSlices(char * fileName, std::vector <int> bbValues, double * imageSpacing, TOutputImage * outputImage)
{
  typedef itk::ImageFileReader< TOutputImage >  ReaderType;
  typedef itk::ImageSeriesReader< TOutputImage > SeriesReaderType;
  typedef itk::ArchetypeSeriesFileNames ArchetypeNameGeneratorType;
  typedef itk::DICOMSeriesFileNames DICOMNameGeneratorType;
  typedef itk::ImageRegionConstIterator< TOutputImage > ConstIteratorType;
  typedef itk::ImageRegionIterator< TOutputImage > IteratorType;

  typedef itk::RegionOfInterestImageFilter < TOutputImage, TOutputImage > RegionOfInterestType;
  
  RegionOfInterestType::Pointer regionExtractor = RegionOfInterestType::New();




    itkStaticConstMacro(ImageDimension, unsigned int, TOutputImage::ImageDimension);
//  std::cout << "Dimension: " << TOutputImage::ImageDimension << std::endl;

  std::string fileExtension = itksys::SystemTools::GetFilenameExtension( fileName );

  if (fileExtension == ".mhd" && ImageDimension == 3)
    {
    // Read the images from file
    std::cout << "In CreateVolumeFromSlices.txx: Reading the images from file" << std::endl;
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName( fileName );

    try
      {
      reader->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }
    
    return true;
    }
  else
    {
    // Create the volume from the slices
    std::cout << "In CreateVolumeFromSlices.txx: Creating the volume from the slices" << std::endl;

    int minX = bbValues[0];
    int maxX = bbValues[1];
    int minY = bbValues[2];
    int maxY = bbValues[3];
    int minZ = bbValues[4];
    int maxZ = bbValues[5];
    int minSlice = bbValues[6];

    // Use the bounding box values to find the origin
    double inputOrigin[3] = {minX * imageSpacing[0], minY * imageSpacing[1], minSlice * imageSpacing[2]};

    outputImage->SetOrigin(inputOrigin);
    outputImage->SetSpacing(imageSpacing);
    outputImage->Modified();

    // Test whether the input file is a DICOM file
    itk::DICOMImageIO2::Pointer dicomIO = itk::DICOMImageIO2::New();
    bool isDicomFile = dicomIO->CanReadFile( fileName );

    // Set up a reader
    std::vector <std::string > inputImageNames;
    if (ImageDimension == 3)
      {
      // Generate the image file names based on what kind of image it is.
      std::vector <std::string > allInputImageNames;
      if (isDicomFile)
        {
        DICOMNameGeneratorType::Pointer inputImageFileGenerator = DICOMNameGeneratorType::New();  
        std::string fileNamePath = itksys::SystemTools::GetFilenamePath( fileName );
        inputImageFileGenerator->SetDirectory( fileNamePath );
        std::vector <std::string> seriesUIDs = inputImageFileGenerator->GetSeriesUIDs();
        std::vector <std::string> seriesDescriptions = inputImageFileGenerator->GetSeriesDescriptions();
        std::vector <std::string> seriesBodyParts = inputImageFileGenerator->GetSeriesBodyParts();
        std::vector <std::string> seriesScanOptions = inputImageFileGenerator->GetSeriesScanOptions();

        for (int i = 0; i < seriesUIDs.size(); i++)
          {
          std::cout << "SeriesUIDs[" << i << "]: " << seriesUIDs[i] << std::endl;
          std::cout << "SeriesDescriptions[" << i << "]: " << seriesDescriptions[i] << std::endl;
          std::cout << "SeriesBodyParts[" << i << "]: " << seriesBodyParts[i] << std::endl;
          std::cout << "SeriesScanOptions[" << i << "]: " << seriesScanOptions[i] << std::endl;
          }

        inputImageFileGenerator->SetFileNameSortingOrderToSortByImageNumber();
//        inputImageFileGenerator->SetFileNameSortingOrderToSortBySliceLocation();
//        inputImageFileGenerator->SetFileNameSortingOrderToSortByImagePositionPatient();


        allInputImageNames = inputImageFileGenerator->GetFileNames( seriesUIDs[0] );
        std::cout << "Series 0 file names" << std::endl;
        std::cout << "allInputImageNames start: " << allInputImageNames[0] << std::endl;
        std::cout << "allInputImageNames end: " << allInputImageNames[allInputImageNames.size()-1] << std::endl;
        }
      else
        {
        ArchetypeNameGeneratorType::Pointer inputImageFileGenerator = ArchetypeNameGeneratorType::New();      
        inputImageFileGenerator->SetArchetype( fileName );
        allInputImageNames = inputImageFileGenerator->GetFileNames();
        std::cout << "allInputImageNames start: " << allInputImageNames[0] << std::endl;
        std::cout << "allInputImageNames end: " << allInputImageNames[allInputImageNames.size()-1] << std::endl;
        }
    
      std::cout << "minZ: " << minZ << std::endl;
      std::cout << "maxZ: " << maxZ << std::endl;

      // Copy only those filenames that correspond to the bounding boxes
      // of the given structure
      std::vector <std::string>::iterator allNameIt;    
      for (allNameIt = allInputImageNames.begin() + minZ; allNameIt != allInputImageNames.begin() + (maxZ+1); allNameIt++ )
        {
        inputImageNames.push_back( *allNameIt );
        }

      std::cout << "In CreateVolumeFromSlices.txx: inputImageNames start: " << inputImageNames[0] << std::endl;
      std::cout << "In CreateVolumeFromSlices.txx: inputImageNames end: " << inputImageNames[inputImageNames.size()-1] << std::endl;

      // Read in the images using a series reader
      SeriesReaderType::Pointer reader = SeriesReaderType::New();

      reader->SetFileNames( inputImageNames );

      // Update the reader
      std::cout << "In CreateVolumeFromSlices.txx: Update the reader" << std::endl;
      try
        {
        reader->Update();
        }
      catch( itk::ExceptionObject & excep )
        {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        }


      // Extract a region out corresponding to the bounding boxes out of
      // the reader.  The reader has the correct z bounds but not the
      // correct x and y bounds.  These regions cannot be set in the
      // reader, so the region has to be extracted in a subsequent step.
      typename TOutputImage::RegionType::IndexType regionExtractorStart;
      regionExtractorStart[0] = minX;
      regionExtractorStart[1] = minY;
      if (ImageDimension == 3)
        {
        regionExtractorStart[2] = 0;
        }

      typename TOutputImage::RegionType::SizeType regionExtractorSize;
      regionExtractorSize[0]  = maxX - minX + 1;
      regionExtractorSize[1]  = maxY - minY + 1;
      if (ImageDimension == 3)
        {
        regionExtractorSize[2] = reader->GetOutput()->GetRequestedRegion().GetSize()[2];
        }

      typename TOutputImage::RegionType regionOfInterest;
      regionOfInterest.SetIndex( regionExtractorStart );
      regionOfInterest.SetSize(  regionExtractorSize  );

      regionExtractor->SetInput( reader->GetOutput() );
      regionExtractor->SetRegionOfInterest( regionOfInterest );

      std::cout << "In CreateVolumeFromSlices.txx: Update the extractor" << std::endl;
      try
        {
        regionExtractor->Update();
        }
      catch( itk::ExceptionObject & excep )
        {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        }
  

      // Allocate the output image
      outputImage->SetRegions( regionExtractor->GetOutput()->GetRequestedRegion() );
      outputImage->Allocate();
      outputImage->FillBuffer(0);


      // Copy all of the input image into the output image.
      ConstIteratorType inputIt( regionExtractor->GetOutput(), regionExtractor->GetOutput()->GetRequestedRegion() );
      IteratorType outputIt( outputImage, outputImage->GetRequestedRegion() );
      inputIt.GoToBegin();
      outputIt.GoToBegin();

      while ( !inputIt.IsAtEnd() )
        {
        outputIt.Set( inputIt.Get() );

        ++inputIt;
        ++outputIt;
        }

      }
    else if (ImageDimension == 2)
      {
      std::cout << "Filename: " << fileName << std::endl;
      ReaderType::Pointer reader = ReaderType::New();
      reader->SetFileName( fileName );     

      if (isDicomFile)
        {
        // Register one factory capable of creating DicomImage readers
        itk::DICOMImageIO2Factory::RegisterOneFactory();
        }

      // Update the reader
      std::cout << "In CreateVolumeFromSlices.txx: Update the reader" << std::endl;
      try
        {
        reader->Update();
        }
      catch( itk::ExceptionObject & excep )
        {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        }



      // Extract a region out corresponding to the bounding boxes out of
      // the reader.  The reader has the correct z bounds but not the
      // correct x and y bounds.  These regions cannot be set in the
      // reader, so the region has to be extracted in a subsequent step.
      typename TOutputImage::RegionType::IndexType regionExtractorStart;
      regionExtractorStart[0] = minX;
      regionExtractorStart[1] = minY;
      if (ImageDimension == 3)
        {
        regionExtractorStart[2] = 0;
        }

      typename TOutputImage::RegionType::SizeType regionExtractorSize;
      regionExtractorSize[0]  = maxX - minX + 1;
      regionExtractorSize[1]  = maxY - minY + 1;
      if (ImageDimension == 3)
        {
        regionExtractorSize[2] = reader->GetOutput()->GetRequestedRegion().GetSize()[2];
        }

      typename TOutputImage::RegionType regionOfInterest;
      regionOfInterest.SetIndex( regionExtractorStart );
      regionOfInterest.SetSize(  regionExtractorSize  );

      regionExtractor->SetInput( reader->GetOutput() );
      regionExtractor->SetRegionOfInterest( regionOfInterest );

      std::cout << "In CreateVolumeFromSlices.txx: Update the extractor" << std::endl;
      try
        {
        regionExtractor->Update();
        }
      catch( itk::ExceptionObject & excep )
        {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        }
  

      // Allocate the output image
      outputImage->SetRegions( regionExtractor->GetOutput()->GetRequestedRegion() );
      outputImage->Allocate();
      outputImage->FillBuffer(0);


      // Copy all of the input image into the output image.
      ConstIteratorType inputIt( regionExtractor->GetOutput(), regionExtractor->GetOutput()->GetRequestedRegion() );
      IteratorType outputIt( outputImage, outputImage->GetRequestedRegion() );
      inputIt.GoToBegin();
      outputIt.GoToBegin();

      while ( !inputIt.IsAtEnd() )
        {
        outputIt.Set( inputIt.Get() );

        ++inputIt;
        ++outputIt;
        }

      }
    else
      {
      std::cout << "Wrong number of dimensions (must be either 2 or 3)" << std::endl;
      return false;
      }

  

    return true;

    }
}
}
#endif
