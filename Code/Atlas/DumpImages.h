/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    $RCSfile: DumpImages.h,v $
  Language:  C++
  Date:      $Date: 2005/01/19 00:19:35 $
  Version:   $Revision: 1.5 $

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef _itkFileDumper_h
#define _itkFileDumper_h

// The following class is a convenience  to dump files 
#include "itkRescaleIntensityImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageSeriesWriter.h"
#include "itkNumericTraits.h"
namespace itk {
template < class TInputImage >
class FileDumper
{
public:
  typedef FileDumper Self;
  itkStaticConstMacro(ImageDimension, unsigned int, TInputImage::ImageDimension);
  typedef Image<unsigned char, itkGetStaticConstMacro(ImageDimension)> OutputImageType;
  typedef Image<unsigned char, 2> WriterImageType;
  FileDumper(const TInputImage* o, std::string filename="foo", std::string extension="%03d.png")
  {
    std::string seriesFormat(filename);
    seriesFormat += extension;
    m_Rescaler = RescaleIntensityImageFilter<TInputImage,OutputImageType>::New();
    m_Rescaler->SetInput(o);
    m_Rescaler->SetOutputMinimum(0);
    m_Rescaler->SetOutputMaximum(255);

    m_Writer = ImageSeriesWriter<OutputImageType,WriterImageType>::New();
    m_Writer->SetSeriesFormat(seriesFormat.c_str());
    m_Writer->SetInput(m_Rescaler->GetOutput());
    m_Writer->Update();
    std::cout << filename << " - Input min,max: "
              << m_Rescaler->GetInputMinimum() << ", "
              << m_Rescaler->GetInputMaximum() << std::endl;
  }
  ~FileDumper() {};
protected:

private:
  FileDumper(); // Purposely not implemented
  void operator=(const Self&); //purposely not implemented

  typename RescaleIntensityImageFilter<TInputImage,OutputImageType>::Pointer m_Rescaler;
  typename ImageSeriesWriter<OutputImageType,WriterImageType>::Pointer m_Writer;

};
template < class TInputImage >
class ThresholdedFileDumper
{
public:
  typedef ThresholdedFileDumper Self;
  itkStaticConstMacro(ImageDimension, unsigned int, TInputImage::ImageDimension);
  typedef Image<unsigned char, itkGetStaticConstMacro(ImageDimension)> OutputImageType;
  typedef Image<unsigned char, 2> WriterImageType;
  ThresholdedFileDumper(const TInputImage* o, std::string filename="foo", std::string extension="%03d.png")
  {
    
    m_BinaryThreshold = BinaryThresholdImageFilter<TInputImage,TInputImage>::New();
    m_BinaryThreshold->SetInput(o);
    m_BinaryThreshold->SetInsideValue(0);
    m_BinaryThreshold->SetOutsideValue(255);
    m_BinaryThreshold->SetLowerThreshold(0);
    m_BinaryThreshold->SetUpperThreshold(NumericTraits<typename TInputImage::PixelType>::max());
    
    FileDumper<TInputImage> dumpFile(m_BinaryThreshold->GetOutput(), filename, extension);
  }
  ~ThresholdedFileDumper() {};
protected:

private:
  ThresholdedFileDumper(); // Purposely not implemented
  void operator=(const Self&); //purposely not implemented

  typename BinaryThresholdImageFilter<TInputImage,TInputImage>::Pointer m_BinaryThreshold;
};
}
#endif
