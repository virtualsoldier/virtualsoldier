#include "FindAnatomyNameFromLabel.h"

#include <iostream>
#include <fstream>

bool FindAnatomyNameFromLabel(char * fileName, int anatomyLabel, std::string & anatomyName)
{
  std::ifstream anatomyFile(fileName);
  if (!anatomyFile) 
    {
    std::cout << "\tCould not open master anatomy file: " << anatomyFile << std::endl;
    return false;
    }

  // Default value for anatomyName for error checking
  anatomyName = "";

  // Declare the variables.
  char entireLine[255];
  std::string line;
  int endLine;
  int begin;
  int nextComma;
  int fileAnatomyLabel;


  // Get rid of header and test it
  begin = 0;
  anatomyFile.getline(entireLine,255);
  line=std::string(entireLine); 
  nextComma = line.find(',',begin);
  std::string headerName = line.substr(0,nextComma-begin);
  if (headerName != "Anatomy")
    {
    return false;
    }


  // Loop through the file looking for the anatomy label.  If it is
  // found, assign its name to a variable and break.
  while (! anatomyFile.eof() )
    {
    anatomyFile.getline(entireLine,255);
    line=std::string(entireLine); 
    endLine = line.length();

    if (! line.empty() )
      {
      begin = 0;
      nextComma = line.find(',',begin);

      fileAnatomyLabel = atoi(line.substr(nextComma+1,endLine-(nextComma+1)).c_str());

      if (fileAnatomyLabel == anatomyLabel)
        {
        anatomyName = line.substr(begin,nextComma-begin);
        break;
        }
      }
    }

  anatomyFile.close();

  // Make sure that the anatomy name was found
  if (anatomyName == "")
    {
    std::cerr << "ERROR: Label " << anatomyLabel << " not listed in MasterAnatomy file" << std::endl;
    return false;
    }
  else
    {
    return true;
    }
}

