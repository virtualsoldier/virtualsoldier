/*=========================================================================

This program is primarily responsible for relabeling the label
images.  The structures of the original label images are labeled
independently on each slice.  Therefore, the labels of structures are
not consistent across slices.  This program reads in all of the label files
and creates a master list of all of the structures in the volume; it
then numbers these structures sequentially and prints out a file of
the mapping between the structures and their new labels.  It then
reads each label image, maps each old label to the new label, and
writes out a relabeled image.  In the process, it also calculates the
bounding boxes for each structure and writes out a file that lists
each structure with its bounding boxes.

Inputs: Label files, label images.
Outputs: Relabeled images, master anatomy file, bounding boxes file. 

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkArray.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionIteratorWithIndex.h"

#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <map>

#include "FindImageInfo.h"

// This function takes an iterator to a vector of label file names,
// parses the file line by line, and passes back a vector of all of
// the anatomy names and a vector of all of the anatomy labels.
bool ParseLabelFile( std::vector <std::string>::iterator nit, std::vector <std::string> &, std::vector <int> & );

// This function reads a line from a file and parses it into its NIH
// label, anatomy name, and anatomy label.  It is called by ParseLabelFile.
void ParseLabelLine( std::string line, std::string &anatomy, int &nLabel );

int main( int argc, char *argv[] )
{
  if (argc < 7)
    {
    std::cout << "Usage: "
              << "lbl_Path " 
              << "input_Path " 
              << "relabeled_Path " 
              << "masterAnatomyFile " 
              << "bbIndicesArrayFile " 
              << "featureImages " 
              << std::endl;
    return 0;
    }  

  
  // Save all arguments as local variables
  char * labelFiles = argv[1];
  char * labelImages = argv[2];
  char * relabelFiles = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  char * featureImages = argv[6];

  // Test the master anatomy output file
  std::ofstream fout1 ( masterFile );
  if (!fout1)
    {
    std::cout << "\tCould not open master anatomy output file" << std::endl;
    return false;
    }

  // Test the bounding boxes file
  std::ofstream fout2 ( boundingBoxesFile );
  if (!fout2) 
    {
    std::cout << "\tCould not open bounding box output file" << std::endl;
    return false;
    }


  typedef   unsigned short   InputPixelType;
  typedef   unsigned short  OutputPixelType;
  
  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::Image< OutputPixelType, 2 >   OutputImageType;
  
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::ImageFileWriter< OutputImageType >  WriterType;

  typedef itk::NumericSeriesFileNames NameGeneratorType;
  
  typedef std::map< std::string, int > StringToIntMapType; //Do we want to map to int or some other data type?
  typedef std::map< int, std::string > IntToStringMapType;
  typedef std::map< int, int> IntToIntMapType;
  
  typedef std::set<std::string> StringSetType;  
  
  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();
  OutputImageType::Pointer outputImage = OutputImageType::New();  
  
  NameGeneratorType::Pointer labelFileGenerator = NameGeneratorType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();
  NameGeneratorType::Pointer outputImageFileGenerator = NameGeneratorType::New();

  StringSetType masterAnatomyStringList;
  StringToIntMapType masterAnatomyStringToIntMapper;

  IntToStringMapType localAnatomyIntToStringMapper; 
  IntToIntMapType localAnatomyIntToIntMapper;


  // Find the image information
  int sliceOffset;
  int inputImageSize[3];
  double imageSpacing[3]; // not used
  if (! FindImageInfo(featureImages, sliceOffset, inputImageSize, imageSpacing) )
    { 
    std::cerr << "In RelabelSlices.cxx: Cannot find image information" << std::endl; 
    return 0;
    }

  // Generate the label file names
  labelFileGenerator->SetStartIndex( sliceOffset );
  labelFileGenerator->SetEndIndex ( inputImageSize[2] + sliceOffset - 1 );
  labelFileGenerator->SetIncrementIndex ( 1 );
  labelFileGenerator->SetSeriesFormat ( labelFiles );
  
  // Generate the input label image names
  inputImageFileGenerator->SetStartIndex( sliceOffset );
  inputImageFileGenerator->SetEndIndex ( inputImageSize[2] + sliceOffset - 1 );
  inputImageFileGenerator->SetIncrementIndex ( 1 );
  inputImageFileGenerator->SetSeriesFormat ( labelImages );
  
  // Generate the output label image names
  outputImageFileGenerator->SetStartIndex( sliceOffset );
  outputImageFileGenerator->SetEndIndex ( inputImageSize[2] + sliceOffset - 1 );
  outputImageFileGenerator->SetIncrementIndex ( 1 );
  outputImageFileGenerator->SetSeriesFormat ( relabelFiles );

  std::cout << labelFileGenerator->GetFileNames()[0] << std::endl;
  std::cout << inputImageFileGenerator->GetFileNames()[0] << std::endl;
  std::cout << outputImageFileGenerator->GetFileNames()[0] << std::endl;

  std::vector <std::string > labelNames = labelFileGenerator->GetFileNames();
  std::vector <std::string > inputImageNames = inputImageFileGenerator->GetFileNames();
  std::vector <std::string > outputImageNames = outputImageFileGenerator->GetFileNames();
  
  std::vector < std::string > vectorOfAnatomies;
  std::vector < int > vectorOfLabels;
  
  std::vector < std::string >::iterator vait;

  
  // Loop through each anatomy file and populate a vector of anatomy strings and a vector of label ints
  for (std::vector <std::string >::iterator nit = labelNames.begin(); nit != labelNames.end(); nit++)
    {
    vectorOfAnatomies.clear();
    vectorOfLabels.clear();
    
    if (! ParseLabelFile(nit, vectorOfAnatomies, vectorOfLabels)) 
      {
      std::cout << "Could not parse the files" << std::endl;
      return false;
      }
    
    // Copy the anatomy names into a string list.  The string list
    // does not allow duplicates, so if, on the next iteration, an
    // anatomy name is in the vector that is already in the string list, 
    // the string list will not add it to its list.
    // The string list is also ordered alphabetically
    for (vait = vectorOfAnatomies.begin(); vait != vectorOfAnatomies.end(); vait++) 
      {
      masterAnatomyStringList.insert( *vait );
      }
    }
  
  std::cout << "MasterAnatomyStringListSize: " << masterAnatomyStringList.size() << std::endl;
  
  
  // The following populates the mapper, which will contain a mapping 
  // from the anatomy name to its label.
  // We number the anatomies sequentially, starting with 1.
  // "Unknown" is set to 0 since this is the standard
  int i=1;
  StringSetType::iterator itS;
  for (itS = masterAnatomyStringList.begin(); itS != masterAnatomyStringList.end(); ++itS)
    {
    if (*itS == "unknown")
      {
      masterAnatomyStringToIntMapper.insert(StringToIntMapType::value_type( *itS, 0));
      }
    else
      {
      masterAnatomyStringToIntMapper.insert(StringToIntMapType::value_type( *itS, i));
      i++;
      }
    }

  // Write out the master anatomy file.
  fout1 << "Anatomy,Label" << std::endl;
  for( StringToIntMapType::iterator itM=masterAnatomyStringToIntMapper.begin(); itM!=masterAnatomyStringToIntMapper.end(); itM++ )
    {
    fout1 << itM->first << "," << itM->second << std::endl;
    std::cout << itM->first << "," << itM->second << std::endl;    
    }
  fout1.close();
  


  // Find the size of the input image.
  // The size is the same for all slices, so only the size of the first slice will be found.
  std::cout << "First file name: " << inputImageNames[0] << std::endl;
  reader->SetFileName( inputImageNames[0].c_str() );

  try 
    { 
    reader->Update();
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 
  
/*
  // Get the size of the input image
  reader->GetOutput()->SetRequestedRegionToLargestPossibleRegion();
  InputImageType::SizeType inputImageSize;
  inputImageSize = (reader->GetOutput()->GetLargestPossibleRegion()).GetSize();
*/  

  // bbIndicesArray stands for boundingBoxIndicesArray
  // The rows of bbIndicesArray correspond to the different anatomies and
  // the columns correspond to the min and max indices of the bounding box for that
  // particular anatomy.
  // The order of the indices is as follows: [minX maxX minY maxY minZ maxZ]
  // These correspond to: [minJ maxJ minI maxI minSlice maxSlice]

  int numberOfAnatomies = masterAnatomyStringList.size();
  
  // Rows will correspond to the anatomies
  // Columns will correspond to the min and max of x, y, and z indices
  int bbIndicesArray[1024][6];
  
  if (numberOfAnatomies > 1024)
    {
    std::cout << "More than 1024 anatomies: cannot fit them in boundary box indices array" << std::endl;
    return 0;
    }


  // Initialize the array.  Set the value of the min indices to the size
  // of the corresponding dimension.  Set the max indices to 0.  This
  // ensures that a new index will be either less than the min index
  // or greater than the max index (both for the first pixelValue)
  for( StringToIntMapType::iterator itM=masterAnatomyStringToIntMapper.begin(); itM!=masterAnatomyStringToIntMapper.end(); itM++ )
    {
    bbIndicesArray[itM->second][0] = inputImageSize[0]-1;
    bbIndicesArray[itM->second][1] = 0;
    bbIndicesArray[itM->second][2] = inputImageSize[1]-1;
    bbIndicesArray[itM->second][3] = 0;
    bbIndicesArray[itM->second][4] = inputImageSize[2]-1;
    bbIndicesArray[itM->second][5] = 0;
    }


  // Create an output image
  // The output image for each labeled slice will be a relabeled
  // slice, where the old labels are replaced by the global labels.
  // It should be the same size as the input image
  // This image will be over-written for each slice iteration
  OutputImageType::IndexType outputImageStart;
  outputImageStart[0] = 0;
  outputImageStart[1] = 0;
  
  OutputImageType::SizeType outputImageSize;
  outputImageSize[0] = inputImageSize[0];
  outputImageSize[1] = inputImageSize[1];
  
  OutputImageType::RegionType outputRegion;
  outputRegion.SetSize(  outputImageSize  );
  outputRegion.SetIndex( outputImageStart );
  
  outputImage->SetRegions(outputRegion);
  outputImage->Allocate();
  outputImage->FillBuffer(0);
  
  std::cout << "Image Dimension: " << outputImage->GetImageDimension() << std::endl;
  std::cout << "Largest Possible Region: " << outputImage->GetLargestPossibleRegion() << std::endl;
  

  int outputPixelValue;
  
  std::vector <std::string> ::iterator lnit = labelNames.begin();
  std::vector <std::string> ::iterator iinit = inputImageNames.begin();
  std::vector <std::string> ::iterator oinit = outputImageNames.begin();
    
  std::vector < int > ::iterator vlit;
  
  int slice;
  // Loop through each anatomy file
  while (lnit != labelNames.end())
    {
    slice = lnit - labelNames.begin();
    std::cout << "Slice: " << slice << std::endl;

    // clear the local mappers
    localAnatomyIntToStringMapper.clear();
    localAnatomyIntToIntMapper.clear();

    // clear the local vectors
    vectorOfAnatomies.clear();
    vectorOfLabels.clear();

    // Parse one file
    if (! ParseLabelFile(lnit, vectorOfAnatomies, vectorOfLabels)) 
      {
      std::cout << "Could not parse the files" << std::endl;
      return false;
      }
    
    // Populate the local IntToString and IntToInt mappers.
    // The IntToString mapper maps from the local label to the local
    // anatomy name.
    // The IntToInt mapper maps from the local label to the master label.
    vait = vectorOfAnatomies.begin();
    vlit = vectorOfLabels.begin();
    while(vait != vectorOfAnatomies.end()) 
      {
      localAnatomyIntToStringMapper.insert(IntToStringMapType::value_type( *vlit, *vait));
      localAnatomyIntToIntMapper.insert(IntToIntMapType::value_type( *vlit, masterAnatomyStringToIntMapper[*vait] ));
      ++vait;
      ++vlit;
      }

// TESTING
/*
    // Look at output of local to master label mapper
    for( IntToIntMapType::iterator itTEST=localAnatomyIntToIntMapper.begin(); itTEST!=localAnatomyIntToIntMapper.end(); itTEST++ )
    {
    std::cout << "local label: " << itTEST->first << " mapped to master label: " << itTEST->second << std::endl;    
    }

    // Look at output of local label to name mapper
    for( IntToStringMapType::iterator itTEST2=localAnatomyIntToStringMapper.begin(); itTEST2!=localAnatomyIntToStringMapper.end(); itTEST2++ )
    {
    std::cout << "local label: " << itTEST2->first << " mapped to master label: " << itTEST2->second << std::endl;    
    }
*/


    // Read the image
    reader->SetFileName ( (*iinit).c_str() );
      
    try 
      { 
      reader->Update();
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 
      
      
    writer->SetFileName ( (*oinit).c_str() );
    writer->SetInput(outputImage);

    // Reset the output image to zero
    outputImage->FillBuffer(0);

    itk::ImageRegionIteratorWithIndex  < InputImageType > it (reader->GetOutput(),reader->GetOutput()->GetRequestedRegion());
    itk::ImageRegionIteratorWithIndex  < InputImageType > lit (outputImage, outputImage->GetRequestedRegion());

    InputImageType::IndexType inputImageIndex;

    it.GoToBegin();
    lit.GoToBegin();
    while (!it.IsAtEnd())
      {
      // Check whether the label is in the list of labels.  The list
      // of labels contains only those defined in the label files, so
      // it is possible that a label exists that was not defined in
      // the label file.  "Find" returns an iterator that designates the
      // first element whose sort key equals Key. If no such element
      // exists, the iterator equals end.
//       IntToIntMapType::iterator localIterator;
//       int testPixelValue;
//       localIterator = localAnatomyIntToIntMapper.find( it.Get() );
//       if (localIterator == localAnatomyIntToIntMapper.end() )
//         {
//         testPixelValue = localAnatomyIntToIntMapper[ it.Get() ];
//         std::cout << "Label not in list: " << it.Get() << std::endl;
//         std::cout << "Label it gets mapped to: " << testPixelValue << std::endl;
//         }

      // map the local label to the global label and set the 
      // output to be this new label
      lit.Set( localAnatomyIntToIntMapper[ it.Get() ] );
      outputPixelValue = lit.Get();
	  
      // Update the bounding box array
      // This is the x,y,z location of the pixel
      inputImageIndex = it.GetIndex();

      // Replace the minimum x index if necessary
      if (inputImageIndex[0] < bbIndicesArray[outputPixelValue][0]) {
      bbIndicesArray[outputPixelValue][0] = inputImageIndex[0]; }
	    
      // Replace the maximum x index if necessary
      if (inputImageIndex[0] > bbIndicesArray[outputPixelValue][1]) {
      bbIndicesArray[outputPixelValue][1] = inputImageIndex[0]; }

      // Replace the minimum y index if necessary
      if (inputImageIndex[1] < bbIndicesArray[outputPixelValue][2]) {
      bbIndicesArray[outputPixelValue][2] = inputImageIndex[1]; }

      // Replace the maximum y index if necessary
      if (inputImageIndex[1] > bbIndicesArray[outputPixelValue][3]) {
      bbIndicesArray[outputPixelValue][3] = inputImageIndex[1]; }

      // Replace the minimum z index if necessary
      if (slice < bbIndicesArray[outputPixelValue][4]) {
      bbIndicesArray[outputPixelValue][4] = slice; }

      // Replace the maximum z index if necessary
      if (slice > bbIndicesArray[outputPixelValue][5]) {
      bbIndicesArray[outputPixelValue][5] = slice; }

      // increment the iterators
      ++it;
      ++lit;
      }

    // Write out the image
    try 
      { 
      writer->Update(); 
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 

    ++lnit;
    ++iinit;
    ++oinit;
    }

  // Write out the bounding boxes file
  fout2 << "Anatomy,minX,maxX,minY,maxY,minZ,maxZ,minSlice,maxSlice" << std::endl;
  for( StringToIntMapType::iterator itM=masterAnatomyStringToIntMapper.begin(); itM!=masterAnatomyStringToIntMapper.end(); itM++ )
    {
    fout2 << itM->first
          << "," << bbIndicesArray[itM->second][0]
          << "," << bbIndicesArray[itM->second][1]
          << "," << bbIndicesArray[itM->second][2]
          << "," << bbIndicesArray[itM->second][3]
          << "," << bbIndicesArray[itM->second][4]
          << "," << bbIndicesArray[itM->second][5]
          << "," << bbIndicesArray[itM->second][4]+sliceOffset
          << "," << bbIndicesArray[itM->second][5]+sliceOffset
          << std::endl;
    }
  fout2.close();
   
  return 0;
  
}

bool ParseLabelFile( std::vector <std::string >::iterator nit, std::vector <std::string> &vectorOfAnatomies, std::vector <int> &vectorOfLabels )
{
  char entireLine[255];
  std::string line;
  std::string anatomy;
  int label; 

  std::ifstream inputFile ( (*nit).c_str() );
  
  if (!inputFile)
    {
    std::cout << "\tCould not open label file: " <<  (*nit).c_str() << std::endl;
    return false;
    }
  
  // For each anatomy file, read the NIH label, the anatomy name, and the label
  while (! inputFile.eof() )
    {
    inputFile.getline(entireLine,255);
    line=std::string(entireLine); 
      
    //Extract anatomical structure name from the line 
    // Then push it onto the vector of anatomies
    if (! line.empty() )
      {
      ParseLabelLine(line, anatomy, label); 
	  
      // Add anatomy to the vector of anatomies
      vectorOfAnatomies.push_back(anatomy);
      vectorOfLabels.push_back(label);
      }
    }
  inputFile.close();
  
  return true;
}


void ParseLabelLine(std::string line, std::string &anatomy, int &nLabel)
{
  int beginName = 0;
  int endName = 0;
  int sIndex = 0;

  // This part finds the first and last indices of the anatomy name.
  // Thus, everything before the first is the NIH number, everything
  // after the last is the label number, and everything in between is
  // the anatomy name.  That is, everything except special characters.
  std::string::iterator lit = line.begin();
  while (lit != line.end())
    {
    // If the character is uppercase, replace it with lowercase
    if ((*lit) >= 'A' && (*lit) <= 'Z')
      {
      line.replace(sIndex,1,1,*lit+32);
	  
      if(beginName == 0) 
        {
        beginName = sIndex; 
        }
      endName = sIndex;
      }
    // If the character is lowercase, find the name's beginning and end
    else if ((*lit) >= 'a' && (*lit) <= 'z')
      {
      if(beginName == 0) 
        {
        beginName = sIndex; 
        }
      endName = sIndex;
      }
    // Ignore numbers since they belong to the label not the name
    else if ((*lit) >= '0' && (*lit) <= '9')
      {
      // Do nothing.
      }
    // Ignore spaces
    else if ((*lit)== ' ')
      {
      // Do nothing.
      }
    // Erase everything else
    else
      {
      line.erase(sIndex,1);
      --sIndex;
      --lit;
      }
      
    ++lit;
    ++sIndex;
    }
  
  int nNIH;
  nNIH = atoi(line.substr(0,beginName-1).c_str());
  //std::cout << "NIHValue: " << nNIH << std::endl;
  anatomy = line.substr(beginName,endName-beginName+1);
  //std::cout << "Anatomy: " << anatomy << std::endl;
  //std::cout << "AnatomyLength: " << anatomy.length() << std::endl;
  
  nLabel =  atoi(line.substr(endName+1,line.length()-endName).c_str());
  //std::cout << "Label: " << nLabel << std::endl;
}


//---Possibly useful:
//std::string("vec.hdr")
//  std::string fullfname=(fname+fnum+exte); //note concatenation.  fname,fnum,exte are strings

//masterAnatomyStringList.size()
//masterAnatomyStringList.clear();

