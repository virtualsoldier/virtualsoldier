#include "itkImage.h"
#include "itkCannySegmentationLevelSetImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkExceptionObject.h"
#include "itkExtractImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

#include "itkVector.h"


// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"
#include "WriteModelToFile.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"


int main( int argc, char *argv[] )
{

  int numberOfParameters = 17;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " ImageInfoFile";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " AdvectionScaling";
    std::cerr << " CannyThreshold ";
    std::cerr << " CannyVariance ";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " BorderSize";
    std::cerr << " DisplayImageType";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  std::string outputModel = argv[6];
  std::string anatomyName = argv[7];
  float propScaling = atof( argv[8] );
  float curvScaling = atof( argv[9] );
  float advecScaling = atof( argv[10] );
  float cannyThreshold = atof( argv[11] );
  float cannyVariance = atof( argv[12] );
  int iterations = atoi( argv[13] );
  float conductance = atof( argv[14] );
  int borderSize = atoi( argv[15] );
  std::string displayImageType = argv[16];


  
  typedef itk::Image< float, 2 > Float2DImageType;
  typedef itk::Image< float, 3 > Float3DImageType;
  typedef itk::Image< unsigned short, 2 > UShort2DImageType;
  typedef itk::Image< unsigned short, 3 > UShort3DImageType;
  typedef itk::Image< unsigned char, 2 > UChar2DImageType;

  typedef itk::ImageFileReader < UShort2DImageType > ReaderTypeUShort2D;
  typedef itk::NumericSeriesFileNames NameGeneratorType;

  typedef itk::LabelStatisticsImageFilter< UShort3DImageType, UShort3DImageType > LabelStatisticsFilterType;

  // Filters for the label image
  typedef itk::BinaryThresholdImageFilter< UShort3DImageType, UShort3DImageType> ThresholdingFilterType1;
  typedef itk::ResampleImageFilter< UShort3DImageType, UShort3DImageType> ResampleType;
  typedef itk::NearestNeighborInterpolateImageFunction< UShort3DImageType, double >  NearestNeighborInterpolatorType;
  typedef itk::BinaryBallStructuringElement< unsigned short, 3> KernelType;
  typedef itk::BinaryErodeImageFilter< UShort3DImageType, UShort3DImageType, KernelType>
    ErodeFilterType;

  // Filters for the feature image
  typedef itk::LinearInterpolateImageFunction< UShort3DImageType, double >  LinearInterpolatorType;
  typedef itk::CastImageFilter< UShort3DImageType, Float3DImageType > CastImageFilterType;  
  typedef itk::GradientAnisotropicDiffusionImageFilter< Float3DImageType, Float3DImageType> DiffusionFilterType;
  typedef itk::CannySegmentationLevelSetImageFilter< UShort3DImageType, Float3DImageType > CannySegmentationLevelSetImageFilterType;
  typedef itk::BinaryThresholdImageFilter< Float3DImageType, UShort3DImageType> ThresholdingFilterType2;

  // Instantiate the filters
  ReaderTypeUShort2D::Pointer reader = ReaderTypeUShort2D::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();
  
  UShort3DImageType::Pointer featureImage = UShort3DImageType::New();
  UShort3DImageType::Pointer labelImage = UShort3DImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  // Filters for the label image
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();
  ResampleType::Pointer labelResampler = ResampleType::New();
  NearestNeighborInterpolatorType::Pointer nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();
  ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();

  // Filters for the feature image
  ResampleType::Pointer featureResampler = ResampleType::New();
  LinearInterpolatorType::Pointer linearInterpolator = LinearInterpolatorType::New();
  CastImageFilterType::Pointer castToFloat = CastImageFilterType::New();
  CastImageFilterType::Pointer castToFloat2 = CastImageFilterType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();
  CannySegmentationLevelSetImageFilterType::Pointer cannySegmentation = CannySegmentationLevelSetImageFilterType::New();
  ThresholdingFilterType2::Pointer thresholder2 = ThresholdingFilterType2::New();

  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImagePrefix,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize, inputImageSpacing) )
    {
    return 0;
    }


  // These are used for determining the model origin
  int minX = bbValues[0] ;
  int maxX = bbValues[1] ;
  int minY = bbValues[2] ;
  int maxY = bbValues[3] ;
  int minZ = bbValues[4] ;
  int maxZ = bbValues[5] ;
  int minSlice = bbValues[6] ;
  int maxSlice = bbValues[7] ;

  std::cout << "Anatomy label: " << anatomyLabel << std::endl;
  std::cout << "minX: " << bbValues[0] << std::endl;
  std::cout << "maxX: " << bbValues[1] << std::endl;
  std::cout << "minY: " << bbValues[2] << std::endl;
  std::cout << "maxY: " << bbValues[3] << std::endl;
  std::cout << "minZ: " << bbValues[4] << std::endl;
  std::cout << "maxZ: " << bbValues[5] << std::endl;
  std::cout << "minSlice: " << bbValues[6] << std::endl;
  std::cout << "maxSlice: " << bbValues[7] << std::endl;



  if (argc > numberOfParameters)
    {
    // Read the images from file
    typedef itk::ImageFileReader< UShort3DImageType >  ReaderType;
    ReaderType::Pointer featureReader = ReaderType::New();
    ReaderType::Pointer labelReader = ReaderType::New();
    featureReader->SetFileName(argv[numberOfParameters]);
    labelReader->SetFileName(argv[numberOfParameters+1]);
    featureImage = featureReader->GetOutput();
    labelImage = labelReader->GetOutput();

    try
      {
      featureImage->Update();
      labelImage->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }
    }
  else
    {
    // Create 3D volumes of the input slices according to the bounding
    // box values
    std::cout << "Creating feature volume from feature image slices" << std::endl;
    itk::CreateVolumeFromSlices<UShort3DImageType>( featureImagePrefix, bbValues, inputImageSpacing, featureImage);
    std::cout << "Creating label volume from label image slices" << std::endl;
    itk::CreateVolumeFromSlices<UShort3DImageType>( labelImagePrefix, bbValues, inputImageSpacing, labelImage);
    }

  std::cout << "Feature image spacing: " << featureImage->GetSpacing() << std::endl;
  std::cout << "Feature image origin: " << featureImage->GetOrigin() << std::endl;
  std::cout << "Feature image size: " << featureImage->GetRequestedRegion().GetSize() << std::endl;

  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

  labelResampler->SetInput ( labelImage );
  thresholder1->SetInput( labelResampler->GetOutput() );
  erodeFilter->SetInput( thresholder1->GetOutput() );
  cannySegmentation->SetInput( erodeFilter->GetOutput() );

  featureResampler->SetInput ( featureImage );
  castToFloat->SetInput ( featureResampler->GetOutput() );
  diffusion->SetInput ( castToFloat->GetOutput() );
  cannySegmentation->SetFeatureImage( diffusion->GetOutput());
  thresholder2->SetInput( cannySegmentation->GetOutput() );

  // TESTING
//  castToFloat2->SetInput( labelResampler->GetOutput() );
//  itkExporter->SetInput( castToFloat2->GetOutput() );


  // Resample the image to have isotropic spacing by downsampling the
  // X and Y dimensions.
  UShort3DImageType::SpacingType inputSpacing;
  inputSpacing[0] = featureImage->GetSpacing()[0];
  inputSpacing[1] = featureImage->GetSpacing()[1];
  inputSpacing[2] = featureImage->GetSpacing()[2];

  UShort3DImageType::SizeType inputSize;
  inputSize[0] = featureImage->GetRequestedRegion().GetSize()[0];
  inputSize[1] = featureImage->GetRequestedRegion().GetSize()[1];
  inputSize[2] = featureImage->GetRequestedRegion().GetSize()[2];

  UShort3DImageType::SpacingType outputSpacing;
  outputSpacing[0] = inputSpacing[2];
  outputSpacing[1] = inputSpacing[2];
  outputSpacing[2] = inputSpacing[2];

  // The output size should be the input size times the output spacing divided by the input z spacing
  UShort3DImageType::SizeType outputSize;
  outputSize[0] = (long unsigned int)(inputSize[0] * inputSpacing[0]/outputSpacing[0]);  // number of pixels along X
  outputSize[1] = (long unsigned int)(inputSize[1] * inputSpacing[1]/outputSpacing[1]);  // number of pixels along Y
  outputSize[2] = inputSize[2];

  featureResampler->SetOutputSpacing( outputSpacing );
  featureResampler->SetOutputOrigin ( featureImage->GetOrigin() );
  featureResampler->SetSize( outputSize );
  featureResampler->SetInterpolator( linearInterpolator );
  
  // Label image resampled the same way
  labelResampler->SetOutputSpacing( outputSpacing );
  labelResampler->SetOutputOrigin ( featureImage->GetOrigin() );
  labelResampler->SetSize( outputSize );
  labelResampler->SetInterpolator( nearestNeighborInterpolator );

  // Update the resamplers
  try
    {
    featureResampler->Update();
    labelResampler->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "New feature image spacing: " << featureResampler->GetOutput()->GetSpacing() << std::endl;
  std::cout << "New feature image origin: " << featureResampler->GetOutput()->GetOrigin() << std::endl;
  std::cout << "New feature image size: " << featureResampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;



  // Set the parameters for the label statistics filter
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  try
    {
    labelStatistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;

  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);


  // Erode the initial level set (labeled images)
  int ballRadius = 0;

  // Create the structuring element
  KernelType ball;
  KernelType::SizeType ballSize;
  ballSize[0] = (long unsigned int)(ballRadius / labelImage->GetSpacing()[0]);
  ballSize[1] = (long unsigned int)(ballRadius / labelImage->GetSpacing()[1]);
  ballSize[2] = (long unsigned int)(ballRadius / labelImage->GetSpacing()[2]);
  ball.SetRadius(ballSize);
  ball.CreateStructuringElement();
  
//  std::cout << "Ball size: " << ballSize << std::endl;
  
  // Connect the input image
  erodeFilter->SetKernel( ball );
  erodeFilter->SetErodeValue( 0 );

  // Smooth the feature image
  diffusion->SetNumberOfIterations( iterations );
  diffusion->SetConductanceParameter( conductance );
  diffusion->UseImageSpacingOn();
  diffusion->SetTimeStep(0.02);
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "# of iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;

  // Set the parameters for the Canny Segmentation
  cannySegmentation->UseImageSpacingOn();
  cannySegmentation->SetMaximumRMSError( 0.02 );
  cannySegmentation->SetNumberOfIterations( 400 );
  cannySegmentation->SetIsoSurfaceValue(127.5);
  cannySegmentation->SetPropagationScaling( propScaling );
  cannySegmentation->SetCurvatureScaling( curvScaling );
  cannySegmentation->SetAdvectionScaling( advecScaling );
  cannySegmentation->SetThreshold( cannyThreshold );
  cannySegmentation->SetVariance( cannyVariance );

  // Show the evolution of the level set
  // This must be done before the level set filter is updated
  itk::ShowLevelSetEvolution<CannySegmentationLevelSetImageFilterType> levelSetEvolution(cannySegmentation,displayImageType);



  // Set the parameters for Thresholder2
  // Thresholder2 is used to threshold the image after the level set
  // segmentation is complete.
  //  thresholder2->SetLowerThreshold( 0.0 );
  thresholder2->SetLowerThreshold( -1000.0 );
  thresholder2->SetUpperThreshold(     0.0 );
  thresholder2->SetOutsideValue( 255 );
  thresholder2->SetInsideValue( 0 );
  try
    {
    thresholder2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Line: " << __LINE__ << std::endl;


/*
  // Find the volume of the binary image of the initial relabeled data
  // and the binary image of the final relabeled data
  typedef itk::StatisticsImageFilter < UShort3DImageType > StatisticsFilterType;
  StatisticsFilterType::Pointer initStatistics = StatisticsFilterType::New();
  StatisticsFilterType::Pointer finalStatistics = StatisticsFilterType::New();

  initStatistics->SetInput( thresholder1->GetOutput() );
  initStatistics->Update();
  
  finalStatistics->SetInput( thresholder2->GetOutput() );
  finalStatistics->Update();

  UShort3DImageType::SizeType ImageSize;
  ImageSize = (thresholder1->GetOutput()->GetLargestPossibleRegion()).GetSize();

  int initSum = (ImageSize[0]*ImageSize[1]*ImageSize[2]) - (initStatistics->GetSum()/255);
  std::cout << "Initial number of pixels: " << initSum << std::endl;
  
  int finalSum = (ImageSize[0]*ImageSize[1]*ImageSize[2]) - (finalStatistics->GetSum()/255);
  std::cout << "Final number of pixels: " << finalSum << std::endl;
*/


  std::cout << "Size: " << thresholder1->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Size: " << thresholder2->GetOutput()->GetRequestedRegion().GetSize() << std::endl;

  // Find the volume of the binary image of the initial relabeled data
  // and the binary image of the final relabeled data
  typedef itk::ImageRegionConstIterator< UShort3DImageType > UShort3DConstIteratorType;
  UShort3DConstIteratorType inputIt1( thresholder1->GetOutput(), thresholder1->GetOutput()->GetRequestedRegion() );
  UShort3DConstIteratorType inputIt2( thresholder2->GetOutput(), thresholder2->GetOutput()->GetRequestedRegion() );
  
  int initVolume = 0;
  int finalVolume = 0;
  for ( inputIt1.GoToBegin(), inputIt2.GoToBegin(); !inputIt1.IsAtEnd(); ++inputIt1, ++inputIt2)
    {
    if (inputIt1.Get() == 0)
      initVolume++;
    if (inputIt2.Get() == 0)
      finalVolume++;
    }
  
  std::cout << "Initial volume: " << initVolume << std::endl;
  std::cout << "Final volume: " << finalVolume << std::endl;


  // Write out a model of the 3D image to file
  cannySegmentation->Update();
  WriteModelToFile<Float3DImageType>(cannySegmentation->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputModel);

  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << cannySegmentation->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << cannySegmentation->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << cannySegmentation->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << cannySegmentation->GetRMSChange() << std::endl;

  itk::FileDumper<Float3DImageType> dumpDiffusion(diffusion->GetOutput(), "diffusion");
  itk::FileDumper<Float3DImageType> dumpOriginal(castToFloat->GetOutput(), "original");
  if (cannySegmentation->GetPropagationScaling() != 0.0)
    {
    itk::FileDumper<Float3DImageType> dumpSpeed(cannySegmentation->GetSpeedImage(), "speed");
    }
  itk::FileDumper<Float3DImageType> dumpEdges(cannySegmentation->GetCannyImage(), "edges");
  itk::FileDumper<Float3DImageType> dumpLabels(castToFloat2->GetOutput(), "labels");
  itk::FileDumper<Float3DImageType> dumpLevelSet(cannySegmentation->GetOutput(), "levelSet");

  // Print out the advection image
  typedef itk::Vector< float, 1 > VectorPixelType;
  typedef itk::Image< VectorPixelType, 3 > VectorImageType;
  typedef itk::ImageFileWriter < VectorImageType > VectorWriterType;
  VectorWriterType::Pointer advectionWriter = VectorWriterType::New();
  //advectionWriter->SetInput( cannySegmentation->GetAdvectionImage() );
  //advectionWriter->Update();


  return 0;
}
