/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: WriteModelToFile.txx,v $
  Language:  C++
  Date:      $Date: 2005/03/16 17:11:31 $
  Version:   $Revision: 1.7 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

#ifndef _WriteModelToFile_txx
#define _WriteModelToFile_txx

#include "WriteModelToFile.h"

template <class TInputImage>
bool WriteModelToFile(TInputImage * displayImage, std::vector <int> bbValues, int * inputImageSize, double * inputImageSpacing, std::string outputModel, std::string marchingCubesType="")
{
  // Filters for creating the vtk model
  typedef itk::VTKImageExport< TInputImage > ImageExportType;
  typename TInputImage::SizeType outputImageSize;
  outputImageSize = displayImage->GetRequestedRegion().GetSize();
  std::cout << "In WriteModelToFile: Output image size: " << outputImageSize << std::endl;
  std::cout << "In WriteModelToFile: Input image size: [" << inputImageSize[0] << ", " << inputImageSize[1] << ", " << inputImageSize[2] << "]" << std::endl;
  std::cout << "In WriteModelToFile: Input image spacing: [" << inputImageSpacing[0] << ", " << inputImageSpacing[1] << ", " << inputImageSpacing[2] << "]" << std::endl;
  

  // Filters for creating the vtk model
  ImageExportType::Pointer itkExporter = ImageExportType::New();
  vtkImageImport *vtkImporter = vtkImageImport::New();
  vtkImageFlip *flipper = vtkImageFlip::New();
  vtkImageChangeInformation *changeInformation = vtkImageChangeInformation::New();
  vtkPolyDataWriter *polyDataWriter = vtkPolyDataWriter::New();
  
  itkExporter->SetInput( displayImage );
  flipper->SetInput( vtkImporter->GetOutput() ); 
  changeInformation->SetInput (flipper->GetOutput() );

  // These are used for determining the model origin
  int minX = bbValues[0] ;
  int minY = bbValues[2] ;
  int maxY = bbValues[3] ;
  int minSlice = bbValues[6] ;

  std::cout << "minY: " << minY << std::endl;

  // Change from itk to vtk
  ConnectPipelines(itkExporter, vtkImporter);

  // Flip the image around the y axis so that it lines up with models
  // created by vtk.
  flipper->SetFilteredAxis (1);

//   // TESTING - also flip around the z axis
//   vtkImageFlip *flipper2 = vtkImageFlip::New();
//   flipper2->SetInput( flipper->GetOutput() ); 
//   flipper2->SetFilteredAxis (2);
//   changeInformation->SetInput( flipper2->GetOutput() );

  // Change the image origin
  // The x and z origin are changed by simply multiplying the origin
  // by the new spacing, but the y origin has been
  // altered by the flipping around the y axis (for compatibility
  // between ITK and VTK).

  // minX, minY, minZ, and inputImageSize are given in pixel units.  
  // Therefore, they must be multiplied by the spacing.
  // outputImageSize, however, is given in mm units.

//  vtkFloatingPointType newSpacing[3] = {displayImage->GetSpacing()[0], displayImage->GetSpacing()[1], displayImage->GetSpacing()[2]};
//  vtkFloatingPointType newOrigin[3] = {minX * newSpacing[0], (inputImageSize[1]-1)*newSpacing[1] - (minY*newSpacing[1] + outputImageSize[1]-1), minSlice * newSpacing[2]};
//  vtkFloatingPointType newOrigin[3] = {minX * inputImageSpacing[0], (inputImageSize[1]-1)*inputImageSpacing[1] - (minY*inputImageSpacing[1] + outputImageSize[1]-1), minSlice * inputImageSpacing[2]};
  vtkFloatingPointType newOrigin[3] = {minX * inputImageSpacing[0], (inputImageSize[1]-1-maxY)*inputImageSpacing[1], minSlice * inputImageSpacing[2]};
  changeInformation->SetOutputOrigin ( newOrigin );


  // Marching cubes
  if (marchingCubesType == "Discrete")
    {
    // The display image is -1 for all values inside the structure and
    // 1 in the background.  For the DiscreteMarchingCubes, it is
    // necessary to specify an actual label number, so it does not
    // work if you specify a value in between these values, like 0.
    // Either -1 or 1 should be specified.
    std::cout << "In WriteModelToFile.txx: Discrete marching cubes" << std::endl;
    vtkDiscreteMarchingCubes *cubes = vtkDiscreteMarchingCubes::New();
    cubes->SetValue(0,1);
    cubes->ComputeNormalsOff();
    cubes->ComputeScalarsOff();
    cubes->ComputeGradientsOff();
    cubes->SetInput( changeInformation->GetOutput() );
    polyDataWriter->SetInput( cubes->GetOutput() );
    }

  else if (marchingCubesType == "DiscreteSmoothed")
    {
    // The display image is -1 for all values inside the structure and
    // 1 in the background.  For the DiscreteMarchingCubes, it is
    // necessary to specify an actual label number, so it does not
    // work if you specify a value in between these values, like 0.
    // Either -1 or 1 should be specified.
    std::cout << "In WriteModelToFile.txx: Discrete marching cubes smoothed" << std::endl;
    vtkDiscreteMarchingCubes *cubes = vtkDiscreteMarchingCubes::New();
    cubes->SetValue(0,1);
    cubes->ComputeNormalsOff();
    cubes->ComputeScalarsOff();
    cubes->ComputeGradientsOff();
    cubes->SetInput( changeInformation->GetOutput() );

    // Smooth the model with a WindowedSincPolyDataFilter
    int iterations = 15;
    float passBand = 0.001;
    vtkWindowedSincPolyDataFilter * smoother = vtkWindowedSincPolyDataFilter::New();
    smoother->SetInput( cubes->GetOutput() );
    smoother->SetNumberOfIterations( iterations );
    smoother->BoundarySmoothingOff();
    smoother->FeatureEdgeSmoothingOff();
    smoother->SetPassBand( passBand );
    smoother->NonManifoldSmoothingOn();
//   smoother->NormalizeCoordinatesOn();
//    smoother->AddObserver EndEvent "puts \"Smoothing complete\""
//    smoother->ReleaseDataFlagOn();

    polyDataWriter->SetInput( smoother->GetOutput() );

    }

  else if (marchingCubesType == "DiscreteDecimated")
    {
    // The display image is -1 for all values inside the structure and
    // 1 in the background.  For the DiscreteMarchingCubes, it is
    // necessary to specify an actual label number, so it does not
    // work if you specify a value in between these values, like 0.
    // Either -1 or 1 should be specified.
    std::cout << "In WriteModelToFile.txx: Discrete marching cubes smoothed" << std::endl;
    vtkDiscreteMarchingCubes *cubes = vtkDiscreteMarchingCubes::New();
    cubes->SetValue(0,1);
    cubes->ComputeNormalsOff();
    cubes->ComputeScalarsOff();
    cubes->ComputeGradientsOff();
    cubes->SetInput( changeInformation->GetOutput() );

    // Smooth the model with a WindowedSincPolyDataFilter
    int iterations = 15;
    float passBand = 0.001;
    vtkWindowedSincPolyDataFilter * smoother = vtkWindowedSincPolyDataFilter::New();
    smoother->SetInput( cubes->GetOutput() );
    smoother->SetNumberOfIterations( iterations );
    smoother->BoundarySmoothingOff();
    smoother->FeatureEdgeSmoothingOff();
    smoother->SetPassBand( passBand );
    smoother->NonManifoldSmoothingOn();
//   smoother->NormalizeCoordinatesOn();
//    smoother->AddObserver EndEvent "puts \"Smoothing complete\""
//    smoother->ReleaseDataFlagOn();

    // Clean the model.  This is necessary to get a good model after decimation.
    vtkCleanPolyData * cleaner = vtkCleanPolyData::New();
    cleaner->SetInput( smoother->GetOutput() );

    // Decimate the model
    vtkDecimatePro * decimator = vtkDecimatePro::New();
    decimator->SetInput( cleaner->GetOutput() );
    decimator->SetTargetReduction( 0.9 );
    decimator->PreserveTopologyOn();
    decimator->BoundaryVertexDeletionOff();

    // Creating normals
    vtkPolyDataNormals * normals = vtkPolyDataNormals::New();
    normals->SetInput( decimator->GetOutput() );
    normals->SetFeatureAngle( 90 );

    polyDataWriter->SetInput( normals->GetOutput() );
    }
  else
    {
    vtkMarchingCubes *cubes = vtkMarchingCubes::New();
    cubes->SetValue(0,0);
    cubes->ComputeNormalsOn();
    cubes->ComputeScalarsOff();
    cubes->SetInput( changeInformation->GetOutput() );
    polyDataWriter->SetInput( cubes->GetOutput() );
    }


  // VTK model writer
  polyDataWriter->SetFileName( outputModel.c_str() );
  polyDataWriter->SetFileTypeToBinary();
  polyDataWriter->Modified();
  polyDataWriter->Write();

  return true;
}

#endif
