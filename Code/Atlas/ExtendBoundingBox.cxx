#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkNumericSeriesFileNames.h"

#include "ExtendBoundingBox.h"

bool ExtendBoundingBox (std::vector <int> & bbValues, int borderSize, int sliceOffset, int * inputImageSize)
{
  // Break up the vector for name convenience
  int minX = bbValues[0];
  int maxX = bbValues[1];
  int minY = bbValues[2];
  int maxY = bbValues[3];
  int minZ = bbValues[4];
  int maxZ = bbValues[5];
  int minSlice = bbValues[6];
  int maxSlice = bbValues[7];

  // Create a border around the input region
  // Add a border in the x direction
  if (minX >= borderSize)
    minX = minX - borderSize;
  else
    minX = 0;
  if (maxX <= inputImageSize[0]-1-borderSize)
    maxX = maxX + borderSize;
  else
    maxX = inputImageSize[0]-1;
  // Add a border in the y direction
  if (minY >= borderSize)
    minY = minY - borderSize;
  else
    minY = 0;
  if (maxY <= inputImageSize[1]-1-borderSize)
    maxY = maxY + borderSize;
  else
    maxY = inputImageSize[1]-1;
  // Add a border in the z direction
  if (minZ >= borderSize)
    minZ = minZ - borderSize;
  else
    minZ = 0;
  if (maxZ <= inputImageSize[2]-1-borderSize)
    maxZ = maxZ + borderSize;
  else
    maxZ = inputImageSize[2]-1;
  // Add a border in the z direction to the slices
  if (minSlice >= sliceOffset+borderSize)
    minSlice = minSlice - borderSize;
  else
    minSlice = sliceOffset;
  if (maxSlice <= sliceOffset+inputImageSize[2]-1-borderSize)
    maxSlice = maxSlice + borderSize;
  else
    maxSlice = sliceOffset + inputImageSize[2]-1;

  // Save the values back into the vector
  bbValues[0] = minX;
  bbValues[1] = maxX;
  bbValues[2] = minY;
  bbValues[3] = maxY;
  bbValues[4] = minZ;
  bbValues[5] = maxZ;
  bbValues[6] = minSlice;
  bbValues[7] = maxSlice;


  return true;
}
