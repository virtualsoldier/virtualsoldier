#ifndef _ShowLevelSetEvolution_txx
#define _ShowLevelSetEvolution_txx

#include "ShowLevelSetEvolution.h"

namespace itk {
template < class TLevelSet >
ShowLevelSetEvolution<TLevelSet>
::ShowLevelSetEvolution(TLevelSet * detector, std::string displayImageType, std::string outputFile)
{
  std::cout << "Dimension: " << ImageDimension << std::endl;

  typename FeatureImageType::PointType imageOrigin;
  imageOrigin = detector->GetFeatureImage()->GetOrigin();
  std::cout << "Image origin: " << imageOrigin << std::endl;

  typename TLevelSet::FeatureImageType::SpacingType imageSpacing;
  imageSpacing = detector->GetFeatureImage()->GetSpacing();
  std::cout << "Image spacing: " << imageSpacing << std::endl;

  typename TLevelSet::FeatureImageType::SizeType imageSize;
  imageSize = detector->GetFeatureImage()->GetBufferedRegion().GetSize();
  std::cout << "Image size: " << imageSize << std::endl;


  // Define an extra itkImage to connect to VTK (to break the pipeline)
  typename FeatureImageType::Pointer levelSetImage = FeatureImageType::New();
  typename FeatureImageType::Pointer featureImage = FeatureImageType::New();

  typedef itk::Image< float, 2 > Float2DImageType;
    
  // Extract one slice to send to the level set evolution display
  typedef itk::ExtractImageFilter < FeatureImageType, Float2DImageType > ExtractFilterType;
  ExtractFilterType::Pointer extractor = ExtractFilterType::New();

/*
  ConnectSpeedImageObject<ExtractFilterType,TLevelSet >  connectSpeedImage( extractor, detector );

  itk::SimpleMemberCommand<ConnectSpeedImageObject>::Pointer connectSpeedImageCommand;
  connectSpeedImageCommand = itk::SimpleMemberCommand<ConnectSpeedImageObject>::New();
  connectSpeedImageCommand->SetCallbackFunction( &connectSpeedImage, &ConnectSpeedImageObject::Connect  );
  extract->AddObserver( itk::StartEvent(), connectSpeedImageCommand );
*/

  // Don't extract anything if the image is 2D
  if (ImageDimension == 2)
    {

    typename TLevelSet::FeatureImageType::IndexType start;
    start[0] = 0;
    start[1] = 0;

    typename TLevelSet::FeatureImageType::RegionType desiredRegion;
    desiredRegion.SetSize ( imageSize );
    desiredRegion.SetIndex ( start );

    extractor->SetInput ( featureImage );
    extractor->SetExtractionRegion ( desiredRegion );
    }

  else if (ImageDimension == 3)
    {
    int middleSlice = (imageSize[2])/2;
    std::cout << "In ShowLevelSetEvolution.txx: Middle slice: " << middleSlice  << std::endl;

    // The indices are relative to the cropped image not the origin
    typename TLevelSet::FeatureImageType::IndexType start;
    start[0] = 0;
    start[1] = 0;
    start[2] = middleSlice;

    typename TLevelSet::FeatureImageType::SizeType size;
    size[0] = imageSize[0];
    size[1] = imageSize[1];
    size[2] = 0;

    std::cout << "start: " << start << std::endl;
    std::cout << "size: " << size << std::endl;

    typename TLevelSet::FeatureImageType::RegionType desiredRegion;
    desiredRegion.SetSize ( size );
    desiredRegion.SetIndex ( start );

    extractor->SetInput ( featureImage );
    extractor->SetExtractionRegion ( desiredRegion );
    }


    

  typedef itk::BinaryThresholdImageFilter<Float2DImageType,Float2DImageType> ThresholdFilter;
  ThresholdFilter::Pointer thresholder = ThresholdFilter::New();
  thresholder->SetInput ( extractor->GetOutput() );
  thresholder->SetInsideValue (255);
  thresholder->SetOutsideValue (0);
  thresholder->SetLowerThreshold ( 0 );
  thresholder->SetUpperThreshold ( 1000000 );

  typedef itk::RescaleIntensityImageFilter<Float2DImageType,Float2DImageType> RescaleIntensityFilter;
  RescaleIntensityFilter::Pointer rescaler = RescaleIntensityFilter::New();
//  rescaler->SetInput( thresholder->GetOutput() );
  rescaler->SetInput( extractor->GetOutput() );
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(255);


  // Connect ITK to VTK

  // connection for the intensity
  itk::VTKImageExport<Float2DImageType>::Pointer intensityExporter
    = itk::VTKImageExport<Float2DImageType>::New();
  intensityExporter->SetInput( rescaler->GetOutput() );

  
  vtkImageImport *intensityImporter = vtkImageImport::New();
  ConnectPipelines(intensityExporter, intensityImporter);

  // Flip the image around the y axis so that it lines up with models
  // created by vtk.
  vtkImageFlip *intensityFlipper = vtkImageFlip::New();
  intensityFlipper->SetInput(intensityImporter->GetOutput() );
  intensityFlipper->SetFilteredAxis (1);

  // level set connection
  itk::VTKImageExport<FeatureImageType>::Pointer levelSetExporter = itk::VTKImageExport<FeatureImageType>::New();
  levelSetExporter->SetInput(levelSetImage); 

  vtkImageImport *levelSetImporter = vtkImageImport::New();
  ConnectPipelines(levelSetExporter, levelSetImporter);

  // Flip the image around the y axis so that it lines up with models
  // created by vtk.      
  vtkImageFlip *levelSetFlipper = vtkImageFlip::New();
  levelSetFlipper->SetInput(levelSetImporter->GetOutput() );
  levelSetFlipper->SetFilteredAxis (1);


  // Set up the filters
  vtkMarchingSquares *marchingSquares = vtkMarchingSquares::New();
  vtkMarchingCubes *marchingCubes = vtkMarchingCubes::New();
  vtkCutter * marchingCutter = vtkCutter::New();
  vtkPlane * plane = vtkPlane::New();
  vtkPolyDataMapper *marchingMapper = vtkPolyDataMapper::New();
  vtkActor *marchingActor = vtkActor::New();


  // Isosurface extraction
  if (ImageDimension == 2)
    {
    marchingSquares->SetInput( levelSetFlipper->GetOutput() );
    marchingMapper->SetInput( marchingSquares->GetOutput() );
    // marchingMapper->SetResolveCoincidentTopologyToPolygonOffset();
    }
  else if (ImageDimension == 3)
    {
    marchingCubes->SetInput( levelSetFlipper->GetOutput() );

    // The (implicit) plane is used to do the cutting.  We want to show
    // the level set contour superimposed on the background image.
    // The background slice shown is the middle slice from the cropped
    // volume.  The z plane origin is specified to be the image origin
    // + the middle slice index.  Since the z image spacing might not
    // be 1, we need to multiply the middle slice index by the z
    // spacing.
    int middleSlice = (imageSize[2]/2);
    double planeOrigin[3] = {0, 0, imageOrigin[2]+(middleSlice*imageSpacing[2])};
    double planeNormal[3] = {0, 0, 1};
    plane->SetOrigin( planeOrigin );
    plane->SetNormal( planeNormal);

    marchingCutter->SetInput( marchingCubes->GetOutput() );
    marchingCutter->SetCutFunction( plane );
//    marchingCutter->GenerateCutScalarsOn();
//    marchingCutter->SetValue( 0, 1 );

    marchingMapper->SetInput( marchingCutter->GetOutput() );
    marchingMapper->ScalarVisibilityOff();
    // marchingMapper->SetResolveCoincidentTopologyToPolygonOffset();
    }
  

  marchingActor->SetMapper( marchingMapper );
  marchingActor->GetProperty()->SetLineWidth(2.0);
  marchingActor->GetProperty()->SetAmbient(1.0);
  marchingActor->GetProperty()->SetColor(1.0, 0, 0);


  //
  // Setup a VTK viewer
  //
  vtkRenderWindowInteractor *interactor = vtkRenderWindowInteractor::New();

  vtkImageViewer2 *viewer = vtkImageViewer2::New();

  // Set the window to fit optimally on a (1024,768) screen.
  // We make the window no greater than (800,600) to fit well.
  // Either the window is constrained by 800 in x or 600 in y.
  int xExtent = (int) (imageSize[0]*imageSpacing[0]);
  int yExtent = (int) (imageSize[1]*imageSpacing[1]);

  if (xExtent/yExtent > 4/3)
    {
    xExtent = 800;
    yExtent = (800*yExtent)/xExtent;
    }
  else 
    {
    xExtent = (600*xExtent)/yExtent;
    yExtent = 600;
    }

  viewer->GetRenderWindow()->SetSize(xExtent, yExtent);
  viewer->GetRenderWindow()->SetWindowName("Level set viewer");
//      viewer->SetColorWindow(3000);//(1600);
//      viewer->SetColorLevel(-600);
  viewer->SetupInteractor( interactor );
  viewer->SetInput( intensityFlipper->GetOutput() );
  viewer->GetRenderer()->AddActor( marchingActor );
  viewer->GetRenderWindow()->DoubleBufferOn();

  //
  // Attach a viewer to all iteration events
  //

  typedef ShowSegmentationProgressObject<TLevelSet> ShowProgressObject;

  ShowProgressObject progressWatch( detector, viewer, levelSetImage, featureImage,
                                    levelSetImporter, intensityImporter,
                                    marchingActor, displayImageType, outputFile);
  itk::SimpleMemberCommand<ShowProgressObject>::Pointer command;
  command = itk::SimpleMemberCommand<ShowProgressObject>::New();
  command->SetCallbackFunction(&progressWatch,
                               &ShowProgressObject::ShowProgress);
  detector->AddObserver( itk::ProgressEvent(), command);
  detector->AddObserver( itk::EndEvent(), command);

  //
  // Run the pipeline
  //
  std::cout << "In ShowLevelSetEvolution.txx: Running Pipeline" << std::endl;
  detector->UpdateLargestPossibleRegion();
  std::cout << "In ShowLevelSetEvolution.txx: Done running Pipeline" << std::endl;
  
  std::cout << "levelSetImage size: " << levelSetImage->GetBufferedRegion().GetSize() << std::endl;
//  viewer->SetInput(intensityFlipper->GetOutput() );
//  viewer->Render();
  
  // Start the interactor
  interactor->Start();

  typedef itk::StatisticsImageFilter<Float2DImageType> StatisticsFilter;
  StatisticsFilter::Pointer statistics = StatisticsFilter::New();
  statistics->SetInput( extractor->GetOutput() );
  statistics->Update();
  float intensityMin = statistics->GetMinimum();
  float intensityMax = statistics->GetMaximum();
  std::cout << "In ShowLevelSetEvolution.txx: Intensity min: " << intensityMin << std::endl;
  std::cout << "In ShowLevelSetEvolution.txx: Intensity max: " << intensityMax << std::endl;


  std::cout << "In ShowLevelSetEvolution.txx: Extract output origin: " << extractor->GetOutput()->GetOrigin() << std::endl;
  std::cout << "In ShowLevelSetEvolution.txx: Rescaler output origin: " << rescaler->GetOutput()->GetOrigin() << std::endl;



/*
  if (ImageDimension == 3)
    {
    // Show the model in 3D
    vtkPolyDataMapper * fullMapper = vtkPolyDataMapper::New();
    fullMapper->SetInput( marchingCubes->GetOutput() );
    vtkActor * fullActor = vtkActor::New();
    fullActor->SetMapper( fullMapper );

// Create graphics stuff
    vtkRenderer * ren1 = vtkRenderer::New();
    vtkRenderWindow * renWin = vtkRenderWindow::New();
    renWin->AddRenderer( ren1 );
    vtkRenderWindowInteractor * iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow( renWin );

    ren1->AddActor( fullActor );
//  ren1->AddActor( marchingActor );
    iren->Initialize();

    // These lines are crucial in .cxx but not in .tcl
    renWin->Render();
    // This starts the event loop and as a side effect causes an initial
    // render.
    iren->Start();
    }
*/


  // clean up
  intensityImporter->Delete();
  intensityFlipper->Delete();
  levelSetImporter->Delete();
  levelSetFlipper->Delete();

  marchingSquares->Delete();
  marchingCubes->Delete();
  marchingCutter->Delete();
  plane->Delete();
  marchingMapper->Delete();
  marchingActor->Delete();

  interactor->Delete();
  viewer->Delete();

}
}



// The following class is used to support callbacks
// on the filter in the pipeline
template < class TLevelSet >
void ShowSegmentationProgressObject<TLevelSet>
::ShowProgress()
{


  // std::cout << "Progress event for " << m_Segmenter->GetNameOfClass()
  //           << std::endl;
    
  // Graft the output of the process object into our display image
  // Update the level set evolution
  m_LevelSetImage->SetRequestedRegion(m_Segmenter->GetOutput()
                                       ->GetRequestedRegion());
  m_LevelSetImage->SetBufferedRegion(m_Segmenter->GetOutput()
                                      ->GetBufferedRegion());
  m_LevelSetImage->SetLargestPossibleRegion(m_Segmenter->GetOutput()
                                             ->GetLargestPossibleRegion());
  m_LevelSetImage->SetPixelContainer( m_Segmenter->GetOutput()
                                       ->GetPixelContainer() );
  m_LevelSetImage->CopyInformation( m_Segmenter->GetOutput() );
  m_LevelSetImage->Modified();

  m_MarchingActor->GetProperty()->SetLineWidth(2.0);
    
  // send a modified to the level set importer
  m_LevelSetImporter->Modified();
    
  // Grab a handle to the feature image
  //m_FeatureImage = m_Segmenter->GetSpeedImage();

  // Graft the output of the process object into our display image
  // Set the feature image to be the speed image

  typename TLevelSet::FeatureImageType::ConstPointer displayImage;
  if (m_DisplayImageType == "FeatureImage")
    {
    displayImage = m_Segmenter->GetFeatureImage();    
    }
  else
    {
    displayImage = m_Segmenter->GetSpeedImage();
    }


  m_FeatureImage->SetRequestedRegion(displayImage
                                       ->GetRequestedRegion());
  m_FeatureImage->SetBufferedRegion(displayImage
                                      ->GetBufferedRegion());
  m_FeatureImage->SetLargestPossibleRegion(displayImage
                                             ->GetLargestPossibleRegion());
  m_FeatureImage->SetPixelContainer( const_cast< FeatureImageType * >(displayImage.GetPointer())
                                       ->GetPixelContainer() );
  m_FeatureImage->CopyInformation( const_cast< FeatureImageType * > (displayImage.GetPointer()) );
//  m_FeatureImage->SetPixelContainer( displayImage
//                                       ->GetPixelContainer() );
//  m_FeatureImage->CopyInformation( displayImage );
  
  // This is not necessary
  // It shows the evolution of the displayImage if, for example, the
  // displayImage is the level set output.  In the case where the
  // displayImage is one that does not change with iterations, it is
  // not necessary to call modified on it.
  m_FeatureImage->Modified();



  // Render the segmentation (with the original image)
  m_Viewer->GetWindowLevel()->UpdateWholeExtent();
  // Display the original image
  m_Viewer->GetImageActor()->SetDisplayExtent( m_Viewer->GetInput()->GetWholeExtent() );
  m_Viewer->GetRenderer()->ResetCamera();
  int *ext = m_Viewer->GetWindowLevel()->GetInput()->GetWholeExtent();
  int xDiff = ext[1] - ext[0] + 1;
  int yDiff = ext[3] - ext[2] + 1;

//  std::cout << "In ShowProgress: Viewer window size: " << m_Viewer->GetRenderWindow()->GetSize() << std::endl;

  typename TLevelSet::FeatureImageType::SpacingType spacing;
  spacing = m_Segmenter->GetOutput()->GetSpacing();


  if (yDiff > xDiff)
    {
    m_Viewer->GetRenderer()->GetActiveCamera()->SetParallelScale(yDiff*spacing[1]/2.0);
    }
  else
    {          
    m_Viewer->GetRenderer()->GetActiveCamera()->SetParallelScale(xDiff*spacing[0]/2.0);
    }   


  m_Viewer->Render();





  if (m_OutputFile != "")
    {
    // Print out the image at each iteration
    itk::OStringStream filename;
    if ( m_Iteration < 10 )
      filename << m_OutputFile << "00" << m_Iteration << ".png";
    else if ( m_Iteration < 100 )
      {
      filename << m_OutputFile << "0"<< m_Iteration << ".png";
      }
    else if ( m_Iteration < 1000 )
      {
      filename << m_OutputFile << m_Iteration << ".png";
      }
    else 
      {
      std::cout << "ERROR: Too many images" << std::endl;
      }
  

    m_Iteration++;

    vtkRendererSource * rendererSource = vtkRendererSource::New();
    rendererSource->SetInput( m_Viewer->GetRenderer() );
    vtkPNGWriter * pngWriter = vtkPNGWriter::New();
    pngWriter->SetInput ( rendererSource->GetOutput() );
    pngWriter->SetFileName ( filename.str().c_str() );
    pngWriter->Write();

    rendererSource->Delete();
    pngWriter->Delete();
    }

}



#endif
