#define ITK_LEAN_AND_MEAN

#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkThresholdSegmentationLevelSetImageFilter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"

#include "itkSimpleFilterWatcher.h"

#include <itksys/SystemTools.hxx>

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"
#include "PreProcessFeatureImage.h"
#include "PreProcessLabelImage.h"
#include "WriteSlicesFromVolume.h"
#include "WriteModelToFile.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"
#include "FindIsolatingThresholds.h"

template < const int ImageDimension >
bool SegmentStructure(char * featureImagePrefix, char * labelImagePrefix, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float mult, float propScaling, float curvScaling, float binaryRadius, float edgeWeight, int iterations, float conductance, int levelSetIterations, float rmsError, int borderSize, std::string displayImageType, bool useAllSeeds,std::string attributesFilesPrefix);


int main( int argc, char *argv[] )
{
  int numberOfParameters = 22;

  if( argc < numberOfParameters )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " ImageInfoFile";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " SigmaMultiplier";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " BinaryErodeRadius";
    std::cerr << " EdgeWeight";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " LevelSetIterations";
    std::cerr << " RMSError";
    std::cerr << " BorderSize";
    std::cerr << " DisplayImageType";
    std::cerr << " Dimension";
    std::cerr << " UseAllSeeds";
    std::cerr << " attributesFilesPrefix";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  std::string outputModel = argv[6];
  std::string anatomyName = argv[7];
  float mult = atof(argv[8]);
  float propScaling = atof( argv[9] );
  float curvScaling = atof( argv[10] );
  float binaryRadius = atof( argv[11] );
  float edgeWeight = atof( argv[12] );
  int iterations = atoi( argv[13] );
  float conductance = atof( argv[14] );
  int levelSetIterations = atoi (argv[15] );
  float rmsError = atof (argv[16] );
  int borderSize = atoi( argv[17] );
  std::string displayImageType = argv[18];
  int dimension = atoi( argv[19] );
  bool useAllSeeds = atoi( argv[20] );
  std::string attributesFilesPrefix= argv[21];

  if (dimension == 2)
    {
    SegmentStructure< 2 > (featureImagePrefix, labelImagePrefix, masterFile, boundingBoxesFile, outputModel, anatomyName, mult, propScaling, curvScaling, binaryRadius, edgeWeight, iterations, conductance,levelSetIterations,rmsError, borderSize, displayImageType,useAllSeeds,attributesFilesPrefix);
    }
  else if (dimension == 3)
    {
    SegmentStructure< 3 > (featureImagePrefix, labelImagePrefix, masterFile, boundingBoxesFile, outputModel, anatomyName, mult, propScaling, curvScaling, binaryRadius, edgeWeight, iterations, conductance,levelSetIterations, rmsError, borderSize, displayImageType,useAllSeeds,attributesFilesPrefix);
    }
  return 0;
}

template < const int ImageDimension >
bool SegmentStructure(char * featureImagePrefix, char * labelImagePrefix, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float mult, float propScaling, float curvScaling, float binaryRadius, float edgeWeight, int iterations, float conductance, int levelSetIterations, float rmsError, int borderSize, std::string displayImageType, bool useAllSeeds, std::string attributesFilesPrefix)
{
  // Find the anatomy info
  // If the structure name has underscores, they will be changed to spaces.
  int anatomyLabel;
  std::vector < int > bbValues(8);
  std::vector < int >::iterator bbValuesIt;
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImagePrefix,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize,inputImageSpacing) )
    {
    return 0;
    }

  for( bbValuesIt = bbValues.begin(); bbValuesIt != bbValues.end(); ++bbValuesIt )
    {
    std::cout << "bbValues[" << bbValuesIt-bbValues.begin() << "]: " << *bbValuesIt << std::endl;    
    }

  typedef short FeaturePixelType;
  typedef unsigned short LabelPixelType;
  typedef float InternalPixelType;

  typedef itk::Image< FeaturePixelType, ImageDimension > FeatureImageType;
  typedef itk::Image< LabelPixelType, ImageDimension > LabelImageType;
  typedef itk::Image< InternalPixelType, ImageDimension > InternalImageType;

  typedef itk::ThresholdSegmentationLevelSetImageFilter< InternalImageType, InternalImageType > ThresholdSegmentationLevelSetImageFilterType;  

  // Instantiate the filters
  FeatureImageType::Pointer featureImage = FeatureImageType::New();
  LabelImageType::Pointer labelImage = LabelImageType::New();
  ThresholdSegmentationLevelSetImageFilterType::Pointer thresholdSegmentation = ThresholdSegmentationLevelSetImageFilterType::New();


  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating feature volume from feature image slices" << std::endl;
//  itk::CreateVolumeFromSlices<FeatureImageType>( featureImagePrefix, bbValues, inputImageSpacing, featureImage);
  std::cout << "Creating label volume from label image slices" << std::endl;
  itk::CreateVolumeFromSlices<LabelImageType>( labelImagePrefix, bbValues, inputImageSpacing, labelImage);
  
  InternalImageType::Pointer preProcessedFeatureImage;
  preProcessedFeatureImage = PreProcessFeatureImage<FeatureImageType,InternalImageType> (featureImage, iterations, conductance);

  InternalImageType::Pointer preProcessedLabelImage;
  preProcessedLabelImage = PreProcessLabelImage<LabelImageType,InternalImageType> (labelImage, anatomyLabel, binaryRadius);


  FeatureImageType::Pointer entireFeatureImage = FeatureImageType::New();
  LabelImageType::Pointer entireLabelImage = LabelImageType::New();

  int sliceOffset = 1;
  // Initialize the bounding box values for all of the structures.
  // We simply initialize the values using the bbValues of the
  // structure of interest.  Then we updated them to encompass all of
  // the structures below.
  std::vector < int > entirebbValues(8);
  std::vector < int >::iterator entirebbValuesIt;
  entirebbValues[0] = inputImageSize[0]-1 ;
  entirebbValues[1] = 0;
  entirebbValues[2] = inputImageSize[1]-1 ;
  entirebbValues[3] = 0 ;
  entirebbValues[4] = inputImageSize[2]-1 ;
  entirebbValues[5] = 0 ;
  entirebbValues[6] = inputImageSize[2]-1+sliceOffset;
  entirebbValues[7] = 0+sliceOffset ;


  // Find the bounding boxes for all structures.
  FindStructureAttributes<int> boundingBoxes(boundingBoxesFile);
  if( boundingBoxes.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;    
    return false;
    }
  std::vector< std::vector< int > > allBoundingBoxes = boundingBoxes.GetAllStructureAttributes();
  std::vector< std::vector< int > >::iterator allBoundingBoxesIt;
  std::vector< int >::iterator oneStructureBoundingBoxesIt;
  bool oddOffset = true;

  // Loop through the vector of vector of bounding boxes.
  for( allBoundingBoxesIt = allBoundingBoxes.begin(); allBoundingBoxesIt != allBoundingBoxes.end(); ++allBoundingBoxesIt )
    {
    // Ignore the background bounding boxes.  The background is found
    // by looking at the x, y, and z bounding boxes and determining
    // whether they each extend from 0 to the image size in that
    // dimension.
    if( (*allBoundingBoxesIt)[0] != 0 || 
        (*allBoundingBoxesIt)[1] != (int)inputImageSize[0]-1 || 
        (*allBoundingBoxesIt)[2] != 0 || 
        (*allBoundingBoxesIt)[3] != (int)inputImageSize[1]-1 ||
        (*allBoundingBoxesIt)[4] != 0 || 
        (*allBoundingBoxesIt)[5] != (int)inputImageSize[2]-1 )
      {
      // Loop through a particular vector of bounding boxes.
      for( oneStructureBoundingBoxesIt = (*allBoundingBoxesIt).begin(), entirebbValuesIt = entirebbValues.begin(); oneStructureBoundingBoxesIt != (*allBoundingBoxesIt).end(); ++oneStructureBoundingBoxesIt, ++entirebbValuesIt )
        {
        // If the index is of a min value, update the min value.  The
        // index is a min value if the offset is odd.
        if (oddOffset)
          {
			  *entirebbValuesIt = std::min(*oneStructureBoundingBoxesIt, *entirebbValuesIt);
          oddOffset = false;
          }
        else
          {
			  *entirebbValuesIt = std::max(*oneStructureBoundingBoxesIt, *entirebbValuesIt);
          oddOffset = true;
          }

        std::cout << *oneStructureBoundingBoxesIt << "\t";
        }
      }
    std::cout << std::endl;
    }

  std::cout << "New bounding boxes" << std::endl;
  for( entirebbValuesIt = entirebbValues.begin(); entirebbValuesIt != entirebbValues.end(); ++entirebbValuesIt )
    {
    std::cout << *entirebbValuesIt << "\t";
    }
  std::cout << std::endl;


  std::cout << "Creating feature volume from feature image slices" << std::endl;
  itk::CreateVolumeFromSlices<FeatureImageType>( featureImagePrefix, entirebbValues, inputImageSpacing, entireFeatureImage);
  std::cout << "Creating label volume from label image slices" << std::endl;
  itk::CreateVolumeFromSlices<LabelImageType>( labelImagePrefix, entirebbValues, inputImageSpacing, entireLabelImage);

  
  typedef itk::ImageFileWriter< FeatureImageType > FeatureWriterType;
  FeatureWriterType::Pointer featureWriter = FeatureWriterType::New();
  featureWriter->SetInput( entireFeatureImage );
  featureWriter->SetFileName( "/home/padfield/TEMP/CroppedFeature.mhd" );
  featureWriter->Update();

  typedef itk::ImageFileWriter< LabelImageType > LabelWriterType;
  LabelWriterType::Pointer labelWriter = LabelWriterType::New();
  labelWriter->SetInput( entireLabelImage );
  labelWriter->SetFileName( "/home/padfield/TEMP/CroppedLabel.mhd" );
  labelWriter->Update();



  

  // Calculate the lower and upper thresholds that will separate the
  // two sets of seeds.
  double lowerThreshold;
  double upperThreshold;
  std::string pigDir=itksys::SystemTools::GetFilenamePath( masterFile );

  itk::FindIsolatingThresholds<FeatureImageType,LabelImageType,InternalImageType> thresholdFinder(entireFeatureImage,entireLabelImage,anatomyName,attributesFilesPrefix);

  if (useAllSeeds)
    {
    thresholdFinder.UseAllSeedsOn();
    }
  else
    {
    thresholdFinder.UseAllSeedsOff();
    }
  thresholdFinder.Compute();
  lowerThreshold = thresholdFinder.GetLower();
  upperThreshold = thresholdFinder.GetUpper();



  // Set up the pipeline
  thresholdSegmentation->SetInput( preProcessedLabelImage );
  thresholdSegmentation->SetFeatureImage( preProcessedFeatureImage );

  std::cout << "rmsError: " << rmsError << std::endl;

  // Set the parameters for the Threshold Segmentation
  thresholdSegmentation->UseImageSpacingOn();
  thresholdSegmentation->SetMaximumRMSError( rmsError );
  thresholdSegmentation->SetNumberOfIterations( levelSetIterations );
  thresholdSegmentation->SetEdgeWeight( edgeWeight );
  thresholdSegmentation->SetSmoothingTimeStep(.0625);

/*
  if (lowerThreshold < itk::NumericTraits<unsigned short>::NonpositiveMin())
    {
    lowerThreshold = itk::NumericTraits<unsigned short>::NonpositiveMin();
    }
  if (upperThreshold > itk::NumericTraits<unsigned short>::max())
    {
    upperThreshold = itk::NumericTraits<unsigned short>::max();
    }
*/

  std::cout << "New lower threshold: " << lowerThreshold << std::endl;
  std::cout << "New upper threshold: " << upperThreshold << std::endl;

  thresholdSegmentation->SetLowerThreshold(lowerThreshold); 
  thresholdSegmentation->SetUpperThreshold(upperThreshold); 
  thresholdSegmentation->SetIsoSurfaceValue(127.5);

  thresholdSegmentation->SetPropagationScaling( propScaling );
  thresholdSegmentation->SetCurvatureScaling( curvScaling );



  // Show the evolution of the level set
  // This must be done before the level set filter is updated
  if (ImageDimension == 2)
    {
    itk::ShowLevelSetEvolution<ThresholdSegmentationLevelSetImageFilterType> levelSetEvolution(thresholdSegmentation,displayImageType);
    }

    try
      {
      thresholdSegmentation->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }


  // Write out a model of the 3D image to file
  if (ImageDimension == 3)
    { 
    WriteModelToFile<InternalImageType>(thresholdSegmentation->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputModel);
    }

  // Find the volume of the binary image of the initial relabeled data.
  typedef itk::ImageRegionConstIterator< InternalImageType > UShortConstIteratorType;
  UShortConstIteratorType inputIt1( preProcessedLabelImage, preProcessedLabelImage->GetRequestedRegion() );
  
  int initVolume = 0;
  for ( inputIt1.GoToBegin(); !inputIt1.IsAtEnd(); ++inputIt1)
    {
    if (inputIt1.Get() == 0)
      initVolume++;
    }
  
  std::cout << "Initial volume: " << initVolume << std::endl;


  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << thresholdSegmentation->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << thresholdSegmentation->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << thresholdSegmentation->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << thresholdSegmentation->GetRMSChange() << std::endl;

  return true;
}
