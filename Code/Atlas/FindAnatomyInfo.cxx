// C++ Classes
#include <fstream>
#include <iostream>

#include "FindAnatomyInfo.h"
#include "FindStructureAttributes.h"

bool FindAnatomyInfo(char * fileName, char * masterAnatomyFile, char *
                     boundingBoxesFile, std::string & anatomyName, int & anatomyLabel, std::vector <int> & bbValues, int borderSize, int * imageSize, double * imageSpacing)
{

  // Find the image information
  int sliceOffset;
  if (! FindImageInfo(fileName, sliceOffset, imageSize, imageSpacing) )
    { 
    std::cout << "In FindAnatomyInfo.cxx: Cannot find image information. " << std::endl;
    return false;
    }

  // Change the anatomy name to have spaces instead of underscores
  int nextUnderscore = -1;
  int prevUnderscore;
  for (float i = 0; i < anatomyName.size(); i++)
    {
    prevUnderscore = nextUnderscore;
    nextUnderscore = anatomyName.find('_',prevUnderscore+1);
    if (nextUnderscore == -1)
      break;
    else
      anatomyName.replace(nextUnderscore , 1 ," ");
    }
  
//  std::cout << "Anatomy name: " << anatomyName << std::endl;

  // Find the anatomy label
  FindStructureAttributes<int> labels(masterAnatomyFile,anatomyName);
  if( labels.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;    
    return false;
    }
  anatomyLabel = labels.GetStructureAttributes()[0];



  
//   // Find the anatomy label
//   if (! FindAnatomyLabelFromName(masterAnatomyFile, anatomyName, anatomyLabel) )
//     { 
//     std::cout << "In FindAnatomyInfo.cxx: Cannot find anatomy label from name. " << std::endl;
//     return false;
//     }

  std::cout << "In FindAnatomyInfo.cxx: Anatomy label: " << anatomyLabel << std::endl;


  // Given the anatomy name, find the bounding boxes for the anatomy
  FindStructureAttributes<int> boundingBoxes(boundingBoxesFile,anatomyName);
  if( boundingBoxes.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;    
    return false;
    }
  bbValues = boundingBoxes.GetStructureAttributes();


//   // Given the anatomy name, find the bounding boxes for the anatomy
//   if (! FindBoundingBoxesFromName(boundingBoxesFile, anatomyName, bbValues) )
//     {
//     std::cout << "In FindAnatomyInfo.cxx: Cannot find bounding boxes." << std::endl;
//     return false;
//     }

  

//   std::cout << "In FindAnatomyInfo.cxx: Before: " << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minX: " << bbValues[0] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxX: " << bbValues[1] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minY: " << bbValues[2] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxY: " << bbValues[3] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minZ: " << bbValues[4] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxZ: " << bbValues[5] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minSlice: " << bbValues[6] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxSlice: " << bbValues[7] << std::endl;

 
  // Given the anatomy name and the bounding box values, compute
  // extended bounding box values
  if (! ExtendBoundingBox(bbValues, borderSize, sliceOffset, imageSize) )
    {
    std::cout << "In FindAnatomyInfo.cxx: Cannot extend bounding boxes." << std::endl;
    return false;
    }

//   std::cout << "In FindAnatomyInfo.cxx: After: " << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minX: " << bbValues[0] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxX: " << bbValues[1] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minY: " << bbValues[2] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxY: " << bbValues[3] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minZ: " << bbValues[4] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxZ: " << bbValues[5] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: minSlice: " << bbValues[6] << std::endl;
//   std::cout << "In FindAnatomyInfo.cxx: maxSlice: " << bbValues[7] << std::endl;


  // Use the entire image when segmenting an isolated heart
  if ( anatomyName == "heart" )
    {
    std::cout << "In FindAnatomyInfo.cxx: Ignoring bbValues and using the entire image" << std::endl;
    bbValues[0] = 0;
    bbValues[1] = imageSize[0]-1 ;
    bbValues[2] = 0 ;
    bbValues[3] = imageSize[1]-1 ;
    bbValues[4] = 0 ;
    bbValues[5] = imageSize[2]-1 ;
    bbValues[6] = 0+sliceOffset ;
    bbValues[7] = imageSize[2]-1+sliceOffset;
    }


  return true;
}
