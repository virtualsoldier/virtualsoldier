#ifndef _PreProcessFeatureImage_txx
#define _PreProcessFeatureImage_txx

#include "itkImage.h"
#include "itkResampleImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkCastImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"

template <class TInputImage, class TOutputImage>
typename TOutputImage::Pointer PreProcessFeatureImage(TInputImage * featureImage, int iterations, float conductance)
{

  itkStaticConstMacro(ImageDimension, unsigned int, TInputImage::ImageDimension);

//  const unsigned int ImageDimension = 3;
//  typedef itk::Image< float, ImageDimension > FloatImageType;

  // Filters for the feature image
  typedef itk::ResampleImageFilter< TInputImage, TInputImage> FeatureResampleType;
  typedef itk::LinearInterpolateImageFunction< TInputImage, double >  LinearInterpolatorType;
  typedef itk::CastImageFilter< TInputImage, TOutputImage > FeatureCastType;  
  typedef itk::GradientAnisotropicDiffusionImageFilter< TOutputImage, TOutputImage> DiffusionFilterType;


  // Filters for the feature image
  FeatureResampleType::Pointer featureResampler = FeatureResampleType::New();
  LinearInterpolatorType::Pointer linearInterpolator = LinearInterpolatorType::New();
  FeatureCastType::Pointer castFeatureToFloat = FeatureCastType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();

  // set up the pipeline
  if (ImageDimension == 2)
    {
    castFeatureToFloat->SetInput ( featureImage );
    }
  else if (ImageDimension == 3)
    {
    featureResampler->SetInput ( featureImage );
    castFeatureToFloat->SetInput ( featureResampler->GetOutput() );
    }
  diffusion->SetInput ( castFeatureToFloat->GetOutput() );


  if (ImageDimension == 3)
    {
    // Resample the image to have isotropic spacing by downsampling the
    // X and Y dimensions.
    typename TInputImage::SpacingType inputSpacing;
    inputSpacing[0] = featureImage->GetSpacing()[0];
    inputSpacing[1] = featureImage->GetSpacing()[1];
    inputSpacing[2] = featureImage->GetSpacing()[2];

    typename TInputImage::SizeType inputSize;
    inputSize[0] = featureImage->GetRequestedRegion().GetSize()[0];
    inputSize[1] = featureImage->GetRequestedRegion().GetSize()[1];
    inputSize[2] = featureImage->GetRequestedRegion().GetSize()[2];

    typename TInputImage::SpacingType outputSpacing;
    outputSpacing[0] = 1; //inputSpacing[2];
    outputSpacing[1] = 1; //inputSpacing[2];
    outputSpacing[2] = 1; //inputSpacing[2];

    // The output size should be the input size times the output spacing divided by the input z spacing
    typename TInputImage::SizeType outputSize;
    outputSize[0] = (unsigned long int)(inputSize[0] * inputSpacing[0]/outputSpacing[0]);  // number of pixels along X
    outputSize[1] = (unsigned long int)(inputSize[1] * inputSpacing[1]/outputSpacing[1]);  // number of pixels along Y
    outputSize[2] = (unsigned long int)(inputSize[2] * inputSpacing[2]/outputSpacing[2]);  // number of pixels along Z

/*
  std::cout << "Input image size: " << inputSize << std::endl;
  std::cout << "Input spacing: " << inputSpacing << std::endl;
  std::cout << "Input origin: " << featureImage->GetOrigin() << std::endl;
  std::cout << "Output image size: " << outputSize << std::endl;
  std::cout << "Output spacing: " << outputSpacing << std::endl;
  std::cout << "Output origin: " << featureImage->GetOrigin() << std::endl;
*/

    featureResampler->SetOutputSpacing( outputSpacing );
    featureResampler->SetOutputOrigin ( featureImage->GetOrigin() );
    featureResampler->SetSize( outputSize );
    featureResampler->SetInterpolator( linearInterpolator );
  

    // Update the resampler
    try
      {
      featureResampler->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    std::cout << "New feature image spacing: " << featureResampler->GetOutput()->GetSpacing() << std::endl;
    std::cout << "New feature image origin: " << featureResampler->GetOutput()->GetOrigin() << std::endl;
    std::cout << "New feature image size: " << featureResampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
    }



  // Smooth the feature image
  diffusion->SetNumberOfIterations( iterations );
  diffusion->SetConductanceParameter( conductance );
  diffusion->UseImageSpacingOn();
  diffusion->SetTimeStep(0.02);
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "# of iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;



  return diffusion->GetOutput();
}

#endif
