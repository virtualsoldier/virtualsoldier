#ifndef _itkFindIsolatingThresholds_h
#define _itkFindIsolatingThresholds_h

#include "itkSimpleFilterWatcher.h"
#include "itkIsolatedConnectedImageFilter.h"

#include <map>

namespace itk {
template<class FeatureImageType,class LabelImageType,class InternalImageType>
class FindIsolatingThresholds
{
public:
  typedef FindIsolatingThresholds Self;

  typedef InternalImageType InputImageType;
  typedef typename InputImageType::Pointer InputImagePointer;
  typedef typename InputImageType::ConstPointer InputImageConstPointer;
  typedef typename InputImageType::RegionType InputImageRegionType; 
  typedef typename InputImageType::PixelType InputImagePixelType; 
  typedef typename InputImageType::IndexType IndexType;
  typedef typename InputImageType::SizeType SizeType;

  typedef std::map< std::string, std::string > StringToStringMapType;
  typedef std::map< std::string, float> StringToFloatMapType;
  typedef std::map< int, std::string > IntToStringMapType;
  typedef std::map< std::string, int > StringToIntMapType;

  itkStaticConstMacro(ImageDimension, unsigned int, InputImageType::ImageDimension);

  typedef itk::IsolatedConnectedImageFilter< FeatureImageType, FeatureImageType >
      IsolatedConnectedType;

  FindIsolatingThresholds( FeatureImageType * entireFeatureImage, LabelImageType * entireLabelImage, std::string anatomyName, std::string dataFilesDirectory);

  ~FindIsolatingThresholds() {};

  bool Compute();
  bool FindIsolatedConnectedThreshold( int & );
  bool FindOtsuThreshold( int & );
  int CalculateOtsuThreshold(FeatureImageType *, LabelImageType *, int &, int &);
  bool ClassifyStructures();
  // Map the structure label to the structure name and vice versa.
  bool StructureLabelMapper();
  // Thresholder is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  typename LabelImageType::Pointer ThresholdLabelImage(LabelImageType *, int);


  void FindUpperThresholdOn()  { m_FindUpperThreshold = true; }
  void FindUpperThresholdOff() { m_FindUpperThreshold = false; }
  void UseAllSeedsOn()  { m_UseAllSeeds = true; }
  void UseAllSeedsOff() { m_UseAllSeeds = false; }
  InputImagePixelType GetLower() const { return m_Lower; }
  InputImagePixelType GetUpper() const { return m_Upper; }

protected:

private:
  FindIsolatingThresholds(); // Purposely not implemented  
  void operator=(const Self&); //purposely not implemented

  typename IsolatedConnectedType::Pointer isolatedConnected;
//  itk::SimpleFilterWatcher watchIsolated(isolatedConnected,"isolated");

  typename FeatureImageType::Pointer m_EntireFeatureImage;
  typename LabelImageType::Pointer m_EntireLabelImage;
  typename LabelImageType::Pointer m_ThresholdedLabelImage;
  typename LabelImageType::Pointer m_AdjacentLabelImage;

  StringToStringMapType m_NameToClassMapper;
  IntToStringMapType m_LabelToNameMapper;
  StringToIntMapType m_NameToLabelMapper;

  InputImagePixelType m_Lower;
  InputImagePixelType m_Upper;
  bool m_FindUpperThreshold;
  bool m_UseAllSeeds;
  std::string m_DataFilesDirectory;
  std::string m_StructureName;
  int m_StructureLabel;

};
}

#ifndef VSP_MANUAL_INSTANTIATION
#include "FindIsolatingThresholds.txx"
#endif

#endif
