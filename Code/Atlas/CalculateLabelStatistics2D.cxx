/*=========================================================================

This program calculates the image mean and standard deviation of each
labeled structure in a series of 2D images.  It uses the
LabelStatisticsImageFilter to calculate the mean, std, sum, and count
of pixels in 2D of a feature image under a label mask.  The mean
across the slices can be found directly from the mean of each slice,
but the standard deviation of the volume is not as easy to calculate.
It can, however, be found using the total pixel count, the total mean,
and the total sum of the squared intensities.

Inputs: Feature image prefix, label image prefix, master anatomy file.
Outputs: Statistics file listing the mean and std of each structure.


=========================================================================*/


#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"

#include "vcl_cmath.h"
#include "vnl/vnl_math.h"

#include <fstream>
#include <string>
#include <vector>

#include "FindStructureAttributes.h"

int main( int argc, char *argv[] )
{
  if( argc < 6 )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix LabelImagePrefix imageInfoFile";
    std::cerr << " MasterAnatomyFile OutputStatisticsFile";
    std::cerr << std::endl;
    return 1;
    }
  

  // Save all arguments as local variables
  char * featureImages = argv[1];
  char * labelImages = argv[2];
  char * imageInfoFile = argv[3];
  std::string masterAnatomyFile = argv[4];
  char * statisticsFile = argv[5];

  for (int i = 0; i < argc; i++)
    {
    std::cout << argv[i] << std::endl;
    }

  // Test the master anatomy file
  std::ifstream anatomyFile( masterAnatomyFile.c_str() );
  if (!anatomyFile) 
    {
    std::cout << "In FindAnatomyLabelFromName.cxx: Could not open master anatomy file: " << anatomyFile << std::endl;
    return false;
    }

  // Test the output file
  std::ofstream fout ( statisticsFile );
  if (!fout)
    {
    std::cout << "Could not open label statistics file" << std::endl;
    return 0;
    }

  typedef itk::Image< unsigned short, 2 > LabelImageType;
  typedef itk::Image< short, 2 > FeatureImageType;
  typedef itk::ImageFileReader< LabelImageType > LabelReaderType;
  typedef itk::ImageFileReader< FeatureImageType >  FeatureReaderType;
  typedef itk::ArchetypeSeriesFileNames NameGeneratorType;
  typedef itk::LabelStatisticsImageFilter< FeatureImageType, LabelImageType > LabelStatisticsFilterType;

  FeatureReaderType::Pointer featureImageReader = FeatureReaderType::New();
  LabelReaderType::Pointer labelImageReader = LabelReaderType::New();
  NameGeneratorType::Pointer featureImageFileGenerator = NameGeneratorType::New();
  NameGeneratorType::Pointer labelImageFileGenerator = NameGeneratorType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();


  // The following populates the mapper, which will contain a mapping 
  // from the anatomy name to its label.
  typedef std::map< std::string, int > StringToIntMapType;
  StringToIntMapType masterAnatomyStringToIntMapper;

  FindStructureAttributes<int> structures(masterAnatomyFile);
  if( structures.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;    
    return 0;
    }
  std::vector< std::string > allStructureNames = structures.GetAllStructureNames();
  std::vector< std::vector < int > > allStructureAttributes = structures.GetAllStructureAttributes();

  std::vector< std::string >::iterator nameIt = allStructureNames.begin();;
  std::vector< std::vector< int > >::iterator allAttributesIt = allStructureAttributes.begin();
  int structureLabel;
  while( nameIt != allStructureNames.end() )
    {
    structureLabel = (*allAttributesIt)[0];
    masterAnatomyStringToIntMapper.insert(StringToIntMapType::value_type( *nameIt, structureLabel ));      
    nameIt++;
    allAttributesIt++;
    }


  const int numberOfAnatomies = masterAnatomyStringToIntMapper.size();

  std::cout << "Number of anatomies: " << numberOfAnatomies << std::endl;
  if (numberOfAnatomies > 1024)
    {
    std::cout << "More than 1024 anatomies: cannot fit them in boundary box indices array" << std::endl;
    return 0;
    }



  // Generate the image file names
  featureImageFileGenerator->SetArchetype ( featureImages );
  labelImageFileGenerator->SetArchetype ( labelImages );

  std::vector <std::string > featureImageNames = featureImageFileGenerator->GetFileNames();
  std::cout << "Input image name 0: " << featureImageNames[0] << std::endl;
  std::cout << "Input image name last: " << featureImageNames[featureImageNames.size() - 1] << std::endl;

  std::vector <std::string > labelImageNames = labelImageFileGenerator->GetFileNames();
  std::cout << "Input image name 0: " << labelImageNames[0] << std::endl;
  std::cout << "Input image name last: " << labelImageNames[ featureImageNames.size() - 1] << std::endl;

  std::vector < std::vector < float > > sumMatrix;
  std::vector < std::vector < float > > countMatrix;
  std::vector < std::vector < float > > meanMatrix;
  std::vector < std::vector < float > > sigmaMatrix;
  std::vector < std::vector < float > > sumSampleSqMatrix;

  int slice;
  std::vector < float > labelMin(numberOfAnatomies);
  std::vector < float > labelMax(numberOfAnatomies);
  std::vector < float > labelSum(numberOfAnatomies);
  std::vector < float > labelCount(numberOfAnatomies);
  std::vector < float > labelMean(numberOfAnatomies);
  std::vector < float > labelSigma(numberOfAnatomies);

  std::vector <std::string> ::iterator finit;
  std::vector <std::string> ::iterator linit;


  // Loop through each anatomy file
  for (finit = featureImageNames.begin(), linit = labelImageNames.begin();
       finit != featureImageNames.end(); finit++, linit++)
    {
    slice = finit - featureImageNames.begin();
    std::cout << "Slice: " << slice << std::endl;

    // Update the slice to be processed
    featureImageReader->SetFileName ( (*finit).c_str() );    
    featureImageReader->Update(); 
    labelImageReader->SetFileName ( (*linit).c_str() );    
    labelImageReader->Update(); 


    // Set the parameters for the label statistics filter.
    // This will calculate the statistics of the pixel values in the
    // feature image that are labeled as part of the anatomy in the
    // labeled image.
    // The mean and standard deviation of these values are used to give
    // appropriate lower and upper threshold values for the 
    // ThresholdSegmentationLevelSetImageFilter.
    labelStatistics->SetInput( featureImageReader->GetOutput() );
    labelStatistics->SetLabelInput( labelImageReader->GetOutput() );
    labelStatistics->Update();

    int index = 0;
    StringToIntMapType::iterator it = masterAnatomyStringToIntMapper.begin();
    for (it = masterAnatomyStringToIntMapper.begin(); it != masterAnatomyStringToIntMapper.end(); it++,index++)
      {
      labelMin[index] = labelStatistics->GetMinimum( it->second );
      labelMax[index] = labelStatistics->GetMaximum( it->second );
      labelSum[index] = labelStatistics->GetSum( it->second );
      labelCount[index] = labelStatistics->GetCount( it->second );
      labelMean[index] = labelStatistics->GetMean( it->second );
      labelSigma[index] = labelStatistics->GetSigma( it->second );

//       if ( it->second == 6)
//         {
//         std::cout << "label: " << it->second << "\t";
//         std::cout << "labelSum: " << labelSum[index] << "\t";
//         std::cout << "labelCount: " << labelCount[index] << "\t";
//         std::cout << "labelMean: " << labelMean[index] << "\t";
//         }
      }
    sumMatrix.push_back ( labelSum );
    countMatrix.push_back ( labelCount );
    meanMatrix.push_back ( labelMean );
    sigmaMatrix.push_back ( labelSigma );
    }

//   std::vector< float >::iterator test = sumMatrix[0].begin();
//   while (test != sumMatrix[0].end() )
//     {
//     std::cout << *test << std::endl;
//     test++;
//     }


//     for( IntToIntMapType::iterator itTEST=localAnatomyIntToIntMapper.begin(); itTEST!=localAnatomyIntToIntMapper.end(); itTEST++ )
//     {
//     std::cout << "local label: " << itTEST->first << " mapped to master label: " << itTEST->second << std::endl;    
//     }
//     // Look at output of local label to name mapper
//     for( IntToStringMapType::iterator itTEST2=localAnatomyIntToStringMapper.begin(); itTEST2!=localAnatomyIntToStringMapper.end(); itTEST2++ )
//     {
//     std::cout << "local label: " << itTEST2->first << " mapped to master label: " << itTEST2->second << std::endl;    
//     }


  std::cout << "The number of slices: " << sumMatrix.size() << std::endl;
  std::cout << "The number of anatomies: " << sumMatrix[0].size() << std::endl;


  // Calculate the sum of the squared intensities for each slice
  std::vector < std::vector < float > > ::iterator  countMatrixIt;
  std::vector < std::vector < float > > ::iterator  meanMatrixIt;
  std::vector < std::vector < float > > ::iterator  sigmaMatrixIt;

  countMatrixIt = countMatrix.begin();
  meanMatrixIt = meanMatrix.begin();
  sigmaMatrixIt = sigmaMatrix.begin();

  std::vector < float > sumSampleSq(numberOfAnatomies);

  std::vector < float > ::iterator countIt;
  std::vector < float > ::iterator meanIt;
  std::vector < float > ::iterator sigmaIt;
  std::vector < float > ::iterator sumSampleSqIt;

  while (countMatrixIt != countMatrix.end())
    {
    countIt = (*countMatrixIt).begin();
    meanIt = (*meanMatrixIt).begin();
    sigmaIt = (*sigmaMatrixIt).begin();
    sumSampleSqIt = sumSampleSq.begin();

    while (countIt != (*countMatrixIt).end())
      {      
      *sumSampleSqIt = (*sigmaIt) * (*sigmaIt) * ((*countIt) - 1) + (*countIt) * (*meanIt) * (*meanIt);
      
      countIt++;
      meanIt++;
      sigmaIt++;
      sumSampleSqIt++;
      }

    sumSampleSqMatrix.push_back ( sumSampleSq );

    countMatrixIt++;
    meanMatrixIt++;
    sigmaMatrixIt++;
    }

  int testAnatomy = 0;
  std::cout << "Count Matrix (0,0): " << countMatrix[0][testAnatomy] << std::endl;
  std::cout << "Mean Matrix (0,0): " << meanMatrix[0][testAnatomy] << std::endl;
  std::cout << "Sigma Matrix (0,0): " << sigmaMatrix[0][testAnatomy] << std::endl;
  std::cout << "SumSampleSq Matrix (0,0): " << sumSampleSqMatrix[0][testAnatomy] << std::endl;


  // Initialize the variables
  std::vector < float > totalSum(numberOfAnatomies);
  std::vector < float > totalCount(numberOfAnatomies);
  std::vector < float > totalMean(numberOfAnatomies);
  std::vector < float > totalSigma(numberOfAnatomies);
  std::vector < float > totalSumSampleSq(numberOfAnatomies);

  // This is not necessary since the default is to initialize to 0
//   for( StringToIntMapType::iterator itM=masterAnatomyStringToIntMapper.begin(); itM!=masterAnatomyStringToIntMapper.end(); itM++ )
//     {
//     totalSum[itM->second] = 0;
//     totalCount[itM->second] = 0;
//     totalMean[itM->second] = 0;
//     totalSigma[itM->second] = 0;
//     totalSumSampleSq[itM->second] = 0;
//     }
  
  // Calculate the sum of the intensities across all slices
  std::vector < std::vector < float >  > ::iterator  sumMatrixIt;
  std::vector < float > ::iterator sumIt;
  std::vector < float > ::iterator totalSumIt;
  for (sumMatrixIt = sumMatrix.begin(); sumMatrixIt != sumMatrix.end(); sumMatrixIt++)
    {
    sumIt = (*sumMatrixIt).begin();
    totalSumIt = totalSum.begin();

    while (sumIt != (*sumMatrixIt).end())
      {      
      *totalSumIt += *sumIt;

      sumIt++;
      totalSumIt++;
      }
    }

  // Calculate the sum of the mean intensities across all slices
  std::vector < float > ::iterator totalCountIt;
  for (countMatrixIt = countMatrix.begin(); countMatrixIt != countMatrix.end(); countMatrixIt++)
    {
    countIt = (*countMatrixIt).begin();
    totalCountIt = totalCount.begin();

    while (countIt != (*countMatrixIt).end())
      {      
      *totalCountIt += *countIt;

      countIt++;
      totalCountIt++;
      }
    }

  // Calculate the mean of the intensities across all slices
  std::vector < float > ::iterator totalMeanIt;

  totalSumIt = totalSum.begin();
  totalCountIt = totalCount.begin();
  totalMeanIt = totalMean.begin();

  while (totalMeanIt != totalMean.end())
    {
    *totalMeanIt = (*totalSumIt)/(*totalCountIt);

    totalSumIt++;
    totalCountIt++;
    totalMeanIt++;
    }

  // Calculate the sum of the squared intensities across all slices
  std::vector < std::vector < float > > ::iterator  sumSampleSqMatrixIt;  
  std::vector < float > ::iterator totalSumSampleSqIt;
  for (sumSampleSqMatrixIt = sumSampleSqMatrix.begin(); sumSampleSqMatrixIt != sumSampleSqMatrix.end(); sumSampleSqMatrixIt++)
    {
    sumSampleSqIt = (*sumSampleSqMatrixIt).begin();
    totalSumSampleSqIt = totalSumSampleSq.begin();

    while (sumSampleSqIt != (*sumSampleSqMatrixIt).end())
      {      
      *totalSumSampleSqIt += *sumSampleSqIt;

      sumSampleSqIt++;
      totalSumSampleSqIt++;
      }
    }

  // Calculate the sigma of the intensities across all slices
  std::vector < float > ::iterator totalSigmaIt;
  totalCountIt = totalCount.begin();
  totalMeanIt = totalMean.begin();
  totalSumSampleSqIt = totalSumSampleSq.begin();
  totalSigmaIt = totalSigma.begin();

  while ( totalSigmaIt != totalSigma.end() )
    {
    *totalSigmaIt = (1/((*totalCountIt) - 1)) * ((*totalSumSampleSqIt) - (*totalCountIt) * (*totalMeanIt) * (*totalMeanIt));

    *totalSigmaIt = sqrt (*totalSigmaIt);

    totalCountIt++;
    totalMeanIt++;
    totalSumSampleSqIt++;
    totalSigmaIt++;
    }

  std::cout << "Count(0): " << totalCount[testAnatomy] << std::endl;
  std::cout << "Mean(0): " << totalMean[testAnatomy] << std::endl;
  std::cout << "Sigma(0): " << totalSigma[testAnatomy] << std::endl;
  std::cout << "SumSampleSq(0): " << totalSumSampleSq[testAnatomy] << std::endl;


  // Print out the values to file
  int index = 0;
  fout << "Anatomy Name, Label Mean, Label Sigma, Label Count, Label Sum" << std::endl;
  for( StringToIntMapType::iterator itM=masterAnatomyStringToIntMapper.begin(); itM!=masterAnatomyStringToIntMapper.end(); itM++,index++ )
    {
    fout << itM->first
         << "," << totalMean[index]
         << "," << totalSigma[index]
         << "," << totalCount[index]
         << "," << totalSum[index]
         << std::endl;
    }
  fout.close();

  return 0;
}
