#include "itkImage.h"
#include "itkCannySegmentationLevelSetImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkExceptionObject.h"
#include "itkExtractImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

#include "itkVector.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"


int main( int argc, char *argv[] )
{

  int numberOfParameters = 17;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " AdvectionScaling";
    std::cerr << " CannyThreshold ";
    std::cerr << " CannyVariance ";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " BorderSize";
    std::cerr << " DisplayImageType";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  char * outputModel = argv[6];
  std::string  anatomyName = argv[7];
  float propScaling = atof( argv[8] );
  float curvScaling = atof( argv[9] );
  float advecScaling = atof( argv[10] );
  float cannyThreshold = atof( argv[11] );
  float cannyVariance = atof( argv[12] );
  int iterations = atoi( argv[13] );
  float conductance = atof( argv[14] );
  int borderSize = atoi( argv[15] );
  std::string displayImageType = argv[16];

  
  typedef itk::Image< float, 2 > Float2DImageType;
  typedef itk::Image< unsigned short, 2 > UShort2DImageType;
  typedef itk::Image< unsigned char, 2 > UChar2DImageType;

  typedef itk::ImageFileReader< UShort2DImageType >  ReaderType;
  typedef itk::LabelStatisticsImageFilter< UShort2DImageType, UShort2DImageType > LabelStatisticsFilterType;

  // Filters for the label image
  typedef itk::BinaryThresholdImageFilter< Float2DImageType, Float2DImageType> ThresholdingFilterType1;

  // Filters for the feature image
  typedef itk::CastImageFilter< UShort2DImageType, Float2DImageType > CastImageFilterType;  
  typedef itk::GradientAnisotropicDiffusionImageFilter< Float2DImageType, Float2DImageType> DiffusionFilterType;
  typedef itk::CannySegmentationLevelSetImageFilter< Float2DImageType, Float2DImageType > CannySegmentationLevelSetImageFilterType;
  typedef itk::BinaryThresholdImageFilter< Float2DImageType, UShort2DImageType> ThresholdingFilterType2;

  UShort2DImageType::Pointer featureImage = UShort2DImageType::New();
  UShort2DImageType::Pointer labelImage = UShort2DImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  // Filters for the label image
  CastImageFilterType::Pointer castLabelToFloat = CastImageFilterType::New();
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();

  // Filters for the feature image
  CastImageFilterType::Pointer castFeatureToFloat = CastImageFilterType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();
  CannySegmentationLevelSetImageFilterType::Pointer cannySegmentation = CannySegmentationLevelSetImageFilterType::New();
  ThresholdingFilterType2::Pointer thresholder2 = ThresholdingFilterType2::New();



  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImagePrefix,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize, inputImageSpacing) )
    {
    return 0;
    }

 
  // Create images that are cropped according to the bounding box values
  itk::CreateVolumeFromSlices<UShort2DImageType>( featureImagePrefix, bbValues, inputImageSpacing, featureImage);
  itk::CreateVolumeFromSlices<UShort2DImageType>( labelImagePrefix, bbValues, inputImageSpacing, labelImage);


  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

  castLabelToFloat->SetInput( labelImage);
  thresholder1->SetInput( castLabelToFloat->GetOutput() );
  cannySegmentation->SetInput( thresholder1->GetOutput() );

  castFeatureToFloat->SetInput ( featureImage );
  diffusion->SetInput ( castFeatureToFloat->GetOutput() );
  cannySegmentation->SetFeatureImage( diffusion->GetOutput());
  thresholder2->SetInput( cannySegmentation->GetOutput() );

  // Set the parameters for the label statistics filter
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  try
    {
    labelStatistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;

  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);


  // Smooth the feature image
  diffusion->SetNumberOfIterations( iterations );
  diffusion->SetConductanceParameter( conductance );
  diffusion->UseImageSpacingOn();
  diffusion->SetTimeStep(0.02);
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "# of iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;

  // Set the parameters for the Canny Segmentation
  cannySegmentation->UseImageSpacingOn();
  cannySegmentation->SetMaximumRMSError( 0.02 );
  cannySegmentation->SetNumberOfIterations( 1200 );
  cannySegmentation->SetIsoSurfaceValue(127.5);
  cannySegmentation->SetPropagationScaling( propScaling );
  cannySegmentation->SetCurvatureScaling( curvScaling );
  cannySegmentation->SetAdvectionScaling( advecScaling );
  cannySegmentation->SetThreshold( cannyThreshold );
  cannySegmentation->SetVariance( cannyVariance );

  // Show the evolution of the level set
  itk::ShowLevelSetEvolution<CannySegmentationLevelSetImageFilterType> levelSetEvolution(cannySegmentation,displayImageType);


  // Set the parameters for Thresholder2
  // Thresholder2 is used to threshold the image after the level set
  // segmentation is complete.
  //  thresholder2->SetLowerThreshold( 0.0 );
  thresholder2->SetLowerThreshold( -1000.0 );
  thresholder2->SetUpperThreshold(     0.0 );
  thresholder2->SetOutsideValue( 255 );
  thresholder2->SetInsideValue( 0 );
  try
    {
    thresholder2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Size: " << thresholder1->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Size: " << thresholder2->GetOutput()->GetRequestedRegion().GetSize() << std::endl;

  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << cannySegmentation->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << cannySegmentation->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << cannySegmentation->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << cannySegmentation->GetRMSChange() << std::endl;

  itk::FileDumper<Float2DImageType> dumpDiffusion(diffusion->GetOutput(), "diffusion");
  itk::FileDumper<Float2DImageType> dumpOriginal(castFeatureToFloat->GetOutput(), "original");
  if (cannySegmentation->GetPropagationScaling() != 0.0)
    {
    itk::FileDumper<Float2DImageType> dumpSpeed(cannySegmentation->GetSpeedImage(), "speed");
    }
  itk::FileDumper<Float2DImageType> dumpEdges(cannySegmentation->GetCannyImage(), "edges");
  itk::FileDumper<Float2DImageType> dumpLabels(castLabelToFloat->GetOutput(), "labels");
  itk::FileDumper<Float2DImageType> dumpLevelSet(cannySegmentation->GetOutput(), "levelSet");
  itk::FileDumper<Float2DImageType> dumpThresholder(thresholder1->GetOutput(), "labelThresholder");
  itk::FileDumper<UShort2DImageType> dumpThresholder2(thresholder2->GetOutput(), "levelSetThresholder");

  // Print out the advection image
  typedef itk::Vector< float, 1 > VectorPixelType;
  typedef itk::Image< VectorPixelType, 3 > VectorImageType;
  typedef itk::ImageFileWriter < VectorImageType > VectorWriterType;
  VectorWriterType::Pointer advectionWriter = VectorWriterType::New();
  //advectionWriter->SetInput( cannySegmentation->GetAdvectionImage() );
  //advectionWriter->SetFileName("AdvectionImage.mhd");
  //advectionWriter->Update();


  return 0;
}
