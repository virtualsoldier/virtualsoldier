/*=========================================================================

This program reads a image information file and extracts the number of
slices, the slice offset, and the image spacing.

Inputs: Image information file.
Outputs: Number of slices, slice offset, and image spacing.

=========================================================================*/
#ifndef _FindImageInfo_h
#define _FindImageInfo_h

#include <string>
#include <vector>

bool FindImageInfo(char * fileName, int & sliceOffset, int * imageSize, double * imageSpacing);

#endif
