#include "FindBoundingBoxesFromName.h"
#include <fstream>
#include <iostream>

bool FindBoundingBoxesFromName(char * boundingBoxesFile, std::string anatomyName, std::vector <int> &  bbValues)
{
  // Test the bounding boxes file
  std::ifstream bbFile(boundingBoxesFile);
  if (!bbFile)
    {
    std::cout << "\tCould not open bounding box file: " << boundingBoxesFile << std::endl;
    return false;
    }
  
  // Default value for bounding box for error checking
  for (unsigned int i = 0; i < bbValues.size(); i++)
    {
    bbValues[i] = -1;
    }

  // Declare the variables.
  char entireLine[255];
  std::string line;
  int endLine;
  int begin;
  int lastComma;
  int nextComma;

  // Remove the first line, which is blank
  bbFile.getline(entireLine,255);

  // Loop through the file looking for the anatomy name.  If it is
  // found, assign the bounding box values to a vector and break.
  while (! bbFile.eof() )
    {
    bbFile.getline(entireLine,255);
    line=std::string(entireLine); 
    endLine = line.length();

    if (! line.empty() )
      {
      begin = 0;
      nextComma = line.find(',',begin);
      std::string fileAnatomyName = line.substr(begin, nextComma-begin);

      if (fileAnatomyName == anatomyName)
        {
        for (unsigned int i = 0; i < bbValues.size()-1; i++)
          {
          lastComma = nextComma;
          nextComma = line.find(',',lastComma+1);
          bbValues[i] = atoi(line.substr(lastComma+1, nextComma-(lastComma+1)).c_str());  
          }
        lastComma = nextComma;
        bbValues[7] = atoi(line.substr(lastComma+1, endLine-(lastComma+1)).c_str());
      
        break;
        }
      }
    }

  bbFile.close();

  // Test that the bounding box values were found.
  if (bbValues[0] == -1)
    {
    std::cerr << "ERROR: " << anatomyName << " not listed in Bounding Box file" << std::endl;
    return false;
    }
  else
    {
    return true;
    }
}
