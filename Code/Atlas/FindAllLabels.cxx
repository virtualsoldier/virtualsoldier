/*=========================================================================

This program takes one label image name, uses ArchetypeSeriesFileNames
to loop through all images in the directory of that label image, and
pushes back all labels that are present in the image to a variable.
This variable is printed at the end of the program to show which
labels are present in the volume.

Inputs: Archetype label image name.
Outputs: Print all the labels out to the screen

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkImageRegionIterator.h"

// C++ classes
#include <set>

int main( int argc, char *argv[] )
{
  if (argc != 3)
    {
    std::cout << "Usage: "
              << "labelImageName masterAnatomyFile" 
              << std::endl;
    return 0;
    }  

  // Save all arguments as local variables
  char * labelImageName = argv[1];
  char * masterAnatomyFile = argv[2];

  // Test the bounding boxes file
  std::ofstream fout ( masterAnatomyFile );
  if (!fout) 
    {
    std::cout << "\tCould not open master anatomy file" << std::endl;
    return false;
    }


  typedef   unsigned short   InputPixelType;
  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::ArchetypeSeriesFileNames NameGeneratorType; 

  ReaderType::Pointer reader = ReaderType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();

  // Generate the image file names
  inputImageFileGenerator->SetArchetype( labelImageName );

  std::cout << inputImageFileGenerator->GetFileNames()[0] << std::endl;
  std::vector <std::string > inputImageNames = inputImageFileGenerator->GetFileNames();

  std::vector <std::string> ::iterator iinit;
  int slice;
  typedef std::set< int > IntSetType;  
  IntSetType allLabels;

  // Loop through each anatomy file
  for (iinit = inputImageNames.begin(); iinit != inputImageNames.end(); ++iinit)
    {
    slice = iinit - inputImageNames.begin();
    std::cout << "Slice: " << slice << std::endl;

    // Read the image
    reader->SetFileName ( (*iinit).c_str() );
      
    try 
      { 
      reader->Update();
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 
    
    itk::ImageRegionIterator  < InputImageType > imageIt (reader->GetOutput(),reader->GetOutput()->GetRequestedRegion());
    for ( imageIt.GoToBegin(); !imageIt.IsAtEnd(); ++imageIt)
      {
      allLabels.insert( imageIt.Get() );
      }
    }

  // Write out the labels to file
  fout << "Anatomy,Label" << std::endl;
  IntSetType::iterator allLabelsIt;
  for (allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++)
    {
    fout << "Struct" << *allLabelsIt << "," << *allLabelsIt << std::endl;
    }
  fout.close();


}
