/*=========================================================================

This program reads in a dicom image, smoothes it with
CurvatureAnisotropicDiffusion in 2D, and then writes out the smoothed
image as a dicom image.

Inputs: Input image, iterations, time step, conductance, use image
spacing [optional].
Outputs: Output image.

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkCurvatureAnisotropicDiffusionImageFilter.h"

#include "itkGDCMImageIO.h"
#include "itkCastImageFilter.h"

int main( int argc, char * argv[] )
{
  if( argc < 6 ) 
    { 
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << "  inputImageFile  outputImageFile ";
    std::cerr << "numberOfIterations  timeStep  conductance useImageSpacingon/off" << std::endl;
    return 1;
    }

  char * inputImage = argv[1];
  char * outputImage = argv[2];
  const unsigned int numberOfIterations = atoi( argv[3] );
  const double       timeStep = atof( argv[4] );
  const double       conductance = atof( argv[5] );
  const bool         useImageSpacing = (argc != 6);

  typedef    short    InputPixelType;
  typedef    float    InternalPixelType;

  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::Image< InternalPixelType, 2 > InternalImageType;

  typedef itk::GDCMImageIO ImageIOType;
  ImageIOType::Pointer dicomIO = ImageIOType::New();

  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName( inputImage );
  reader->SetImageIO( dicomIO );

  // Conduct CurvatureAnisotropicDiffusion on the image.
  typedef itk::CurvatureAnisotropicDiffusionImageFilter< InputImageType, InternalImageType >  FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput( reader->GetOutput() );
  filter->SetNumberOfIterations( numberOfIterations );
  filter->SetTimeStep( timeStep );
  filter->SetConductanceParameter( conductance );
  if (useImageSpacing)
    {
    filter->UseImageSpacingOn();
    }

  try
    {
    filter->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Cast the image back from the output type of the smoother to the
  // input type.
  typedef itk::CastImageFilter< InternalImageType, InputImageType > CastType;
  CastType::Pointer caster = CastType::New();
  caster->SetInput( filter->GetOutput() );
  caster->GetOutput()->SetMetaDataDictionary( reader->GetOutput()->GetMetaDataDictionary() );

  // Write out the image as a dicom image.
  typedef itk::ImageFileWriter< InputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName( outputImage );
  dicomIO->KeepOriginalUIDOn();
  writer->SetImageIO( dicomIO );
  writer->SetInput( caster->GetOutput() );

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  return 0;
}

