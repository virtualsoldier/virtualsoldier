#include "itkImage.h"
#include "itkLaplacianSegmentationLevelSetImageFilter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkExceptionObject.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"
#include "PreProcessFeatureImage.h"
#include "PreProcessLabelImage.h"
#include "WriteSlicesFromVolume.h"
#include "WriteModelToFile.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"

template < const int Dimension >
bool SegmentStructure(char * featureImagePrefix, char * labelImagePrefix, char * imageInfoFile, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float propScaling, float curvScaling, float binaryRadius, int iterations, float conductance, int borderSize, std::string displayImageType);

int main( int argc, char *argv[] )
{
  int numberOfParameters = 16;

  if( argc < numberOfParameters )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " ImageInfoFile";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " BinaryErodeRadius";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " BorderSize";
    std::cerr << " DisplayImageType";
    std::cerr << " Dimension";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  std::string outputModel = argv[6];
  std::string anatomyName = argv[7];
  float propScaling = atof( argv[8] );
  float curvScaling = atof( argv[9] );
  float binaryRadius = atof( argv[10] );
  int iterations = atoi( argv[11] );
  float conductance = atof( argv[12] );
  int borderSize = atoi( argv[13] );
  std::string displayImageType = argv[14];
  int Dimension = atoi (argv[15]);

  if (Dimension == 2)
    {
    SegmentStructure< 2 > (featureImagePrefix, labelImagePrefix, imageInfoFile, masterFile, boundingBoxesFile, outputModel, anatomyName, propScaling, curvScaling, binaryRadius, iterations, conductance, borderSize, displayImageType);
    }
  else if (Dimension == 3)
    {
    SegmentStructure< 3 > (featureImagePrefix, labelImagePrefix, imageInfoFile, masterFile, boundingBoxesFile, outputModel, anatomyName, propScaling, curvScaling, binaryRadius, iterations, conductance, borderSize, displayImageType);
    }
  return 0;
}

template < const int Dimension >
bool SegmentStructure(char * featureImagePrefix, char * labelImagePrefix, char * imageInfoFile, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float propScaling, float curvScaling, float binaryRadius, int iterations, float conductance, int borderSize, std::string displayImageType)
{
  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImagePrefix,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize,inputImageSpacing) )
    {
    return 0;
    }

  for (unsigned int i = 0; i < bbValues.size(); i++)
    {
    std::cout << "bbValues[" << i << "]: " << bbValues[i] << std::endl;
    }


  typedef short FeaturePixelType;
  typedef unsigned short LabelPixelType;
  typedef float InternalPixelType;

  typedef itk::Image< short, Dimension > ShortImageType;
  typedef itk::Image< unsigned short, Dimension > UShortImageType;
  typedef itk::Image< float, Dimension > FloatImageType;

  typedef itk::LabelStatisticsImageFilter< ShortImageType, UShortImageType > LabelStatisticsFilterType;

  typedef itk::LaplacianSegmentationLevelSetImageFilter< FloatImageType, FloatImageType > LaplacianSegmentationLevelSetImageFilterType;

  // Instantiate the filters
  ShortImageType::Pointer featureImage = ShortImageType::New();
  UShortImageType::Pointer labelImage = UShortImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  LaplacianSegmentationLevelSetImageFilterType::Pointer laplacianSegmentation = LaplacianSegmentationLevelSetImageFilterType::New();


  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating feature volume from feature image slices" << std::endl;
  itk::CreateVolumeFromSlices<ShortImageType>( featureImagePrefix, bbValues, inputImageSpacing, featureImage);
  std::cout << "Creating label volume from label image slices" << std::endl;
  itk::CreateVolumeFromSlices<UShortImageType>( labelImagePrefix, bbValues, inputImageSpacing, labelImage);
  
  FloatImageType::Pointer preProcessedFeatureImage;
  preProcessedFeatureImage = PreProcessFeatureImage<ShortImageType,FloatImageType> (featureImage, iterations, conductance);

  FloatImageType::Pointer preProcessedLabelImage;
  preProcessedLabelImage = PreProcessLabelImage<UShortImageType,FloatImageType> (labelImage, anatomyLabel, binaryRadius);


  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

  laplacianSegmentation->SetInput( preProcessedLabelImage );
  laplacianSegmentation->SetFeatureImage( preProcessedFeatureImage );


  // Set the parameters for the label statistics filter
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  labelStatistics->Update();
  
  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;


  // Set the parameters for the Laplacian Segmentation
  laplacianSegmentation->UseImageSpacingOn();
  laplacianSegmentation->SetMaximumRMSError( 0.02 );
  laplacianSegmentation->SetNumberOfIterations( 400 );
  laplacianSegmentation->SetIsoSurfaceValue(127.5);
  laplacianSegmentation->SetPropagationScaling( propScaling );
  laplacianSegmentation->SetCurvatureScaling( curvScaling );

  if (Dimension == 2)
    {
    // Show the evolution of the level set
    // This must be done before the level set filter is updated
    itk::ShowLevelSetEvolution<LaplacianSegmentationLevelSetImageFilterType> levelSetEvolution(laplacianSegmentation, displayImageType);
    }

  if (Dimension == 3)
    {
    // Write out a model of the 3D image to file
    laplacianSegmentation->Update();
    WriteModelToFile<FloatImageType>(laplacianSegmentation->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputModel);
    }

  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << laplacianSegmentation->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << laplacianSegmentation->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << laplacianSegmentation->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << laplacianSegmentation->GetRMSChange() << std::endl;

  return true;
}
