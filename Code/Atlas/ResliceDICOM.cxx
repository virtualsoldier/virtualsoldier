// Description
// Read in a series of DICOM images, reorient them, and write them out
// as DICOM images.

#include "itkImageSeriesReader.h"
#include "itkImageSeriesWriter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkOrientImageFilter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkMetaDataObject.h"
#include "itkResampleImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"

#include <vector>
#include <string>

int main( int argc, char* argv[] )
{
  if( argc < 2 )
    {
    std::cerr << "Usage: " << argv[0] << 
      " DicomDirectory OutputDicomDirectoryAndPrefix" << std::endl;
    return 0;
    }

  typedef short PixelType;
  typedef itk::Image<PixelType,3>                 InputImageType;
  typedef itk::ImageSeriesReader< InputImageType >     ReaderType;
  typedef itk::GDCMImageIO                        ImageIOType;
  typedef itk::GDCMSeriesFileNames                SeriesFileNames;
  typedef itk::Image< PixelType,  2>              OutputImageType;
  typedef itk::ImageSeriesWriter< InputImageType, OutputImageType > SeriesWriterType;
  typedef itk::OrientImageFilter< InputImageType, InputImageType > OrientType;
  typedef itk::ResampleImageFilter< InputImageType, InputImageType> FeatureResampleType;
  typedef itk::LinearInterpolateImageFunction< InputImageType, double >  LinearInterpolatorType;

  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  SeriesFileNames::Pointer nameGenerator = SeriesFileNames::New();
  ReaderType::Pointer reader = ReaderType::New();
  OrientType::Pointer orienter = OrientType::New();
  SeriesWriterType::Pointer swriter = SeriesWriterType::New();
  FeatureResampleType::Pointer resampler = FeatureResampleType::New();
  LinearInterpolatorType::Pointer linearInterpolator = LinearInterpolatorType::New();


  // Get the DICOM filenames from the directory
  nameGenerator->SetInputDirectory( argv[1] );

  // Write out the input file names
  const ReaderType::FileNamesContainer & filenames = nameGenerator->GetInputFileNames();
  unsigned int numberOfFilenames =  filenames.size();
  std::cout << numberOfFilenames << std::endl; 
  for(unsigned int fni = 0; fni<numberOfFilenames; fni++)
    {
    std::cout << "filename # " << fni << " = ";
    std::cout << filenames[fni] << std::endl;
    }

  // Read the images into a volume.
  itk::SimpleFilterWatcher readerWatch(reader);
  reader->SetFileNames( filenames );
  reader->SetImageIO( gdcmIO );
  try
    {
    reader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while writing the image" << std::endl;
    std::cerr << excp << std::endl;
    }


  // Resample the image to have isotropic spacing by downsampling the
  // X and Y dimensions.
  InputImageType::SpacingType inputSpacing;
  inputSpacing[0] = reader->GetOutput()->GetSpacing()[0];
  inputSpacing[1] = reader->GetOutput()->GetSpacing()[1];
  inputSpacing[2] = reader->GetOutput()->GetSpacing()[2];

  InputImageType::SizeType inputSize;
  inputSize[0] = reader->GetOutput()->GetRequestedRegion().GetSize()[0];
  inputSize[1] = reader->GetOutput()->GetRequestedRegion().GetSize()[1];
  inputSize[2] = reader->GetOutput()->GetRequestedRegion().GetSize()[2];

  InputImageType::SpacingType outputSpacing;
  outputSpacing[0] = inputSpacing[2];
  outputSpacing[1] = inputSpacing[2];
  outputSpacing[2] = inputSpacing[2];

  // The output size should be the input size times the output spacing divided by the input z spacing
  InputImageType::SizeType outputSize;
//  outputSize[0] = (unsigned long int)(inputSize[0] * inputSpacing[0]/outputSpacing[0]);  // number of pixels along X
//  outputSize[1] = (unsigned long int)(inputSize[1] * inputSpacing[1]/outputSpacing[1]);  // number of pixels along Y
//  outputSize[2] = (unsigned long int)(inputSize[2] * inputSpacing[2]/outputSpacing[2]);  // number of pixels along Z
  // Make all of dimension sizes 256 for convenience.  The original
  // P198 isolated heart data has 237 slices and P184 has 195 slices.
  // Also, their z spacing is 0.5, which remains the same after the
  // resampling.  Therefore, 256 is greater than the number of slices.
  outputSize[0] = 256;
  outputSize[1] = 256;
  outputSize[2] = 256;

  resampler->SetInput( reader->GetOutput() );
  resampler->SetOutputSpacing( outputSpacing );
  resampler->SetOutputOrigin ( reader->GetOutput()->GetOrigin() );
  resampler->SetSize( outputSize );
  resampler->SetInterpolator( linearInterpolator );
  // Make sure that the padded values are the same as the background,
  // which is -2048 in this case.
  resampler->SetDefaultPixelValue( -2048 );

  
  try
    {
    itk::SimpleFilterWatcher resamplerWatch(resampler);
    resampler->Update();   
    }
  catch (itk::ExceptionObject & e)
    {
    std::cerr << "exception in file writer " << std::endl;
    std::cerr << e.GetDescription() << std::endl;
    std::cerr << e.GetLocation() << std::endl;
    }
  std::cout << "Original spacing: " << reader->GetOutput()->GetSpacing() << std::endl;
  std::cout << "Resampled spacing: " << resampler->GetOutput()->GetSpacing() << std::endl;


  // Reorient the volume
  orienter->SetInput( resampler->GetOutput() );   
  orienter->SetGivenCoordinateOrientation( itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RIA );
  orienter->SetDesiredCoordinateOrientation( itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAI );
  try
    {
    itk::SimpleFilterWatcher orienterWatch(orienter);
    orienter->Update();   
    }
  catch (itk::ExceptionObject & e)
    {
    std::cerr << "exception in file writer " << std::endl;
    std::cerr << e.GetDescription() << std::endl;
    std::cerr << e.GetLocation() << std::endl;
    }

  std::cout << "Original image size: " << reader->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Old image size: " << resampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "New image size: " << orienter->GetOutput()->GetRequestedRegion().GetSize() << std::endl;

  // Create new names for the output images.
  std::vector< std::string > outputNames( orienter->GetOutput()->GetRequestedRegion().GetSize()[2] );
  std::vector< std::string >::iterator outputNamesIt;
  int slice = 1;
  for( outputNamesIt = outputNames.begin(); outputNamesIt != outputNames.end(); ++outputNamesIt )
    {
    itk::OStringStream outputName;
    if ( slice < 10 )
      {
      outputName << argv[2] << "000" << slice << ".dcm";      
      }
    else if ( slice < 100 )
      {
      outputName << argv[2] << "00" << slice << ".dcm";      
      }
    else if ( slice < 1000 )
      {
      outputName << argv[2] << "0" << slice << ".dcm";      
      }
    else 
      {
      std::cout << "ERROR: Too many images" << std::endl;
      }

    std::cout << "outputName: " << outputName.str() << std::endl;

    *outputNamesIt = outputName.str();
    slice++;
    }

  std::cout << "output names size: " << outputNames.size() << std::endl;
  typedef itk::MetaDataDictionary                   DictionaryType;
  typedef itk::MetaDataDictionary *                 DictionaryRawPointer;
  typedef std::vector< DictionaryRawPointer >  DictionaryArrayType;
  typedef const DictionaryArrayType *          DictionaryArrayRawPointer;
  DictionaryArrayType metaDataDictArray;
  for(unsigned int k=1; k<=outputNames.size(); k++)
    {
    DictionaryRawPointer dict = new DictionaryType;
    metaDataDictArray.push_back(dict);
    std::string key = "0020|0013"; //0020 0013 IS 1 Image Number
    itksys_ios::ostringstream s;
    s << k;
    itk::EncapsulateMetaData<std::string>(*dict, key, s.str() );
    }

  // Write the images
//  nameGenerator->SetOutputDirectory( argv[2] );
  swriter->SetInput( orienter->GetOutput() );
  ImageIOType::Pointer gdcmIO2 = ImageIOType::New();
  swriter->SetImageIO( gdcmIO2 );
//  swriter->SetFileNames( nameGenerator->GetOutputFileNames() );
  swriter->SetFileNames( outputNames );
//  swriter->SetMetaDataDictionaryArray( reader->GetMetaDataDictionaryArray() );
  swriter->SetMetaDataDictionaryArray( &metaDataDictArray );

  try
    {
    swriter->Update();
    }
  catch (itk::ExceptionObject & e)
    {
    std::cerr << "exception in file writer " << std::endl;
    std::cerr << e.GetDescription() << std::endl;
    std::cerr << e.GetLocation() << std::endl;
    }

  return 0;
}

