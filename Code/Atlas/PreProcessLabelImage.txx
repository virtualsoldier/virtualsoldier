#ifndef _PreProcessLabelImage_txx
#define _PreProcessLabelImage_txx

#include "itkImage.h"
#include "itkResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkCastImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h"

template <class TInputImage, class TOutputImage>
typename TOutputImage::Pointer PreProcessLabelImage(TInputImage * labelImage, int anatomyLabel, float binaryRadius)
{
  itkStaticConstMacro(ImageDimension, unsigned int, TInputImage::ImageDimension);

  typedef itk::ResampleImageFilter< TInputImage, TInputImage> LabelResampleType;
  typedef itk::NearestNeighborInterpolateImageFunction< TInputImage, double >  NearestNeighborInterpolatorType;
  typedef itk::BinaryThresholdImageFilter< TInputImage, TInputImage> ThresholdingFilterType1;
  typedef itk::BinaryBallStructuringElement< unsigned short, ImageDimension> KernelType;
  typedef itk::BinaryErodeImageFilter< TInputImage, TInputImage, KernelType>
    ErodeFilterType;
  typedef itk::CastImageFilter< TInputImage, TOutputImage > LabelCastType;  

  LabelResampleType::Pointer labelResampler = LabelResampleType::New();
  NearestNeighborInterpolatorType::Pointer nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();
  ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();
  LabelCastType::Pointer castLabelToFloat = LabelCastType::New();

  if (ImageDimension == 2)
    {
    thresholder1->SetInput( labelImage );
    }
  else if (ImageDimension == 3)
    {
    labelResampler->SetInput ( labelImage );
    thresholder1->SetInput( labelResampler->GetOutput() );
    }
  erodeFilter->SetInput( thresholder1->GetOutput() );
  castLabelToFloat->SetInput( erodeFilter->GetOutput() );

  if (ImageDimension == 3)
    {
    // Resample the image to have isotropic spacing by downsampling the
    // X and Y dimensions.
    typename TInputImage::SpacingType inputSpacing;
    inputSpacing[0] = labelImage->GetSpacing()[0];
    inputSpacing[1] = labelImage->GetSpacing()[1];
    inputSpacing[2] = labelImage->GetSpacing()[2];

    typename TInputImage::SizeType inputSize;
    inputSize[0] = labelImage->GetRequestedRegion().GetSize()[0];
    inputSize[1] = labelImage->GetRequestedRegion().GetSize()[1];
    inputSize[2] = labelImage->GetRequestedRegion().GetSize()[2];

    typename TInputImage::SpacingType outputSpacing;
    outputSpacing[0] = 1; //inputSpacing[2];
    outputSpacing[1] = 1; //inputSpacing[2];
    outputSpacing[2] = 1; //inputSpacing[2];

    // The output size should be the input size times the output spacing divided by the input z spacing
    typename TInputImage::SizeType outputSize;
    outputSize[0] = (unsigned long int)(inputSize[0] * inputSpacing[0]/outputSpacing[0]);  // number of pixels along X
    outputSize[1] = (unsigned long int)(inputSize[1] * inputSpacing[1]/outputSpacing[1]);  // number of pixels along Y
    outputSize[2] = (unsigned long int)(inputSize[2] * inputSpacing[2]/outputSpacing[2]);  // number of pixels along Z

/*
  std::cout << "Input image size: " << inputSize << std::endl;
  std::cout << "Input spacing: " << inputSpacing << std::endl;
  std::cout << "Input origin: " << labelImage->GetOrigin() << std::endl;
  std::cout << "Output image size: " << outputSize << std::endl;
  std::cout << "Output spacing: " << outputSpacing << std::endl;
  std::cout << "Output origin: " << labelImage->GetOrigin() << std::endl;
*/

  
    // Label image resampled the same way
    labelResampler->SetOutputSpacing( outputSpacing );
    labelResampler->SetOutputOrigin ( labelImage->GetOrigin() );
    labelResampler->SetSize( outputSize );
    labelResampler->SetInterpolator( nearestNeighborInterpolator );

    // Update the resamplers
    try
      {
      labelResampler->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    std::cout << "In PreProcessLabelImage.txx: New label image spacing: " << labelResampler->GetOutput()->GetSpacing() << std::endl;
    std::cout << "In PreProcessLabelImage.txx: New label image origin: " << labelResampler->GetOutput()->GetOrigin() << std::endl;
    std::cout << "In PreProcessLabelImage.txx: New label image size: " << labelResampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
    }

  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);


  try
    {
    thresholder1->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }



  // Erode the initial level set (labeled images)
  // Create the structuring element
  KernelType ball;
  KernelType::SizeType ballSize;
  ballSize[0] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[0]));
  ballSize[1] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[1]));
  if (ImageDimension == 3)
    {
    ballSize[2] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[2]));
    }
  ball.SetRadius(ballSize);
  ball.CreateStructuringElement();
  
  std::cout << "In PreProcessLabelImage.txx: Ball size: " << ballSize << std::endl;
  
  // Connect the input image
  erodeFilter->SetKernel( ball );
  erodeFilter->SetErodeValue( 0 );
  try
    {
    erodeFilter->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  try
    {
    castLabelToFloat->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  

  return castLabelToFloat->GetOutput();
}

#endif
