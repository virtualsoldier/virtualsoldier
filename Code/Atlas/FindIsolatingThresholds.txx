#ifndef _FindIsolatingThresholds_txx
#define _FindIsolatingThresholds_txx

#include "FindIsolatingThresholds.h"
#include "FindStructureAttributes.h"

#include "itkCastImageFilter.h"
#include "itkOtsuMultipleThresholdsCalculator.h"
#include "itkHistogram.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkOrImageFilter.h"
#include "itkShiftScaleImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h"


#include <algorithm>

  // #include "itkScalarImageToListAdaptor.h"
  // #include "itkListSampleToHistogramGenerator.h"
  // #include <fstream>



namespace itk
{
template<class FeatureImageType,class LabelImageType,class InternalImageType>
FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::FindIsolatingThresholds( FeatureImageType * entireFeatureImage, LabelImageType * entireLabelImage, std::string structureName, std::string dataFilesDirectory )
{

  m_StructureName = structureName;

  // Default values
  m_FindUpperThreshold = true;
  m_UseAllSeeds = false;
  m_Lower = NumericTraits<InputImagePixelType>::NonpositiveMin();
  m_Upper = NumericTraits<InputImagePixelType>::max();
  
  m_EntireFeatureImage = entireFeatureImage;
  m_EntireLabelImage = entireLabelImage;

  m_DataFilesDirectory = dataFilesDirectory;  
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
bool FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::Compute()
{
  // Mappers
  if ( !StructureLabelMapper() )
    {
    std::cout << "In FindIsolatingThresholds.txx: ERROR: Could not create structure label mapping" << std::endl;
    return false;
    }

  m_StructureLabel = m_NameToLabelMapper[ m_StructureName ];
  m_ThresholdedLabelImage = ThresholdLabelImage(m_EntireLabelImage, m_StructureLabel);

  ClassifyStructures();
  
  // Map the structure name to the adjacent structures
  std::string adjacentLabelsFile = m_DataFilesDirectory + "AdjacentAnatomyLabels.csv";
  
  FindStructureAttributes< int > adjacentLabels( adjacentLabelsFile, m_StructureName );
  if ( adjacentLabels.GetParsingFailed() )
    {
    std::cout << "In FindIsolatingThresholds.txx: Adjacent Labels File Parsing Failed" << std::endl;
    return false;
    }

  std::vector< int > adjacentStructures = adjacentLabels.GetStructureAttributes();
  std::vector< int >::iterator adjacentIt;

  std::string structureClass = m_NameToClassMapper[m_StructureName];
  std::cout << m_StructureName << ": " << structureClass << std::endl;

  // Determine whether to find the upper or lower threshold.
  if (structureClass == "bright")
    {
    std::cout << "FindUpperThresholdOff()" << std::endl;
    FindUpperThresholdOff();
    }
  else
    {
    std::cout << "FindUpperThresholdOn()" << std::endl;
    FindUpperThresholdOn();
    }

  std::vector < int > allThresholds;
  int threshold;

  // Write the data to file.
  std::string findingThresholdOutput = m_DataFilesDirectory + "FindingThresholdOutput_" + m_StructureName + ".txt";
  std::ofstream fout( findingThresholdOutput.c_str() );
  if (!fout)
    { 
    std::cout << "In FindIsolatingThresholds.txx: Could not open FindingThresholdOutput file " << findingThresholdOutput << std::endl;
    }

  // Find the isolating thresholds based on adjacent classifications.
  std::string adjacentClass;
  for (adjacentIt = adjacentStructures.begin(); adjacentIt != adjacentStructures.end(); adjacentIt++)
    {
    // Only find the thresholds if the adjacent structure is not
    // "unknown", 0.
    if (*adjacentIt != 0 )
      {
      std::cout << "Adjacent label: " << *adjacentIt << "\t";
      std::cout << "Adjacent name: " << m_LabelToNameMapper[ *adjacentIt ] << "\t";
      adjacentClass = m_NameToClassMapper[ m_LabelToNameMapper[ *adjacentIt ] ];
      std::cout << "Adjacent class: " << adjacentClass << std::endl;
      
      fout << "Adjacent label: " << *adjacentIt << "\t";
      fout << "Adjacent name: " << m_LabelToNameMapper[ *adjacentIt ] << "\t";
      fout << "Adjacent class: " << adjacentClass << std::endl;
      
      // Reassign the AdjacentLabelImage to be the image of the new
      // adjacent label.
      m_AdjacentLabelImage = ThresholdLabelImage(m_EntireLabelImage, *adjacentIt);

      // If the intensity classes are the same, use isolated
      // connected.  
      if ( structureClass == adjacentClass )
        {
        std::cout << "In FindIsolatingThresholds.txx: Using IsolatedConnected" << std::endl;
        fout << "Using IsolatedConnected" << std::endl;
        if (FindIsolatedConnectedThreshold( threshold ) )
          {
          std::cout << "In FindIsolatingThresholds.txx: Isolated Connected Thresholding Succeeded" << std::endl;
          std::cout << "In FindIsolatingThresholds.txx: Threshold: " << threshold << std::endl;
          fout << "Isolated Connected Thresholding Succeeded" << std::endl;
          fout << "Threshold: " << threshold << std::endl;
          allThresholds.push_back( threshold );
          }
        else
          {
          std::cout << "In FindIsolatingThresholds.txx: Isolated Connected Thresholding Failed" << std::endl;
          fout << "Isolated Connected Thresholding Failed" << std::endl;
          }
        }
      // Otherwise, use Otsu.
      else if ( structureClass != adjacentClass )
        {
        std::cout << "In FindIsolatingThresholds.txx: Using Otsu" << std::endl;
        fout << "Using Otsu" << std::endl;
        if (FindOtsuThreshold( threshold ) )
          {
          std::cout << "In FindIsolatingThresholds.txx: Otsu Thresholding Succeeded" << std::endl;          
          std::cout << "In FindIsolatingThresholds.txx: Threshold: " << threshold << std::endl;
          fout << "Otsu Thresholding Succeeded" << std::endl;          
          fout << "Threshold: " << threshold << std::endl;
          allThresholds.push_back( threshold );
          }
        else
          {
          std::cout << "In FindIsolatingThresholds.txx: Otsu Thresholding Failed" << std::endl;          
          fout << "Otsu Thresholding Failed" << std::endl;          
          }
        }
      else
        {
        std::cout << "In FindIsolatingThresholds.txx: ERROR: Classification not defined" << std::endl;
        fout << "ERROR: Classification not defined" << std::endl;
        //return false;
        }
      
      std::cout << "---------------------------------------" << std::endl;
      fout << "---------------------------------------" << std::endl;
      }
    else
      { 
      std::cout << "Adjacent label: 0. Ignored" << std::endl;
      }
    }


  std::vector < int >::iterator thresholdIt;
  for (thresholdIt = allThresholds.begin(); thresholdIt != allThresholds.end(); thresholdIt++)
    {
    std::cout << "Before sorting: Thresholds: " << *thresholdIt << std::endl;
    }

  // Sort the vector of thresholds.
  std::sort(allThresholds.begin(),allThresholds.end());

  for (thresholdIt = allThresholds.begin(); thresholdIt != allThresholds.end(); thresholdIt++)
    {
    std::cout << "After sorting: Thresholds: " << *thresholdIt << std::endl;
    }

  
  // Find a reasonable value for the threshold not found by the above method.
  float mult = 1.5;
  typedef itk::LabelStatisticsImageFilter< FeatureImageType, LabelImageType > LabelStatisticsFilterType;
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  labelStatistics->SetInput( m_EntireFeatureImage );
  labelStatistics->SetLabelInput( m_EntireLabelImage );
  labelStatistics->Update();
  
  float labelMin = labelStatistics->GetMinimum(m_StructureLabel);
  float labelMax = labelStatistics->GetMaximum(m_StructureLabel);
  float labelMean = labelStatistics->GetMean(m_StructureLabel);
  float labelSigma = labelStatistics->GetSigma(m_StructureLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;
  fout << "\tMinimum = " << labelMin  << std::endl;
  fout << "\tMaximum = " << labelMax << std::endl;
  fout << "\tMean = " << labelMean << std::endl;
  fout << "\tSigma = " << labelSigma << std::endl;


  if (m_FindUpperThreshold)
    {
    m_Lower = labelMean - (mult*labelSigma);
    if (! allThresholds.empty() ) // If no isolating thresholds were found
      { m_Upper = allThresholds[(int)(allThresholds.size()/2)]; }
    else
      { m_Upper = labelMean + (mult*labelSigma); }
    }
  else
    {
    if (! allThresholds.empty() ) // If no isolating thresholds were found
      { m_Lower = allThresholds[(int)(allThresholds.size()/2)]; }
    else
      { m_Lower = labelMean - (mult*labelSigma); }
    m_Upper = labelMean + (mult*labelSigma);
    }

  fout << "Lower: " << m_Lower << std::endl;
  fout << "Upper: " << m_Upper << std::endl;
  fout.close();    
  return true;

}


template<class FeatureImageType,class LabelImageType,class InternalImageType>
bool FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::ClassifyStructures()
{  
  int lowerThreshold;
  int upperThreshold;
  int otsuThreshold;
  otsuThreshold = CalculateOtsuThreshold( m_EntireFeatureImage, m_EntireLabelImage, lowerThreshold, upperThreshold );
  std::cout << "In FindIsolatingThresholds.txx: All structures Otsu Threshold: " << otsuThreshold << std::endl;

  // Read in the means of each structure that were created using
  // CalculateLabelStatistics2D.
  StringToFloatMapType nameToMeanMapper;

  std::string statisticsFile = m_DataFilesDirectory + "AnatomyStatistics.csv";

  FindStructureAttributes< float > structures( statisticsFile );
  if ( structures.GetParsingFailed() )
    {
    std::cout << "In FindIsolatingThresholds.txx: Statistics File Parsing Failed" << std::endl;
    return false;
    }

  std::vector< std::string > allStructureNames = structures.GetAllStructureNames();
  std::vector< float > structureAttributes = structures.GetStructureAttributes();
  std::vector< std::vector < float > > allStructureAttributes = structures.GetAllStructureAttributes();

  std::vector< std::string >::iterator allNamesIt = allStructureNames.begin();
  std::vector< std::vector< float > >::iterator allAttributesIt = allStructureAttributes.begin();
  std::string name;
  float mean;
  while ( allNamesIt != allStructureNames.end() )
    {
    name = *allNamesIt;
    mean = (*allAttributesIt)[0];
    nameToMeanMapper.insert(StringToFloatMapType::value_type( name, mean));
    
    allNamesIt++;
    allAttributesIt++;
    }

  // Compare the means of each label to the otsuThreshold.  If the
  // mean is less than the threshold, the structure will be called
  // "dark".  Otherwise, it will be called "bright".
  StringToFloatMapType::iterator itMean;
  itMean = nameToMeanMapper.begin(); 
  while ( itMean!=nameToMeanMapper.end() )
    {
    if (itMean->second < otsuThreshold)
      {
      m_NameToClassMapper.insert(StringToStringMapType::value_type(itMean->first, "dark") );
      }
    else 
      {
      m_NameToClassMapper.insert(StringToStringMapType::value_type(itMean->first, "bright") );
      }
    ++itMean;
    }
  

  // Look at the output
  for (itMean = nameToMeanMapper.begin(); itMean != nameToMeanMapper.end(); itMean++)
    {
    std::cout << "In FindIsolatingThresholds.txx: " << itMean->first << ":\t" << itMean->second << std::endl;
    }

  // Look at the output
  StringToStringMapType::iterator itClass;
  for (itClass = m_NameToClassMapper.begin(); itClass != m_NameToClassMapper.end(); itClass++)
    {
    std::cout << "In FindIsolatingThresholds.txx: " << itClass->first << ":\t" << itClass->second << std::endl;
    }

  return true;
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
bool FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::FindIsolatedConnectedThreshold( int & threshold )
{
  // Set up the IsolatedConnectedImageFilter
  isolatedConnected = IsolatedConnectedType::New();
  isolatedConnected->SetInput( m_EntireFeatureImage );

  // The first seeds for the isolatedConnectedImageFilter will be the
  // eroded label map of the structure of interest.  The second seeds
  // will be the eroded label map of the adjacent structures.

  // Using all the seeds
  if (m_UseAllSeeds)
    {
    int binaryRadius = 3;

    typedef itk::BinaryBallStructuringElement< unsigned short, ImageDimension> KernelType;
    typedef itk::BinaryErodeImageFilter< LabelImageType, LabelImageType, KernelType>
      ErodeFilterType;

    // Erode the initial level set (labeled images)
    // Create the structuring element
    KernelType ball;
    KernelType::SizeType ballSize;
    ballSize[0] = (long unsigned int)(ceil(binaryRadius / m_ThresholdedLabelImage->GetSpacing()[0]));
    ballSize[1] = (long unsigned int)(ceil(binaryRadius / m_ThresholdedLabelImage->GetSpacing()[1]));
    if (ImageDimension == 3)
      {
      ballSize[2] = (long unsigned int)(ceil(binaryRadius / m_ThresholdedLabelImage->GetSpacing()[2]));
      }
    ball.SetRadius(ballSize);
    ball.CreateStructuringElement();
  
    std::cout << "In FindIsolatingThresholds.txx: Ball size: " << ballSize << std::endl;
  
    // Connect the input image
    ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();
    erodeFilter->SetInput( m_ThresholdedLabelImage );
    erodeFilter->SetKernel( ball );
    erodeFilter->SetErodeValue( 0 );
    erodeFilter->Update();    
    
    ErodeFilterType::Pointer erodeAdjacentFilter = ErodeFilterType::New();
    erodeAdjacentFilter->SetInput( m_AdjacentLabelImage );
    erodeAdjacentFilter->SetKernel( ball );
    erodeAdjacentFilter->SetErodeValue( 0 );
    erodeAdjacentFilter->Update();    

    typedef itk::ImageRegionIteratorWithIndex < LabelImageType > IteratorType;
    IteratorType labelImageIt( erodeFilter->GetOutput(), erodeFilter->GetOutput()->GetRequestedRegion() );
  
    typedef itk::ImageRegionIteratorWithIndex < LabelImageType > IteratorType2;
    IteratorType2 labelImageIt2( erodeAdjacentFilter->GetOutput(), erodeAdjacentFilter->GetOutput()->GetRequestedRegion() );

    int numberOfSeeds1 = 0;
    for( labelImageIt.GoToBegin(); !labelImageIt.IsAtEnd(); ++labelImageIt)
      {
      if (labelImageIt.Get() == 0)
        {
        isolatedConnected->AddSeed1( labelImageIt.GetIndex() );
        numberOfSeeds1++;
        }
      }
    std::cout << "Number of seeds1: " << numberOfSeeds1 << std::endl;

    int numberOfSeeds2 = 0;
    for( labelImageIt2.GoToBegin(); !labelImageIt2.IsAtEnd(); ++labelImageIt2)
      {
      if (labelImageIt2.Get() == 0)
        {
        isolatedConnected->AddSeed2( labelImageIt2.GetIndex() );
        numberOfSeeds2++;
        }
      }
    std::cout << "Number of seeds2: " << numberOfSeeds2 << std::endl;

    if (numberOfSeeds1 == 0 || numberOfSeeds2 == 0)
      {
      std::cout << "In FindIsolatingThresholds.txx: ERROR: Structure not present" << std::endl;
      return false;
      }

    }
  // Using the centroids of the seeds
  else
    {
    typedef itk::ImageRegionIteratorWithIndex < LabelImageType > IteratorType;
    IteratorType labelImageIt( m_ThresholdedLabelImage, m_ThresholdedLabelImage->GetRequestedRegion() );
  
    typedef itk::ImageRegionIteratorWithIndex < LabelImageType > IteratorType2;
    IteratorType2 labelImageIt2( m_AdjacentLabelImage, m_AdjacentLabelImage->GetRequestedRegion() );

    int numberOfSeeds1 = 0;
    int centroidX = 0;
    int centroidY = 0;
    int centroidZ;
    if (ImageDimension == 3) { centroidZ = 0; }

    for( labelImageIt.GoToBegin(); !labelImageIt.IsAtEnd(); ++labelImageIt)
      {
      if (labelImageIt.Get() == 0)
        {
//      std::cout << "Index: " << labelImageIt.GetIndex() << std::endl;
        centroidX += labelImageIt.GetIndex()[0];
        centroidY += labelImageIt.GetIndex()[1];
        if (ImageDimension == 3) { centroidZ += labelImageIt.GetIndex()[2]; }
        numberOfSeeds1++;
        }
      }

//    std::cout << "Number of seeds1: " << numberOfSeeds1 << std::endl;

    if (numberOfSeeds1 == 0)
      {
      std::cout << "In FindIsolatingThresholds.txx: ERROR: Structure not present" << std::endl;
      return false;
      }
  
    centroidX = int(centroidX/numberOfSeeds1);
    centroidY = int(centroidY/numberOfSeeds1);

    std::cout << "In FindIsolatingThresholds.txx: Seed1 CentroidX: " << centroidX << std::endl;
    std::cout << "In FindIsolatingThresholds.txx: Seed1 CentroidY: " << centroidY << std::endl;

    if (ImageDimension == 3)
      {
      centroidZ = int(centroidZ/numberOfSeeds1);
      std::cout << "In FindIsolatingThresholds.txx: Seed1 CentroidZ: " << centroidZ << std::endl;
      }
    
    typename InternalImageType::IndexType  indexSeed1;
    indexSeed1[0] = centroidX;
    indexSeed1[1] = centroidY;
    if (ImageDimension == 3) 
      { 
      indexSeed1[2] = centroidZ; 
      }


    // Now find the centroid of the second seed
    int numberOfSeeds2 = 0;
    centroidX = 0;
    centroidY = 0;
    centroidZ;
    if (ImageDimension == 3) { centroidZ = 0; }

    for( labelImageIt2.GoToBegin(); !labelImageIt2.IsAtEnd(); ++labelImageIt2)
      {
      if (labelImageIt2.Get() == 0)
        {
//      std::cout << "Index: " << labelImageIt2.GetIndex() << std::endl;
        centroidX += labelImageIt2.GetIndex()[0];
        centroidY += labelImageIt2.GetIndex()[1];
        if (ImageDimension == 3) { centroidZ += labelImageIt2.GetIndex()[2]; }
        numberOfSeeds2++;
        }
      }

//     std::cout << "Number of seeds2: " << numberOfSeeds2 << std::endl;

    if (numberOfSeeds2 == 0)
      {
      std::cout << "In FindIsolatingThresholds.txx: ERROR: Structure not present" << std::endl;
      return false;
      }


  
    centroidX = int(centroidX/numberOfSeeds2);
    centroidY = int(centroidY/numberOfSeeds2);

    std::cout << "In FindIsolatingThresholds.txx: Seed2 CentroidX: " << centroidX << std::endl;
    std::cout << "In FindIsolatingThresholds.txx: Seed2 CentroidY: " << centroidY << std::endl;

    if (ImageDimension == 3)
      {
      centroidZ = int(centroidZ/numberOfSeeds2);
      std::cout << "In FindIsolatingThresholds.txx: Seed2 CentroidZ: " << centroidZ << std::endl;
      }

    typename InternalImageType::IndexType  indexSeed2;
    indexSeed2[0] = centroidX;
    indexSeed2[1] = centroidY;
    if (ImageDimension == 3) 
      { 
      indexSeed2[2] = centroidZ; 
      }

    isolatedConnected->AddSeed1( indexSeed1 ); 
    isolatedConnected->AddSeed2( indexSeed2 );
    }


  typedef itk::MinimumMaximumImageCalculator < FeatureImageType > MinMaxCalculatorType;
  MinMaxCalculatorType::Pointer minMaxCalculator = MinMaxCalculatorType::New();
  minMaxCalculator->SetImage( m_EntireFeatureImage );
  minMaxCalculator->Compute();
  int imageMin = minMaxCalculator->GetMinimum();
  int imageMax = minMaxCalculator->GetMaximum();


//   std::cout << "Image min: " << imageMin << std::endl;
//   std::cout << "Image max: " << imageMax << std::endl;

  isolatedConnected->SetLower( imageMin );
  isolatedConnected->SetUpper( imageMax );
  if (m_FindUpperThreshold)
    { isolatedConnected->FindUpperThresholdOn(); }
  else
    { isolatedConnected->FindUpperThresholdOff(); }
  isolatedConnected->SetReplaceValue( 255 );

  try
    {
    isolatedConnected->Update();   
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  if( isolatedConnected->GetThresholdingFailed() )
    { 
//    std::cout << "Thresholding Failed" << std::endl;
    return false;
    }
  else
    { 
    //std::cout << "Thresholding Succeeded" << std::endl; 
    }

//   if (m_FindUpperThreshold)
//     {
//     m_Lower = isolatedConnected->GetLower();
//     m_Upper = isolatedConnected->GetIsolatedValue();
//     }
//   else
//     {
//     m_Lower = isolatedConnected->GetIsolatedValue();
//     m_Upper = isolatedConnected->GetUpper();
//     }

  threshold = isolatedConnected->GetIsolatedValue();

  return true;
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
bool FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::FindOtsuThreshold( int & otsuThreshold )
{
  // Since the masks are defined to have "0" in the structure and
  // 255 elsewhere but the MaskImageFilter masks areas that are "0",
  // we want to invert the image intensities.
  typedef itk::ShiftScaleImageFilter < LabelImageType, LabelImageType > ShiftScaleType;
  ShiftScaleType::Pointer labelShifter = ShiftScaleType::New();
  labelShifter->SetInput( m_ThresholdedLabelImage );
  labelShifter->SetShift( -255 );
  labelShifter->SetScale( -1 );
  ShiftScaleType::Pointer adjacentLabelShifter = ShiftScaleType::New();
  adjacentLabelShifter->SetInput( m_AdjacentLabelImage );
  adjacentLabelShifter->SetShift( -255 );
  adjacentLabelShifter->SetScale( -1 );

  // We need to OR the two masks together
  typedef itk::OrImageFilter< LabelImageType, LabelImageType, LabelImageType > OrFilterType;
  OrFilterType::Pointer orer = OrFilterType::New();
  orer->SetInput1( labelShifter->GetOutput() );
  orer->SetInput2( adjacentLabelShifter->GetOutput() );

  try
    {
    orer->Update();
    }
  catch(itk::ExceptionObject & exp)
    {
    std::cerr << exp << std::endl;
    }

  // Use the mask to mask the FeatureImage so that the result is
  // non-zero only where the structures are present.
  typedef itk::MaskImageFilter< FeatureImageType, LabelImageType, FeatureImageType> MaskFilterType;
  MaskFilterType::Pointer masker = MaskFilterType::New();
  masker->SetInput1( m_EntireFeatureImage );
  masker->SetInput2( orer->GetOutput() );
    
  try
    {
    masker->Update();   
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


//   if (ImageDimension == 2)
//     {
//     itk::FileDumper<LabelImageType> dumpLabelShifter(labelShifter->GetOutput(), "c:/TEMP/labelShifter");
//     itk::FileDumper<LabelImageType> dumpOrer(orer->GetOutput(), "c:/TEMP/Orer");
//     itk::FileDumper<FeatureImageType> dumpMasker(masker->GetOutput(), "c:/TEMP/Masker");
//     }

  int lowerThreshold;
  int upperThreshold;
  otsuThreshold = CalculateOtsuThreshold(masker->GetOutput(), orer->GetOutput(), lowerThreshold, upperThreshold );
  
//   m_Lower = lowerThreshold;
//   m_Upper = upperThreshold;
  
  return true;
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
int FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::CalculateOtsuThreshold( FeatureImageType * featureImage, LabelImageType * labelImage, int & lowerThreshold, int & upperThreshold )
{
  typedef itk::MinimumMaximumImageCalculator < FeatureImageType > MinMaxCalculatorType;
  MinMaxCalculatorType::Pointer minMaxCalculator = MinMaxCalculatorType::New();
  minMaxCalculator->SetImage( featureImage );
  minMaxCalculator->Compute();
  int maskMin = minMaxCalculator->GetMinimum();
  int maskMax = minMaxCalculator->GetMaximum();
  int range = maskMax - maskMin;
  std::cout << "In FindIsolatingThresholds.txx: maskMin: " << maskMin << std::endl;
  std::cout << "In FindIsolatingThresholds.txx: maskMax: " << maskMax << std::endl;
  std::cout << "In FindIsolatingThresholds.txx: Range: " << range << std::endl;
   
  // Create a histogram of the masked intensities
  typedef int HistogramMeasurementType;
  typedef itk::Statistics::Histogram< HistogramMeasurementType, 1 > HistogramType ;
  HistogramType::Pointer histogram = HistogramType::New();

  // initialize histogram
  HistogramType::SizeType size;
  size.Fill( range ) ;
  HistogramType::MeasurementVectorType lowerBound ;
  HistogramType::MeasurementVectorType upperBound ;
  lowerBound.Fill(maskMin);
  upperBound.Fill(maskMax+1);
  
  histogram->Initialize(size, lowerBound, upperBound ) ;
  histogram->SetToZero();

    
//   HistogramType::IndexType index; 
  HistogramType::MeasurementVectorType measurements ;
//   HistogramType::InstanceIdentifier id;

  typedef itk::ImageRegionConstIterator < FeatureImageType > ConstFeatureIteratorType;
  ConstFeatureIteratorType featureIterator( featureImage, featureImage->GetBufferedRegion() );
  typedef itk::ImageRegionConstIterator < LabelImageType > ConstLabelIteratorType;
  ConstLabelIteratorType labelIterator( labelImage, labelImage->GetBufferedRegion() );
  featureIterator.GoToBegin();
  labelIterator.GoToBegin();

  // Add to the histogram only those feature pixels that are masked by
  // non-zero masks (the zero mask is, by convention, background).
  while ( !featureIterator.IsAtEnd() )
    {
    if ( labelIterator.Get() != 0 )
      {
      measurements.Fill( featureIterator.Get() );
      if ( !histogram->IncreaseFrequency( measurements, 1 ) )
        {
        std::cout << "measurements: " << measurements << std::endl;
        std::cout << "Index out of bounds" << std::endl;
        }
      }
    ++featureIterator;
    ++labelIterator;
    }

//   // Look at the output values
//   std::string otsuHistogramFile = m_DataFilesDirectory + "OtsuHistogram.csv";
// //  std::cout << "In FindIsolatingThresholds.txx: Otsu histogram file: " << otsuHistogramFile << std::endl;
//   std::ofstream fout( otsuHistogramFile.c_str() );
//   if (!fout)
//     { 
//     std::cout << "In FindIsolatingThresholds.txx: Could not open Otsu Histogram file " << otsuHistogramFile << std::endl;
//     }

//   HistogramType::Iterator iter = histogram->Begin();
//   while ( iter != histogram->End() )
//     {
//     //      std::cout << "Measurement vectors = " << iter.GetMeasurementVector()
//     //                << " frequency = " << iter.GetFrequency() << std::endl;
//     fout << iter.GetMeasurementVector() << ",";
//     fout << iter.GetFrequency() << std::endl;

//     ++iter;
//     }
//   fout.close();

//   std::cout << "Size = " << histogram->Size() << std::endl;
//   std::cout << "Total frequency = " << histogram->GetTotalFrequency() << std::endl;
    
    
  typedef itk::OtsuMultipleThresholdsCalculator<HistogramType>  OtsuMultipleThresholdCalculatorType;
  OtsuMultipleThresholdCalculatorType::Pointer otsuThresholdCalculator = OtsuMultipleThresholdCalculatorType::New();

  otsuThresholdCalculator->SetInputHistogram( histogram.GetPointer() );
  otsuThresholdCalculator->SetNumberOfThresholds( 1 );

  try
    {
    otsuThresholdCalculator->Update();
    }
  catch(itk::ExceptionObject & exp)
    {
    std::cerr << exp << std::endl;
    }

  OtsuMultipleThresholdCalculatorType::OutputType otsuThresholds = otsuThresholdCalculator->GetOutput();
  
//   // Look at the threshold values
//   for (unsigned int i = 0; i < otsuThresholds.size(); i++)
//     {
//     std::cout << "Threshold[" << i << "]: " << otsuThresholds[i] << std::endl;
//     }
    
  if (m_FindUpperThreshold)
    {
    lowerThreshold = maskMin;
    upperThreshold = otsuThresholds[0];
    }      
  else
    {
    lowerThreshold = otsuThresholds[0];
    upperThreshold = maskMax;
    }

  return otsuThresholds[0];
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
bool FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::StructureLabelMapper()
{
  std::string masterAnatomyFile = m_DataFilesDirectory + "MasterAnatomy.csv";

  FindStructureAttributes< int > masterAnatomy( masterAnatomyFile );
  if ( masterAnatomy.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;
    return false;
    }

  std::vector< std::string > allStructureNames = masterAnatomy.GetAllStructureNames();
  std::vector< std::vector< int > > allStructureLabels = masterAnatomy.GetAllStructureAttributes();

  std::vector< std::string >::iterator allNamesIt = allStructureNames.begin();
  std::vector< std::vector< int > >::iterator allLabelsIt = allStructureLabels.begin();

  std::string name;
  int label;
  while ( allNamesIt != allStructureNames.end() )
    {
    name = *allNamesIt;
    label = (*allLabelsIt)[0];
    m_LabelToNameMapper.insert(IntToStringMapType::value_type( label, name ));    
    m_NameToLabelMapper.insert(StringToIntMapType::value_type( name, label ));
    allNamesIt++;
    allLabelsIt++;
    }

  StringToIntMapType::iterator nameToLabelMapperIt;
  for ( nameToLabelMapperIt = m_NameToLabelMapper.begin(); nameToLabelMapperIt != m_NameToLabelMapper.end(); ++nameToLabelMapperIt )
    {
    std::cout << "In FindIsolatingThresholds.txx: structure name: " << nameToLabelMapperIt->first << "\t"
              << "label value: " << nameToLabelMapperIt->second << std::endl;
    }
  
  return true;
}

template<class FeatureImageType,class LabelImageType,class InternalImageType>
typename LabelImageType::Pointer FindIsolatingThresholds<FeatureImageType, LabelImageType, InternalImageType>
::ThresholdLabelImage( LabelImageType * entireLabelImage, int threshold)
{
  typedef itk::BinaryThresholdImageFilter< LabelImageType, LabelImageType> ThresholdingFilterType;
  ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

  thresholder->SetInput( entireLabelImage );
  thresholder->SetInsideValue (0);
  thresholder->SetOutsideValue (255);
  thresholder->SetLowerThreshold( threshold );
  thresholder->SetUpperThreshold( threshold );
  try
    {
    thresholder->Update();      
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  return thresholder->GetOutput();
}



} // end namespace itk

#endif
