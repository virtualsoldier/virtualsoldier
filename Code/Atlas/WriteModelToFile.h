/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: WriteModelToFile.h,v $
  Language:  C++
  Date:      $Date: 2005/03/16 17:11:37 $
  Version:   $Revision: 1.5 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

#ifndef _WriteModelToFile_h
#define _WriteModelToFile_h


// itk Classes
#include "itkImage.h"
#include "itkVTKImageExport.h"

// vtk Classes
#include "vtkImageChangeInformation.h"
#include "vtkImageFlip.h"
#include "vtkImageImport.h"
#include "vtkMarchingCubes.h"
#include "vtkDiscreteMarchingCubes.h"
#include "vtkPolyDataWriter.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkCleanPolyData.h"
#include "vtkDecimatePro.h"
#include "vtkPolyDataNormals.h"

// vtk / itk classes
#include "vtk2itk2vtk.h"

// C++ classes
#include <vector>
#include <string>

template <class TInputImage>
bool WriteModelToFile(TInputImage * displayImage, std::vector <int> bbValues, int * inputImageSize, double * inputImageSpacing, char * outputModel, std::string marchingCubesType="");

#ifndef VSP_MANUAL_INSTANTIATION
#include "WriteModelToFile.txx"
#endif


#endif
