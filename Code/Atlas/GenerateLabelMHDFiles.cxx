/*=========================================================================

This program reads both feature and raw label images, and generates
.mhd files for the label files from the headers of the feature images.

If the first file in the feature image directory and the first file in
the label image directory correspond, imageNumberingOffset should be
set to zero.  However, if there are more feature images than labeled
images, imageNumberingOffset should be set to the difference between
the first feature image and the feature image that lines up with the
first label image.

Inputs: 
Outputs:

=========================================================================*/


#include "itkImage.h"
#include "itkDICOMSeriesFileNames.h"
#include "itkArchetypeSeriesFileNames.h"

#include <itksys/SystemTools.hxx>

#include "itkDICOMImageIO2.h"
#include "itkDICOMImageIO2Factory.h"

#include "itkImageIOBase.h"

#include <string>
#include <vector>

int main( int argc, char *argv[] )
{
  if (argc < 4)
    {
    std::cout << "Usage: "
              << " DicomFileName"
              << " ExampleLabelFileName"
              << " ImageNumberingOffset"
              << std::endl;
    return 0;
    }  

  for (int i = 0; i < argc; i++)
    {
    std::cout << "Argument[" << i << "]: " << argv[i] << std::endl;
    }

  // Save all arguments as local variables
  char * exampleDicomFileName = argv[1];
  char * exampleLabelFileName = argv[2];
  int imageNumberingOffset = atoi( argv[3] );

  std::string dicomDirectory = itksys::SystemTools::GetFilenamePath( exampleDicomFileName );

  // Given a directory name, get all of the filenames.
  typedef itk::DICOMSeriesFileNames DICOMNameGeneratorType;
  DICOMNameGeneratorType::Pointer dicomFileGenerator = DICOMNameGeneratorType::New();  
  dicomFileGenerator->SetDirectory( dicomDirectory );
  dicomFileGenerator->SetFileNameSortingOrderToSortByImageNumber();

  typedef itk::ArchetypeSeriesFileNames ArchetypeNameGeneratorType;
  ArchetypeNameGeneratorType::Pointer labelFileGenerator = ArchetypeNameGeneratorType::New();
  labelFileGenerator->SetArchetype( exampleLabelFileName );

  // Copy the label filenames into a vector of strings
  std::vector <std::string > labelImageNames;
  labelImageNames = labelFileGenerator->GetFileNames( );

  // Copy the dicom filenames into a vector of strings.
  std::vector <std::string > dicomImageNames;
  dicomImageNames = dicomFileGenerator->GetFileNames( );

  std::cout << "vector size: " << dicomImageNames.size() << std::endl;
  std::cout << "dicomImageNames[0]: " << dicomImageNames[0] << std::endl;
  std::cout << "labelImageNames[0]: " << labelImageNames[0] << std::endl;
  std::cout << "labelImageNames[last]: " << labelImageNames[labelImageNames.size() - 1] << std::endl;

  // Loop through each dicom filename, extract the information
  // relevant to the .mhd files, and write out the .mhd files.
  itk::DICOMImageIO2::Pointer dicomIO = itk::DICOMImageIO2::New();
  std::ofstream fout;
  
  std::string dicomFileName;
  std::string labelFileName;
  std::string labelDirectory;
  std::string labelName;
  std::vector < std::string > labelNames;
  std::string labelMHDFileName;
  std::string byteOrder;
  std::string elementType;

  labelNames.clear();

  std::vector <std::string>::iterator dicomNameIt = dicomImageNames.begin() + imageNumberingOffset; 
  std::vector <std::string>::iterator labelNameIt = labelImageNames.begin();

  while ( labelNameIt != labelImageNames.end() )
    {
    std::cout << "Slice number: " << dicomNameIt - dicomImageNames.begin() + 1 << std::endl;

    dicomFileName = *dicomNameIt;
    labelFileName = *labelNameIt;

    // The label name is assumed to have no extension since it is a
    // raw file.
    labelDirectory = itksys::SystemTools::GetFilenamePath( labelFileName );
    labelName = itksys::SystemTools::GetFilenameName( labelFileName );
    labelNames.push_back( labelName );
    labelMHDFileName = labelDirectory + "/" + labelName + ".mhd";
//     std::cout << "label directory: " << labelDirectory << std::endl;
//     std::cout << "label name: " << labelName << std::endl;
//     std::cout << "label MHD file name: " << labelMHDFileName << std::endl;

    bool isDicomFile = dicomIO->CanReadFile( dicomFileName.c_str() );
    if (!isDicomFile)
      {
      std::cout << "ERROR: Original image is not of type dicom." << std::endl;
      return 0;
      }


    dicomIO->SetFileName( dicomFileName.c_str() );
    dicomIO->ReadImageInformation();

//    std::cout << "Image size: " << dicomIO->GetDimensions(0) << std::endl;
//    std::cout << "Byte order: " << dicomIO->GetByteOrder() << std::endl;
//    std::cout << "Byte order as string: " << dicomIO->GetByteOrderAsString (dicomIO->GetByteOrder() ) << std::endl;

//    std::cout << "Pixel type: " << dicomIO->GetPixelType() << std::endl;
//    std::cout << "IO component type: " << dicomIO->GetComponentType() << std::endl;
//    std::string componentType = dicomIO->GetComponentTypeAsString( dicomIO->GetComponentType() );
//    std::cout << "IO component type as string: " << componentType << std::endl;
//    std::cout << "Origin: " << dicomIO->GetOrigin(0) << "," << dicomIO->GetOrigin(1) << "," << dicomIO->GetOrigin(2) << std::endl;
    
    if ( dicomIO->GetByteOrder() == itk::ImageIOBase::LittleEndian )
      {
      byteOrder = "false";
      }

    // What if the byteorder is not applicable?
    switch ( dicomIO->GetByteOrder() )
      {
      case itk::ImageIOBase::BigEndian :
        byteOrder = "true";
        break;
      case itk::ImageIOBase::LittleEndian :
        byteOrder = "false";
        break;
      case itk::ImageIOBase::OrderNotApplicable :
        byteOrder = "orderNotApplicable";
        break;
      }
    

    switch( dicomIO->GetComponentType() )
      {
      case itk::ImageIOBase::UCHAR:
        elementType = "MET_UCHAR";
        break;
      case itk::ImageIOBase::CHAR:
        elementType = "MET_CHAR";
        break;
      case itk::ImageIOBase::USHORT:
        elementType = "MET_USHORT";
        break;
      case itk::ImageIOBase::SHORT:
        elementType = "MET_SHORT";
        break;
      case itk::ImageIOBase::UINT:
        elementType = "MET_UINT";
        break;
      case itk::ImageIOBase::INT:
        elementType = "MET_INT";
        break;
      case itk::ImageIOBase::ULONG:
        elementType = "MET_ULONG";
        break;
      case itk::ImageIOBase::LONG:
        elementType = "MET_LONG";
        break;
      case itk::ImageIOBase::FLOAT:
        elementType = "MET_FLOAT";
        break;
      case itk::ImageIOBase::DOUBLE:
        elementType = "MET_DOUBLE";
        break;
      case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
      default:
        elementType = "unknown";
        break;
      }

//    std::cout << "Byte Order: " << byteOrder << std::endl;
//    std::cout << "Element type: " << elementType << std::endl;

    // Write out a .mhd file for each raw label file.
    fout.open( labelMHDFileName.c_str() );
    fout << "ObjectType = Image" << std::endl;
    fout << "NDims = 2"  << std::endl;
    fout << "DimSize = " << dicomIO->GetDimensions(0) << " " << dicomIO->GetDimensions(1) << std::endl;
    fout << "ElementSpacing = " << dicomIO->GetSpacing(0) << " " << dicomIO->GetSpacing(1) << std::endl;
    fout << "Position = " << dicomIO->GetOrigin(0) << " " << dicomIO->GetOrigin(1) << std::endl;
    fout << "ElementByteOrderMSB = " << byteOrder.c_str() << std::endl;
    fout << "ElementType = " << elementType.c_str() << std::endl;
    fout << "HeaderSize = -1" << std::endl;
    fout << "ElementDataFile = " << labelName << std::endl;
    fout.close();

    dicomNameIt++;
    labelNameIt++;
    }

  // Write out a .mhd file to read the raw label files as a volume.
  // Reread the first image to get the patient position corresponding
  // to the first slice.
  dicomIO->SetFileName( dicomImageNames[0].c_str() );
  dicomIO->ReadImageInformation();

  std::string labelVolumeMHDFileName;
  labelVolumeMHDFileName = labelDirectory + "/" + "Volume" + ".mhd";
  fout.open( labelVolumeMHDFileName.c_str() );
  fout << "ObjectType = Image" << std::endl;
  fout << "NDims = 3"  << std::endl;
  fout << "DimSize = " 
       << dicomIO->GetDimensions(0) << " " 
       << dicomIO->GetDimensions(1) << " "
       << labelNames.size() << std::endl;
  fout << "ElementSpacing = " 
       << dicomIO->GetSpacing(0) << " " 
       << dicomIO->GetSpacing(1) << " " 
       << dicomIO->GetSpacing(2) << std::endl;
  fout << "Position = "
       << dicomIO->GetOrigin(0) << " "
       << dicomIO->GetOrigin(1) << " "
       << dicomIO->GetOrigin(2) << std::endl;
  fout << "ElementByteOrderMSB = " << byteOrder.c_str() << std::endl;
  fout << "ElementType = " << elementType.c_str() << std::endl;
  fout << "HeaderSize = -1" << std::endl;
  fout << "ElementDataFile = LIST" << std::endl;
  std::vector < std::string >::iterator labelNamesIt;
  for (labelNamesIt = labelNames.begin(); labelNamesIt != labelNames.end(); labelNamesIt++)
    {
    fout << *labelNamesIt << std::endl;
    }
  fout.close();

  return 1;
  
}
