#ifndef _ParseIMLFile_cxx
#define _ParseIMLFile_cxx

#include "ParseIMLFile.h"

ParseIMLFile::ParseIMLFile( std::string inputFile)
{
  // Set all of the variables.
  m_AllStructNames.clear();
  m_AllPointPairs.clear();
  m_ParsingFailed = true;;

  if( ParseLine( inputFile ) )
    {
    m_ParsingFailed = false;
    }
}

bool ParseIMLFile::ParseLine( std::string inputFile)
{
  std::string line;

  if ( !TestFile( inputFile, line ) )
    {
    std::cout << "In ParseIMLFile::ParseLine. Cannot open file." << std::endl;
    return false;
    }

  int regionStartIndex = 0;
  int regionEndIndex = 0;
  int nextRegionOffsetIndex = 0;
  // Loop through the entire line, extracting out regions.
  while( 1 )
    {
    regionStartIndex = line.find("<Region",nextRegionOffsetIndex);
    //      std::cout << "start: " << regionStartIndex << std::endl;
    regionEndIndex = line.find("</Region>",nextRegionOffsetIndex);
    //      std::cout << "end: " << regionEndIndex << std::endl;
    if( regionStartIndex == -1 || regionEndIndex == -1 )
      {
      break;
      }
    nextRegionOffsetIndex = regionEndIndex+1;
    std::string regionSubStr = line.substr(regionStartIndex, regionEndIndex-(regionStartIndex));
    //      std::cout << regionSubStr << std::endl;
    ParseRegion( regionSubStr ); 
    }

return 0;
}

bool ParseIMLFile::TestFile( std::string inputFile, std::string & line )
{
  // Test the input file
  std::ifstream fin;
  fin.open(inputFile.c_str());

  if (!fin)
    {
    std::cout << "In ParseIMLFile::TestFile.  Could not open input file: " << inputFile << std::endl;
    return false;
    }

  // Remove the header
  char headerLine[255];
  fin.getline(headerLine,255);

  // Read in the rest of the file, which is stored in one line.
  char entireLine[65536];
  fin.getline(entireLine,65536);

  line=std::string(entireLine); 

  if ( line.empty() )
    {
    std::cout << "In ParseIMLFile::TestFile.  File empty: " << inputFile << std::endl;
    return false;
    }

  return true;
}


bool ParseIMLFile::ParseRegion( std::string regionSubStr )
{
  int tagNameLength = 7;
  int nameStartIndex = regionSubStr.find("<label>",0)+tagNameLength;
  int nameEndIndex = regionSubStr.find("</label>",0);
  std::string structName = regionSubStr.substr(nameStartIndex, nameEndIndex - nameStartIndex);
//  std::cout << structName << std::endl;

  m_AllStructNames.push_back( structName );

  int pointsStartIndex = 0;
  int pointsEndIndex = 0;
  int nextPointsOffsetIndex = regionSubStr.find("<outline>",0); 

//  std::cout << regionSubStr << std::endl;

  std::vector< int > pointPair(2);
  std::vector< std::vector< int > > pointPairs;
  pointPairs.clear();

// nextPointsOffsetIndex < (int) regionSubStr.length()
  while( 1 )
     {
     pointsStartIndex = regionSubStr.find("<pt",nextPointsOffsetIndex);
//     std::cout << "pointsStartIndex: " << pointsStartIndex << std::endl;
     pointsEndIndex = regionSubStr.find("/>",nextPointsOffsetIndex);
//     std::cout << "pointsEndIndex: " << pointsEndIndex << std::endl;
     if( pointsStartIndex == -1 || pointsEndIndex == -1)
       {
       break;
       }

     nextPointsOffsetIndex = pointsEndIndex+1;
     std::string pointsSubStr = regionSubStr.substr(pointsStartIndex, pointsEndIndex-pointsStartIndex);
//     std::cout << "pointsSubStr: " << pointsSubStr << std::endl;
     ParsePoints( pointsSubStr, pointPair );
     pointPairs.push_back( pointPair );
//     std::cout << "pointPair: " << pointPair[0] << "\t" << pointPair[1] << std::endl;
     }

  m_AllPointPairs.push_back( pointPairs );
  
  return true;
}

bool ParseIMLFile::ParsePoints( std::string pointsSubStr, std::vector< int > & pointPair )
{
  int pointsSubStrLength = pointsSubStr.length();
  int pointStartIndexX = pointsSubStr.find('"',0)+1;
  int pointEndIndexX = pointsSubStr.find('"',pointStartIndexX);
  pointPair[0] = atoi( pointsSubStr.substr( pointStartIndexX,pointEndIndexX-pointStartIndexX).c_str());
//  std::cout << "x: " << x << std::endl;

  int pointStartIndexY = pointsSubStr.find('"',pointEndIndexX+1)+1;
  int pointEndIndexY = pointsSubStr.find('"',pointStartIndexY);
  pointPair[1] = atoi( pointsSubStr.substr( pointStartIndexY, pointEndIndexY - pointStartIndexY ).c_str() );
//  std::cout << "y: " << y << std::endl;

  return true;
}

#endif
