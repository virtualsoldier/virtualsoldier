/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    $RCSfile: RawToPNG.cxx,v $
  Language:  C++
  Date:      $Date: 2004/04/05 14:28:00 $
  Version:   $Revision: 1.1 $

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "itkImage.h"
#include "itkRawImageIO.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkExceptionObject.h"


int main( int argc, char ** argv )
{
  // Verify the number of parameters in the command line
  if( argc < 3 )
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile  outputImageFile " << std::endl;
    return -1;
    }

  typedef itk::Image< unsigned short, 2 >       ImageType;
  typedef itk::ImageFileReader< ImageType >  ReaderType;
  typedef itk::ImageFileWriter< ImageType >  WriterType;

  itk::RawImageIO<unsigned short,2>::Pointer io;
  io = itk::RawImageIO<unsigned short,2>::New();\
  unsigned int dim[2] = {512,512};
  for(unsigned int i=0; i<2; i++)
    {
    io->SetDimensions(i,dim[i]);
    io->SetSpacing(i,1.0);
    io->SetOrigin(i,0.0);
    }

  io->SetHeaderSize (3416);
  io->SetByteOrderToBigEndian ();

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();

  const char * inputFilename  = argv[1];
  const char * outputFilename = argv[2];

  reader->SetImageIO (io);
  reader->SetFileName( inputFilename  );
  writer->SetFileName( outputFilename );

  ImageType::Pointer image = reader->GetOutput();
  writer->SetInput( image );
  try
    {
    reader->Update();
    writer->Update();
    }
  catch (itk::ExceptionObject &ex)
    {
    std::cout << ex << std::endl;
    int foo;
    return 1;
    }
  return 0;
}



