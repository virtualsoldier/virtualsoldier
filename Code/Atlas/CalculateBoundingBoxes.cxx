/*=========================================================================

This program creates bounding boxes for each structure and writes them
out to file.  It initializes the lower bound of the bound bounding box in
each dimension to be the max index in that dimension, and it
initializes the upper bound to be 0.  Then, as it finds labels of a
given structure, it uses the index of that label to modify the lower
and upper bound of the structure's bounding box.

Inputs: Labelled image, image information file, master anatomy file. 
Outputs: Bounding boxes file.

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkArray.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkImageRegionIteratorWithIndex.h"

// C++ classes
#include <fstream>
#include <iostream>
#include <string>
#include <map>

#include "FindStructureAttributes.h"

int main( int argc, char *argv[] )
{
  if (argc != 4)
    {
    std::cout << "Usage: "
              << "labelImageName " 
              << "masterAnatomyFile "
              << "boundingBoxesFile " 
              << std::endl;
    return 0;
    }  

  for (int i = 0; i < argc; i++)
    {
    std::cout << "Argument[" << i << "]: " << argv[i] << std::endl;
    }

  // Save all arguments as local variables
  char * labelImageName = argv[1];
  char * masterAnatomyFile = argv[2];
  char * boundingBoxesFile = argv[3];
  
  // Test the master anatomy file
  std::ifstream anatomyFile(masterAnatomyFile);
  if (!anatomyFile) 
    {
    std::cout << "In FindAnatomyLabelFromName.cxx: Could not open master anatomy file: " << anatomyFile << std::endl;
    return false;
    }

  // Test the bounding boxes file
  std::ofstream fout ( boundingBoxesFile );
  if (!fout) 
    {
    std::cout << "\tCould not open bounding box output file" << std::endl;
    return false;
    }

  typedef   unsigned short   InputPixelType;
  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::ArchetypeSeriesFileNames NameGeneratorType;
 
  ReaderType::Pointer reader = ReaderType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();

  // Generate the image file names
  inputImageFileGenerator->SetArchetype( labelImageName );
  std::cout << inputImageFileGenerator->GetFileNames()[0] << std::endl;
  std::vector <std::string > inputImageNames = inputImageFileGenerator->GetFileNames();

  // Set the slice offset to be the four digit number of the first image.
  std::string firstImageName = itksys::SystemTools::GetFilenameWithoutExtension( inputImageNames[0] );
  std::string firstImageNumber = firstImageName.substr( firstImageName.size()-4,4 ); 
  std::cout << "In FindImageInfo.txx: first image number: " << firstImageNumber << std::endl;
  int sliceOffset = atoi( firstImageNumber.c_str() );
  std::cout << "In FindImageInfo.txx: Slice offset: " << sliceOffset << std::endl;

  // Get the image information
  int imageSize[3];
  reader->SetFileName( inputImageNames[0].c_str() );
  reader->Update();
  imageSize[0] = reader->GetOutput()->GetBufferedRegion().GetSize()[0];
  imageSize[1] = reader->GetOutput()->GetBufferedRegion().GetSize()[1];
  imageSize[2] = inputImageNames.size(); 

  std::cout << "ImageSize: " << imageSize[0] << "\t" << imageSize[1] << "\t" << imageSize[2] << std::endl;


  // The following populates the mapper, which will contain a mapping 
  // from the anatomy name to its label.
  typedef std::map< std::string, InputPixelType > StringToInputPixelTypeMapType;
  StringToInputPixelTypeMapType nameToLabelMapper;
  StringToInputPixelTypeMapType::iterator nameToLabelMapperIt;

  // This method reads all of the structure attribute information from file.
  FindStructureAttributes< InputPixelType > structures(masterAnatomyFile);
  if( structures.GetParsingFailed() )
    {
    std::cout << "Parsing Failed" << std::endl;    
    return 0;
    }
  std::vector< std::string > allStructureNames = structures.GetAllStructureNames();
  std::vector< std::vector < InputPixelType > > allStructureAttributes = structures.GetAllStructureAttributes();

  // Copy the information from structures to the nameToLabelMapper.
  std::vector< std::string >::iterator nameIt = allStructureNames.begin();;
  std::vector< std::vector< InputPixelType > >::iterator allAttributesIt = allStructureAttributes.begin();
  int structureLabel;
  while( nameIt != allStructureNames.end() )
    {
    structureLabel = (*allAttributesIt)[0];
    nameToLabelMapper[ *nameIt ] = structureLabel;
    nameIt++;
    allAttributesIt++;
    }

  // Write out mapper to see the list
  for( nameToLabelMapperIt=nameToLabelMapper.begin(); nameToLabelMapperIt!=nameToLabelMapper.end(); nameToLabelMapperIt++ )
    {
    std::cout << nameToLabelMapperIt->first << ": " << nameToLabelMapperIt->second << std::endl;    
    }

  // The rows of allBoundingBoxes correspond to the different anatomies and
  // the columns correspond to the min and max indices of the bounding box for that
  // particular anatomy.
  // The order of the indices is as follows: [minX maxX minY maxY minZ maxZ]
  // These correspond to: [minJ maxJ minI maxI minSlice maxSlice]
  // This array only accepts up to 1024 anatomies.
  typedef std::map< InputPixelType, std::vector < int > > InputPixelTypeToVectorOfIntMapType;
  InputPixelTypeToVectorOfIntMapType allBoundingBoxes;
  std::vector < int > boundingBoxes;

  // Initialize the array
  // Set the value of the min indices to the size of the corresponding dimension.
  // Set the max indices to 0.
  // This ensures that a new index will be either less than the min index or
  // greater than the max index (both for the first pixelValue).
  for( nameToLabelMapperIt=nameToLabelMapper.begin(); nameToLabelMapperIt!=nameToLabelMapper.end(); nameToLabelMapperIt++ )
    {
    boundingBoxes.clear();
    boundingBoxes.push_back( imageSize[0]-1 );
    boundingBoxes.push_back( 0 );
    boundingBoxes.push_back( imageSize[1]-1 );
    boundingBoxes.push_back( 0 );
    boundingBoxes.push_back( imageSize[2]-1 );
    boundingBoxes.push_back( 0 );

    allBoundingBoxes[ nameToLabelMapperIt->second ] = boundingBoxes;
    }
  
  InputPixelType inputPixelValue;
  InputImageType::IndexType inputImageIndex;
  std::vector <std::string> ::iterator iinit = inputImageNames.begin();
  int slice;

  // Loop through each anatomy file
  while (iinit != inputImageNames.end())
    {
    slice = iinit - inputImageNames.begin();
    std::cout << "Slice: " << slice << std::endl;

    // Read the image
    reader->SetFileName ( (*iinit).c_str() );
      
    try 
      { 
      reader->Update();
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 
      
    itk::ImageRegionIteratorWithIndex  < InputImageType > it (reader->GetOutput(),reader->GetOutput()->GetRequestedRegion());

    it.GoToBegin();
    while (!it.IsAtEnd())
      {
      inputPixelValue = it.Get();
	  
      // Update the bounding box array
      // This is the x,y,z location of the pixel
      inputImageIndex = it.GetIndex();

      // Test the minimum x index
      if (inputImageIndex[0] < allBoundingBoxes[inputPixelValue][0]) {
      allBoundingBoxes[inputPixelValue][0] = inputImageIndex[0]; }
	    
      // Test the maximum x index
      if (inputImageIndex[0] > allBoundingBoxes[inputPixelValue][1]) {
      allBoundingBoxes[inputPixelValue][1] = inputImageIndex[0]; }

      // Test the minimum y index
      if (inputImageIndex[1] < allBoundingBoxes[inputPixelValue][2]) {
      allBoundingBoxes[inputPixelValue][2] = inputImageIndex[1]; }

      // Test the maximum y index
      if (inputImageIndex[1] > allBoundingBoxes[inputPixelValue][3]) {
      allBoundingBoxes[inputPixelValue][3] = inputImageIndex[1]; }

      // Test the minimum z index
      if (slice < allBoundingBoxes[inputPixelValue][4]) {
      allBoundingBoxes[inputPixelValue][4] = slice; }

      // Test the maximum z index
      if (slice > allBoundingBoxes[inputPixelValue][5]) {
      allBoundingBoxes[inputPixelValue][5] = slice; }

      // increment the iterators
      ++it;
      }

    // increment the input image iterator
    ++iinit;
    }
  

  // Write out the bounding boxes file
  fout << "Anatomy,minX,maxX,minY,maxY,minZ,maxZ,minSlice,maxSlice" << std::endl;
  for( nameToLabelMapperIt=nameToLabelMapper.begin(); nameToLabelMapperIt!=nameToLabelMapper.end(); nameToLabelMapperIt++ )
    {
    fout << nameToLabelMapperIt->first
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][0]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][1]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][2]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][3]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][4]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][5]
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][4]+sliceOffset
          << "," << allBoundingBoxes[nameToLabelMapperIt->second][5]+sliceOffset
          << std::endl;
    }
  fout.close();
   
  return 0;
  
}
