#include "FindImageInfo.h"

#include "itkImage.h"
#include "itkDICOMSeriesFileNames.h"
#include "itkDICOMImageIO2.h"
#include "itkDICOMImageIO2Factory.h"

#include <itksys/SystemTools.hxx>


bool FindImageInfo( char * fileName, int & sliceOffset, int * imageSize, double * imageSpacing )
{
  // Test whether the input file is a DICOM file
  itk::DICOMImageIO2::Pointer dicomIO = itk::DICOMImageIO2::New();
  bool isDicomFile = dicomIO->CanReadFile( fileName );

  if (!isDicomFile)
    {
    std::cout << "In FindImageInfo.cxx: ERROR: Image is not DICOM" << std::endl;
    return false;
    }
  else
    {
//    sliceOffset = 1; // default since slices start from number 1

    dicomIO->SetFileName( fileName );
    dicomIO->ReadImageInformation();

    imageSpacing[0] = dicomIO->GetSpacing(0);
    imageSpacing[1] = dicomIO->GetSpacing(1);
    imageSpacing[2] = dicomIO->GetSpacing(2);

    imageSize[0] = dicomIO->GetDimensions(0);
    imageSize[1] = dicomIO->GetDimensions(1);

    // Find the size of the vector of filenames in order to know the imageSize[3]
    std::vector <std::string > allInputImageNames;
    typedef itk::DICOMSeriesFileNames DICOMNameGeneratorType;
    DICOMNameGeneratorType::Pointer inputImageFileGenerator = DICOMNameGeneratorType::New();  
    std::string fileNamePath = itksys::SystemTools::GetFilenamePath( fileName );
    inputImageFileGenerator->SetDirectory( fileNamePath );      
    inputImageFileGenerator->SetFileNameSortingOrderToSortByImageNumber();
      
    allInputImageNames = inputImageFileGenerator->GetFileNames();
    std::cout << "File name start: " << allInputImageNames[0] << std::endl;
    std::cout << "File name end: " << allInputImageNames[allInputImageNames.size()-1] << std::endl;

    imageSize[2] = allInputImageNames.size();

//     itk::DICOMImageIO2::Pointer firstDicomIO = itk::DICOMImageIO2::New();
//     firstDicomIO->SetFileName( allInputImageNames[0] );
//     firstDicomIO->ReadImageInformation();
//     sliceOffset = firstDicomIO->GetOrigin(2);
//     std::cout << "Slice offset: " << sliceOffset << std::endl;

    // Set the slice offset to be the four digit number at the first image.
    std::string firstImageName = itksys::SystemTools::GetFilenameWithoutExtension( allInputImageNames[0] );
    std::string firstImageNumber = firstImageName.substr( firstImageName.size()-4,4 ); 
    std::cout << "In FindImageInfo.txx: first image number: " << firstImageNumber << std::endl;
    sliceOffset = atoi( firstImageNumber.c_str() );
    std::cout << "In FindImageInfo.txx: Slice offset: " << sliceOffset << std::endl;

    }

    std::cout << "In FindImageInfo.cxx: imageSize = " 
              << imageSize[0] << " " 
              << imageSize[1] << " " 
              << imageSize[2] << std::endl;
    std::cout << "In FindImageInfo.cxx: imageSpacing = " 
              << imageSpacing[0] << " " 
              << imageSpacing[1] << " " 
              << imageSpacing[2] << std::endl;
    std::cout << "In FindImageInfo.cxx: Position = " 
              << dicomIO->GetOrigin(0) << " " 
              << dicomIO->GetOrigin(1) << std::endl;

  return true;
}
