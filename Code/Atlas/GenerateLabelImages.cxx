#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkImageSeriesWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkErodeObjectMorphologyImageFilter.h"
#include "itkDilateObjectMorphologyImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkMinimumMaximumImageCalculator.h"

#include "itkConnectedComponentImageFilter.h"
#include "itkRelabelComponentImageFilter.h"


#include <itksys/SystemTools.hxx>

//#include <vxl/vcl/vcl_cmath.h>

// Atlas classes
#include "FindImageInfo.h"
#include "CreateVolumeFromSlices.h"
#include "PreProcessLabelImage.h"
#include "WriteModelToFile.h"

template < const int ImageDimension >
bool ProcessImages(char * featureImagePrefix, char * labelImagePrefix, char * imageInfoFile, int lowerThreshold, int upperThreshold, float binaryRadius);


int main( int argc, char *argv[] )
{
  int numberOfParameters = 8;

  if( argc < numberOfParameters )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix ";
    std::cerr << " LabelImagePrefix ";
    std::cerr << " ImageInfoFile";
    std::cerr << " LowerThreshold";
    std::cerr << " UpperThreshold";
    std::cerr << " BinaryErodeRadius";
    std::cerr << " Dimension";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  int lowerThreshold = atoi( argv[4] );
  int upperThreshold = atoi( argv[5] );
  float binaryRadius = atof( argv[6] );
  int dimension = atoi( argv[7] );

  if (dimension == 2)
    {
    ProcessImages< 2 > (featureImagePrefix, labelImagePrefix, imageInfoFile, lowerThreshold, upperThreshold, binaryRadius);
    }
  else if (dimension == 3)
    {
    ProcessImages< 3 > (featureImagePrefix, labelImagePrefix, imageInfoFile, lowerThreshold, upperThreshold, binaryRadius);
    }
  return 0;
}

template < const int ImageDimension >
bool ProcessImages(char * featureImagePrefix, char * labelImagePrefix, char * imageInfoFile, int lowerThreshold, int upperThreshold, float binaryRadius)
{
  typedef short FeaturePixelType;
  typedef unsigned short LabelPixelType;
  typedef float InternalPixelType;

  typedef itk::Image< FeaturePixelType, ImageDimension > FeatureImageType;
  typedef itk::Image< LabelPixelType, ImageDimension > LabelImageType;
  typedef itk::Image< InternalPixelType, ImageDimension > InternalImageType;


  // Find the image information
  int sliceOffset;
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindImageInfo(featureImagePrefix, sliceOffset, inputImageSize, inputImageSpacing) )
    { 
    std::cout << "Cannot find image information. " << std::endl;
    return false;
    }

  FeatureImageType::Pointer entireFeatureImage = FeatureImageType::New();
  std::vector < int > entirebbValues(8);
  entirebbValues[0] = 0;
  entirebbValues[1] = inputImageSize[0]-1 ;
  entirebbValues[2] = 0 ;
  entirebbValues[3] = inputImageSize[1]-1 ;
  entirebbValues[4] = 0 ;
  entirebbValues[5] = inputImageSize[2]-1 ;
  entirebbValues[6] = 0+sliceOffset ;
  entirebbValues[7] = inputImageSize[2]-1+sliceOffset;

  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating feature volume from feature image slices" << std::endl;
  itk::CreateVolumeFromSlices<FeatureImageType>( featureImagePrefix, entirebbValues, inputImageSpacing, entireFeatureImage);


  typedef itk::BinaryThresholdImageFilter< FeatureImageType, LabelImageType> ThresholdType;
  typedef itk::BinaryBallStructuringElement< unsigned short, ImageDimension> KernelType;
//  typedef itk::BinaryErodeImageFilter< LabelImageType, LabelImageType, KernelType> ErodeFilterType;
  typedef itk::ErodeObjectMorphologyImageFilter< LabelImageType, LabelImageType, KernelType> ErodeFilterType;
//  typedef itk::BinaryDilateImageFilter< LabelImageType, LabelImageType, KernelType> DilateFilterType;
  typedef itk::DilateObjectMorphologyImageFilter< LabelImageType, LabelImageType, KernelType> DilateFilterType;

  ThresholdType::Pointer thresholder1 = ThresholdType::New();
  ErodeFilterType::Pointer eroder = ErodeFilterType::New();
  DilateFilterType::Pointer dilater = DilateFilterType::New();

  // Set up the pipeline
  thresholder1->SetInput( entireFeatureImage );
//  dilater->SetInput( thresholder1->GetOutput() );
//  eroder->SetInput( dilater->GetOutput() );
  eroder->SetInput( thresholder1->GetOutput() );
  dilater->SetInput( eroder->GetOutput() );


//   // Calculate the min and max of the image
//   typedef itk::MinimumMaximumImageCalculator < FeatureImageType > MinMaxCalculatorType;
//   MinMaxCalculatorType::Pointer minMaxCalculator = MinMaxCalculatorType::New();
//   minMaxCalculator->SetImage( entireFeatureImage );
//   minMaxCalculator->Compute();
//   int imageMin = minMaxCalculator->GetMinimum();
//   int imageMax = minMaxCalculator->GetMaximum();

  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue( 255 );
  thresholder1->SetOutsideValue( 0 );
  thresholder1->SetLowerThreshold( lowerThreshold );
  thresholder1->SetUpperThreshold( upperThreshold );
  thresholder1->ReleaseDataFlagOn();

  try
    {
    thresholder1->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Delete the input image
  entireFeatureImage = 0;

  // Create the structuring element
  KernelType ball;
  KernelType::SizeType ballSize;
  ballSize[0] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[0]));
  ballSize[1] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[1]));
  if (ImageDimension == 3)
    {
    ballSize[2] = (long unsigned int)(ceil(binaryRadius / thresholder1->GetOutput()->GetSpacing()[2]));
    }
  ball.SetRadius(ballSize);
  ball.CreateStructuringElement();
  
  // Dilate the image
  dilater->SetKernel( ball );
  dilater->SetObjectValue( 255 );
  dilater->ReleaseDataFlagOn();

  // Erode the image
  eroder->SetKernel( ball );
  eroder->SetObjectValue( 255 );
  eroder->ReleaseDataFlagOn();

  try
    {
    dilater->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }




  if (ImageDimension == 2)
    {
    typedef itk::Image< unsigned char, 2 > WriteImageType;

    typedef itk::CastImageFilter< LabelImageType, WriteImageType > CasterType;
    CasterType::Pointer caster = CasterType::New();
    caster->SetInput( dilater->GetOutput() );

    typedef itk::ImageFileWriter<WriteImageType> WriterType; 
    WriterType::Pointer writer = WriterType::New();
    writer->SetInput( caster->GetOutput() );
    writer->SetFileName( labelImagePrefix );
    
    try
      {
      writer->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    }

  else if (ImageDimension == 3)
    {

    std::string labelImagePath = itksys::SystemTools::GetFilenamePath( labelImagePrefix );
    std::string labelImageName = itksys::SystemTools::GetFilenameWithoutExtension( labelImagePrefix );
    labelImageName.erase(labelImageName.length()-4);

    std::cout << "Label image path: " << labelImagePath << std::endl;
    std::cout << "Label image name: " << labelImageName << std::endl;

    char format[4096];
    sprintf (format, "%s/%s%%04d.%s", labelImagePath.c_str(), labelImageName.c_str(), "png");
    std::cout << "Format = " << format << std::endl;

    itk::NumericSeriesFileNames::Pointer fit = itk::NumericSeriesFileNames::New();
    fit->SetStartIndex(sliceOffset);
    fit->SetEndIndex(sliceOffset+inputImageSize[2]-1);
    fit->SetIncrementIndex(1);
    fit->SetSeriesFormat (format);


    typedef itk::Image< unsigned char, 2 > WriteImageType;
    typedef itk::Image< unsigned char, ImageDimension > WriteImageType2;

    typedef itk::CastImageFilter< LabelImageType, WriteImageType2 > CasterType;
    CasterType::Pointer caster = CasterType::New();
    caster->SetInput( dilater->GetOutput() );
    caster->ReleaseDataFlagOn();

    typedef itk::ImageSeriesWriter<WriteImageType2,WriteImageType> WriterType; 
    WriterType::Pointer writer = WriterType::New();
    writer->SetInput( caster->GetOutput() );
    writer->SetFileNames( fit->GetFileNames() );

     typedef itk::BinaryThresholdImageFilter< LabelImageType, LabelImageType> LabelLabelThresholdType;
//     std::cout << "Thresholder2. " << std::endl;
//     LabelLabelThresholdType::Pointer thresholder2 = LabelLabelThresholdType::New();
//     thresholder2->SetInput( thresholder1->GetOutput() );
//     thresholder2->SetLowerThreshold( 255 );
//     thresholder2->SetUpperThreshold( 255 );
//     thresholder2->SetInsideValue( -1 );
//     thresholder2->SetOutsideValue( 1 );
//     thresholder2->Update();
//     thresholder2->ReleaseDataFlagOn();

//     // Write the model out to file
//     std::string labelModel = "c:/TEMP/labelModel.vtk";
//     WriteModelToFile<LabelImageType>(thresholder2->GetOutput(),entirebbValues,inputImageSize,inputImageSpacing,labelModel,"DiscreteSmoothed");

    std::cout << "Run connected components." << std::endl;
    typedef itk::ConnectedComponentImageFilter< LabelImageType, LabelImageType > ConnectedComponentType;
    ConnectedComponentType::Pointer connectedComponent = ConnectedComponentType::New();
    connectedComponent->SetInput( dilater->GetOutput() );
//    connectedComponent->ReleaseDataFlagOn();

    try
      {
      connectedComponent->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    std::cout << "Relabel components." << std::endl;
    typedef itk::RelabelComponentImageFilter< LabelImageType, LabelImageType > RelabelComponentType;
    RelabelComponentType::Pointer relabelComponent = RelabelComponentType::New();
    relabelComponent->SetInput( connectedComponent->GetOutput() );
//    relabelComponent->ReleaseDataFlagOn();
    try
      {
      relabelComponent->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }



    std::cout << "Thresholder3." << std::endl;
    LabelLabelThresholdType::Pointer thresholder3 = LabelLabelThresholdType::New();
    thresholder3->SetInput( relabelComponent->GetOutput() );
    thresholder3->SetLowerThreshold( 1 );
    thresholder3->SetUpperThreshold( 1 );
//     thresholder3->SetInsideValue (-1);
//     thresholder3->SetOutsideValue (1);
    thresholder3->SetInsideValue( 255 );
    thresholder3->SetOutsideValue( 1 );
//    thresholder3->ReleaseDataFlagOn();
    try
      {
      thresholder3->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }


//     // Write the model out to file
//     std::string connectedLabelModel = "c:/TEMP/ConnectedInitialLevelSetModel.vtk";
//     WriteModelToFile<LabelImageType>(thresholder3->GetOutput(),entirebbValues,inputImageSize,inputImageSpacing,connectedLabelModel,"DiscreteSmoothed");

    // TESTING
    caster->SetInput( thresholder3->GetOutput() );

    try
      {
      writer->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    }

  return true;

}

