/*=========================================================================

This program takes a 3D volume and prints it out to file as slices.
It works just like DumpImages but was written before we started using
that program.

Inputs: Image file name, bounding boxes values, 3D volume
Outputs: 2D slices of the volume.

=========================================================================*/

#ifndef _WriteSlicesFromVolume_h
#define _WriteSlicesFromVolume_h

#include <vector>
#include <string>

bool WriteSlicesFromVolume(char * fileName, std::vector <int> bbValues, ::itk::Image< unsigned short, 3 >::Pointer image3D);

#endif




