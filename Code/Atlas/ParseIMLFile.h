/*=========================================================================


Inputs:
Outputs:

=========================================================================*/

#ifndef _ParseIMLFile_h
#define _ParseIMLFile_h

#include <string>
#include <vector>
#include <fstream>
#include <iostream>


class ParseIMLFile
{
public:
  ParseIMLFile( std::string inputFile);
  ~ParseIMLFile() {};
  
  bool TestFile( std::string inputFile, std::string & line);
  bool ParseLine( std::string inputFile);
  bool ParseRegion( std::string regionSubStr );
  bool ParsePoints( std::string pointsSubStr, std::vector< int > & pointPair );

  // Accessor methods
  std::vector < std::string > GetAllStructNames() const { return m_AllStructNames; }
  std::vector< std::vector< std::vector < int > > > GetAllPointPairs() const { return m_AllPointPairs; }
  bool GetParsingFailed() const { return m_ParsingFailed; }

protected:


private:
  ParseIMLFile(); // Purposely not implemented.

  std::vector < std::string > m_AllStructNames;
  std::vector< std::vector< std::vector < int > > > m_AllPointPairs;
  bool m_ParsingFailed;

};

#ifndef VSP_MANUAL_INSTANTIATION
#include "ParseIMLFile.cxx"
#endif


#endif
