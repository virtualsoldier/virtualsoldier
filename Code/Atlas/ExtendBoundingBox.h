/*=========================================================================

This program simply extends the bounding box values by a given border
size.  The extension is constrained so that it does not extend past
the edge of the image in any dimension.

Inputs: Feature image prefix, bounding box vector of values, border
size, slice offset, and number of slices.
Outputs: Modified bounding box vector of values

=========================================================================*/


#ifndef _ExtendBoundingBox_h
#define _ExtendBoundingBox_h

#include <vector>
#include <string>

bool ExtendBoundingBox (std::vector <int> & bbValues, int borderSize, int sliceOffset, int * inputImageSize);

#endif



