#define ITK_LEAN_AND_MEAN

#include "itkImage.h"
#include "itkThresholdSegmentationLevelSetImageFilter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkExceptionObject.h"
#include "itkImageRegionConstIterator.h"

#include "itkConnectedComponentImageFilter.h"
#include "itkRelabelComponentImageFilter.h"

#include "itkSimpleFilterWatcher.h"
#include "itkShiftScaleImageFilter.h"

#include <itksys/SystemTools.hxx>

#include "itkBinaryThresholdImageFilter.h"
#include "itkBinaryMedianImageFilter.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"
#include "PreProcessFeatureImage.h"
#include "PreProcessLabelImage.h"
#include "WriteSlicesFromVolume.h"
#include "WriteModelToFile.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"

template < const int Dimension >
bool SegmentStructure(char * featureImageName, char * initialLabelImageName, char * finalLabelImageName, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float lowerThreshold, float upperThreshold, float propScaling, float curvScaling, float binaryRadius, float edgeWeight, int iterations, float conductance, int levelSetIterations, float rmsError, int borderSize, std::string displayImageType, std::string evolutionImages);



int main( int argc, char *argv[] )
{
  int numberOfParameters = 21;

  if( argc < numberOfParameters )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLabelImageName";
    std::cerr << " FinalLabelImageName";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " LowerThreshold";
    std::cerr << " UpperThreshold";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " BinaryErodeRadius";
    std::cerr << " EdgeWeight";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " LevelSetIterations";
    std::cerr << " RMSError";
    std::cerr << " BorderSize";
    std::cerr << " DisplayImageType";
    std::cerr << " Dimension";
    std::cerr << " [EvolutionImages] ";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImageName = argv[1];
  char * initialLabelImageName = argv[2];
  char * finalLabelImageName = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  std::string outputModel = argv[6];
  std::string anatomyName = argv[7];
  float lowerThreshold = atof( argv[8] );
  float upperThreshold = atof(argv[9]);
  float propScaling = atof( argv[10] );
  float curvScaling = atof( argv[11] );
  float binaryRadius = atof( argv[12] );
  float edgeWeight = atof( argv[13] );
  int iterations = atoi( argv[14] );
  float conductance = atof( argv[15] );
  int levelSetIterations = atoi (argv[16] );
  float rmsError = atof (argv[17] );
  int borderSize = atoi( argv[18] );
  std::string displayImageType = argv[19];
  int dimension = atoi (argv[20]);

  std::string evolutionImages;
  if (numberOfParameters < argc)
    {
    evolutionImages = argv[18];
    }  
  else
    {
    evolutionImages = "";
    }

  if (dimension == 2)
    {
    SegmentStructure< 2 > (featureImageName, initialLabelImageName, finalLabelImageName, masterFile, boundingBoxesFile, outputModel, anatomyName, lowerThreshold, upperThreshold, propScaling, curvScaling, binaryRadius, edgeWeight, iterations, conductance, levelSetIterations, rmsError, borderSize, displayImageType,evolutionImages);
    }
  else if (dimension == 3)
    {
    SegmentStructure< 3 > (featureImageName, initialLabelImageName, finalLabelImageName, masterFile, boundingBoxesFile, outputModel, anatomyName, lowerThreshold, upperThreshold, propScaling, curvScaling, binaryRadius, edgeWeight, iterations, conductance, levelSetIterations, rmsError, borderSize, displayImageType,evolutionImages);
    }
  return 0;
}

template < const int Dimension >
bool SegmentStructure(char * featureImageName, char * initialLabelImageName, char * finalLabelImageName, char * masterFile, char * boundingBoxesFile, std::string outputModel, std::string anatomyName, float lowerThreshold, float upperThreshold, float propScaling, float curvScaling, float binaryRadius, float edgeWeight, int iterations, float conductance, int levelSetIterations, float rmsError, int borderSize, std::string displayImageType, std::string evolutionImages)
{
  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImageName,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize,inputImageSpacing) )
    {
    return 0;
    }

  for (unsigned int i = 0; i < bbValues.size(); i++)
    {
    std::cout << "bbValues[" << i << "]: " << bbValues[i] << std::endl;
    }


  typedef short FeaturePixelType;
  typedef unsigned short LabelPixelType;
  typedef float InternalPixelType;

  typedef itk::Image< FeaturePixelType, Dimension > FeatureImageType;
  typedef itk::Image< LabelPixelType, Dimension > LabelImageType;
  typedef itk::Image< InternalPixelType, Dimension > InternalImageType;

  typedef itk::LabelStatisticsImageFilter< FeatureImageType, LabelImageType > LabelStatisticsFilterType;

  typedef itk::ThresholdSegmentationLevelSetImageFilter< LabelImageType, InternalImageType > ThresholdSegmentationLevelSetImageFilterType;  


  // Instantiate the filters
  FeatureImageType::Pointer featureImage = FeatureImageType::New();
  LabelImageType::Pointer labelImage = LabelImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  ThresholdSegmentationLevelSetImageFilterType::Pointer thresholdSegmentation = ThresholdSegmentationLevelSetImageFilterType::New();

  itk::SimpleFilterWatcher watchThresholdSegmentation(thresholdSegmentation,"thresholdSegmentation");



  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating feature volume from feature image slices" << std::endl;
  itk::CreateVolumeFromSlices<FeatureImageType>( featureImageName, bbValues, inputImageSpacing, featureImage);
  std::cout << "Creating label volume from label image slices" << std::endl;
  itk::CreateVolumeFromSlices<LabelImageType>( initialLabelImageName, bbValues, inputImageSpacing, labelImage);
  
  if( featureImage->GetRequestedRegion().GetSize() != labelImage->GetRequestedRegion().GetSize() )
    {
    std::cout << "ERROR: Feature and label images are different sizes" << std::endl;
    return 0;
    }

  InternalImageType::Pointer preProcessedFeatureImage;
  preProcessedFeatureImage = PreProcessFeatureImage<FeatureImageType,InternalImageType> (featureImage, iterations, conductance);

  LabelImageType::Pointer preProcessedLabelImage;
  preProcessedLabelImage = PreProcessLabelImage<LabelImageType,LabelImageType> (labelImage, anatomyLabel, binaryRadius);

//------------------------------------------------------------------------------------------------
  // TEMPORARY

  // Run a median filter to remove the flippers of the model
  typedef itk::BinaryMedianImageFilter< LabelImageType,LabelImageType > MedianType;
  MedianType::Pointer median = MedianType::New();
  median->SetInput( preProcessedLabelImage );
  median->SetForegroundValue( 0 );
  median->SetBackgroundValue( 255 );
  
  // define the neighborhood size used for the median filter
  LabelImageType::SizeType neighRadius;
  neighRadius[0] = 0;
  neighRadius[1] = 0;
  neighRadius[2] = 5;
  median->SetRadius(neighRadius);

  try
    {
    median->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  
//------------------------------------------------------------------------------------------------

  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

//  thresholdSegmentation->SetInput( preProcessedLabelImage );
  thresholdSegmentation->SetInput( median->GetOutput() );
  thresholdSegmentation->SetFeatureImage( preProcessedFeatureImage );


  // Set the parameters for the label statistics filter
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  labelStatistics->Update();
  
  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;

  // Used for the ThresholdLevelSet
//   double lowerThreshold = labelMean - mult * labelSigma;
//   double upperThreshold = labelMean + mult * labelSigma;


  // Set the parameters for the Threshold Segmentation
  thresholdSegmentation->UseImageSpacingOn();
  thresholdSegmentation->SetMaximumRMSError( rmsError );
  thresholdSegmentation->SetNumberOfIterations( levelSetIterations );
  thresholdSegmentation->SetEdgeWeight( edgeWeight );
  thresholdSegmentation->SetSmoothingTimeStep(.0625);

//   if (lowerThreshold < itk::NumericTraits<unsigned short>::NonpositiveMin())
//     {
//     lowerThreshold = itk::NumericTraits<unsigned short>::NonpositiveMin();
//     }
//   if (upperThreshold > itk::NumericTraits<unsigned short>::max())
//     {
//     upperThreshold = itk::NumericTraits<unsigned short>::max();
//     }

  thresholdSegmentation->SetLowerThreshold(lowerThreshold); 
  thresholdSegmentation->SetUpperThreshold(upperThreshold); 
  thresholdSegmentation->SetIsoSurfaceValue(127.5);

  thresholdSegmentation->SetPropagationScaling( propScaling );
  thresholdSegmentation->SetCurvatureScaling( curvScaling );


  if (Dimension == 2)
    {
    // Show the evolution of the level set
    // This must be done before the level set filter is updated
    itk::ShowLevelSetEvolution<ThresholdSegmentationLevelSetImageFilterType> levelSetEvolution(thresholdSegmentation,displayImageType,evolutionImages);
    }

  // Write out a model of the 3D image to file
  if (Dimension == 3)
    { 
    thresholdSegmentation->Update();
    WriteModelToFile<InternalImageType>(thresholdSegmentation->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputModel);
    }


  std::cout << "Threshold segmentation complete." << std::endl;
  
  // Set the parameters for Thresholder
  // Thresholder is used to threshold the image after the level set
  // segmentation is complete.
  typedef itk::BinaryThresholdImageFilter< InternalImageType, LabelImageType> ThresholdingFilterType;
  ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
  thresholder->SetInput( thresholdSegmentation->GetOutput() );
  thresholder->SetLowerThreshold( -1000.0 );
  thresholder->SetUpperThreshold(     0.0 );
  thresholder->SetOutsideValue( 255 );
  thresholder->SetInsideValue( 0 );
  try
    {
    thresholder->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  typedef itk::ShiftScaleImageFilter < LabelImageType, LabelImageType > ShiftScaleType;
  ShiftScaleType::Pointer labelShifter = ShiftScaleType::New();
  labelShifter->SetInput( thresholder->GetOutput() );
  labelShifter->SetShift( -255 );
  labelShifter->SetScale( -1 );

  // Find the connected components
  typedef itk::ConnectedComponentImageFilter< LabelImageType, LabelImageType > ConnectedComponentType;
  ConnectedComponentType::Pointer connectedComponent = ConnectedComponentType::New();
  connectedComponent->SetInput( labelShifter->GetOutput() );

  typedef itk::RelabelComponentImageFilter< LabelImageType, LabelImageType > RelabelComponentType;
  RelabelComponentType::Pointer relabelComponent = RelabelComponentType::New();
  relabelComponent->SetInput( connectedComponent->GetOutput() );

  typedef itk::BinaryThresholdImageFilter< LabelImageType, LabelImageType> ThresholdingFilterType2;
  ThresholdingFilterType2::Pointer finalThresholder = ThresholdingFilterType2::New();
  finalThresholder->SetInput( relabelComponent->GetOutput() );
  finalThresholder->SetLowerThreshold( 1 );
  finalThresholder->SetUpperThreshold( 1 );
  finalThresholder->SetInsideValue (-1);
  finalThresholder->SetOutsideValue (1);

  try
    {
    finalThresholder->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  if ( Dimension == 2 )
    {
    itk::FileDumper<LabelImageType> dumpFinalThresholder( finalThresholder->GetOutput(), finalLabelImageName);    
    }

  if ( Dimension == 3 )
    {
    // Now resample the image back to the original size
    typedef itk::ResampleImageFilter< LabelImageType, LabelImageType> LabelResampleType;
    typedef itk::NearestNeighborInterpolateImageFunction< LabelImageType, double >  NearestNeighborInterpolatorType;
    
    LabelResampleType::Pointer labelResampler = LabelResampleType::New();
    NearestNeighborInterpolatorType::Pointer nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();

    typename LabelImageType::SpacingType outputSpacing;
    outputSpacing[0] = inputImageSpacing[0];
    outputSpacing[1] = inputImageSpacing[1];
    outputSpacing[2] = inputImageSpacing[2];

    // The output size should be the input size times the output spacing divided by the input z spacing
    typename LabelImageType::SizeType outputSize;
    outputSize[0] = inputImageSize[0];
    outputSize[1] = inputImageSize[1];
    outputSize[2] = inputImageSize[2];
  
    // Label image resampled the same way
    labelResampler->SetInput( finalThresholder->GetOutput() );
    labelResampler->SetOutputSpacing( outputSpacing );
    labelResampler->SetOutputOrigin ( finalThresholder->GetOutput()->GetOrigin() );
    labelResampler->SetSize( outputSize );
    labelResampler->SetInterpolator( nearestNeighborInterpolator );

    // Update the resamplers
    try
      {
      labelResampler->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }

    std::cout << "New label image spacing: " << labelResampler->GetOutput()->GetSpacing() << std::endl;
    std::cout << "New label image origin: " << labelResampler->GetOutput()->GetOrigin() << std::endl;
    std::cout << "New label image size: " << labelResampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;

    itk::FileDumper<LabelImageType> dumpResampler( labelResampler->GetOutput(), finalLabelImageName);
  
    }




  // Write out the connected component model
  std::string outputModelPath = itksys::SystemTools::GetFilenamePath( outputModel );
  std::string outputModelName = itksys::SystemTools::GetFilenameName( outputModel );
  std::string connectedComponentOutputModel = outputModelPath + "/ConnectedComponent_" + outputModelName;
  std::string connectedComponentSmoothedOutputModel = outputModelPath + "/ConnectedComponentSmoothed_" + outputModelName;

  std::cout << "connected component output model: " << connectedComponentOutputModel << std::endl;

  if (Dimension == 3)
    { 
    WriteModelToFile<LabelImageType>(finalThresholder->GetOutput(),bbValues,inputImageSize,inputImageSpacing,connectedComponentOutputModel,std::string("Discrete"));

    WriteModelToFile<LabelImageType>(finalThresholder->GetOutput(),bbValues,inputImageSize,inputImageSpacing,connectedComponentSmoothedOutputModel,std::string("DiscreteSmoothed"));
    }



  // Find the volume of the binary image of the initial relabeled data.
  typedef itk::ImageRegionConstIterator< LabelImageType > UShortConstIteratorType;
  UShortConstIteratorType inputIt1( preProcessedLabelImage, preProcessedLabelImage->GetRequestedRegion() );
  
  int initVolume = 0;
  for ( inputIt1.GoToBegin(); !inputIt1.IsAtEnd(); ++inputIt1)
    {
    if (inputIt1.Get() == 0)
      initVolume++;
    }
  
  std::cout << "Initial volume: " << initVolume << std::endl;


  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << thresholdSegmentation->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << thresholdSegmentation->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << thresholdSegmentation->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << thresholdSegmentation->GetRMSChange() << std::endl;

  return true;
}
