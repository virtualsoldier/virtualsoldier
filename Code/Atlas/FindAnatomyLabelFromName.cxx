#include "FindAnatomyLabelFromName.h"

#include <iostream>
#include <fstream>

bool FindAnatomyLabelFromName(char * masterFile, std::string anatomyName, int & anatomyLabel)
{
  // Test the master anatomy file
  std::ifstream anatomyFile(masterFile);
  if (!anatomyFile) 
    {
    std::cout << "In FindAnatomyLabelFromName.cxx: Could not open master anatomy file: " << masterFile << std::endl;
    return false;
    }

  // Default value for anatomy label for error checking
  anatomyLabel = -1;

  // Declare the variables.
  char entireLine[255];
  std::string line;
  int endLine;
  int begin;
  int nextComma;
  std::string fileAnatomyName;

  // Get rid of header and test it
  begin = 0;
  anatomyFile.getline(entireLine,255);
  line=std::string(entireLine); 
  nextComma = line.find(',',begin);
  std::string headerName = line.substr(0,nextComma-begin);
  if (headerName != "Anatomy")
    {
    std::cout << "In FindAnatomyLabelFromName.cxx: Header not removed successfully" << std::endl;
    return false;
    }


  // Loop through the file looking for the anatomy name.  If it is
  // found, assign its label to a variable and break.
  while (! anatomyFile.eof() )
    {
    anatomyFile.getline(entireLine,255);
    line=std::string(entireLine); 
    endLine = line.length();

    if (! line.empty() )
      {
      begin = 0;
      nextComma = line.find(',',begin);
      fileAnatomyName = line.substr(begin,nextComma-begin);

      if (fileAnatomyName == anatomyName)
        {
        anatomyLabel = atoi(line.substr(nextComma+1,endLine-(nextComma+1)).c_str());
        break;
        }
      }
    }

  anatomyFile.close();

  // Make sure that the anatomy label was found
  if (anatomyLabel == -1)
    {
    std::cerr << "ERROR: " << anatomyName << " not listed in MasterAnatomy file" << std::endl;
    return false;
    }
  else
    {
    return true;
    }
}
