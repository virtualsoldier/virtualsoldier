/*=========================================================================

This program reads in a series of images and an anatomy label and
prints out the slice numbers of the images where the anatomy appears.
The bounding boxes of some anatomies were not initialized, which means
that these anatomies were not labeled, so this program was written
simply to check that those anatomies, while they appear in the label
files, do not appear in the images.  So, if any of these anatomy
labels are passed in, the output should not list any of them.

Inputs: Label image prefix, sliceOffset, numberOfSlices, anatomyLabels.
Outputs: Output file of found anatomies.

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionIteratorWithIndex.h"

#include <fstream>
#include <iostream>
#include <string>


int main( int argc, char *argv[] )
{
  if (argc < 6)
    {
    std::cout << "Usage: labelImagePrefix sliceOffset numberOfSlices outputFoundFile anatomyLabels" << std::endl;
    return 0;
    }  


  // Test the output file
  std::ofstream fout ( argv[4] );
  if (!fout)
    {
    std::cout << "Could not open found pixels file" << std::endl;
    return 0;
    }

  typedef   unsigned short   InputPixelType;
  typedef itk::Image< InputPixelType,  2 >   InputImageType;
  typedef itk::ImageFileReader< InputImageType >  ReaderType;
  typedef itk::NumericSeriesFileNames NameGeneratorType;
  
  ReaderType::Pointer reader = ReaderType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();

  int sliceOffset = atoi (argv[2]);
  int numberOfSlices = atoi (argv[3]);

  std::cout << "Slice offset: " << sliceOffset << std::endl;
  std::cout << "Number of slices: " << numberOfSlices << std::endl;

  // Generate the image file names
  inputImageFileGenerator->SetStartIndex( sliceOffset );
  inputImageFileGenerator->SetEndIndex ( numberOfSlices + sliceOffset - 1 );
  inputImageFileGenerator->SetIncrementIndex ( 1 );
  inputImageFileGenerator->SetSeriesFormat ( argv[1] );
  
  std::vector < unsigned short > anatomyLabels;
  for (int i = 5; i < argc; i++)
    {
    anatomyLabels.push_back( atoi (argv[i]) );
    std::cout << "Anatomy label: " << atoi(argv[i]) << std::endl;
    }


  // Print the values to file  
//  std::cout << "Slice, Found Anatomy Labels" << std::endl;
  fout << "Slice, Found Anatomy Labels" << std::endl;

  std::vector <std::string > inputImageNames = inputImageFileGenerator->GetFileNames();
  std::vector <std::string> ::iterator iinit = inputImageNames.begin();
      
  int slice = sliceOffset;
  // Loop through each anatomy file
  while (iinit != inputImageNames.end())
    {
//    std::cout << slice;
    fout << slice;

    // Read the image
    reader->SetFileName ( (*iinit).c_str() );
      
    try 
      { 
      reader->Update();
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 
      
    // Loop through each pixel to see whether it matches any of the anatomy
    // labels being compared
    itk::ImageRegionIteratorWithIndex  < InputImageType > it (reader->GetOutput(),reader->GetOutput()->GetRequestedRegion());

    it.GoToBegin();
    while (!it.IsAtEnd())
      {
      for (unsigned short i = 0; i < anatomyLabels.size(); i++)
        {
        if (it.Get() == anatomyLabels[i])
          {
          // Write out the isolated pixels
//          std::cout << ",\t" << it.Get();
          fout << "," << it.Get();
          }
        }

      // increment the iterator
      ++it;
      }

//    std::cout << std::endl;
    fout << std::endl;

    ++iinit;
    ++slice;
    }

  fout.close();

  return 0;
  
}
