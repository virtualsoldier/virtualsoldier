/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: GeodesicActiveContourLevelSet2D.cxx,v $
  Language:  C++
  Date:      $Date: 2004/10/28 13:56:49 $
  Version:   $Revision: 1.2 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

// itk Classes
#include "itkImage.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkVTKImageExport.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkExceptionObject.h"
#include "itkExtractImageFilter.h"


// vtk Classes
#include "vtkImageImport.h"
#include "vtkMarchingCubes.h"
#include "vtkPolyDataWriter.h"
#include "vtkImageChangeInformation.h"
#include "vtkImageFlip.h"

// vtk / itk classes
#include "vtk2itk2vtk.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"


int main( int argc, char *argv[] )
{
  int numberOfParameters = 12;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " AdvectionScaling";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << std::endl;
    return 1;
    }

  // Image Typedefs
  typedef itk::Image< float, 2 > Float2DImageType;
  typedef itk::Image< unsigned short, 2 > UShort2DImageType;

  typedef itk::LabelStatisticsImageFilter< UShort2DImageType, UShort2DImageType > LabelStatisticsFilterType;

  // Filters for the label image
  // This is used as the initial level set
  typedef itk::BinaryThresholdImageFilter< UShort2DImageType, UShort2DImageType> ThresholdingFilterType1;
  typedef itk::BinaryBallStructuringElement< unsigned short, 3> KernelType;

  // Filters for the feature image
  typedef itk::LinearInterpolateImageFunction< UShort2DImageType, double >  LinearInterpolatorType;
  typedef itk::CastImageFilter< UShort2DImageType, Float2DImageType > CastImageFilterType;  
  typedef itk::GradientAnisotropicDiffusionImageFilter< Float2DImageType, Float2DImageType> DiffusionFilterType;
  typedef itk::CurvatureAnisotropicDiffusionImageFilter< Float2DImageType, Float2DImageType> CurvatureDiffusionFilterType;
  typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< Float2DImageType, Float2DImageType >  GaussianGradientFilterType;
  typedef itk::GradientMagnitudeImageFilter< Float2DImageType, Float2DImageType >  GradientFilterType;
  typedef itk::SigmoidImageFilter< Float2DImageType, Float2DImageType  >  SigmoidFilterType;
  typedef  itk::GeodesicActiveContourLevelSetImageFilter< UShort2DImageType, Float2DImageType > GeodesicActiveContourFilterType;
  typedef itk::BinaryThresholdImageFilter< Float2DImageType, UShort2DImageType> ThresholdingFilterType2;


  // Instantiate the filters  
  UShort2DImageType::Pointer featureImage = UShort2DImageType::New();
  UShort2DImageType::Pointer labelImage = UShort2DImageType::New();
  Float2DImageType::Pointer speedImage = Float2DImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  // Filters for the label image
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();

  // Filters for the feature image
  CastImageFilterType::Pointer castToFloat = CastImageFilterType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();
  CurvatureDiffusionFilterType::Pointer curvatureDiffusion = CurvatureDiffusionFilterType::New();
  GaussianGradientFilterType::Pointer gaussianGradientMagnitude = GaussianGradientFilterType::New();
  GradientFilterType::Pointer gradientMagnitude = GradientFilterType::New();
  SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();
  GeodesicActiveContourFilterType::Pointer geodesicActiveContour = GeodesicActiveContourFilterType::New();
  ThresholdingFilterType2::Pointer thresholder2 = ThresholdingFilterType2::New();

  // Find the anatomy info
  std::string anatomyName;
  anatomyName = std::string( argv[6] ); 
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int borderSize = 10;
  int sliceOffset = 1253;
  int numberOfSlices = 411;
  if (! FindAnatomyInfo(argv[1],argv[3],argv[4],argv[6],anatomyLabel,bbValues,borderSize,sliceOffset,numberOfSlices ) )
    {
    return 0;
    }


  // Create images that are cropped according to the bounding box values
  itk::CreateVolumeFromSlices<UShort2DImageType>(argv[1], bbValues, featureImage);
  itk::CreateVolumeFromSlices<UShort2DImageType>(argv[2], bbValues, labelImage);

  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

  thresholder1->SetInput( labelImage );
  geodesicActiveContour->SetInput( thresholder1->GetOutput() );

  castToFloat->SetInput ( featureImage );
  diffusion->SetInput ( castToFloat->GetOutput() );
  curvatureDiffusion->SetInput ( castToFloat->GetOutput() );
  gradientMagnitude->SetInput ( curvatureDiffusion->GetOutput() );
  gaussianGradientMagnitude->SetInput ( curvatureDiffusion->GetOutput() );
  //gradientMagnitude->SetInput ( diffusion->GetOutput() );
  sigmoid->SetInput ( gradientMagnitude->GetOutput() );
  geodesicActiveContour->SetFeatureImage( speedImage );
  //geodesicActiveContour->SetFeatureImage( sigmoid->GetOutput());
  thresholder2->SetInput( geodesicActiveContour->GetOutput() );



  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);

  // Set the parameters for the label statistics filter.
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  try
    {
    labelStatistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;

  // Set the smoothing parameters
  diffusion->SetNumberOfIterations( atof (argv[10]) );
  diffusion->SetConductanceParameter( atof (argv[11]) );
  diffusion->SetTimeStep( 0.0625 );
  diffusion->UseImageSpacingOn();
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;

  // Set the curvature diffusion parameters
  curvatureDiffusion->SetNumberOfIterations( atof (argv[10]) );
  curvatureDiffusion->SetConductanceParameter( atof (argv[11]) );
  curvatureDiffusion->SetTimeStep( 0.0625 );
  curvatureDiffusion->UseImageSpacingOn();
  try
    {
    curvatureDiffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  // Set the gradient magnitude parameters
  const double sigma = 1;
  gaussianGradientMagnitude->SetSigma(  sigma  );
  try
    {
    gradientMagnitude->Update();    
    gaussianGradientMagnitude->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Create a speed image
  Float2DImageType::RegionType imageRegion;  
  imageRegion.SetSize(castToFloat->GetOutput()->GetRequestedRegion().GetSize());
  imageRegion.SetIndex(castToFloat->GetOutput()->GetRequestedRegion().GetIndex());
  
  speedImage->SetRegions( imageRegion );
  speedImage->Allocate();
  speedImage->FillBuffer(0);

  std::cout << "Speed image spacing: " << speedImage->GetSpacing() << std::endl;
  std::cout << "Speed image size: " << speedImage->GetRequestedRegion().GetSize() << std::endl;


  // Set up the input region iterator for the speed image
  typedef itk::ImageRegionIterator < Float2DImageType > Float3DIteratorType;
  typedef itk::ImageRegionConstIterator < Float2DImageType > Float3DConstIteratorType;

  Float3DIteratorType inputIt ( gradientMagnitude->GetOutput(), imageRegion );
  Float3DIteratorType outputIt( speedImage, imageRegion );

  float pixelValue;

  for ( inputIt.GoToBegin(), outputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt, ++outputIt)
    {
    pixelValue = inputIt.Get();
    outputIt.Set( 1/(1+pixelValue) );
//    outputIt.Set( exp(-pixelValue) );
    }



  // Set up the sigmoid parameters
  sigmoid->SetOutputMinimum(  0.0  );
  sigmoid->SetOutputMaximum(  1.0  );
  const double alpha = -0.3;
  const double beta  = 2.0;
  sigmoid->SetAlpha( alpha );
  sigmoid->SetBeta( beta );

  // Set the parameters for the Shape Detection Segmentation
  geodesicActiveContour->UseImageSpacingOn();
  geodesicActiveContour->SetMaximumRMSError( 0.002 );
  geodesicActiveContour->SetNumberOfIterations( 400 );
  geodesicActiveContour->SetIsoSurfaceValue(127.5);
  geodesicActiveContour->SetPropagationScaling( atof(argv[7]) );
  geodesicActiveContour->SetCurvatureScaling( atof(argv[8]) );
  geodesicActiveContour->SetAdvectionScaling( atof(argv[9]) );
  

  // Show the evolution of the level set
  itk::ShowLevelSetEvolution<Float2DImageType, GeodesicActiveContourFilterType> levelSetEvolution(diffusion->GetOutput(),geodesicActiveContour);


  // Set the parameters for Thresholder2
  // Thresholder2 is used to threshold the image after the level set
  // segmentation is complete.
  //  thresholder2->SetLowerThreshold( 0.0 );
  thresholder2->SetLowerThreshold( -1000.0 );
  thresholder2->SetUpperThreshold(     0.0 );
  thresholder2->SetOutsideValue( 255 );
  thresholder2->SetInsideValue( 0 );
  try
    {
    thresholder2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Size: " << thresholder1->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Size: " << thresholder2->GetOutput()->GetRequestedRegion().GetSize() << std::endl;


  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << geodesicActiveContour->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << geodesicActiveContour->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << geodesicActiveContour->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << geodesicActiveContour->GetRMSChange() << std::endl;

  itk::FileDumper<Float2DImageType> dumpOriginal(castToFloat->GetOutput(), "original");
  itk::FileDumper<Float2DImageType> dumpCurvatureDiffusion(curvatureDiffusion->GetOutput(), "curvatureDiffusion");
//  itk::FileDumper<Float2DImageType> dumpDiffusion(diffusion->GetOutput(), "diffusion");
  itk::FileDumper<Float2DImageType> dumpGradientMagnitude(gradientMagnitude->GetOutput(), "gradientMagnitude");
  itk::FileDumper<Float2DImageType> dumpSpeed(speedImage, "speed");
//  itk::FileDumper<Float2DImageType> dumpSigmoid(sigmoid->GetOutput(), "sigmoid");
//  itk::FileDumper<Float2DImageType> dumpSpeed(geodesicActiveContour->GetSpeedImage(), "speed");
  itk::FileDumper<Float2DImageType> dumpLevelSet(geodesicActiveContour->GetOutput(), "levelset");


  return 0;
}
