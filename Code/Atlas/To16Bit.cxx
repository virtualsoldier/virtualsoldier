/*=========================================================================

Program:   Insight Segmentation & Registration Toolkit
Module:    $RCSfile: To16Bit.cxx,v $
Language:  C++
Date:      $Date: 2005/02/07 20:41:45 $
Version:   $Revision: 1.3 $

Copyright (c) Insight Software Consortium. All rights reserved.
See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "itkImage.h"
#include "itkImageSeriesReader.h"
#include "itkImageSeriesWriter.h"
#include "itkExceptionObject.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkSimpleFilterWatcher.h"

#include <itksys/SystemTools.hxx>

int main( int argc, char ** argv )
{
  // Verify the number of parameters in the command line
  if( argc < 3 )
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile outputImageType [flip]" << std::endl;
    return -1;
    }
  
  std::string inputFileName  = argv[1];
  std::string outputFileType = argv[2];

  typedef itk::Image< unsigned short, 3 >       ImageInputType;
  typedef itk::Image< unsigned short, 2 >       ImageOutputType;
  typedef itk::ImageSeriesReader< ImageInputType >  ReaderType;
  typedef itk::ImageSeriesWriter< ImageInputType, ImageOutputType >  WriterType;
  typedef itk::ArchetypeSeriesFileNames NameGeneratorType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();

  itk::SimpleFilterWatcher watchReader(reader,"reader");
  itk::SimpleFilterWatcher watchWriter(writer,"writer");


  // Generate the image file names
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();
  inputImageFileGenerator->SetArchetype( inputFileName );
  std::vector < std::string > inputFileNames = inputImageFileGenerator->GetFileNames();

  reader->SetFileNames( inputFileNames );
  try
    {
    reader->Update();   
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  

  std::vector < std::string > outputFileNames;
  std::string outputFileName;
  // Copy the input file names to the output file names, replacing the
  // extension with the new extension.
  if (argc < 4)
    {
    std::vector < std::string >::iterator inputFileNamesIt;
    for (inputFileNamesIt = inputFileNames.begin(); inputFileNamesIt != inputFileNames.end(); ++inputFileNamesIt)
      {
      std::string inputFilePath = itksys::SystemTools::GetFilenamePath( *inputFileNamesIt );
      std::string inputFileName = itksys::SystemTools::GetFilenameWithoutExtension( *inputFileNamesIt );
    
      outputFileName=inputFilePath + "/" + inputFileName + "." + outputFileType;
      outputFileNames.push_back( outputFileName );
//      std::cout << "File name: " << outputFileName << std::endl;
      }
    }
  if (argc == 4)
    {
    // Copy the input file names to the output file names in reverse order, replacing the
    // extension with the new extension.
    std::cout << "Flipping output slices" << std::endl;
    std::vector < std::string >::reverse_iterator inputFileNamesIt;
    for (inputFileNamesIt = inputFileNames.rbegin(); inputFileNamesIt != inputFileNames.rend(); ++inputFileNamesIt)
      {
      std::string inputFilePath = itksys::SystemTools::GetFilenamePath( *inputFileNamesIt );
      std::string inputFileName = itksys::SystemTools::GetFilenameWithoutExtension( *inputFileNamesIt );
    
      outputFileName=inputFilePath + "/" + inputFileName + "." + outputFileType;
      outputFileNames.push_back( outputFileName );
//      std::cout << "File name: " << outputFileName << std::endl;
      }
    }


  writer->SetFileNames( outputFileNames );

  writer->SetInput( reader->GetOutput() );
  try
    {
    writer->Update();   
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  return 0;
}



