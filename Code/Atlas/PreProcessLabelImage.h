/*=========================================================================

This templated function performs the pre-processing of the Label
image.  If the image is 3D, this function first resamples the x and y
dimensions to have the same spacing as the z dimension.  It then uses
the threshold filter to create a binary image of the structure of
interest.  Using this binary image, it erodes the structure.  It
returns this eroded image.

Inputs: Input image, 
Outputs: Resampled and eroded image.

=========================================================================*/
#ifndef _PreProcessLabelImage_h
#define _PreProcessLabelImage_h

template <class TInputImage, class TOutputImage>
typename TOutputImage::Pointer PreProcessLabelImage(TInputImage * inputImage, int anatomyLabel, float binaryRadius);

#ifndef VSP_MANUAL_INSTANTIATION
#include "PreProcessLabelImage.txx"
#endif


#endif
