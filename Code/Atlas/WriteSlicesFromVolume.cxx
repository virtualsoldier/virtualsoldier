#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkCastImageFilter.h"

#include "WriteSlicesFromVolume.h"

#include <iostream>
#include <fstream>

bool WriteSlicesFromVolume(char * fileName, std::vector <int> bbValues, itk::Image< unsigned short, 3 >::Pointer image3D)
{

  const unsigned short Dimension3D = 3;
  const unsigned short Dimension2D = 2;

//  typedef itk::Image< float, 3 > Float3DImageType;
//  typedef itk::Image< float, 2 > Float2DImageType;
  typedef itk::Image< unsigned short, 2 > UShort2DImageType;
  typedef itk::Image< unsigned short, 3 > UShort3DImageType;

  typedef itk::ImageFileWriter < UShort2DImageType >  WriterType;
  
  typedef itk::NumericSeriesFileNames NameGeneratorType;
  typedef itk::ImageRegionConstIterator< UShort3DImageType > ConstIteratorType;
  typedef itk::ImageRegionIterator< UShort2DImageType > IteratorType;
  typedef itk::CastImageFilter < UShort2DImageType, UShort2DImageType > CastType;

  UShort2DImageType::Pointer image2D = UShort2DImageType::New();  

  WriterType::Pointer writer = WriterType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();
  CastType::Pointer caster = CastType::New();

  int minX = bbValues[0];
  int maxX = bbValues[1];
  int minY = bbValues[2];
  int maxY = bbValues[3];
  int minZ = bbValues[4];
  int maxZ = bbValues[5];
  int minSlice = bbValues[6];
  int maxSlice = bbValues[7];

  // Generate the image file names
  inputImageFileGenerator->SetStartIndex( minSlice );
  inputImageFileGenerator->SetEndIndex ( maxSlice );
  inputImageFileGenerator->SetIncrementIndex ( 1 );
  inputImageFileGenerator->SetSeriesFormat ( fileName );

  std::cout << inputImageFileGenerator->GetFileNames()[0] << std::endl;
  std::vector <std::string > inputImageNames = inputImageFileGenerator->GetFileNames();

  // Find the input image size
  UShort3DImageType::SizeType inputImageSize;
  inputImageSize = (image3D->GetLargestPossibleRegion()).GetSize();

  // Set up the 2D region iterator
  UShort2DImageType::RegionType imageRegion2D;

  UShort2DImageType::RegionType::IndexType imageRegion2DStart;
  UShort2DImageType::RegionType::SizeType  imageRegion2DSize;

  imageRegion2DStart[0] = 0;
  imageRegion2DStart[1] = 0;

  imageRegion2DSize[0]  = maxX - minX + 1; //inputImageSize[0];
  imageRegion2DSize[1]  = maxY - minY + 1; //inputImageSize[1];

  imageRegion2D.SetSize( imageRegion2DSize );
  imageRegion2D.SetIndex( imageRegion2DStart );

  // Create a 2D Image
  image2D->SetRegions(imageRegion2D);
  image2D->Allocate();
  image2D->FillBuffer(0);


  // Set up the 3D region iterator
  UShort3DImageType::RegionType imageRegion3D;

  UShort3DImageType::RegionType::IndexType imageRegion3DStart;
  UShort3DImageType::RegionType::SizeType  imageRegion3DSize;

  imageRegion3DStart[0] = 0;
  imageRegion3DStart[1] = 0;
  imageRegion3DStart[2] = 0;

  imageRegion3DSize[0] = maxX - minX + 1; //inputImageSize[0];
  imageRegion3DSize[1] = maxY - minY + 1; //inputImageSize[1];
  imageRegion3DSize[2] = maxSlice - minSlice + 1; //inputImageSize[2];

  imageRegion3D.SetSize( imageRegion3DSize );
  imageRegion3D.SetIndex( imageRegion3DStart );

  ConstIteratorType image3DIt (image3D, imageRegion3D);
  image3DIt.GoToBegin();

  int slice;
  // Loop through each anatomy file
  for (std::vector <std::string> ::iterator iinit = inputImageNames.begin(); 
       iinit != inputImageNames.end(); iinit++)
    {
    slice = iinit - inputImageNames.begin();
//    std::cout << "Slice: " << slice << std::endl;

    IteratorType image2DIt( image2D, imageRegion2D );
    
    for ( image2DIt.GoToBegin(); !image2DIt.IsAtEnd(); ++image2DIt, ++image3DIt)
      {
      image2DIt.Set( image3DIt.Get() );
      }

    caster->SetInput(image2D);
    caster->Modified();
    writer->SetFileName ( (*iinit).c_str() );    
    writer->SetInput(caster->GetOutput());


    try 
      { 
      writer->Update(); 
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 
    }

  return true;
}
