/*=========================================================================

This templated function reads input slices and outputs either 2D or 3D
cropped regions.  If the output image is to be 2D, it crops out the
region of interest corresponding to the given structure.  If the
output image is 3D, it iterates over the relevant slices, cropping out
the region of interest, and creates a 3D volume.  This is used by the
level set algorithms to run the algorithms on a region of interest.
It is used to crop both the feature and label images.

Inputs: Feature image prefix, bounding box vector of values.
Outputs: 2D or 3D cropped image.

=========================================================================*/

#ifndef _CreateVolumeFromSlices_h
#define _CreateVolumeFromSlices_h

#include <vector>
#include <string>

namespace itk {
template <class TInputImage>
bool CreateVolumeFromSlices(char * fileName, std::vector <int> bbValues, double * imageSpacing, TInputImage * inputImage);
}

#ifndef VSP_MANUAL_INSTANTIATION
#include "CreateVolumeFromSlices.txx"
#endif

#endif



