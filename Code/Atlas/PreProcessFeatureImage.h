/*=========================================================================

This templated function performs the pre-processing of the Feature
image.  If the image is 3D, this function first resamples the x and y
dimensions to have the same spacing as the z dimension.  It then uses
the iterations and conductance parameters to smooth the image
isotropically.  It returns this diffused image.

Inputs: Input image, number of smoothing iterations, smoothing conductance.
Outputs: Resampled and smoothed image.

=========================================================================*/
#ifndef _PreProcessFeatureImage_h
#define _PreProcessFeatureImage_h

template <class TInputImage, class TOutputImage>
typename TOutputImage::Pointer PreProcessFeatureImage(TInputImage * inputImage, int iterations, float conductance);

#ifndef VSP_MANUAL_INSTANTIATION
#include "PreProcessFeatureImage.txx"
#endif


#endif
