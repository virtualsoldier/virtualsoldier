/*=========================================================================

This templated class accepts either 2D or 3D level set filters and
displays the evolution of the level set at each iteration.  It
displays a background image, which can be the feature image or another
image of interest, and overlays the evolving contour.

Inputs: 2D or 3D feature image, level set filter. 
Outputs: Displayed evolution.

=========================================================================*/

#ifndef _ShowLevelSetEvolution_h
#define _ShowLevelSetEvolution_h

// itk classes
#include "itkImage.h"
#include "itkSegmentationLevelSetImageFilter.h"
#include "itkCommand.h"
#include "itkPNGImageIO.h"
#include "itkDicomImageIO.h"
#include "itkCastImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkChangeInformationImageFilter.h"
#include "itkPolygonCell.h"
#include "itkDICOMImageIO2Factory.h"

#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkVTKImageExport.h"

// vtk classes
#include "vtkImageViewer2.h"
#include "vtkImageImport.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkMarchingSquares.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkImageActor.h"
#include "vtkProperty.h"
#include "vtkImageAppendComponents.h"
#include "vtkImageShiftScale.h"
#include "vtkImageCast.h"
#include "vtkUnstructuredGrid.h"
#include "vtkImageData.h"
#include "vtkPolyData.h"
#include "vtkImageFlip.h"

#include "vtkMarchingCubes.h"
#include "vtkCutter.h"
#include "vtkPlane.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"

#include "vtkRendererSource.h"
#include "vtkPNGWriter.h"

#include <string>

#include "vtk2itk2vtk.h"
#include "DumpImages.h"


namespace itk {
template < class TLevelSet >
class ShowLevelSetEvolution
{
public:
  typedef ShowLevelSetEvolution Self;
  typedef typename TLevelSet::FeatureImageType FeatureImageType;
  itkStaticConstMacro(ImageDimension, unsigned int,
                      FeatureImageType::ImageDimension);  

  ShowLevelSetEvolution(TLevelSet * detector, std::string displayImageType = "SpeedImage", std::string outputFile = "");
  ~ShowLevelSetEvolution() {};

private:
  ShowLevelSetEvolution(); // Purposely not implemented
  void operator=(const Self&); //purposely not implemented
};
}


// The following class is used to support callbacks
// on the filter in the pipeline
template < class TLevelSet >
class ShowSegmentationProgressObject
{
public:
  typedef typename TLevelSet::FeatureImageType FeatureImageType;

  ShowSegmentationProgressObject(TLevelSet* o,
                                 vtkImageViewer2* v,
                                 typename TLevelSet::FeatureImageType* levelSetImage,
                                 typename TLevelSet::FeatureImageType* featureImage,
                                 vtkImageImport* levelSetImporter,
                                 vtkImageImport* featureImporter,
                                 vtkActor* marchingActor,
                                 std::string displayImageType = "SpeedImage",
                                 std::string outputFile = "")
    {
      m_Segmenter = o; // The level set filter
      m_Viewer = v;
      m_LevelSetImage = levelSetImage;  // 
                                        // Level set contour (initially blank)
      m_FeatureImage = featureImage;
      m_LevelSetImporter = levelSetImporter;
      m_FeatureImporter = featureImporter;
      m_MarchingActor = marchingActor;
      m_Iteration = 0;
      
      m_DisplayImageType = displayImageType;
      m_OutputFile = outputFile;
    }
  

  ~ShowSegmentationProgressObject() {};
  ShowSegmentationProgressObject(); // Purposely not implemented

  void ShowProgress();

private:
  // data
  typename TLevelSet::Pointer m_Segmenter;
  typename TLevelSet::FeatureImageType::Pointer m_LevelSetImage;
  typename TLevelSet::FeatureImageType::Pointer m_FeatureImage;
  vtkImageImport *m_LevelSetImporter;
  vtkImageImport *m_FeatureImporter;
  vtkImageViewer2 *m_Viewer;
  vtkActor *m_MarchingActor;
  int m_Iteration;
  std::string m_DisplayImageType;
  std::string m_OutputFile;
};

/*
// The following class is used to support callbacks
// on the filter in the pipeline
template <class TFilter, class TLevelSet>
class ConnectSpeedImageObject
{
public:
  ConnectSpeedImageObject(TFilter* o,
                          TLevelSet* ls)
    {
      m_Filter = o;
      m_LevelSet = ls;
    }
  

  ~ConnectSpeedImageObject() {};
  ConnectSpeedImageObject(); // Purposely not implemented

  void Connect() { m_Filter->SetInput( m_LevelSet->GetSpeedImage() ); };

  // data
  typename TFilter::Pointer m_Filter;
  typename TLevelSet::Pointer m_LevelSet;
};
*/



#ifndef VSP_MANUAL_INSTANTIATION
#include "ShowLevelSetEvolution.txx"
#endif



#endif


