/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: CreateAModel.cxx,v $
  Language:  C++
  Date:      $Date: 2005/03/16 17:11:46 $
  Version:   $Revision: 1.6 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

// itk Classes
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkBinaryMedianImageFilter.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"
#include "WriteModelToFile.h"

#include "DumpImages.h"

int main( int argc, char *argv[] )
{
  int numberOfParameters = 7;

  std::cout << "argc: " << argc << std::endl;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " LabelImageName";
    std::cerr << " FeatureImageName";
    std::cerr << " MasterAnatomyFile";
    std::cerr << " BoundingBoxesFile";
    std::cerr << " OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " [OutputSmoothModel]";
    std::cerr << " [OutputDecimatedModel]";
    std::cerr << std::endl;
    return 1;
    }

  for (int i = 0; i < argc; i++)
    {
    std::cout << "Argument[" << i << "]: " << argv[i] << std::endl;
    }

  // Save all arguments as local variables
  char * labelImageName = argv[1];
  char * featureImageName = argv[2];
  char * masterFile = argv[3];
  char * boundingBoxesFile = argv[4];
  std::string outputModel = argv[5];
  std::string anatomyName = argv[6];

  // Image Typedefs
  typedef itk::Image< unsigned short, 3 > LabelImageType;
  typedef itk::BinaryThresholdImageFilter< LabelImageType, LabelImageType> ThresholdType;
  typedef itk::CastImageFilter< LabelImageType, LabelImageType > CastType;  
  typedef itk::StatisticsImageFilter < LabelImageType > StatisticsType;

  // Instantiate the filters  
  LabelImageType::Pointer labelImage = LabelImageType::New();
  ThresholdType::Pointer thresholder = ThresholdType::New();
  CastType::Pointer castToFloat = CastType::New();
  StatisticsType::Pointer statistics = StatisticsType::New();

  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int borderSize = 10;
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImageName,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize, inputImageSpacing) )
    {
    return 0;
    }

  std::vector< int >::iterator bbValuesIt;
  std::cout << "bbValues: \t";
  for( bbValuesIt = bbValues.begin(); bbValuesIt != bbValues.end(); ++bbValuesIt )
    {
    std::cout << *bbValuesIt << "\t";
    }
  std::cout << std::endl;

  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating label volume from label image slices" << std::endl;
  itk::CreateVolumeFromSlices<LabelImageType>( labelImageName, bbValues, inputImageSpacing, labelImage);

  std::cout << "Label image size: " << labelImage->GetRequestedRegion().GetSize() << std::endl;

  // Set the parameters for Thresholder
  // Thresholder is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder->SetLowerThreshold (anatomyLabel);
  thresholder->SetUpperThreshold (anatomyLabel);
  thresholder->SetInsideValue (-1);
  thresholder->SetOutsideValue (1);


//-------------------------------------------------------------------------------------------
  typedef itk::BinaryMedianImageFilter< LabelImageType,LabelImageType > MedianType;
  MedianType::Pointer median = MedianType::New();
  median->SetInput( thresholder->GetOutput() );
  median->SetForegroundValue( -1 );
  median->SetBackgroundValue( 1 );
  
  // define the neighborhood size used for the median filter
  LabelImageType::SizeType neighRadius;
  neighRadius[0] = 0;
  neighRadius[1] = 0;
  neighRadius[2] = 5;
  median->SetRadius(neighRadius);
//-------------------------------------------------------------------------------------------

  // set up the pipeline
//  castToFloat->SetInput( labelImage );
//  thresholder->SetInput ( castToFloat->GetOutput() );
  thresholder->SetInput ( labelImage );
  statistics->SetInput ( thresholder->GetOutput() );

  try
    {
    thresholder->Update();
    median->Update();
    statistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  float min = statistics->GetMinimum();
  float max = statistics->GetMaximum();
  float mean = statistics->GetMean();
  float sigma = statistics->GetSigma();

  std::cout << "\tMinimum = " << min  << std::endl;
  std::cout << "\tMaximum = " << max << std::endl;
  std::cout << "\tMean = " << mean << std::endl;
  std::cout << "\tSigma = " << sigma << std::endl;

  // Write out a model of the 3D image to file
  std::cout << "Writing the model to a file" << std::endl;

  WriteModelToFile< LabelImageType >( thresholder->GetOutput(),bbValues,&inputImageSize[0],&inputImageSpacing[0],outputModel,std::string("Discrete"));

  // Write out a smooth model of the 3D image to file
  if (argc > 7)
    {
    std::string outputSmoothModel = argv[7];
    std::cout << "Writing the smoothed model to a file." << std::endl;
    WriteModelToFile< LabelImageType >( thresholder->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputSmoothModel,std::string("DiscreteSmoothed"));
    }
  if (argc > 8)
    {
    std::string outputDecimatedModel = argv[8];
    std::cout << "Writing the decimated model to a file." << std::endl;
    WriteModelToFile< LabelImageType >( thresholder->GetOutput(),bbValues,inputImageSize,inputImageSpacing,outputDecimatedModel,std::string("DiscreteDecimated"));
    }

  return 0;
}
