/*=========================================================================

This program searches through the master anatomy file for a given
anatomy name and returns its corresponding anatomy label.

Inputs: Master anatomy file, anatomy name. 
Outputs: Anatomy label.

=========================================================================*/

#ifndef _FindAnatomyLabelFromName_h
#define _FindAnatomyLabelFromName_h

#include <vector>
#include <string>

bool FindAnatomyLabelFromName(char * masterFile, std::string anatomyName, int & anatomyLabel);

#endif


