/*=========================================================================

This is a convenience program that reads in 2D slices
and writes out a raw volume of the given structure.  This speeds up
the level set algorithms because they can read this file directly
instead of having to crop the structure out of the feature slices and
label slices, which takes a long time.  The tradeoff is that the file
side that is output takes up a lot of space.

Inputs: Image prefix, master anatomy file, bounding boxes
file, anatomy file.

Outputs: Raw volume file.

=========================================================================*/


#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"

#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"

int main( int argc, char *argv[] )
{
  if( argc < 6 )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0];
    std::cerr << " ImagePrefix";
    std::cerr << " ImageInfoFile";
    std::cerr << " MasterAnatomyFile";
    std::cerr << " BoundingBoxesFile";
    std::cerr << " OutputVolume";
    std::cerr << " AnatomyName";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * imagePrefix = argv[1];
  char * imageInfoFile = argv[2];
  char * masterFile = argv[3];
  char * boundingBoxesFile = argv[4];
  char * outputVolume = argv[5];
  std::string anatomyName = argv[6];

  typedef itk::Image< unsigned short, 2 > UShort2DImageType;
  typedef itk::Image< unsigned short, 3 > UShort3DImageType;

  typedef itk::ImageFileReader< UShort2DImageType >  ReaderType;
  typedef itk::NumericSeriesFileNames NameGeneratorType;
  typedef itk::ImageRegionConstIterator< UShort2DImageType > ConstIteratorType;
  typedef itk::ImageRegionIterator< UShort3DImageType > IteratorType;

  UShort3DImageType::Pointer image3D = UShort3DImageType::New();
  ReaderType::Pointer reader = ReaderType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();


  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int borderSize = 10;
  int imageSize[3];
  double imageSpacing[3];
  if (! FindAnatomyInfo(imageInfoFile,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,imageSize,imageSpacing) )
    {
    std::cout << "In CreateVolume.cxx: Cannot read anatomy information" << std::endl;
    return 0;
    }

  int minX = bbValues[0] ;
  int maxX = bbValues[1] ;
  int minY = bbValues[2] ;
  int maxY = bbValues[3] ;
  int minZ = bbValues[4] ;
  int maxZ = bbValues[5] ;
  int minSlice = bbValues[6] ;
  int maxSlice = bbValues[7] ;

  std::cout << "After: " << std::endl;
  std::cout << "minX: " << minX << std::endl;
  std::cout << "maxX: " << maxX << std::endl;
  std::cout << "minY: " << minY << std::endl;
  std::cout << "maxY: " << maxY << std::endl;
  std::cout << "minZ: " << minZ << std::endl;
  std::cout << "maxZ: " << maxZ << std::endl;
  std::cout << "minSlice: " << minSlice << std::endl;
  std::cout << "maxSlice: " << maxSlice << std::endl;

  
  // Create 3D volumes of the input slices according to the bounding
  // box values
  std::cout << "Creating volume from image slices" << std::endl;
  itk::CreateVolumeFromSlices<UShort3DImageType>( imagePrefix, bbValues, imageSpacing, image3D);

  // Write out volume
  typedef itk::ImageFileWriter < UShort3DImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName( outputVolume );
  writer->SetInput( image3D );  

  try 
    { 
    writer->Update(); 
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 

  return true;
}
