/*=========================================================================

This program reads in a series of DICOM images, shifts the intensities
up by 1024, and then thresholds the intensities to clamp the lower
intensities to 0 (in case some artifacts caused intensities lower than
-1024).  It then writes the images out as 16 bit unsigned short PNGs
(or whatever other format the user specifies).  It also calculates and
writes out the min and max values after each stage for verification.

Inputs: Input DICOM images.
Outputs: Output images (expected to be 16 bit PNGs).

=========================================================================*/

#include "itkDICOMImageIO2Factory.h"
#include "itkDICOMImageIO2.h"
#include "itkImageSeriesReader.h"
#include "itkDICOMSeriesFileNames.h"
#include "itkShiftScaleImageFilter.h"
#include "itkImageSeriesWriter.h"
#include "itkNumericSeriesFileNames.h"

#include "itkMinimumMaximumImageCalculator.h"
#include "itkThresholdImageFilter.h"

int main(int ac, char* av[])
{

  if(ac < 4)
  {
    std::cerr << "Usage: " << av[0] << " DicomDirectory OutputDirectory FileSuffix\n";
    return 0;
  }

  typedef itk::Image< short,3 > InputImageType;
  typedef itk::Image< unsigned short, 2 > OutputImageType;

  itk::DICOMImageIO2::Pointer io = itk::DICOMImageIO2::New();

  // Get the DICOM filenames from the directory
  itk::DICOMSeriesFileNames::Pointer names = itk::DICOMSeriesFileNames::New();
  names->SetDirectory(av[1]);
  names->SetFileNameSortingOrderToSortByImageNumber();

  // Read the series of images
  typedef itk::ImageSeriesReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileNames(names->GetFileNames());
  reader->SetImageIO(io);
  std::cout << names;

  try
    {
    reader->Update();
    }
  catch (itk::ExceptionObject &ex)
    {
    std::cout << ex;
    return 0;
    }

  // Calculate the min and max of the input image.
  typedef itk::MinimumMaximumImageCalculator< InputImageType > CalculatorType;
  CalculatorType::Pointer calculator = CalculatorType::New();
  calculator->SetImage( reader->GetOutput() );
  calculator->Compute();
  std::cout << "Input image min: " << calculator->GetMinimum() << std::endl;
  std::cout << "Input image max: " << calculator->GetMaximum() << std::endl;
  
  // Shift the image so that there aren't any negative pixel values.
  typedef itk::ShiftScaleImageFilter< InputImageType, InputImageType > ShiftScaleFilterType;
  ShiftScaleFilterType::Pointer shifter = ShiftScaleFilterType::New();
  shifter->SetInput(reader->GetOutput());
  shifter->SetScale( 1 );
  shifter->SetShift( 1024 );

  try
    {
    shifter->Update();
    }
  catch (itk::ExceptionObject &ex)
    {
    std::cout << ex;
    return 0;
    }

  // Calculate the min and max of the shifted image.
  calculator->SetImage( shifter->GetOutput() );
  calculator->Compute();
  std::cout << "Shifter image min: " << calculator->GetMinimum() << std::endl;
  std::cout << "Shifter image max: " << calculator->GetMaximum() << std::endl; 
  
  // Threshold the image to get rid of any pixels that may still be
  // less than 0 (there shouldn't be any, but sometimes there are artifacts).
  typedef itk::ThresholdImageFilter< InputImageType > ThresholdType;
  ThresholdType::Pointer thresholder = ThresholdType::New();
  thresholder->SetInput( shifter->GetOutput() );
  thresholder->ThresholdBelow( 0 );
  thresholder->SetOutsideValue( 0 );

  try
    {
    thresholder->Update();
    }
  catch (itk::ExceptionObject &ex)
    {
    std::cout << ex;
    return 0;
    }

  // Calculate the min and max of the thresholded image.
  calculator->SetImage( thresholder->GetOutput() );
  calculator->Compute();
  std::cout << "Thresholder image min: " << calculator->GetMinimum() << std::endl;
  std::cout << "Thresholder image max: " << calculator->GetMaximum() << std::endl;


  // Write out the images
  itk::NumericSeriesFileNames::Pointer fit = itk::NumericSeriesFileNames::New();

  typedef  itk::ImageSeriesWriter<InputImageType,OutputImageType> WriterType; 
  WriterType::Pointer writer = WriterType::New();

  char format[4096];
  sprintf (format, "%s/im%%03d.%s", av[2], av[3]); 

  std::cout << "Format = " << format << std::endl;

  InputImageType::RegionType region = reader->GetOutput()->GetBufferedRegion();
  InputImageType::SizeType   size = region.GetSize();

  fit->SetStartIndex(0);
  fit->SetEndIndex(size[2]-1);
  fit->SetIncrementIndex(1);
  fit->SetSeriesFormat (format);

  writer->SetInput( thresholder->GetOutput() );
  writer->SetFileNames(  fit->GetFileNames() );

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << "Error while writing the series with SeriesFileNames generator" << std::endl;
    std::cerr << excp << std::endl;
    return 0;
    }
  std::cout << "Test with NumericSeriesFileNames PASSED !" << std::endl;
  
  return 1;

}
