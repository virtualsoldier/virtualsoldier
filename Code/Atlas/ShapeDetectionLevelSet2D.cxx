/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: ShapeDetectionLevelSet2D.cxx,v $
  Language:  C++
  Date:      $Date: 2005/01/21 02:16:58 $
  Version:   $Revision: 1.3 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

// itk Classes
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkExceptionObject.h"
#include "itkExtractImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkLaplacianImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkNumericSeriesFileNames.h"
#include "itkNumericTraits.h"
#include "itkResampleImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkShapeDetectionLevelSetImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkZeroCrossingImageFilter.h"


// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"


int main( int argc, char *argv[] )
{
  int numberOfParameters = 13;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " FeatureImage InitialLevelSetImage";
    std::cerr << " ImageInfoFile ";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << " ErodeRadius";
    std::cerr << std::endl;
    return 1;
    }

  // Save all arguments as local variables
  char * featureImagePrefix = argv[1];
  char * labelImagePrefix = argv[2];
  char * imageInfoFile = argv[3];
  char * masterFile = argv[4];
  char * boundingBoxesFile = argv[5];
  char * outputModel = argv[6];
  std::string anatomyName = argv[7];
  float propScaling = atof( argv[8] );
  float curvScaling = atof( argv[9] );
  int iterations = atoi( argv[10] );
  float conductance = atof( argv[11] );
  int erodeSize = atoi( argv[12] );


  // Image Typedefs
  typedef itk::Image< float, 2 > Float2DImageType;
  typedef itk::Image< unsigned short, 2 > UShort2DImageType;
  typedef itk::Image< unsigned char, 2 > UChar2DImageType;

  typedef itk::ImageFileReader< UShort2DImageType >  ReaderType;
//  typedef itk::LabelStatisticsImageFilter< Float2DImageType, Float2DImageType > LabelStatisticsFilterType;

  // Filters for the label image
  // This is used as the initial level set
  typedef itk::CastImageFilter< UShort2DImageType, Float2DImageType > CastImageFilterType;  
  typedef itk::BinaryThresholdImageFilter< Float2DImageType, Float2DImageType> ThresholdingFilterType1;
  typedef itk::BinaryBallStructuringElement< unsigned short, 2> KernelType;
  typedef itk::BinaryErodeImageFilter< Float2DImageType, Float2DImageType, KernelType>
    ErodeFilterType;

  // Filters for the feature image
  typedef itk::GradientAnisotropicDiffusionImageFilter< Float2DImageType, Float2DImageType> DiffusionFilterType;
  typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< Float2DImageType, Float2DImageType >  GaussianGradientFilterType;
  typedef itk::GradientMagnitudeImageFilter< Float2DImageType, Float2DImageType >  GradientFilterType;
  typedef itk::SigmoidImageFilter< Float2DImageType, Float2DImageType  >  SigmoidFilterType;
  typedef itk::LaplacianImageFilter< Float2DImageType, Float2DImageType > LaplacianFilterType;
  typedef itk::ShapeDetectionLevelSetImageFilter< Float2DImageType, Float2DImageType >    ShapeDetectionFilterType;
  typedef itk::BinaryThresholdImageFilter< Float2DImageType, Float2DImageType> ThresholdingFilterType2;


  // Instantiate the filters  
  Float2DImageType::Pointer speedImage = Float2DImageType::New();
//  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();
  UShort2DImageType::Pointer featureImage = UShort2DImageType::New();
  UShort2DImageType::Pointer labelImage = UShort2DImageType::New();

  // Filters for the label image
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();
  CastImageFilterType::Pointer castLabelToFloat = CastImageFilterType::New();
  ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();

  // Filters for the feature image
  CastImageFilterType::Pointer castFeatureToFloat = CastImageFilterType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();
  GaussianGradientFilterType::Pointer gaussianGradientMagnitude = GaussianGradientFilterType::New();
  GradientFilterType::Pointer gradientMagnitude = GradientFilterType::New();
  SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();
  LaplacianFilterType::Pointer laplacian = LaplacianFilterType::New();
  ShapeDetectionFilterType::Pointer shapeDetection = ShapeDetectionFilterType::New();                              
  ThresholdingFilterType2::Pointer thresholder2 = ThresholdingFilterType2::New();

  // Find the anatomy info
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int borderSize = 10;
  int inputImageSize[3];
  double inputImageSpacing[3];
  if (! FindAnatomyInfo(featureImagePrefix,masterFile,boundingBoxesFile,anatomyName,anatomyLabel,bbValues,borderSize,inputImageSize, inputImageSpacing) )
    {
    return 0;
    }

  std::cout << "Anatomy label: " << anatomyLabel << std::endl;
  std::cout << "minX: " << bbValues[0] << std::endl;
  std::cout << "maxX: " << bbValues[1] << std::endl;
  std::cout << "minY: " << bbValues[2] << std::endl;
  std::cout << "maxY: " << bbValues[3] << std::endl;
  std::cout << "minZ: " << bbValues[4] << std::endl;
  std::cout << "maxZ: " << bbValues[5] << std::endl;
  std::cout << "minSlice: " << bbValues[6] << std::endl;
  std::cout << "maxSlice: " << bbValues[7] << std::endl;


  // Create images that are cropped according to the bounding box values
  itk::CreateVolumeFromSlices<UShort2DImageType>( featureImagePrefix, bbValues, inputImageSpacing, featureImage);
  itk::CreateVolumeFromSlices<UShort2DImageType>( labelImagePrefix, bbValues, inputImageSpacing, labelImage);


  // Set up the pipeline
//  labelStatistics->SetInput( cropFeatureImage->GetOutput() );
//  labelStatistics->SetLabelInput( cropLabelImage->GetOutput() );

  castLabelToFloat->SetInput ( labelImage );
  thresholder1->SetInput( castLabelToFloat->GetOutput() );
  erodeFilter->SetInput( thresholder1->GetOutput() );
  shapeDetection->SetInput( erodeFilter->GetOutput() );

  castFeatureToFloat->SetInput ( featureImage );
  diffusion->SetInput ( castFeatureToFloat->GetOutput() );
  gradientMagnitude->SetInput ( diffusion->GetOutput() );
  gaussianGradientMagnitude->SetInput ( diffusion->GetOutput() );
  shapeDetection->SetFeatureImage( speedImage );
  laplacian->SetInput( diffusion->GetOutput() );
//  shapeDetection->SetFeatureImage( laplacian->GetOutput() );
  thresholder2->SetInput( shapeDetection->GetOutput() );


  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);

  // Erode the initial level set (labeled images)
  // Create the structuring element
  if (erodeSize < 0)
    {
    // dilate instead of erode
    erodeFilter->SetErodeValue( 255 );
    erodeSize = -erodeSize;
    }
  else
    {
    erodeFilter->SetErodeValue( 0 );
    }

  KernelType ball;
  KernelType::SizeType ballSize;
  ballSize[0] = erodeSize; // / labelReader->GetOutput()->GetSpacing()[0];
  ballSize[1] = erodeSize; // / labelReader->GetOutput()->GetSpacing()[1];
  ball.SetRadius(ballSize);
  ball.CreateStructuringElement();
  
  std::cout << "Ball size: " << ballSize << std::endl;
  
  // Connect the input image
  erodeFilter->SetKernel( ball );
  try
    {
    erodeFilter->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  // Set the parameters for the label statistics filter.
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  try
    {
    //labelStatistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

//   float labelMin = labelStatistics->GetMinimum(anatomyLabel);
//   float labelMax = labelStatistics->GetMaximum(anatomyLabel);
//   float labelMean = labelStatistics->GetMean(anatomyLabel);
//   float labelSigma = labelStatistics->GetSigma(anatomyLabel);

//   std::cout << "\tMinimum = " << labelMin  << std::endl;
//   std::cout << "\tMaximum = " << labelMax << std::endl;
//   std::cout << "\tMean = " << labelMean << std::endl;
//   std::cout << "\tSigma = " << labelSigma << std::endl;

  // Set the smoothing parameters
  diffusion->SetNumberOfIterations( iterations );
  diffusion->SetConductanceParameter( conductances );
  diffusion->SetTimeStep( 0.02 );
  diffusion->UseImageSpacingOn();
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;


  // Set the gradient magnitude parameters
  const double sigma = 1;
  gaussianGradientMagnitude->SetSigma(  sigma  );
  try
    {
    gradientMagnitude->Update();    
    gaussianGradientMagnitude->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Create a speed image
  Float2DImageType::RegionType imageRegion;  
  imageRegion.SetSize(castFeatureToFloat->GetOutput()->GetRequestedRegion().GetSize());
  imageRegion.SetIndex(castFeatureToFloat->GetOutput()->GetRequestedRegion().GetIndex());
  
  speedImage->SetRegions( imageRegion );
  speedImage->Allocate();
  speedImage->FillBuffer(0);

  std::cout << "Speed image spacing: " << speedImage->GetSpacing() << std::endl;
  std::cout << "Speed image size: " << speedImage->GetRequestedRegion().GetSize() << std::endl;


  // Set up the input region iterator for the speed image
  typedef itk::ImageRegionIterator < Float2DImageType > Float3DIteratorType;
  typedef itk::ImageRegionConstIterator < Float2DImageType > Float3DConstIteratorType;

  Float3DIteratorType inputIt ( gradientMagnitude->GetOutput(), imageRegion );
  Float3DIteratorType outputIt( speedImage, imageRegion );

  float pixelValue;

  for ( inputIt.GoToBegin(), outputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt, ++outputIt)
    {
    pixelValue = inputIt.Get();
    outputIt.Set( 100/(1+pixelValue) );
//    outputIt.Set( exp(-pixelValue) );
    }


  std::cout << "Line: " << __LINE__ << std::endl;
  try
    {
    laplacian->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  itk::FileDumper<Float2DImageType> dumpLaplacian(laplacian->GetOutput(), "laplacian");


  // Set the parameters for the Shape Detection Segmentation
  shapeDetection->UseImageSpacingOn();
  shapeDetection->SetMaximumRMSError( 0.02 );
  shapeDetection->SetNumberOfIterations( 400 );
  shapeDetection->SetIsoSurfaceValue(127.5);
  shapeDetection->SetPropagationScaling( propScaling );
  shapeDetection->SetCurvatureScaling( curvScaling );
//  FilterWatcher shapeDetectionWatcher(shapeDetection);


  // Show the evolution of the level set
  itk::ShowLevelSetEvolution<Float2DImageType, ShapeDetectionFilterType> levelSetEvolution(diffusion->GetOutput(), shapeDetection);


  // Set the parameters for Thresholder2
  // Thresholder2 is used to threshold the image after the level set
  // segmentation is complete.
  //  thresholder2->SetLowerThreshold( 0.0 );
  thresholder2->SetLowerThreshold( -1000.0 );
  thresholder2->SetUpperThreshold(     0.0 );
  thresholder2->SetOutsideValue( 255 );
  thresholder2->SetInsideValue( 0 );
  try
    {
    thresholder2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Size: " << thresholder1->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Size: " << thresholder2->GetOutput()->GetRequestedRegion().GetSize() << std::endl;


  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << shapeDetection->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << shapeDetection->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << shapeDetection->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << shapeDetection->GetRMSChange() << std::endl;


  // Just for testing
  typedef itk::ImageFileWriter< Float2DImageType >  WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName("shapeDetectionSpeedImage.mhd");
  writer->SetInput ( shapeDetection->GetSpeedImage() );
  writer->Update();

  
  // Dump useful images to file
  itk::FileDumper<Float2DImageType> dumpDiffusion(diffusion->GetOutput(), "diffusion");
  itk::FileDumper<Float2DImageType> dumpOriginal(castFeatureToFloat->GetOutput(), "original");
  itk::FileDumper<Float2DImageType> dumpSpeed(shapeDetection->GetSpeedImage(), "speed");
  itk::FileDumper<Float2DImageType> dumpLevelSet(shapeDetection->GetOutput(), "levelset");
  itk::FileDumper<Float2DImageType> dumpThresholder(thresholder1->GetOutput(), "labelThresholder");
  itk::FileDumper<Float2DImageType> dumpThresholder2(thresholder2->GetOutput(), "levelSetThresholder");
  itk::FileDumper<Float2DImageType> dumpErode(erodeFilter->GetOutput(), "erode");

  return 0;
}
