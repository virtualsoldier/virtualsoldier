/*=========================================================================

This program reads in all of the slices of the labeled data, and
counts the number of isolated pixels.  Isolated pixels are defined as
those that have a value different from all of its 26 neighbors.  It
gathers this data in a histogram by using neighborhood iterators and
then writes the data to file.

Inputs: Label image prefix.
Outputs: Isolated pixels file.

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkNumericSeriesFileNames.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkArray.h"

#include <fstream>
#include <iostream>
#include <string>
#include <set>
#include <vector>


// This function takes three neighborhood iterators, iterates over each
// of them, and increments a count variable if any of the neighborhood
// pixels are equal to the center pixel of the middle slice.  If the
// final count is 1, then the pixel is isolated, and the function
// returns 1.  Otherwise, it returns zero.
unsigned short IsIsolatedPixel(itk::ConstNeighborhoodIterator< itk::Image< unsigned short,2 > > itPrev, 
                               itk::ConstNeighborhoodIterator< itk::Image< unsigned short,2 > > itCurr,
                               itk::ConstNeighborhoodIterator< itk::Image< unsigned short,2 > > itNext,
                               bool threeSlices, unsigned short anatomyLabel);

int main( int argc, char *argv[] )
{
  if (argc < 3)
    {
    std::cout << "Usage: labelImagePrefix isolatedPixelsFile" << std::endl;
    return 0;
    }  

  // Test the output file
  std::ofstream fout ( argv[2] );
  if (!fout)
    {
    std::cout << "Could not open isolated pixels file" << std::endl;
    return 0;
    }

  typedef   unsigned short   PixelType;
  typedef itk::Image< PixelType,  2 >   ImageType;
  typedef itk::ImageFileReader< ImageType >  ReaderType;
  typedef itk::NumericSeriesFileNames NameGeneratorType;
  typedef itk::ConstNeighborhoodIterator< 
  ImageType > ConstNeighborhoodIteratorType;
  typedef itk::NeighborhoodIterator< 
  ImageType > NeighborhoodIteratorType;
  
  typedef itk::NeighborhoodAlgorithm::ImageBoundaryFacesCalculator<
  ImageType > FaceCalculatorType;
  
  typedef itk::Array < PixelType > ArrayType;

  ReaderType::Pointer reader = ReaderType::New();
  ImageType::Pointer prevImage;
  ImageType::Pointer currImage;
  ImageType::Pointer nextImage;
  NameGeneratorType::Pointer inputImageFileGenerator =
    NameGeneratorType::New();
  NameGeneratorType::Pointer outputImageFileGenerator =
    NameGeneratorType::New();

  FaceCalculatorType faceCalculator;
  FaceCalculatorType::FaceListType faceList;
  FaceCalculatorType::FaceListType::iterator fit;
 
  int numberOfSlices = 411;
  int sliceOffset = 1253;

  std::cout << "NumberOfSlices: " << numberOfSlices << std::endl;
  
  // Generate the image file names
  inputImageFileGenerator->SetStartIndex( sliceOffset );
  inputImageFileGenerator->SetEndIndex ( numberOfSlices + sliceOffset - 1 );
  inputImageFileGenerator->SetIncrementIndex ( 1 );
  inputImageFileGenerator->SetSeriesFormat ( argv[1] );

  std::cout << inputImageFileGenerator->GetFileNames()[0] << std::endl;

  std::vector <std::string > inputImageNames =
    inputImageFileGenerator->GetFileNames();
  std::vector <std::string > outputImageNames =
    outputImageFileGenerator->GetFileNames();
  
  ConstNeighborhoodIteratorType itPrev;
  ConstNeighborhoodIteratorType itCurr;  
  ConstNeighborhoodIteratorType itNext;

  int slice;

  // Initialize a histogram array.  The bins of this histogram
  // correspond to the labels of the anatomies, and the number of
  // counts in each bin will correspond to the number of isolated
  // pixels that are found with that anatomy label.
  unsigned short histo[512];
  for (int i = 0; i < 512; i++)
    {
    histo[i] = 0;
    }

  std::vector <std::string> ::iterator iinit =
    inputImageNames.begin();
  std::vector <std::string> ::iterator oinit =
    outputImageNames.begin();

  // Set the previous image to be the first slice
  reader->SetFileName(inputImageNames[0].c_str());

  try 
    { 
    prevImage = reader->GetOutput();
    prevImage->Update();
    prevImage->DisconnectPipeline();
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 

  // Set the current image to be the second slice
  reader->SetFileName(inputImageNames[1].c_str());

  try 
    { 
    currImage = reader->GetOutput();
    currImage->Update();
    currImage->DisconnectPipeline();
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 

  std::cout << "File name: " << reader->GetFileName() << std::endl;


  // The radius of the neighborhood should be 1
  NeighborhoodIteratorType::RadiusType radius;
  radius.Fill(1);

  faceList = faceCalculator( prevImage, 
                             prevImage->GetRequestedRegion(), 
                             radius );

  // Process the slices
  iinit = inputImageNames.begin();
  oinit = outputImageNames.begin();

  // Increment input image pointer so that it points to the
  // third image.
  ++iinit;
  ++iinit;
  // Increment output image pointer so that is points to the second image.
  ++oinit;

  // Loop through slices
  while (iinit != inputImageNames.end())
    {
    slice = iinit - inputImageNames.begin() - 1;

    // Read the next image
    try 
      { 
      reader->SetFileName ( (*iinit).c_str() );
      nextImage = reader->GetOutput();
      nextImage->Update();
      nextImage->DisconnectPipeline();
      } 
    catch( itk::ExceptionObject & err ) 
      { 
      std::cout << "ExceptionObject caught !" << std::endl; 
      std::cout << err << std::endl; 
      return -1;
      } 

    std::cout << "Processing middle slice: " << slice << std::endl;


    // Loop through each face
    for ( fit=faceList.begin();
          fit != faceList.end(); ++fit)
      {
      // These are neighborhood iterators.
      itPrev = ConstNeighborhoodIteratorType( radius, prevImage , *fit );
      itCurr = ConstNeighborhoodIteratorType( radius, currImage , *fit );
      itNext = ConstNeighborhoodIteratorType( radius, nextImage , *fit );

      //std::cout << "Processing middle face: " << fit-faceList.begin() << std::endl;
      //std::cout << "Processing middle slice: " << slice << std::endl;


      // Loop through each neighborhood of slices 1:n-1
      for (itPrev.GoToBegin(), itCurr.GoToBegin(), itNext.GoToBegin();
           !itCurr.IsAtEnd(); ++itPrev, ++itCurr, ++itNext)
        {
        ConstNeighborhoodIteratorType::PixelType anatomyLabel = itCurr.GetCenterPixel();

        histo[anatomyLabel] = histo[anatomyLabel] + IsIsolatedPixel(itPrev,itCurr,itNext,1,anatomyLabel);
        }


      // Do the processing for the first slice ("slice" is
      // pointing to the second slice).
      if (slice == 1)
        {
        //std::cout << "Processing first slice: " << slice-1 << std::endl;

        for (itPrev.GoToBegin(), itCurr.GoToBegin(); !itCurr.IsAtEnd(); 
             ++itPrev, ++itCurr) 
          {
          ConstNeighborhoodIteratorType::PixelType anatomyLabel = itPrev.GetCenterPixel();

          // The third parameter is a dummy variable since the fourth
          // parameter is 0.
          histo[anatomyLabel] = histo[anatomyLabel] + IsIsolatedPixel(itPrev,itCurr,itCurr,0,anatomyLabel);
          }
        }

      // Do the processing for the last slice ("slice" is pointing
      // to the second last slice)
      if (slice == numberOfSlices-2)
        {
        //std::cout << "Processing last slice: " << slice+1 << std::endl;
       
        for (itCurr.GoToBegin(), itNext.GoToBegin(); 
             !itCurr.IsAtEnd(); ++itCurr, ++itNext)
          {  
          ConstNeighborhoodIteratorType::PixelType anatomyLabel = itNext.GetCenterPixel();
       
          // Treat the next image as the current and the current as
          // the previous in finding the correct label.
          // The third parameter is a dummy variable since the fourth
          // parameter is 0.
          histo[anatomyLabel] = histo[anatomyLabel] + IsIsolatedPixel(itCurr,itNext,itNext,0,anatomyLabel);
          }
        }
      }

    prevImage = currImage;
    currImage = nextImage;
    ++iinit;
    ++oinit;
    }
  
  // Print the values to file  
  fout << "Label, Number of isolated pixels" << std::endl;
  
  // Write out the isolated pixels
  for (int i = 0; i < 512; i++)
    {
    fout << i << "," << histo[i] << std::endl;
    }
  fout.close();

  return 0;
}

unsigned short
IsIsolatedPixel(itk::ConstNeighborhoodIterator< itk::Image< unsigned
                short,2 > > itPrev, 
                itk::ConstNeighborhoodIterator< itk::Image< unsigned short,2 > > itCurr,
                itk::ConstNeighborhoodIterator< itk::Image< unsigned short,2 > > itNext,
                bool threeSlices, unsigned short anatomyLabel)
{

  int count = 0;
  
  for (unsigned int indexValue = 0; indexValue < 9; indexValue++)
    {
    if (itPrev.GetPixel(indexValue) == anatomyLabel)
      {
      count++;
      }
    if (itCurr.GetPixel(indexValue) == anatomyLabel)
      {
      count++;
      }
    if (threeSlices)
      {
      if (itNext.GetPixel(indexValue) == anatomyLabel)
        {
        count++;
        }
      }
    }
// Count should be at least one because the center pixel is the
// same as anatomyLabel
// If count is only one, then the pixel is isolated.
  if (count == 1)
    {
    return 1;
    }
  else
    {
    return 0;
    }
}

