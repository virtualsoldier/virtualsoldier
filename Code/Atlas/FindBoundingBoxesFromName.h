/*=========================================================================

This program searches through the bounding boxes file for a given
anatomy name and returns its corresponding bounding boxes.

Inputs: Bounding boxes file, anatomy name. 
Outputs: Bounding box values.

=========================================================================*/

#ifndef _FindBoundingBoxesFromName_h
#define _FindBoundingBoxesFromName_h

#include <string>
#include <vector>

bool FindBoundingBoxesFromName(char * boundingBoxesFile, std::string anatomyName, std::vector <int> & bbValues);

#endif
