/*=========================================================================

  Program:   DARPA Virtual Soldier
  Module:    $RCSfile: ShapeDetectionLevelSet3D.cxx,v $
  Language:  C++
  Date:      $Date: 2004/10/28 14:00:40 $
  Version:   $Revision: 1.5 $

  Copyright (c) General Electric Corporation

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.

=========================================================================*/

// itk Classes
#include "itkImage.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkShapeDetectionLevelSetImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkZeroCrossingImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkNumericTraits.h"
#include "itkVTKImageExport.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkExceptionObject.h"
#include "itkExtractImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"


// vtk Classes
#include "vtkImageImport.h"
#include "vtkMarchingCubes.h"
#include "vtkPolyDataWriter.h"
#include "vtkImageChangeInformation.h"
#include "vtkImageFlip.h"

// C++ Classes
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// vtk / itk classes
#include "vtk2itk2vtk.h"

// Atlas classes
#include "FindAnatomyInfo.h"
#include "CreateVolumeFromSlices.h"

// Helper classes
#include "DumpImages.h"
#include "ShowLevelSetEvolution.h"


int main( int argc, char *argv[] )
{
  int numberOfParameters = 11;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << " FeatureImagePrefix InitialLevelSetImagePrefix";
    std::cerr << " MasterAnatomyFile BoundingBoxesFile OutputModel";
    std::cerr << " AnatomyName";
    std::cerr << " PropagationScaling";
    std::cerr << " CurvatureScaling";
    std::cerr << " DiffusionIterations";
    std::cerr << " DiffusionConductance";
    std::cerr << std::endl;
    return 1;
    }

  // Image Typedefs
  typedef itk::Image< float, 3 > Float3DImageType;
  typedef itk::Image< unsigned short, 3 > UShort3DImageType;

  typedef itk::LabelStatisticsImageFilter< UShort3DImageType, UShort3DImageType > LabelStatisticsFilterType;

  // Filters for the label image
  // This is used as the initial level set
  typedef itk::BinaryThresholdImageFilter< UShort3DImageType, UShort3DImageType> ThresholdingFilterType1;
  typedef itk::ResampleImageFilter< UShort3DImageType, UShort3DImageType> ResampleType;
  typedef itk::NearestNeighborInterpolateImageFunction< UShort3DImageType, double >  NearestNeighborInterpolatorType;
  typedef itk::BinaryBallStructuringElement< unsigned short, 3> KernelType;

  // Filters for the feature image
  typedef itk::LinearInterpolateImageFunction< UShort3DImageType, double >  LinearInterpolatorType;
  typedef itk::CastImageFilter< UShort3DImageType, Float3DImageType > CastImageFilterType;  
  typedef itk::GradientAnisotropicDiffusionImageFilter< Float3DImageType, Float3DImageType> DiffusionFilterType;
  typedef itk::CurvatureAnisotropicDiffusionImageFilter< Float3DImageType, Float3DImageType> CurvatureDiffusionFilterType;
  typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< Float3DImageType, Float3DImageType >  GaussianGradientFilterType;
  typedef itk::GradientMagnitudeImageFilter< Float3DImageType, Float3DImageType >  GradientFilterType;
  typedef itk::SigmoidImageFilter< Float3DImageType, Float3DImageType  >  SigmoidFilterType;
  typedef itk::ShapeDetectionLevelSetImageFilter< UShort3DImageType, Float3DImageType >    ShapeDetectionFilterType;
  typedef itk::BinaryThresholdImageFilter< Float3DImageType, UShort3DImageType> ThresholdingFilterType2;

  // Filters for creating the vtk model
  typedef itk::VTKImageExport< Float3DImageType > ImageExportType;

  // Instantiate the filters  
  UShort3DImageType::Pointer featureImage = UShort3DImageType::New();
  UShort3DImageType::Pointer labelImage = UShort3DImageType::New();
  Float3DImageType::Pointer speedImage = Float3DImageType::New();
  LabelStatisticsFilterType::Pointer labelStatistics = LabelStatisticsFilterType::New();

  // Filters for the label image
  ThresholdingFilterType1::Pointer thresholder1 = ThresholdingFilterType1::New();
  ResampleType::Pointer labelResampler = ResampleType::New();
  NearestNeighborInterpolatorType::Pointer nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();

  // Filters for the feature image
  ResampleType::Pointer featureResampler = ResampleType::New();
  LinearInterpolatorType::Pointer linearInterpolator = LinearInterpolatorType::New();
  CastImageFilterType::Pointer castToFloat = CastImageFilterType::New();
  DiffusionFilterType::Pointer diffusion = DiffusionFilterType::New();
  CurvatureDiffusionFilterType::Pointer curvatureDiffusion = CurvatureDiffusionFilterType::New();
  GaussianGradientFilterType::Pointer gaussianGradientMagnitude = GaussianGradientFilterType::New();
  GradientFilterType::Pointer gradientMagnitude = GradientFilterType::New();
  SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();
  ShapeDetectionFilterType::Pointer shapeDetection = ShapeDetectionFilterType::New();                              
  ThresholdingFilterType2::Pointer thresholder2 = ThresholdingFilterType2::New();

  // Filters for creating the vtk model
  ImageExportType::Pointer itkExporter = ImageExportType::New();
  vtkImageImport *vtkImporter = vtkImageImport::New();
  vtkImageFlip *flipper = vtkImageFlip::New();
  vtkImageChangeInformation *changeInformation = vtkImageChangeInformation::New();
  vtkMarchingCubes *cubes = vtkMarchingCubes::New();
  vtkPolyDataWriter *polyDataWriter = vtkPolyDataWriter::New();

  // Find the anatomy info
  std::string anatomyName;
  anatomyName = std::string( argv[6] ); 
  int anatomyLabel;
  std::vector < int > bbValues(8);
  int borderSize = 10;
  int sliceOffset = 1253;
  int numberOfSlices = 411;
  FindAnatomyInfo(argv[1],argv[3],argv[4],argv[6],anatomyLabel,bbValues,borderSize,sliceOffset,numberOfSlices );

  // Break up the vector for name convenience
  int minX = bbValues[0] ;
  int maxX = bbValues[1] ;
  int minY = bbValues[2] ;
  int maxY = bbValues[3] ;
  int minZ = bbValues[4] ;
  int maxZ = bbValues[5] ;
  int minSlice = bbValues[6] ;
  int maxSlice = bbValues[7] ;


  // Either read the 3D volumes from file or create them
  if (argc > numberOfParameters)
    {
    // Read the images from file
    typedef itk::ImageFileReader< UShort3DImageType >  ReaderType;
    ReaderType::Pointer featureReader = ReaderType::New();
    ReaderType::Pointer labelReader = ReaderType::New();
    featureReader->SetFileName(argv[numberOfParameters]);
    labelReader->SetFileName(argv[numberOfParameters+1]);
    featureImage = featureReader->GetOutput();
    labelImage = labelReader->GetOutput();

    try
      {
      featureImage->Update();
      labelImage->Update();
      }
    catch( itk::ExceptionObject & excep )
      {
      std::cerr << "Exception caught !" << std::endl;
      std::cerr << excep << std::endl;
      }
    }
  else
    {
    // Create 3D volumes of the input slices according to the bounding
    // box values
    std::cout << "Creating feature volume from feature image slices" << std::endl;
    itk::CreateVolumeFromSlices<UShort3DImageType>(argv[1], bbValues, featureImage);
    std::cout << "Creating label volume from label image slices" << std::endl;
    itk::CreateVolumeFromSlices<UShort3DImageType>(argv[2], bbValues, labelImage);
    }

  std::cout << "Feature image spacing: " << featureImage->GetSpacing() << std::endl;
  std::cout << "Feature image origin: " << featureImage->GetOrigin() << std::endl;
  std::cout << "Feature image size: " << featureImage->GetRequestedRegion().GetSize() << std::endl;



  // Set up the pipeline
  labelStatistics->SetInput( featureImage );
  labelStatistics->SetLabelInput( labelImage );

  labelResampler->SetInput ( labelImage );
  thresholder1->SetInput( labelResampler->GetOutput() );
  shapeDetection->SetInput( thresholder1->GetOutput() );

  featureResampler->SetInput ( featureImage );
  castToFloat->SetInput ( featureResampler->GetOutput() );
  diffusion->SetInput ( castToFloat->GetOutput() );
  curvatureDiffusion->SetInput ( castToFloat->GetOutput() );
  gradientMagnitude->SetInput ( curvatureDiffusion->GetOutput() );
  gaussianGradientMagnitude->SetInput ( curvatureDiffusion->GetOutput() );
  //gradientMagnitude->SetInput ( diffusion->GetOutput() );
  sigmoid->SetInput ( gradientMagnitude->GetOutput() );
  shapeDetection->SetFeatureImage( speedImage );
  //shapeDetection->SetFeatureImage( sigmoid->GetOutput());
  thresholder2->SetInput( shapeDetection->GetOutput() );

  itkExporter->SetInput( shapeDetection->GetOutput() );
  flipper->SetInput( vtkImporter->GetOutput() ); 
  changeInformation->SetInput (flipper->GetOutput() );
  cubes->SetInput( changeInformation->GetOutput() );
  polyDataWriter->SetInput( cubes->GetOutput() );


  // Resample the image to have isotropic spacing by downsampling the
  // X and Y dimensions.
  UShort3DImageType::SpacingType inputSpacing;
  inputSpacing[0] = featureImage->GetSpacing()[0];
  inputSpacing[1] = featureImage->GetSpacing()[1];
  inputSpacing[2] = featureImage->GetSpacing()[2];

  UShort3DImageType::SizeType inputSize;
  inputSize[0] = featureImage->GetRequestedRegion().GetSize()[0];
  inputSize[1] = featureImage->GetRequestedRegion().GetSize()[1];
  inputSize[2] = featureImage->GetRequestedRegion().GetSize()[2];

  UShort3DImageType::SpacingType outputSpacing;
  outputSpacing[0] = inputSpacing[2];
  outputSpacing[1] = inputSpacing[2];
  outputSpacing[2] = inputSpacing[2];

  // The output size should be the input size times the output spacing
  // divided by the input z spacing.
  UShort3DImageType::SizeType outputSize;
  outputSize[0] = inputSize[0] * inputSpacing[0]/outputSpacing[0];  // number of pixels along X
  outputSize[1] = inputSize[1] * inputSpacing[1]/outputSpacing[1];  // number of pixels along Y
  outputSize[2] = inputSize[2];

  featureResampler->SetOutputSpacing( outputSpacing );
  featureResampler->SetOutputOrigin ( featureImage->GetOrigin() );
  featureResampler->SetSize( outputSize );
  featureResampler->SetInterpolator( linearInterpolator );
  
  // Label image resampled the same way
  labelResampler->SetOutputSpacing( outputSpacing );
  labelResampler->SetOutputOrigin ( featureImage->GetOrigin() );
  labelResampler->SetSize( outputSize );
  labelResampler->SetInterpolator( nearestNeighborInterpolator );

  // Update the resamplers
  try
    {
    featureResampler->Update();
    labelResampler->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "New feature image spacing: " << featureResampler->GetOutput()->GetSpacing() << std::endl;
  std::cout << "New feature image origin: " << featureResampler->GetOutput()->GetOrigin() << std::endl;
  std::cout << "New feature image size: " << featureResampler->GetOutput()->GetRequestedRegion().GetSize() << std::endl;


  // Set the parameters for Thresholder1
  // Thresholder1 is used to create a binary image of the relabeled
  // image where the inside pixels are those belonging to the anatomy
  // label passed in.
  thresholder1->SetInsideValue (0);
  thresholder1->SetOutsideValue (255);
  thresholder1->SetLowerThreshold (anatomyLabel);
  thresholder1->SetUpperThreshold (anatomyLabel);

  // Set the parameters for the label statistics filter.
  // This will calculate the statistics of the pixel values in the
  // feature image that are labeled as part of the anatomy in the
  // labeled image.
  // The mean and standard deviation of these values are used to give
  // appropriate lower and upper threshold values for the 
  // ThresholdSegmentationLevelSetImageFilter.
  try
    {
    labelStatistics->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  float labelMin = labelStatistics->GetMinimum(anatomyLabel);
  float labelMax = labelStatistics->GetMaximum(anatomyLabel);
  float labelMean = labelStatistics->GetMean(anatomyLabel);
  float labelSigma = labelStatistics->GetSigma(anatomyLabel);

  std::cout << "\tMinimum = " << labelMin  << std::endl;
  std::cout << "\tMaximum = " << labelMax << std::endl;
  std::cout << "\tMean = " << labelMean << std::endl;
  std::cout << "\tSigma = " << labelSigma << std::endl;

  // Set the smoothing parameters
  diffusion->SetNumberOfIterations( atof (argv[9]) );
  diffusion->SetConductanceParameter( atof (argv[10]) );
  diffusion->SetTimeStep( 0.0625 );
  diffusion->UseImageSpacingOn();
  try
    {
    diffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "iterations: " << diffusion->GetNumberOfIterations() << std::endl;
  std::cout << "conductance: " << diffusion->GetConductanceParameter() << std::endl;

  // Set the curvature diffusion parameters
  curvatureDiffusion->SetNumberOfIterations( atof (argv[9]) );
  curvatureDiffusion->SetConductanceParameter( atof (argv[10]) );
  curvatureDiffusion->SetTimeStep( 0.0625 );
  curvatureDiffusion->UseImageSpacingOn();
  try
    {
    curvatureDiffusion->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  // Set the gradient magnitude parameters
  const double sigma = 1;
  gaussianGradientMagnitude->SetSigma(  sigma  );
  try
    {
    gradientMagnitude->Update();    
    gaussianGradientMagnitude->Update();    
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Create a speed image
  Float3DImageType::RegionType imageRegion;  
  imageRegion.SetSize(featureResampler->GetOutput()->GetRequestedRegion().GetSize());
  imageRegion.SetIndex(featureResampler->GetOutput()->GetRequestedRegion().GetIndex());
  
  speedImage->SetRegions( imageRegion );
  speedImage->Allocate();
  speedImage->FillBuffer(0);

  std::cout << "Speed image spacing: " << speedImage->GetSpacing() << std::endl;
  std::cout << "Speed image size: " << speedImage->GetRequestedRegion().GetSize() << std::endl;


  // Set up the input region iterator for the speed image
  typedef itk::ImageRegionIterator < Float3DImageType > Float3DIteratorType;
  typedef itk::ImageRegionConstIterator < Float3DImageType > Float3DConstIteratorType;

  Float3DIteratorType inputIt ( gradientMagnitude->GetOutput(), imageRegion );
  Float3DIteratorType outputIt( speedImage, imageRegion );

  float pixelValue;

  for ( inputIt.GoToBegin(), outputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt, ++outputIt)
    {
    pixelValue = inputIt.Get();
    outputIt.Set( 1/(1+pixelValue) );
//    outputIt.Set( exp(-pixelValue) );
    }



  // Set up the sigmoid parameters
  sigmoid->SetOutputMinimum(  0.0  );
  sigmoid->SetOutputMaximum(  1.0  );
  const double alpha = -0.3;
  const double beta  = 2.0;
  sigmoid->SetAlpha( alpha );
  sigmoid->SetBeta( beta );

  // Set the parameters for the Shape Detection Segmentation
  shapeDetection->UseImageSpacingOn();
  shapeDetection->SetMaximumRMSError( 0.002 );
  shapeDetection->SetNumberOfIterations( 400 );
  shapeDetection->SetIsoSurfaceValue(127.5);
  shapeDetection->SetPropagationScaling( atof(argv[7]) );
  shapeDetection->SetCurvatureScaling( atof(argv[8]) );

  // Show the evolution of the level set
  // This must be done before the level set filter is updated
  itk::ShowLevelSetEvolution<Float3DImageType,ShapeDetectionFilterType> levelSetEvolution(diffusion->GetOutput(),shapeDetection);


  // Set the parameters for Thresholder2
  // Thresholder2 is used to threshold the image after the level set
  // segmentation is complete.
  //  thresholder2->SetLowerThreshold( 0.0 );
  thresholder2->SetLowerThreshold( -1000.0 );
  thresholder2->SetUpperThreshold(     0.0 );
  thresholder2->SetOutsideValue( 255 );
  thresholder2->SetInsideValue( 0 );
  try
    {
    thresholder2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Line: " << __LINE__ << std::endl;
  std::cout << "Size: " << thresholder1->GetOutput()->GetRequestedRegion().GetSize() << std::endl;
  std::cout << "Size: " << thresholder2->GetOutput()->GetRequestedRegion().GetSize() << std::endl;

  // Find the volume of the binary image of the initial relabeled data
  // and the binary image of the final relabeled data
  typedef itk::ImageRegionConstIterator< UShort3DImageType > UShort3DConstIteratorType;
  UShort3DConstIteratorType inputIt1( thresholder1->GetOutput(), thresholder1->GetOutput()->GetRequestedRegion() );
  UShort3DConstIteratorType inputIt2( thresholder2->GetOutput(), thresholder2->GetOutput()->GetRequestedRegion() );
  
  int initVolume = 0;
  int finalVolume = 0;
  for ( inputIt1.GoToBegin(), inputIt2.GoToBegin(); !inputIt1.IsAtEnd(); ++inputIt1, ++inputIt2)
    {
    if (inputIt1.Get() == 0)
      initVolume++;
    if (inputIt2.Get() == 0)
      finalVolume++;
    }
  
  std::cout << "Initial volume: " << initVolume << std::endl;
  std::cout << "Final volume: " << finalVolume << std::endl;




  // Change from itk to vtk
  ConnectPipelines(itkExporter, vtkImporter);

  // Flip the image around the y axis so that it lines up with models
  // created by vtk.
  flipper->SetFilteredAxis (1);

  // Change the image spacing
  vtkFloatingPointType newSpacing[3] = {featureImage->GetSpacing()[0], featureImage->GetSpacing()[1], featureImage->GetSpacing()[2]};
  vtkFloatingPointType newOrigin[3] = {minX * newSpacing[0], 1349*newSpacing[1] - (minY*newSpacing[1] + outputSize[1]-1), minSlice * newSpacing[2]};
  changeInformation->SetOutputOrigin ( newOrigin );

  // Marching cubes
  cubes->SetValue(0,0);
  cubes->ComputeNormalsOn();
  cubes->ComputeScalarsOff();

  // VTK model writer
  polyDataWriter->SetFileName( argv[5] );
  polyDataWriter->SetFileTypeToBinary();
  polyDataWriter->Modified();
  polyDataWriter->Write();

  std::cout << std::endl;
  std::cout << "Max. no. iterations: " << shapeDetection->GetNumberOfIterations() << std::endl;
  std::cout << "Max. RMS error: " << shapeDetection->GetMaximumRMSError() << std::endl;
  std::cout << std::endl;
  std::cout << "No. elapsed iterations: " << shapeDetection->GetElapsedIterations() << std::endl;
  std::cout << "RMS change: " << shapeDetection->GetRMSChange() << std::endl;

  itk::FileDumper<Float3DImageType> dumpOriginal(castToFloat->GetOutput(), "original");
  itk::FileDumper<Float3DImageType> dumpCurvatureDiffusion(curvatureDiffusion->GetOutput(), "curvatureDiffusion");
//  itk::FileDumper<Float3DImageType> dumpDiffusion(diffusion->GetOutput(), "diffusion");
  itk::FileDumper<Float3DImageType> dumpGradientMagnitude(gradientMagnitude->GetOutput(), "gradientMagnitude");
  itk::FileDumper<Float3DImageType> dumpSpeed(speedImage, "speed");
//  itk::FileDumper<Float3DImageType> dumpSigmoid(sigmoid->GetOutput(), "sigmoid");
//  itk::FileDumper<Float3DImageType> dumpSpeed(shapeDetection->GetSpeedImage(), "speed");
  itk::FileDumper<Float3DImageType> dumpLevelSet(shapeDetection->GetOutput(), "levelset");


  return 0;
}
