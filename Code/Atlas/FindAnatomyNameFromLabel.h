/*=========================================================================

This program searches through the master anatomy file for a given
anatomy label and returns its corresponding anatomy name.

Inputs: Master anatomy file, anatomy label. 
Outputs: Anatomy name.

=========================================================================*/

#ifndef _FindAnatomyNameFromLabel_h
#define _FindAnatomyNameFromLabel_h

#include <vector>
#include <string>

bool FindAnatomyNameFromLabel(char * fileName, int anatomyLabel, std::string & anatomyName);

#endif


