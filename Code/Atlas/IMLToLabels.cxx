#include "itkPolygonGroupSpatialObject.h"
#include "itkImage.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageFileWriter.h"

#include "itkPolygonSpatialObject.h"
#include <iostream>

#include "FindStructureAttributes.h"
#include "ParseIMLFile.h"

int main(int argc, char *argv[])
{
  if( argc < 3 )
    {
    std::cerr << "Missing Parameters " << std::endl;
    std::cerr << "Usage: " << argv[0] << " ";
    std::cerr << "MasterAnatomyFile ";
    std::cerr << "IMLFile";
    std::cerr << "OutputLabelImageName";
    return 1;
    }

  std::string masterAnatomyFile = argv[1];
  std::string IMLFile = argv[2];
  char * outputLabelImageName = argv[3];

  // Parse the master anatomy file.
  FindStructureAttributes<int> labels( masterAnatomyFile );
  std::vector < std::string > structureNames = labels.GetAllStructureNames();
  std::vector< std::vector< int > > structureLabels = labels.GetAllStructureAttributes();

  typedef std::map< std::string, int > StringToIntMapType;
  StringToIntMapType nameToLabelMapper;
  for( unsigned int i = 0; i < structureNames.size(); i++ )
    {
    nameToLabelMapper.insert(StringToIntMapType::value_type( structureNames[i], structureLabels[i][0] ));      
    }

  
  // Parse the IML file and save the results in vectors.
  ParseIMLFile parsedFile( IMLFile );
  std::vector < std::string > allStructNames = parsedFile.GetAllStructNames();
  std::vector< std::vector< std::vector < int > > > allPointPairs = parsedFile.GetAllPointPairs();

  itk::PolygonSpatialObject<3>::Pointer strand;
  std::vector < itk::PolygonSpatialObject<3>::Pointer > strands;

  for( unsigned int regionNumber = 0; regionNumber < allStructNames.size(); regionNumber++)
    {
    strand = itk::PolygonSpatialObject<3>::New();
    strand->SetThickness(1.0);

    // add all points to this strand.
    int z = 0.0;
    double pos[3];
    for(int i = 0; i < parsedFile.GetAllPointPairs()[regionNumber].size(); i++)
      {
      pos[0] = parsedFile.GetAllPointPairs()[regionNumber][i][0];
      pos[1] = parsedFile.GetAllPointPairs()[regionNumber][i][1];
      pos[2] = z;
      itk::PolygonSpatialObject<3>::PointType curpoint(pos);
      if(!strand->AddPoint(curpoint)) 
        {
        std::cerr << "Error adding point" << std::endl;
        return -1;
        }
      }

    // Add the first point again to close the loop.
    pos[0] = parsedFile.GetAllPointPairs()[regionNumber][0][0];
    pos[1] = parsedFile.GetAllPointPairs()[regionNumber][0][1];
    pos[2] = z;
    itk::PolygonSpatialObject<3>::PointType curpoint(pos);
    if(!strand->AddPoint(curpoint)) 
      {
      std::cerr << "Error adding point" << std::endl;
      return -1;
      }

    strands.push_back( strand );
    }

  std::cout << "Number of strands: " << strands.size() << std::endl;
//  std::cout << "Strand 0 area: " << (strands[0])->MeasureArea() << std::endl;


  // Create a label image.
  typedef itk::Image<unsigned short,2> ImageType;
  ImageType::Pointer labelImage = ImageType::New();
  ImageType::SizeType size = {{ 512, 512 }};
  ImageType::RegionType region;
  region.SetSize(size);
  labelImage->SetRegions(region);
  labelImage->Allocate();
  labelImage->FillBuffer( 0 );

  // Now iterate over the image, setting the image pixel
  // inside the polygon.
  typedef itk::ImageRegionIteratorWithIndex < ImageType > IteratorType;
  IteratorType labelImageIt( labelImage, labelImage->GetRequestedRegion() );

  double insidepos[3];  
  int label;
  for( labelImageIt.GoToBegin(); !labelImageIt.IsAtEnd(); ++labelImageIt)
    {
    insidepos[0] = labelImageIt.GetIndex()[0];
    insidepos[1] = labelImageIt.GetIndex()[1];
    insidepos[2] = 0.0;

    itk::PolygonSpatialObject<3>::PointType insidepoint(insidepos);

    for( unsigned int i = 0; i < strands.size(); i++ )
      {
      if(strands[i]->IsInside(insidepoint))
        {
        label = nameToLabelMapper[ allStructNames[i] ];
        if( label )
          {
          labelImageIt.Set( label );
          }
        else
          {
          std::cout << "ERROR: label not found" << std::endl;
          return 0;
          }
        }      
      }
    }

  typedef itk::ImageFileWriter< ImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName( outputLabelImageName );
  writer->SetInput( labelImage );

  try
    {
    writer->Update();
    }
  catch (itk::ExceptionObject & e)
    {
    std::cerr << "exception in file writer " << std::endl;
    std::cerr << e.GetDescription() << std::endl;
    std::cerr << e.GetLocation() << std::endl;
    }


  return 0;
}
