/*=========================================================================

This program combines the finding of the anatomy label, bounding
boxes, and extended bounding boxes from the anatomy name.  It does
this by calling three other functions.

Inputs: Feature image prefix, master anatomy file, bounding boxes
file, anatomy name, border size.
Outputs: Anatomy label, modified bounding box values, and image spacing.

=========================================================================*/
#ifndef _FindAnatomyInfo_h
#define _FindAnatomyInfo_h

#include <string>
#include <vector>

#include "FindImageInfo.h"
#include "FindAnatomyLabelFromName.h"
#include "FindBoundingBoxesFromName.h"
#include "ExtendBoundingBox.h"

bool FindAnatomyInfo(char * imageInfoFile, char * masterFile, char * boundingBoxesFile,std::string & anatomyName, int & anatomyLabel, std::vector <int> &  bbValues, int borderSize, int * imageSize, double * imageSpacing);

#endif
