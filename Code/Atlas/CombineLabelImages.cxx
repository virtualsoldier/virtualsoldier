#include "itkImageFileReader.h"
#include "itkImageSeriesReader.h"
#include "itkArchetypeSeriesFileNames.h"
#include "itkAddImageFilter.h"
#include "itkFlipImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkAbsoluteValueDifferenceImageFilter.h"
#include "itkImageSeriesWriter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkOrImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkErodeObjectMorphologyImageFilter.h"
#include "itkBinaryBallStructuringElement.h"

#include "DumpImages.h"

int main( int argc, char *argv[] )
{
  int numberOfParameters = 5;

  std::cout << "argc: " << argc << std::endl;

  if( argc < numberOfParameters )
    {
    std::cerr << " Missing Parameters " << std::endl;
    std::cerr << " Usage: " << argv[0];
    std::cerr << "ObjectImageName ";
    std::cerr << "SubObjectImageName1 ";
    std::cerr << "SubObjectImageName2 ";
    std::cerr << "OutputImageName ";
    std::cerr << std::endl;
    return 1;
    }

  std::string objectImageName = argv[1];
  std::string subObjectImageName1 = argv[2];
  std::string subObjectImageName2 = argv[3];
  std::string outputImageName = argv[4];

  const unsigned int ImageDimension = 3;
  typedef unsigned char PixelType;
  typedef itk::Image< PixelType, ImageDimension > InputImageType;
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  typedef itk::ImageSeriesReader< InputImageType > SeriesReaderType;
  typedef itk::ArchetypeSeriesFileNames ArchetypeNameGeneratorType;
  typedef itk::AddImageFilter< InputImageType, InputImageType, InputImageType > AddType;
  typedef itk::AbsoluteValueDifferenceImageFilter< InputImageType, InputImageType, InputImageType > DifferenceType;
  typedef itk::FlipImageFilter< InputImageType > FlipType;
  typedef itk::RescaleIntensityImageFilter< InputImageType, InputImageType > RescaleType;

  ArchetypeNameGeneratorType::Pointer inputImageFileGenerator = ArchetypeNameGeneratorType::New();      
  inputImageFileGenerator->SetArchetype( objectImageName.c_str() );

  std::cout << "First image: " << inputImageFileGenerator->GetFileNames()[0] << std::endl;
  std::cout << "Last image: " << inputImageFileGenerator->GetFileNames()[inputImageFileGenerator->GetFileNames().size()-1] << std::endl;

  ReaderType::Pointer subObjectReader1 = ReaderType::New();
  subObjectReader1->SetFileName( subObjectImageName1.c_str() );
  subObjectReader1->ReleaseDataFlagOn();

  ReaderType::Pointer subObjectReader2 = ReaderType::New();
  subObjectReader2->SetFileName( subObjectImageName2.c_str() );
  subObjectReader2->ReleaseDataFlagOn();

  std::cout << "Reading subObject images." << std::endl;
  try
    {
    subObjectReader1->Update();
    subObjectReader2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
  
  std::cout << "SubObject images read." << std::endl;

  std::cout << "Flip combined subObject image." << std::endl;
  FlipType::Pointer flipper1 = FlipType::New();
  FlipType::Pointer flipper2 = FlipType::New();
//  itk::SimpleFilterWatcher watchFlipper(flipper,"flipper");
  flipper1->SetInput( subObjectReader1->GetOutput() );
  flipper2->SetInput( subObjectReader2->GetOutput() );
  bool bArray[ImageDimension] = { false, false, true };
  FlipType::FlipAxesArrayType flipAxes( bArray );
  flipper1->SetFlipAxes( flipAxes );
  flipper2->SetFlipAxes( flipAxes );
//  flipper->ReleaseDataFlagOn();
  try
    {
    flipper1->Update();
    flipper2->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Add subObject images." << std::endl;
  AddType::Pointer adder = AddType::New();
  itk::SimpleFilterWatcher watchAdder(adder,"adder");
  adder->SetInput1( flipper1->GetOutput() );
  adder->SetInput2( flipper2->GetOutput() );
  adder->ReleaseDataFlagOn();
  try
    {
    adder->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  std::cout << "Rescale subObject image." << std::endl;
  RescaleType::Pointer rescaler = RescaleType::New();
  itk::SimpleFilterWatcher watchRescaler(rescaler,"rescaler");
  rescaler->SetInput( adder->GetOutput() );
  rescaler->SetOutputMinimum( 0 );
  rescaler->SetOutputMaximum( 255 );
  rescaler->ReleaseDataFlagOn();
  try
    {
    rescaler->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  // Now read the object image
  std::cout << "Read object image." << std::endl;
  SeriesReaderType::Pointer objectReader = SeriesReaderType::New();
  objectReader->SetFileNames( inputImageFileGenerator->GetFileNames() );
//  objectReader->ReleaseDataFlagOn();
  try
    {
    objectReader->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Take the difference between the object and subObject images." << std::endl;
  DifferenceType::Pointer differencer = DifferenceType::New();
  itk::SimpleFilterWatcher watchDifferencer(differencer,"differencer");
  differencer->SetInput1( objectReader->GetOutput() );
  differencer->SetInput2( rescaler->GetOutput() );
  differencer->ReleaseDataFlagOn();
  try
    {
    differencer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  
  // The ventricle labels could be outside of the heart boundaries or
  // touching it.  This is why we use a DifferenceImageFilter instead
  // of a SubtractImageFilter.  However, if the ventricle labels are
  // touching the boundary, that boundary will disappear.  Therefore,
  // we do one last step to get back the boundary.

  // Create the structuring element
  typedef itk::BinaryBallStructuringElement< unsigned short, ImageDimension> KernelType;
  KernelType ball;
  KernelType::SizeType ballSize;
  ballSize[0] = 1;
  ballSize[1] = 1;
  ballSize[2] = 1;
  ball.SetRadius(ballSize);
  ball.CreateStructuringElement();

  std::cout << "Erode the object image" << std::endl;
  typedef itk::ErodeObjectMorphologyImageFilter< InputImageType, InputImageType, KernelType > ErodeType;
  ErodeType::Pointer eroder = ErodeType::New();
  itk::SimpleFilterWatcher watchEroder(eroder,"eroder");
  eroder->SetInput( objectReader->GetOutput() );
  eroder->SetKernel( ball );
  eroder->SetObjectValue( 255 );
//  eroder->ReleaseDataFlagOn();
  try
    {
    eroder->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Subtract the eroded image from the object image" << std::endl;
  typedef itk::SubtractImageFilter< InputImageType, InputImageType, InputImageType > SubtractType;
  SubtractType::Pointer subtractor = SubtractType::New();
  itk::SimpleFilterWatcher watchSubtractor(subtractor,"subtractor");
  subtractor->SetInput1( objectReader->GetOutput() );
  subtractor->SetInput2( eroder->GetOutput() );
  subtractor->ReleaseDataFlagOn();
  try
    {
    subtractor->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  std::cout << "Or the subtracted image with the difference image" << std::endl;
  typedef itk::OrImageFilter< InputImageType, InputImageType, InputImageType > ORType;
  ORType::Pointer orer = ORType::New();
  itk::SimpleFilterWatcher watchOrer(orer,"orer");
  orer->SetInput1( subtractor->GetOutput() );
  orer->SetInput2( differencer->GetOutput() );
  orer->ReleaseDataFlagOn();
  try
    {
    orer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }


  typedef itk::Image<unsigned short, 3> FinalLabelType;
  FinalLabelType::Pointer final = FinalLabelType::New();
  final->SetRegions( orer->GetOutput()->GetRequestedRegion() );
  final->Allocate();

  itk::ImageRegionIterator<FinalLabelType> fit = itk::ImageRegionIterator<FinalLabelType>(final, final->GetBufferedRegion() );
  itk::ImageRegionIterator<InputImageType> hit = itk::ImageRegionIterator<InputImageType>(orer->GetOutput(), orer->GetOutput()->GetRequestedRegion() );
  itk::ImageRegionIterator<InputImageType> lit = itk::ImageRegionIterator<InputImageType>(flipper1->GetOutput(), orer->GetOutput()->GetRequestedRegion() );
  itk::ImageRegionIterator<InputImageType> rit = itk::ImageRegionIterator<InputImageType>(flipper2->GetOutput(), orer->GetOutput()->GetRequestedRegion() );

  const unsigned short heartId = 114;
  const unsigned short leftId = 211;
  const unsigned short rightId = 328;
  const unsigned short backId = 0;

  while (! fit.IsAtEnd() )
    {
    if (hit.Get())
      {
      fit.Set( heartId );
      }
    else if (lit.Get())
      {
      fit.Set( leftId );
      }
    else if (rit.Get())
      {
      fit.Set( rightId );
      }
    else
      {
      fit.Set( backId );
      }
    ++fit;
    ++hit;
    ++lit;
    ++rit;
    }

  // Make an output name by getting rid of the number and extension
  // of the output file.
  std::string labelImagePath = itksys::SystemTools::GetFilenamePath( outputImageName.c_str() );
  std::string labelImageName = itksys::SystemTools::GetFilenameWithoutExtension( outputImageName.c_str() );
  labelImageName.erase(labelImageName.length()-4);
  std::string outputImagePrefix = labelImagePath + "/" + labelImageName; 
  
  std::cout << "Write out images." << std::endl;
//   itk::FileDumper<InputImageType> dumpResampler( orer->GetOutput(), outputImagePrefix, "%04d.png");
  typedef itk::Image<unsigned short, 2> FinalLabel2DType;
  typedef itk::ImageSeriesWriter<FinalLabelType, FinalLabel2DType> SeriesWriterType;
  SeriesWriterType::Pointer writer = SeriesWriterType::New();
  writer->SetInput( final );
  writer->SetSeriesFormat( (outputImagePrefix + "%04d.png").c_str() );
  writer->Update();
                         
}  
