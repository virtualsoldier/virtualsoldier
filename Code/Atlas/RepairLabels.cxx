/*=========================================================================

This program takes in a list of anatomy labels that are dusty and
need to be repaired.  For each pixel in the input image that
has one of these labels, the program replaces that pixel value with
the mode of its 26 neighbors.

Inputs: Image files, sliceOffset, numberOfSlices, anatomy labels.
Outputs: Output files.

=========================================================================*/

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkNeighborhoodIterator.h"
#include "itkArray.h"

#include <fstream>
#include <iostream>
#include <string>
#include <set>
#include <vector>

unsigned short
FindCorrectLabel(itk::NeighborhoodIterator< itk::Image< unsigned short,2 > > itPrev, 
                 itk::NeighborhoodIterator< itk::Image< unsigned short,2 > > itCurr,
                 itk::NeighborhoodIterator< itk::Image< unsigned short,2 > > itNext,
                 bool threeSlices,
                 std::vector < unsigned short > anatomyLabels);

int main( int argc, char *argv[] )
{
  if (argc < 6)
    {
    std::cout << "Usage: inputFilePrefix sliceOffset numberOfSlices outputFilePrefix anatomyLabels" << std::endl;
    return 0;
    }  

  typedef   unsigned short   PixelType;
  typedef itk::Image< PixelType,  2 >   ImageType;
  typedef itk::ImageFileReader< ImageType >  ReaderType;
  typedef itk::ImageFileWriter < ImageType > WriterType;
  typedef itk::NumericSeriesFileNames NameGeneratorType;
  typedef itk::NeighborhoodIterator< ImageType > NeighborhoodIteratorType;
  
  typedef itk::NeighborhoodAlgorithm::ImageBoundaryFacesCalculator<
  ImageType > FaceCalculatorType;
  
  typedef itk::Array < PixelType > ArrayType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();
  ImageType::Pointer prevImage;
  ImageType::Pointer currImage;
  ImageType::Pointer nextImage;
  ImageType::Pointer outImage = ImageType::New();
  ImageType::Pointer outFirstImage = ImageType::New();
  ImageType::Pointer outLastImage = ImageType::New();
  NameGeneratorType::Pointer inputImageFileGenerator = NameGeneratorType::New();
  NameGeneratorType::Pointer outputImageFileGenerator = NameGeneratorType::New();

  FaceCalculatorType faceCalculator;
  FaceCalculatorType::FaceListType faceList;
  FaceCalculatorType::FaceListType::iterator fit;
  

  std::vector < unsigned short > anatomyLabels;
  for (int i = 5; i < argc; i++)
    {
    anatomyLabels.push_back( atoi (argv[i]) );
    std::cout << "Anatomy label: " << atoi(argv[i]) << std::endl;
    }

  int sliceOffset = atoi (argv[2]);
  int numberOfSlices = atoi (argv[3]);

  std::cout << "Slice offset: " << sliceOffset << std::endl;
  std::cout << "Number of slices: " << numberOfSlices << std::endl;
  
  // Generate the image file names
  inputImageFileGenerator->SetStartIndex( sliceOffset );
  inputImageFileGenerator->SetEndIndex ( numberOfSlices + sliceOffset - 1 );
  inputImageFileGenerator->SetIncrementIndex ( 1 );
  inputImageFileGenerator->SetSeriesFormat ( argv[1] );
  
  outputImageFileGenerator->SetStartIndex( sliceOffset );
  outputImageFileGenerator->SetEndIndex ( numberOfSlices + sliceOffset - 1 );
  outputImageFileGenerator->SetIncrementIndex ( 1 );
  outputImageFileGenerator->SetSeriesFormat ( argv[4] );

  std::vector <std::string > inputImageNames =
    inputImageFileGenerator->GetFileNames();
  std::vector <std::string > outputImageNames =
    outputImageFileGenerator->GetFileNames();


  // Set the previous image to be the first slice
  reader->SetFileName(inputImageNames[0].c_str());

  try 
    { 
    prevImage = reader->GetOutput();
    prevImage->Update();
    prevImage->DisconnectPipeline();
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 

  // Set the current image to be the second slice
  reader->SetFileName(inputImageNames[1].c_str());

  try 
    { 
    currImage = reader->GetOutput();
    currImage->Update();
    currImage->DisconnectPipeline();
    } 
  catch( itk::ExceptionObject & err ) 
    { 
    std::cout << "ExceptionObject caught !" << std::endl; 
    std::cout << err << std::endl; 
    return -1;
    } 

  // Create an output image the same size as the input image.
  ImageType::RegionType region;
  region.SetSize(currImage->GetLargestPossibleRegion().GetSize());
  region.SetIndex(currImage->GetLargestPossibleRegion().GetIndex());
  outImage->SetRegions(region);
  outImage->Allocate();
  outFirstImage->SetRegions(region);
  outFirstImage->Allocate();
  outLastImage->SetRegions(region);
  outLastImage->Allocate();


  // The radius of the neighborhood should be 1
  NeighborhoodIteratorType::RadiusType radius;
  radius.Fill(1);

  faceList = faceCalculator( prevImage, 
                             prevImage->GetRequestedRegion(), 
                             radius );
  
  NeighborhoodIteratorType itPrev;
  NeighborhoodIteratorType itCurr;  
  NeighborhoodIteratorType itNext;
  NeighborhoodIteratorType itOut;
  NeighborhoodIteratorType itOutFirst;
  NeighborhoodIteratorType itOutLast;

  int slice;
  std::vector <std::string> ::iterator iinit =
    inputImageNames.begin();
  std::vector <std::string> ::iterator oinit =
    outputImageNames.begin();


  iinit = inputImageNames.begin();
  oinit = outputImageNames.begin();

  // Increment input image pointer so that it points to the
  // third image.
  ++iinit;
  ++iinit;
  // Increment output image pointer so that is points to the second image.
  ++oinit;

  // Loop through slices
  while (iinit != inputImageNames.end())
    {
    slice = iinit - inputImageNames.begin() - 1;
    std::cout << "Processing slice: " << slice + sliceOffset << std::endl;

    /*
   // Reinitialize the output image to zero
   try 
     { 
     outImage->FillBuffer(0);
     outImage->Update();
     } 
   catch( itk::ExceptionObject & err ) 
     { 
     std::cout << "ExceptionObject caught !" << std::endl; 
     std::cout << err << std::endl; 
     return -1;
     } 
    */

   // Read the next image
   try 
     { 
     reader->SetFileName ( (*iinit).c_str() );
     nextImage = reader->GetOutput();
     nextImage->Update();
     nextImage->DisconnectPipeline();
     } 
   catch( itk::ExceptionObject & err ) 
     { 
     std::cout << "ExceptionObject caught !" << std::endl; 
     std::cout << err << std::endl; 
     return -1;
     } 

   bool reIterateFlag = false;

   // Loop through each face
   for ( fit=faceList.begin();
         fit != faceList.end(); ++fit)
     {
     itPrev = NeighborhoodIteratorType( radius, prevImage , *fit );
     itCurr = NeighborhoodIteratorType( radius, currImage , *fit );
     itNext = NeighborhoodIteratorType( radius, nextImage , *fit );
     itOut =  NeighborhoodIteratorType( radius, outImage  , *fit );
     itOutFirst = NeighborhoodIteratorType( radius, outFirstImage  , *fit );
     itOutLast = NeighborhoodIteratorType( radius, outLastImage  , *fit );

     PixelType correctLabel;
     bool labelCorrected;

     //std::cout << "Processing middle face: " << fit-faceList.begin() << std::endl;
     //std::cout << "Processing middle slice: " << slice << std::endl;


     // Loop through each neighborhood of slices 1:n-1
     for (itPrev.GoToBegin(), itCurr.GoToBegin(), itNext.GoToBegin(), 
            itOut.GoToBegin(); !itCurr.IsAtEnd(); ++itPrev, ++itCurr, 
            ++itNext, ++itOut)
       {
       NeighborhoodIteratorType::PixelType centerPixelCurr = itCurr.GetCenterPixel();

       // Initialize the correct label to an impossible label
       labelCorrected = false;

       // Loop through all the labels of the anatomies that need to be
       // repaired.  If the center pixel matches one of them, repair it.
       for (unsigned short i = 0; i < anatomyLabels.size(); i++)
         {
         if (centerPixelCurr == anatomyLabels[i])
           {
           correctLabel = FindCorrectLabel(itPrev, itCurr, itNext, true, anatomyLabels);
           
           // The label should be corrected only if the correctLabel is not 0
           if (correctLabel)
             labelCorrected = true;
           else
             {
             //std::cout << "Corrected with 'unknown'" << std::endl;
             labelCorrected = false;
             reIterateFlag = false;
             }
           // Break out of the for loop is we have found a label
           // because each pixel can only have one label
           // break;
           }
         }
      
       if (labelCorrected)
         {
         // Repair the pixel
         itOut.SetCenterPixel(correctLabel);
         //itCurr.SetCenterPixel(correctLabel);
         }
       else
         {
         // Assign the center pixel to the output pixel
         itOut.SetCenterPixel(centerPixelCurr);
         }
       }

     // Do the processing for the first slice ("slice" is
     // pointing to the second slice).
     if (slice == 1)
       {
       //std::cout << "Processing first slice: " << slice-1 << std::endl;

       for (itPrev.GoToBegin(), itCurr.GoToBegin(),
              itOutFirst.GoToBegin(); !itCurr.IsAtEnd(); ++itPrev, ++itCurr, 
              ++itOutFirst)
         {
         NeighborhoodIteratorType::PixelType centerPixelPrev = itPrev.GetCenterPixel();

         labelCorrected = false;

         // Loop through all the labels of the anatomies that need to be
         // repaired.  If the center pixel matches one of them, repair it.
         for (unsigned short i = 0; i < anatomyLabels.size(); i++)
           {
           if (centerPixelPrev == anatomyLabels[i])
             {
             // The third parameter is a dummy variable since the last
             // parameter is 0.
             correctLabel = FindCorrectLabel(itPrev, itCurr, itCurr, false, anatomyLabels);
                          
             // The label should be corrected only if the correctLabel is not 0
             if (correctLabel)
               labelCorrected = true;
             else
               {
               //std::cout << "Corrected with 'unknown'" << std::endl;
               labelCorrected = false;
               reIterateFlag = false;
               }
             }
           }
      
         if (labelCorrected)
           {
           // Repair the pixel
           itOutFirst.SetCenterPixel(correctLabel);
           }
         else
           {
           // Assign the center pixel to the output pixel
           itOutFirst.SetCenterPixel(centerPixelPrev);
           }
         }
       }

     // Do the processing for the last slice ("slice" is pointing
     // to the second last slice)
     if (slice == numberOfSlices-2)
       {
       //std::cout << "Processing last slice: " << slice+1 << std::endl;
       
       for (itCurr.GoToBegin(), itNext.GoToBegin(),itOutLast.GoToBegin(); 
            !itCurr.IsAtEnd(); ++itCurr, ++itNext, ++itOutLast)
         {  
         NeighborhoodIteratorType::PixelType centerPixelNext = itNext.GetCenterPixel();
       
         labelCorrected = false;

         // Loop through all the labels of the anatomies that need to be
         // repaired.  If the center pixel matches one of them, repair it.
         for (unsigned short i = 0; i < anatomyLabels.size(); i++)
           {
           if (centerPixelNext == anatomyLabels[i])
             {
             // Treat the next image as the current and the current as
             // the previous in finding the correct label
             // The third parameter is a dummy variable since the last
             // parameter is 0.
             correctLabel = FindCorrectLabel(itCurr, itNext, itNext, false , anatomyLabels);

             // The label should be corrected only if the correctLabel is not 0
             if (correctLabel)
               labelCorrected = true;
             else
               {
               //std::cout << "Corrected with 'unknown'" << std::endl;
               labelCorrected = false;
               reIterateFlag = false;
               }
             }
           }
      
         if (labelCorrected)
           {
           // Repair the pixel
             itOutLast.SetCenterPixel(correctLabel);
           }
         else
           {
             itOutLast.SetCenterPixel(centerPixelNext);
           }
         } // end of iteration over neighborhood
       } // end of processing of last slice
     } // end of iteration over face

   // If any pixel was assigned 0 in the images, loop through the
   // image again.
   // Do not write out the images or increment the file iterator.
   // Do not pass GO; do not collect $200.
   if (reIterateFlag)
     {
     std::cout << "Iterating over the image again" << std::endl;
     continue;
     }
     

   // Write out the first slice
   if (slice == 1)
     {
     --oinit;
     std::cout << "Output File: " << (*oinit).c_str() << std::endl;

     try 
       { 
       writer->SetFileName ( (*oinit).c_str() );
       writer->SetInput(outFirstImage);
       writer->Update();
       } 
     catch( itk::ExceptionObject & err ) 
       { 
       std::cout << "ExceptionObject caught !" << std::endl; 
       std::cout << err << std::endl; 
       return -1;
       }
  
     ++oinit;
     }

   // Write the middle slices
   std::cout << "Output File: " << (*oinit).c_str() << std::endl;   
   writer->SetFileName ( (*oinit).c_str() );
   writer->SetInput(outImage);
   try 
     { 
     writer->Update();
     } 
   catch( itk::ExceptionObject & err ) 
     { 
     std::cout << "ExceptionObject caught !" << std::endl; 
     std::cout << err << std::endl; 
     return -1;
     } 

   // Write out the last slice
   if (slice == numberOfSlices-2)
     {
     ++oinit;
     std::cout << "Output File: " << (*oinit).c_str() << std::endl;

     try 
       { 
       writer->SetFileName ( (*oinit).c_str() );
       writer->SetInput(outLastImage);
       writer->Update();
       } 
     catch( itk::ExceptionObject & err ) 
       { 
       std::cout << "ExceptionObject caught !" << std::endl; 
       std::cout << err << std::endl; 
       return -1;
       } 

     --oinit;
     }

   prevImage = currImage;
   currImage = nextImage;
   ++iinit;
   ++oinit;
    }
  
  return 0;
  
}


unsigned short
FindCorrectLabel(itk::NeighborhoodIterator< itk::Image< unsigned
                 short,2 > > itPrev, 
                 itk::NeighborhoodIterator< itk::Image< unsigned
                 short,2 > > itCurr,
                 itk::NeighborhoodIterator< itk::Image< unsigned
                 short,2 > > itNext,
                 bool threeSlices,
                 std::vector < unsigned short > anatomyLabels)
{
  unsigned short histo[512];

  itk::NeighborhoodIterator< itk::Image< unsigned
    short,2 > >::PixelType prevNeighPixel;
  itk::NeighborhoodIterator< itk::Image< unsigned
    short,2 > >::PixelType currNeighPixel;
  itk::NeighborhoodIterator< itk::Image< unsigned
    short,2 > >::PixelType nextNeighPixel;


  // Reset the histogram array
  for (int i = 0; i < 512; i++)
    {
    histo[i] = 0;
    }
          
  for (unsigned int indexValue = 0; indexValue < 9;
       indexValue++)
    {

    prevNeighPixel = itPrev.GetPixel(indexValue);
    //std::cout << "Prev(" << indexValue << "): " << prevNeighPixel << "\t";
    histo[prevNeighPixel]++;
  

    currNeighPixel = itCurr.GetPixel(indexValue);
    histo[currNeighPixel]++;
    //std::cout << "Curr(" << indexValue << "): " << currNeighPixel << "\t";

    if (threeSlices)
      {
      nextNeighPixel = itNext.GetPixel(indexValue);
      //std::cout << "Next(" << indexValue << "): " << nextNeighPixel << std::endl;
      histo[nextNeighPixel]++;
      }
    //else 
    //std::cout << std::endl;
    }

  // Make sure that the new label is not any of the old labels or "unknown"
  histo[0] = 0;
/*
  for (unsigned short i = 0; i < anatomyLabels.size(); i++)
    {
    histo[anatomyLabels[i]] = 0;
    }
*/
  unsigned short correctLabel = 0;
  int maxFrequency = 0;
  for (int i = 0; i < 512; i++)
    {
//	if (histo[i] > 0)
//		  std::cout << "Bin " << i << " has frequency " << histo[i] << std::endl;
    if (histo[i] > maxFrequency)
      {
      maxFrequency = histo[i];
      correctLabel = i;
      }
    }

  //std::cout << "Correct Label: " << correctLabel << std::endl;
  //std::cout << "Frequency of correct label: " << maxFrequency << std::endl;

  return correctLabel;

}


