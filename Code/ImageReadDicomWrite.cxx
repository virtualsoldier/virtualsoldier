/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    $RCSfile: ImageReadDicomWrite.cxx,v $
  Language:  C++
  Date:      $Date: 2005/09/19 20:56:11 $
  Version:   $Revision: 1.1 $

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

//  Software Guide : BeginLatex
//
//  This example illustrates how to read a 3D Meta Image and then save
//  this volume into a series of Dicom files.
//
//  Software Guide : EndLatex 


// Software Guide : BeginCodeSnippet
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkExtractImageFilter.h"
#include "itkGDCMImageIO.h"
#include "itkMetaDataObject.h"
#include "itkIOCommon.h"
#include <string>
#include <itksys/ios/sstream>
#include <itksys/Base64.h>
// Software Guide : EndCodeSnippet

#include <vector>
#include <itksys/SystemTools.hxx>

int main( int argc, char* argv[] )
{
  if( argc < 3 )
    {
    std::cerr << "Usage: " << argv[0] << 
      " MetaFile  DicomPrefix [entry value] ..." << std::endl;
    return EXIT_FAILURE;
    }

  //  Software Guide : BeginLatex
  //
  //  First we define the image type to be used in this example. From the image
  //  type we can define the type of the reader. We also declare types for the
  //  \doxygen{GDCMImageIO} object that will actually write the DICOM
  //  images.
  //
  //  Software Guide : EndLatex 


  // Software Guide : BeginCodeSnippet
  typedef itk::Image<short,3>               Image3DType;
  typedef itk::Image<short,2>               Image2DType;
  typedef itk::ImageFileReader< Image3DType > ReaderType;
  typedef itk::ExtractImageFilter< Image3DType, Image2DType > ExtractType;
  typedef itk::ImageFileWriter< Image2DType > WriterType;
  typedef itk::GDCMImageIO                  ImageIOType;
  // Software Guide : EndCodeSnippet


  // Software Guide : BeginCodeSnippet
  // Software Guide : EndCodeSnippet

  ReaderType::Pointer reader = ReaderType::New();

  // Software Guide : EndCodeSnippet

  try
    {
    reader->SetFileName(argv[1]);
    reader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the image file: " << argv[1] << std::endl;
    std::cerr << excp << std::endl;

    return EXIT_FAILURE;
    }

  typedef itk::MetaDataDictionary   DictionaryType;
  unsigned int numberOfSlices = reader->GetOutput()->GetLargestPossibleRegion().GetSize()[2];

  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  DictionaryType dictionary;
  for (unsigned int i = 0; i < numberOfSlices; i++)
    {
    itksys_ios::ostringstream value;
    Image3DType::PointType origin;
    Image3DType::IndexType index;
    index.Fill(0);
    index[2] = i;
    reader->GetOutput()->TransformIndexToPhysicalPoint(index, origin);

    // Set all required DICOM fields
    value.str("");
    value << origin[0] << "\\" << origin[1] << "\\" << origin[2];
    itk::EncapsulateMetaData<std::string>(dictionary,"0020|0032", value.str());

    value.str("");
    value << i + 1;
    itk::EncapsulateMetaData<std::string>(dictionary,"0020|0013", value.str());

    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0008",std::string("ORIGINAL\\PRIMARY\\AXIAL"));  // Image Type
    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0016",std::string("1.2.840.10008.5.1.4.1.1.2")); // SOP Class UID
    itk::EncapsulateMetaData<std::string>(dictionary,"0010|0030",std::string("20050101")); // Patient's Birthdate
    itk::EncapsulateMetaData<std::string>(dictionary,"0010|0032",std::string("010100.000000") ); // Patient's Birth Time
    itk::EncapsulateMetaData<std::string>(dictionary,"0010|0040", std::string("M")); // Patient's Sex
    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0020", std::string("20050101")); // Study Date
    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0030", std::string("010100.000000")); // Study Time
    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0050", std::string("1")); // Accession Number
    itk::EncapsulateMetaData<std::string>(dictionary,"0008|0090", std::string("Unknown")); // Referring Physician's Name
    itk::EncapsulateMetaData<std::string>(dictionary,"0018|5100", std::string("HFS")); // Patient Position
    itk::EncapsulateMetaData<std::string>(dictionary,"0020|1040", std::string("SN"));  // Position Reference Indicator

    itk::Array<double> spacing(3);
    reader->Update();
    spacing[0] = reader->GetOutput()->GetSpacing()[0];
    spacing[1] = reader->GetOutput()->GetSpacing()[1];
    spacing[2] = reader->GetOutput()->GetSpacing()[2];
    itk::EncapsulateMetaData<itk::Array<double> >(dictionary,itk::ITK_Spacing, spacing);
    // Add any fields/value pairs that are on the command line
    for (int j = 3; j < argc; j+=2)
      {
      std::string entryId( argv[j] );
      std::string value( argv[j+1] );
      itk::EncapsulateMetaData<std::string>( dictionary, entryId, value );
      }

    Image3DType::RegionType extractRegion;
    Image3DType::SizeType extractSize;
    Image3DType::IndexType extractIndex;
    extractSize = reader->GetOutput()->GetLargestPossibleRegion().GetSize();
      extractIndex.Fill(0);
      extractIndex[2] = numberOfSlices - i - 1;
      extractIndex[2] = i;
      extractSize[2] = 0;
      extractRegion.SetSize(extractSize);
      extractRegion.SetIndex(extractIndex);

    ExtractType::Pointer extract = ExtractType::New();
      extract->SetInput(reader->GetOutput());
      extract->SetExtractionRegion(extractRegion);
      extract->GetOutput()->SetMetaDataDictionary(dictionary);

    WriterType::Pointer writer = WriterType::New();
      value.str("");
      value << argv[2] << i + 1 << ".dcm";
      writer->SetFileName(value.str().c_str());
      writer->SetInput(extract->GetOutput());
      std::cout << "file: " << value.str() << std::endl;
      try
        {
        writer->SetImageIO(gdcmIO);
        writer->Update();
        }
      catch( itk::ExceptionObject & excp )
        {
        std::cerr << "Exception thrown while writing the file " << std::endl;
        std::cerr << excp << std::endl;
        return EXIT_FAILURE;
        }
    }
  reader->GetOutput()->Print(std::cout);
  // Software Guide : EndCodeSnippet
  return EXIT_SUCCESS;
}

