#
# Display All the models
#

package require vtk
package require vtkinteraction

if {$argc < 1} {
  puts "usage: vtk DisplayModels.tcl *.vtk"
  puts "   or: vtk DisplayModels.tcl file.txt"
  exit
}

vtkMath math
vtkLookupTable lut

proc pick {} {
    set actor [lindex [split [lindex [split [[iren GetPicker] GetActor] .] 0] / ] end ]
    set structure [regsub -all _ $actor " "]
    textMapper SetInput $structure
    textActor SetPosition 10 10
    textActor VisibilityOn
    renWin Render
}

proc makecolors {n} {
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
   }
}

makecolors 512

vtkRenderer ren1
  ren1 SetBackground 0.4667 0.5333 0.6000

vtkRenderWindow renWin
  renWin AddRenderer ren1
  renWin SetSize 640 480

vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin


set allFiles ""
for {set i 0} {$i < $argc} {incr i} {
  set extension [lindex [split [lindex $argv $i] .] end]
  if {$extension == "txt" } {
    set txtFID [open [lindex $argv $i] r]
    set loc ""
    while {[gets $txtFID line] >= 0} {
      if { [string first "/" $line] != -1 } {
        set loc $line
        continue
      }
      lappend allFiles ${loc}${line}.vtk
    }
  close $txtFID
  } else {
    if {$extension == "vtk" } {
      lappend allFiles [glob [concat [lindex $argv $i]]]
    } else {
      lappend allFiles [glob [concat [lindex $argv $i].vtk]]
    }
  }
}
foreach file $allFiles {
    catch { vtkPolyDataReader ${file}Reader}
    ${file}Reader SetFileName $file
    ${file}Reader AddObserver StartEvent "puts -nonewline \"$file\"; flush stdout"
    ${file}Reader AddObserver EndEvent {puts " Complete"}
    ${file}Reader ReleaseDataFlagOn
    
    catch { vtkPolyDataNormals ${file}Normals}
    ${file}Normals SetInputConnection [${file}Reader GetOutputPort]
    ${file}Normals SetFeatureAngle 60
    
    catch { vtkPolyDataMapper ${file}Mapper}
    ${file}Mapper SetInputConnection [${file}Reader GetOutputPort]
    ${file}Mapper SetInputConnection [${file}Normals GetOutputPort]
    ${file}Mapper SetLookupTable lut
    ${file}Mapper SetScalarRange 0 [lut GetNumberOfColors]
    ${file}Mapper ImmediateModeRenderingOn

    catch { vtkLODActor ${file}Actor; ren1 AddActor ${file}Actor}
    ${file}Actor SetMapper ${file}Mapper
    [${file}Actor GetProperty] SetDiffuseColor [math Random .2 1]  [math Random .2 1]  [math Random .2 1]
    if { [string first "Colored" $file] != -1 } {
      [${file}Actor GetProperty] SetAmbient .3
    }

  [ren1 GetActiveCamera] SetViewUp 0 0 -1
  [ren1 GetActiveCamera] SetFocalPoint 0 0 0
  [ren1 GetActiveCamera] SetPosition 0 1 0
  ren1 ResetCamera
  ren1 ResetCameraClippingRange
  renWin Render
}

# Create a text mapper and actor to display the results of picking
vtkTextMapper textMapper
set tprop [textMapper GetTextProperty]
    $tprop SetFontFamilyToArial
    $tprop SetFontSize 20
vtkActor2D textActor
    textActor VisibilityOff
    textActor SetMapper textMapper

ren1 AddActor textActor
[ren1 GetActiveCamera] SetViewUp 0 0 -1
[ren1 GetActiveCamera] SetFocalPoint 0 0 0
[ren1 GetActiveCamera] SetPosition 0 1 0
ren1 ResetCamera
ren1 ResetCameraClippingRange

iren AddObserver UserEvent {wm deiconify .vtkInteract}
iren AddObserver EndPickEvent pick
    iren Initialize

renWin Render
wm withdraw .
iren Start

