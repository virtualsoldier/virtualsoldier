#
# Add vertex colors to a vtk model and produce a ply model
#

if {$argc < 1} {
  puts "usage: vtk vtkToply model_name"
  exit
}

proc realize.ConvertCellData { polydata } {
    if {[info commands ${polydata}.CellData] == ""} {
        vtkUnsignedCharArray ${polydata}.CellData
    }
    puts "[$polydata GetNumberOfCells]"
    ${polydata}.CellData SetNumberOfComponents 3
    ${polydata}.CellData SetNumberOfTuples [$polydata GetNumberOfCells]
    ${polydata}.CellData SetName RGB
    set oldCellData [[${polydata} GetCellData] GetScalars]
    for {set i 0} { $i < [$polydata GetNumberOfCells] } {incr i} {
        set r [expr [$oldCellData GetComponent $i 0] * 255]
        set g [expr [$oldCellData GetComponent $i 1] * 255]
        set b [expr [$oldCellData GetComponent $i 2] * 255]
        ${polydata}.CellData SetTuple3 $i $r $g $b
    }    
    [${polydata} GetCellData] SetScalars ${polydata}.CellData
}

if {$tcl_platform(platform) == "windows"} {
  set MasterAnatomyFile "s:/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "s:/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Relabeled/Relabeled"
  set RGBFilePrefix "s:/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set SmoothedPath "s:/Data/Atlas/Processed/Models/Smoothed/"
  set DiscretePath "s:/Data/Atlas/Processed/Models/Discrete/"
  set ThresholdLevelSetsPath "s:/Data/Atlas/Processed/Models/ThresholdLevelSets/"
  set ColoredThresholdLevelSetsPath "s:/Data/Atlas/Processed/Models/ColoredThresholdLevelSets/"
  set LaplacianLevelSetsPath "s:/Data/Atlas/Processed/Models/LaplacianLevelSets/"
  set ColoredLaplacianLevelSetsPath "s:/Data/Atlas/Processed/Models/ColoredLaplacianLevelSets/"
  set ColoredPath "s:/Data/Atlas/Processed/Models/Colored/"
} else {
  set MasterAnatomyFile "/projects/soldier/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "/projects/soldier/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "/projects/soldier/Data/Atlas/Processed/Relabeled/Relabeled"
  set RGBFilePrefix "/projects/soldier/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set SmoothedPath "/projects/soldier/Data/Atlas/Processed/Models/Smoothed/"
  set DiscretePath "/projects/soldier/Data/Atlas/Processed/Models/Discrete/"
  set ColoredPath "/projects/soldier/Data/Atlas/Processed/Models/Colored/"
}
set LabelFilePattern "%s%d.png"
set RGBFilePattern "%s%d.png"
set structure [lindex $argv 0]


###################################
# Parameters
#
set WHICH_MODELS ${ColoredLaplacianLevelSetsPath}

vtkPolyDataReader readPD
  readPD SetFileName "${WHICH_MODELS}${structure}.vtk"
  readPD Update

vtkCleanPolyData clean
  clean SetInput [readPD GetOutput]
  clean Update

  realize.ConvertCellData [clean GetOutput]
puts "[[[[clean GetOutput] GetCellData] GetScalars] Print]"
vtkPLYWriter PLYwriter
  PLYwriter SetFileName "${WHICH_MODELS}${structure}.ply"
  PLYwriter SetInput [clean GetOutput]
  PLYwriter SetFileTypeToASCII
  PLYwriter SetColorModeToDefault
  PLYwriter SetArrayName RGB
  PLYwriter Modified
  puts "${WHICH_MODELS}${structure}.ply"
  PLYwriter Write

exit
