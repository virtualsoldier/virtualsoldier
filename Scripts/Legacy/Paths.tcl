###############################################################################
#
# Define paths and files for Virtual Soldier scripts
# These paths corresponds to the organization of files at GE Global Research.
# The paths described here can be overridden in the tcl script LocalPaths.tcl.
#

if {$tcl_platform(platform) == "windows"} {
  set MasterAnatomyFile "s:/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "s:/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"

  set RGBFilePrefix "s:/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Repaired/Repaired"
  set VHMCTFilePrefix "s:/Data/Atlas/Processed/VHMCT/c_vm"

  set DiscretePath "s:/Data/Atlas/Processed/Models/Discrete/"
  set SmoothedPath "s:/Data/Atlas/Processed/Models/Smoothed/"
  set ColoredPath "s:/Data/Atlas/Processed/Models/Colored/"
  set DecimatedPath "s:/Data/Atlas/Processed/Models/Decimated/"
} else {
  set MasterAnatomyFile "/projects/soldier/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "/projects/soldier/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"

  set RGBFilePrefix "/projects/soldier/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set LabelFilePrefix "/projects/soldier/Data/Atlas/Processed/Relabeled/Relabeled"
  set VHMCTFilePrefix "/project/soldier/Data/Atlas/Processed/VHMCT/c_vm"
  set DiscretePath "/projects/soldier/Data/Atlas/Processed/Models/Discrete/"
  set SmoothedPath "/projects/soldier/Data/Atlas/Processed/Models/Smoothed/"
  set ColoredPath "/projects/soldier/Data/Atlas/Processed/Models/Colored/"
  set DecimatedPath "/projects/soldier/Data/Atlas/Processed/Models/Decimated/"
}

#
# If present, the file LocalPaths.tcl can override the paths and files defined
# in this script
#

if {[file exists [file dirname [info script]]/LocalPaths.tcl] } {
    source [file dirname [info script]]/LocalPaths.tcl
}
