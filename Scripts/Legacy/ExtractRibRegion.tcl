package require vtk
package require vtkinteraction
package require vtktesting

#if {$argc < 1} {
#  puts "usage: vtkITK ExtractRibs.tcl inputFile"
#  exit
#}

#Tested on P124DcmPre30
set inputFile z:/Projects/VirtualSoldier/VirtualSoldier-data/ISR/P184DcmPre80/P184DcmPre80sp/P184DcmPre80spSeries1/P184DcmPre80spSeries1IM0001.dcm

catch {vtkITKArchetypeImageSeriesReader reader}
reader SetArchetype $inputFile
reader ReleaseDataFlagOn
reader AddObserver StartEvent { puts "Reading" }
reader AddObserver ProgressEvent { puts "." }
reader AddObserver EndEvent { puts "Done reading" }

catch {vtkMarchingCubes cubes}
cubes SetInput [reader GetOutput]
cubes SetValue 0 700
cubes AddObserver StartEvent { puts "Marching cubes" }
cubes AddObserver ProgressEvent { puts "." }
cubes AddObserver EndEvent { puts "Done cubing" }

catch {vtkStripper stripper}
stripper SetInput [cubes GetOutput]

catch {vtkSphereSource sphere}
sphere SetRadius 100
sphere SetCenter -25 275 -800

catch {vtkSmoothPolyDataFilter smoother}
smoother SetSource [cubes GetOutput]
smoother SetInput [sphere GetOutput]
smoother AddObserver StartEvent { puts "Smoothing" }
smoother AddObserver ProgressEvent { puts "." }
smoother AddObserver EndEvent { puts "Done smoothing" }

catch {vtkImplicitModeller modeller}
    modeller SetInput [smoother GetOutput]
    modeller SetMaximumDistance 0.25
    modeller SetSampleDimensions 100 100 100
#    modeller SetModelBounds -1.0 10.0 -1.0 3.0 -1.0 1.0

catch {vtkMetaImageWriter writer}
writer SetInput [modeller GetOutput]
writer SetFileName {z:/TEMP/ExtractRibs.mhd}
writer Write

vtkPolyDataMapper mapper
    mapper SetInput [smoother GetOutput]
vtkActor actor
    actor SetMapper mapper
   [actor GetProperty] SetRepresentationToWireframe

vtkPolyDataMapper cubesmapper
    cubesmapper SetInput [stripper GetOutput]
vtkActor cubesactor
    cubesactor SetMapper cubesmapper

# Create graphics stuff
vtkRenderer ren1
    ren1 AddActor actor
    ren1 AddActor cubesactor
vtkRenderWindow renWin
    renWin AddRenderer ren1
    renWin SetSize 300 300
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin
    iren Initialize

# render the image
iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
wm withdraw .
