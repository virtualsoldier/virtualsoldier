#
# Make vtk files of a given structure
#

package require vtk
package require vtkinteraction
package require vtktesting

if {$argc < 3} {
  puts "usage: vtkITK MakeAModel.tcl inputFile structureLabel outputFile"
  exit
}

set inputFile [lindex $argv 0]
puts $inputFile
set structureLabel [lindex $argv 1]
puts $structureLabel
set outputFile [lindex $argv 2]
puts $outputFile

#catch {vtkITKArchetypeImageSeriesReader reader}
#reader SetArchetype $inputFile
catch {vtkMetaImageReader reader}
reader SetFileName $inputFile
reader ReleaseDataFlagOn
reader AddObserver StartEvent { puts "Reading" }
reader AddObserver ProgressEvent { puts "." }
reader AddObserver EndEvent { puts "Done reading" }

catch {vtkDiscreteMarchingCubes cubes}
cubes SetInput [reader GetOutput]
cubes SetValue 0 $structureLabel
cubes ComputeNormalsOff
cubes ComputeScalarsOff
cubes ComputeGradientsOff
cubes AddObserver StartEvent { puts "Marching cubes" }
cubes AddObserver ProgressEvent { puts "." }
cubes AddObserver EndEvent { puts "Done marching cubes" }

#vtkPolyDataMapper mapper
#    mapper SetInput [cubes GetOutput]
#vtkActor actor
#    actor SetMapper mapper

# Create graphics stuff
#vtkRenderer ren1
#    ren1 AddActor actor
#vtkRenderWindow renWin
#    renWin AddRenderer ren1
#    renWin SetSize 300 300
#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin
#    iren Initialize

# render the image
#iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
#wm withdraw .

# Smooth the model with a WindowedSincPolyDataFilter
set iterations 15;
set passBand 0.001;
vtkWindowedSincPolyDataFilter smoother
smoother SetInput [ cubes GetOutput ]
smoother SetNumberOfIterations $iterations 
smoother BoundarySmoothingOff
smoother FeatureEdgeSmoothingOff
smoother SetPassBand $passBand
smoother NonManifoldSmoothingOn
smoother AddObserver StartEvent { puts "Smoothing" }
smoother AddObserver ProgressEvent { puts "." }
smoother AddObserver EndEvent { puts "Done smoothing" }


# Write the model out to file
catch {vtkPolyDataWriter writer}
writer SetFileName $outputFile
writer SetInput [smoother GetOutput]
writer SetFileTypeToBinary
writer Modified
writer Write