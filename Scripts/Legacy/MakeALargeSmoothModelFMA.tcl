#
# Make vtk files of a given structure
#

if {$argc < 1} {
  puts "usage: vtk MakeASmoothModel.tcl model_name"
  exit
}

set structure [lindex $argv 0]

source [file dirname [info script]]/PathsFMA.tcl

set modelPath ${SmoothedPath}
set LabelFilePattern "%s%d.png"
set structure [lindex $argv 0]
#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
  set SegmentNum($label,object) $object

}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

# Process one object at a time
# Limit number of input slices to SliceCapacity
set SliceCapacity 200

# pad with slices on top and bottom so that objects are capped
set iMin [lindex $Segment(${structure},bounds) 0]
set iMin [expr $iMin - 1]

set iMax [lindex $Segment(${structure},bounds) 1]
set iMax [expr $iMax + 1]

set jMin [lindex $Segment(${structure},bounds) 2]
set jMin [expr $jMin - 1]

set jMax [lindex $Segment(${structure},bounds) 3]
set jMax [expr $jMax + 1]

set sliceMin [lindex $Segment(${structure},bounds) 6]
set sliceMin [expr $sliceMin - 1]
if {$sliceMin < 1253} { set sliceMin 1253}

set sliceMax [lindex $Segment(${structure},bounds) 7]
set sliceMax [expr $sliceMax + 1]
if {$sliceMax > 1663} { set sliceMax 1663}

set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin $sliceMin"
puts "sliceMax $sliceMax"
puts "slices $slices"
if {$sliceMax < $sliceMin} {exit}
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
    catch {vtkPNGReader reader}
    reader SetDataSpacing .333333 .333333 1
    reader SetFilePattern $LabelFilePattern
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataOrigin 0 0 $sliceMin
    reader SetDataExtent 0 2047 0 1349 $slice $slice

  catch {vtkImageClip clip}
    clip SetInput [reader GetOutput]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr 1349 - $jMax] [expr 1349 - $jMin] $slice $slice
    clip Update

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInput [clip GetOutput]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    [clip GetOutput] SetSource ""
    clip Delete
}
  buildUpImage UpdateWholeExtent

  catch {vtkImageChangeInformation shiftOrigin}
    shiftOrigin SetInput [buildUpImage GetOutput]
    shiftOrigin SetOutputOrigin [expr $iMin * .333333] [expr ( 1349 - $jMax ) * .333333] $sliceMin

  catch {vtkImageAccumulate histo}
    histo SetInput [shiftOrigin GetOutput]
    histo SetComponentExtent 0 2999 0 0 0 0
    histo SetComponentOrigin 0 0 0
    histo SetComponentSpacing 1 1 1
    histo Update

  catch {vtkDiscreteMarchingCubes cubes}
    cubes SetInput [shiftOrigin GetOutput]
    cubes SetValue 0 $Segment(${structure},label)
    cubes ComputeNormalsOff
    cubes ComputeGradientsOff
    cubes ReleaseDataFlagOn
    cubes AddObserver EndEvent "puts \"Cubes complete\""
set iterations 15
set passBand 0.001

catch {vtkWindowedSincPolyDataFilter smoother}
    smoother SetInput [cubes GetOutput]
    smoother SetNumberOfIterations $iterations
    smoother BoundarySmoothingOff
    smoother FeatureEdgeSmoothingOff
    smoother SetPassBand $passBand
    smoother NonManifoldSmoothingOn
    smoother NormalizeCoordinatesOn
    smoother AddObserver EndEvent "puts \"Smoothing complete\""
    smoother ReleaseDataFlagOn

catch {vtkThreshold thresholder}
    thresholder SetInput [smoother GetOutput]
    thresholder SetAttributeModeToUseCellData
    thresholder ThresholdBetween $Segment(${structure},label) $Segment(${structure},label)
    thresholder AddObserver EndEvent "puts \"Thresholding complete\""
    thresholder ReleaseDataFlagOn

catch {vtkGeometryFilter geom}
    geom SetInput [thresholder GetOutput]
    geom AddObserver EndEvent "puts \"Geom complete\""

  catch {vtkPolyDataWriter writer}
    writer SetFileName "${modelPath}${structure}.vtk"
puts "${modelPath}${structure}.vtk"
    writer SetInput [geom GetOutput]
    writer SetFileTypeToBinary
    writer Modified
    writer Write

exit
