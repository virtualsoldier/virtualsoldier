
source [file dirname [info script]]/Paths.tcl
vtkRenderer ren1
vtkRenderWindow renWin
  renWin AddRenderer ren1
vtkRenderWindowInteractor iren
  iren SetRenderWindow renWin
package provide app-watershed 1.0
package require vtk
package require vtkinteraction
#---Global Variables
set sliceMax 1447
set sliceMin 1255
set sliceMax 1662
set photoSliceShrinkFactor 3
set ctSliceShrinkFactor 1
set ctXDim 512
set ctYDim 512
set photoXDim 2048
set photoYDim 1350
#---Read in color data
for { set slice [expr $sliceMin+1] } { $slice <= [expr $sliceMax+1] } { incr slice 3 } {
    catch {vtkPNGReader photoReader; photoReader AddObserver EndEvent {puts "Read [photoReader GetInternalFileName]" }}
    photoReader SetFileName ${RGBFilePrefix}${slice}.png
    photoReader SetDataExtent 0 2047 0 1349 0 0
    photoReader SetDataSpacing .3 .3 1
  catch {vtkImageClip photoReaderClip}
    photoReaderClip SetInput [photoReader GetOutput]
    photoReaderClip SetOutputWholeExtent 0 2047 0 1200 0 0     
    photoReaderClip ClipDataOn
  catch {vtkImageShrink3D photoReaderShrink}
    photoReaderShrink SetInput [photoReaderClip GetOutput]
    photoReaderShrink SetShrinkFactors $photoSliceShrinkFactor $photoSliceShrinkFactor 1
    photoReaderShrink Update
  catch {vtkImageAppend buildUpPhotoImage}
    buildUpPhotoImage AddInput [photoReaderShrink GetOutput]
    buildUpPhotoImage PreserveExtentsOff
    buildUpPhotoImage SetAppendAxis 2    
    [photoReaderShrink GetOutput] SetSource ""
    photoReaderShrink Delete
}
buildUpPhotoImage UpdateWholeExtent
buildUpPhotoImage ReleaseDataFlagOn
vtkImageChangeInformation photoShiftOrigin
  photoShiftOrigin SetInput [buildUpPhotoImage GetOutput]
eval photoShiftOrigin SetOutputSpacing [expr $photoSliceShrinkFactor*.3] [expr $photoSliceShrinkFactor*.3] 3
#  photoShiftOrigin ReleaseDataFlagOn
#---Convert Color Image to Gray Scale
vtkImageLuminance photoLuminance 
  photoLuminance SetInput [photoShiftOrigin GetOutput]
vtkImageChangeInformation photoCenter
  photoCenter SetInput [photoLuminance GetOutput]
  photoCenter CenterImageOn
vtkImageShrink3D photoCenterShrink
  photoCenterShrink SetInput [photoCenter GetOutput]
  photoCenterShrink SetShrinkFactors 4 4 1
  photoCenterShrink Update
#---Normalize the Luminance Image
vtkITKNormalizeImageFilter photoNormalize
  photoNormalize CastInputOn
  photoNormalize SetInput [photoCenterShrink GetOutput]
  photoNormalize Update
#---ct data begin
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice 3 } {
    catch {vtkPNGReader ctReader;ctReader AddObserver EndEvent {puts "Read [ctReader GetInternalFileName]" }}
    ctReader SetFileName ${VHMCTFilePrefix}${slice}.png
#    if { [file exists [ctReader GetFileName]] == 0} {
#        puts "Could not read [ctReader GetFileName]"
#        continue
#    }
    ctReader SetDataExtent 0 511 0 511 0 0
    ctReader SetDataSpacing .8984375 .8984375 1
  catch {vtkImageShrink3D ctReaderShrink}
    ctReaderShrink SetInput [ctReader GetOutput]
    ctReaderShrink SetShrinkFactors $ctSliceShrinkFactor $ctSliceShrinkFactor 1
    ctReaderShrink Update
  catch {vtkImageAppend buildUpCTImage}
    buildUpCTImage AddInput [ctReaderShrink GetOutput]
    buildUpCTImage PreserveExtentsOff
    buildUpCTImage SetAppendAxis 2    
# there are two slices that are shifted by 3 mm's
# try to correct that here
    if {$slice == 1447} {
        incr slice
    buildUpCTImage AddInput [ctReaderShrink GetOutput]
    buildUpCTImage PreserveExtentsOff
    buildUpCTImage SetAppendAxis 2    
    }
    if {$slice == 1481} {
    incr slice
    buildUpCTImage AddInput [ctReaderShrink GetOutput]
    buildUpCTImage PreserveExtentsOff
    buildUpCTImage SetAppendAxis 2    
    }
    [ctReaderShrink GetOutput] SetSource ""
    ctReaderShrink Delete
}
buildUpCTImage UpdateWholeExtent
buildUpCTImage ReleaseDataFlagOn
vtkImageChangeInformation ctShiftOrigin
  ctShiftOrigin SetInput [buildUpCTImage GetOutput]
eval ctShiftOrigin SetOutputSpacing [expr $ctSliceShrinkFactor*.8984375] [expr $ctSliceShrinkFactor*.8984375] 3
#  ctShiftOrigin ReleaseDataFlagOn
vtkImageChangeInformation ctCenter
  ctCenter SetInput [ctShiftOrigin GetOutput]  
  ctCenter CenterImageOn
vtkTransform initialTransform
vtkImageReslice initialCT
  initialCT SetInterpolationModeToCubic
  initialCT SetInput [ctCenter GetOutput] 
  initialCT SetResliceTransform initialTransform
  initialCT SetOutputSpacing [expr $ctSliceShrinkFactor*.8984375] [expr $ctSliceShrinkFactor*.8984375] 3
  initialCT SetBackgroundLevel 0
  initialCT Update
vtkImageShrink3D ctCenterShrink
    ctCenterShrink SetInput [initialCT GetOutput]
    ctCenterShrink SetShrinkFactors 4 4 1
    ctCenterShrink Update
vtkITKNormalizeImageFilter ctShiftNormalize
  ctShiftNormalize CastInputOn
  ctShiftNormalize SetInput [ctCenterShrink GetOutput]
  ctShiftNormalize Update
#---ct data end
vtkITKMutualInformationTransform mutual
    mutual SetSourceImage [ctShiftNormalize GetOutput]
    mutual SetTargetImage [photoNormalize GetOutput]
    mutual SetTargetStandardDeviation .4
    mutual SetSourceStandardDeviation .4
    mutual SetNumberOfIterations 0
    mutual SetNumberOfSamples 50
    mutual SetLearningRate .01
    mutual SetTranslateScale 64    
    mutual MattesOn
    mutual SetNumberOfSamples 10000
    mutual SetNumberOfHistogramBins 50
vtkImageReslice newCT
  newCT SetInterpolationModeToCubic
  newCT SetInput [initialCT GetOutput]
  newCT SetResliceTransform mutual
  newCT SetBackgroundLevel 0
eval newCT SetOutputExtent [[photoShiftOrigin GetOutput] GetWholeExtent]
eval newCT SetOutputSpacing [[photoShiftOrigin GetOutput] GetSpacing]
set colorLevel 1000
set colorWindow 2000
vtkWindowLevelLookupTable wlLut
  wlLut SetWindow $colorWindow
  wlLut SetLevel $colorLevel
  wlLut Build
vtkImageMapToColors wlCT
  wlCT SetInput [newCT  GetOutput]
  wlCT SetLookupTable wlLut
  wlCT SetOutputFormatToRGB  
vtkImageCheckerboard checkers
  checkers SetInput 1 [photoShiftOrigin GetOutput]
  checkers SetInput 0 [wlCT GetOutput]
  checkers SetNumberOfDivisions 2 2 1
vtkImageMapper imageMapperSource
    imageMapperSource SetInput [checkers GetOutput]
    imageMapperSource SetZSlice 2
    imageMapperSource SetColorWindow 255
    imageMapperSource SetColorLevel 127.5
vtkActor2D actor1 
    actor1 SetMapper imageMapperSource
#
# Add the actor to the renderer, set the background and size
ren1 AddActor actor1
renWin SetSize [expr int($photoXDim/$photoSliceShrinkFactor)] [expr int($photoYDim/$photoSliceShrinkFactor)]
#
# Render the image
iren AddObserver UserEvent {wm deiconify .vtkInteract}
renWin Render
vtkTransform a
vtkTransform b
proc slice {n} {
  imageMapperSource SetZSlice $n
  renWin Render
}
proc go {{repeat 10}} {
    global fileID
    for {set i 0} { $i < $repeat } {incr i} {
    mutual Modified
    renWin Render
        eval a SetMatrix [mutual GetMatrix]
        puts "[a GetOrientation] [a GetPosition] [a GetScale]"
    }
#    writeFile $repeat
}
vtkTransform foo
vtkTransform bar
iren Initialize
#make interface
source [file join [file dirname [info script]] WindowLevelCT.tcl]
proc SaveResampled {} {
    catch {vtkPNGWriter writer}
    writer SetFilePattern "%s%d.png"
    writer SetFileDimensionality 2
    writer SetFilePrefix "resampledVHM"
    writer SetInput [newCT GetOutput]
    writer Write
}
