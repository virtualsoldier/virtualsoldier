#!/bin/sh
if [ $# -lt 1 ]; then
    echo "usage: CopyFilesFromArchive PigDir [currentUserName]"
    exit
fi

if [ $# -lt 2 ]; then
    echo "usage: CopyFilesFromArchive PigDir [currentUserName]"
    currentUserName=padfield
else
    currentUserName=$2
fi



soldierRoot="/projects/soldier"
baseDir="${soldierRoot}/Data/CT/ISR"
transferDir="${soldierRoot}/Data/ArchiveTransfer"

cd $baseDir
for pigDir in $1
do
    echo pigDir: $pigDir
    scp ${currentUserName}@cvs.virtualsoldier.net:/home/archive/Files/NewSubjects/${pigDir}*up* $transferDir
    tarFiles=`dir ${transferDir}/${pigDir}*up*`
    echo tarFiles: $tarFiles
done

# In some cases there may be several tar files (like different parts and versions).  If so, we don't want them to overwrite one another.  The user will have to rename the directories in baseDir afterwards to reflect the correct names.  If there is only one tar file, there is no problem.
for tarFile in $tarFiles
do
    echo tarFile: $tarFile

    # Create a temporary directory and untar the file into it.
    outDir=TEMP
    mkdir $outDir
    cd $outDir

    tar zxf $tarFile
    rm $tarFile

    # Name TEMP to be the same name as the untarred directory except without a "up" at the end.
    untarredDirName=`dir`
    untarredDirNameLength=`expr length $untarredDirName`
    parentDirNameLength=`expr $untarredDirNameLength - 2`
    parentDirName=`expr substr $untarredDirName 1 $parentDirNameLength`
    
    cd $baseDir
    if [ ! -d $parentDirName ]; then
        mv $outDir $parentDirName
    else
        echo ERROR: Directory already exists!
        rm -rf TEMP
    fi
done

#outDir=${baseDir}/TEMP
#if [ ! -d $outDir ]; then
#    mkdir $outDir
#fi
#cd $outDir

# Untar all files that were downloaded to the output directory.
#for tarFile in `ls ${transferDir}/*.tgz`
#do
#    echo $tarFile
#    tar zxvf $tarFile
#done

#rm -rf ${transferDir}/*.tgz
#dir ${transferDir}/*.tgz

