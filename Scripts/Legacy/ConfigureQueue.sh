#!/bin/sh

# VALID LENGTHS:
# 1 =   >30 Minutes
# 2 =   >up to 4 hrs
# 3 =   >4 to 8 hrs
# 4 =   >8 to 16 hrs
# 5 =   >16 to 24 hrs
# 6 =   >1 to 2 days
# 7 =   >2 to 3 days
# 8 =   >Don't Know or > 3 days

# VALID PRIORITIES:
# debug
# low
# medium
# high
# urgent

#setup MANDATORY env variables:
export PRIORITY=high
export LENGTH=1
export NUMPROC=1


#call queue and runlimit calculator
export queue=`/software/ecomputing/gelsf/bin/calc_queue.pl`
export timeResource=`/software/ecomputing/gelsf/bin/calc_runlimit.pl`


# test to make sure output is returned... if not.. exit
if [ -z "$queue" ]; then
    echo "no queue... exit"
    exit 1
fi

if [ -z "$timeResource" ]; then
    echo "no time resource determined... exit"
    exit 1
fi

echo "Queue: $queue"
echo "TimeResource: $timeResource"
echo "_________"

