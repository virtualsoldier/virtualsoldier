operationType=$1
pigDir=$2
series=$3
imageNo=$4

# Read the MasterAnatomy file and create models for each structure
# These paths can be changed based on your setup
if (test $OS = "Windows_NT" )
then
    baseDir="c:/MYDOCU~1/VirtualSoldier-data/ISR"
elif (test $OS = "2.4" )
then
    baseDir="/projects/soldier/Data/CT/ISR"
fi

masterFile="${baseDir}/${pigDir}/${pigDir}lp/${pigDir}lpSeries${series}/${pigDir}fpSeries${series}MasterAnatomy.csv"
echo $masterFile
numberOfLines=`cat $masterFile | awk 'END {print NR}'`
numberOfLines=`expr $numberOfLines - 1`

# Loop through the file skipping the header line
line=$numberOfLines
while [ $line -ge 1 ]
do
    structureWithSpaces=`cut -d, -f1 $masterFile | tail -$line | head -1`
#    echo $structureWithSpaces

    numberOfWords=`cut -d, -f1 $masterFile | awk '{ print NF }' | tail -$line | head -1`
    structure=`cut -d, -f1 $masterFile | tail -$line | head -1 | cut -d' ' -f1`

    wordNumber=2
    while [ $wordNumber -le $numberOfWords ]
    do
        word=`cut -d, -f1 $masterFile | tail -$line | head -1 | cut -d' ' -f${wordNumber}`

        structure=${structure}_${word}
        wordNumber=`expr $wordNumber + 1`
    done

    # Create models except for "unknown", which is usually background.
    if (test $structure != "unknown" && test $structure != "Struct0" )
    then
        echo $structure
        ./PigLevelSets.sh $operationType $pigDir $series $imageNo 3 $structure dummy dummy dummy dummy
    fi

    line=`expr $line - 1`
done
