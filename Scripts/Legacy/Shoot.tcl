
set frameNumber 1000

proc Record {prefix} {
    global frameNumber
    catch {vtkRendererSource captureImage}
      captureImage SetInput ren1
      captureImage Modified
    catch {vtkPNGWriter captureWriter}
      captureWriter SetFileName ${prefix}${frameNumber}.png
    captureWriter SetInput [captureImage GetOutput]
      captureWriter Write
    incr frameNumber
}
			 