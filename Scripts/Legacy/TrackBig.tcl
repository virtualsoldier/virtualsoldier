#
# Generate a simulated track through a 3D model
#

package require vtk
package require vtkinteraction

source [file dirname [info script]]/Paths.tcl

set LabelFilePattern "%s%d.png"
set RGBFilePattern "%s%d.png"


###################################
# Parameters
#
set WHICH_MODELS_IN ${SmoothedPath}
set WHICH_MODELS_IN ${DecimatedPath}
set MAX_I 2048
set MAX_J 1350
set MIN_SLICE 1253
set MAX_SLICE 1663
set PASS_CELL_DATA 0
#
set MAX_I_EXTENT [expr $MAX_I - 1]
set MAX_J_EXTENT [expr $MAX_J - 1]
###################################

#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
  set Segment($label,object) $object
}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

set iMin 0
set iMax [expr $MAX_I - 1]
set jMin 0
set jMax [expr $MAX_J - 1]
set sliceMin $MIN_SLICE
set sliceMax $MAX_SLICE
set sliceIncr 2
set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin, Max, Number: $sliceMin, $sliceMax, $slices"
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice $sliceIncr} {
    catch {vtkPNGReader reader; reader AddObserver EndEvent {puts "Read [reader GetInternalFileName]"}
 }
    reader SetFilePattern $LabelFilePattern
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataExtent 0 $MAX_I_EXTENT 0 $MAX_J_EXTENT $slice $slice
    reader SetDataSpacing .33 .33 $sliceIncr

  catch {vtkImageClip clip}
    clip SetInput [reader GetOutput]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr $MAX_J_EXTENT - $jMax] [expr $MAX_J_EXTENT - $jMin] $slice $slice

  catch {vtkImageShrink3D shrink}
    shrink SetInput [clip GetOutput]
    shrink SetShrinkFactors 3 3 1
    shrink AveragingOff
    shrink Update

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInput [shrink GetOutput]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    [shrink GetOutput] SetSource ""
    shrink Delete
}
buildUpImage UpdateWholeExtent
buildUpImage ReleaseDataFlagOn

vtkImageChangeInformation shiftOrigin
  shiftOrigin SetInput [buildUpImage GetOutput]
eval shiftOrigin SetExtentTranslation 0 0 [expr -$sliceMin + ( $sliceMin / $sliceIncr )]
  shiftOrigin Update

vtkLineSource track
  track SetPoint1 400 250 1210
  track SetPoint2 400 250 1200
  track SetResolution 1000
  track Update

vtkLineSource bullet
  bullet SetPoint2 400 250 410
  bullet SetPoint1 400 400 400
  bullet SetResolution 2

vtkTubeFilter trackTube
  trackTube SetInput [bullet GetOutput]
  trackTube SetNumberOfSides 11
  trackTube SetRadius 4.5

vtkPolyDataMapper trackMapper
  trackMapper SetInput [trackTube GetOutput]

vtkRenderWindow renWin
  renWin SetSize 3200 2400
  renWin SetOffScreenRendering 1

  renWin SetOffScreenRendering 0
  renWin SetSize 640 480

vtkRenderer ren1
  renWin AddRenderer ren1
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin

set width [lindex [renWin GetSize] 0]
set height [lindex [renWin GetSize] 1]

set titleFontSize [expr int ( .08 * $height ) ]
set textFontSize [expr int ( .05 * $height ) ]

vtkTextProperty titleProperty
  titleProperty SetFontSize $titleFontSize
  titleProperty SetJustificationToCentered
  titleProperty ShadowOn

vtkTextMapper titleMapper
  titleMapper SetInput "Implicit Anatomical Modeling"
  titleMapper SetTextProperty titleProperty

vtkActor2D titleActor
  titleActor SetPosition [expr $width / 2] [expr int ( $height - 1.5 * $titleFontSize ) ]
  titleActor SetMapper titleMapper

vtkTextProperty textProperty
  textProperty SetFontSize $textFontSize
  textProperty ShadowOn

vtkTextMapper textMapper
  textMapper SetInput "foo"
  textMapper SetTextProperty textProperty

vtkActor2D textActor
  textActor SetPosition 25 25
  textActor SetMapper textMapper

vtkActor trackActor
  trackActor SetMapper trackMapper

vtkPolyDataReader torsoReader
  torsoReader SetFileName torso.vtk
vtkPolyDataMapper torsoMapper
  torsoMapper SetInput [torsoReader GetOutput]
  torsoMapper ImmediateModeRenderingOn
vtkLODActor torsoActor
  torsoActor SetMapper torsoMapper
 [torsoActor GetProperty] SetColor 1.0000 0.4900 0.2500

[torsoActor GetProperty] SetOpacity .3

ren1 AddActor trackActor
ren1 AddActor textActor
ren1 AddActor titleActor
ren1 AddActor torsoActor
ren1 SetBackground 0.4667 0.5333 0.6000

renWin Render
iren AddObserver UserEvent {wm deiconify .vtkInteract}
iren AddObserver EndPickEvent {puts "[[iren GetPicker] GetActor]"}
iren Initialize

wm withdraw .

##################################3
# supporting procs
#

proc makecolors {n} {
   catch {vtkLookupTable lut}
   catch {vtkMath math}
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
   }
}

makecolors 512

#
# Convert x,y,z in mm's to an i,j,k index
#
proc XYZToIJK { xyz } {
    # convert the position to an image index
    set idx [[shiftOrigin GetOutput] FindPoint [lindex $xyz 0] [lindex $xyz 1] [lindex $xyz 2]]
    # if the position is not in the image, idx is set to -1.
    # in this case, skip the rest of the method
    if {$idx == -1} {return {-1 0 0 0}}
    
    # get the bounds of the dataset
    set bounds [[shiftOrigin GetOutput] GetBounds]
    
    # convert position to ijk
    set spacing [[shiftOrigin GetOutput] GetSpacing]
    set origin [[shiftOrigin GetOutput] GetOrigin]
    
    set i [expr int(([lindex $xyz 0] - [lindex $origin 0]) / [lindex $spacing 0] + 0.5)]
    set j [expr int(([lindex $xyz 1] - [lindex $origin 1]) / [lindex $spacing 1] + 0.5)]
    set k [expr int(([lindex $xyz 2] - [lindex $origin 2]) / [lindex $spacing 2] + 0.5)]
    
    return [list $idx $i $j $k]
}

#
# Given an x,y,z coordinate in mm's, determine the label from the label map
#
proc ProbeLabels {px py pz} {
  set pickPosition "$px $py $pz"
  # convert to an index
  set ijk [XYZToIJK $pickPosition]
  set idx [lindex $ijk 0]
  # if idx is -1, then position was out of bounds, just return
  if {$idx == -1} { return 0}

  # probe the label 
  set l [[[[shiftOrigin GetOutput] GetPointData] GetScalars] GetValue $idx]
  return $l
}

#
# A list of structures to ignore
#
set Ignore {unknown mediastinum pericardium diaphragm central_tendon_of_diaphram}

#
# Show the labeled structures for each structure touched by the track
#
proc ShowLabels {{movie ""}} {
    global frameNumber
    global Labels Segment WHICH_MODELS_IN
    global lastObject Ignore
    puts "--------------------------"

    # Remove all actors from the renderer
    set actors [ren1 GetActors]
    $actors InitTraversal
    while {[set actor [$actors GetNextItem]] != ""} {
      ren1 RemoveActor $actor
      if {$actor != "trackActor" && $actor != "torsoActor"} {
	  [[[$actor GetMapper] GetInput] GetSource] Delete
	  [[$actor GetMapper] GetInput] Delete
	  [$actor GetMapper] Delete
	  $actor Delete
      }
    }
    ren1 AddActor trackActor
    ren1 AddActor torsoActor

    set frameNumber 1000
    set lastObject ""
    set points [[track GetOutput] GetPoints]
    set n [$points GetNumberOfPoints]
    for {set i 0} {$i < $n} {incr i} {
     set point [$points GetPoint $i]
     eval bullet SetPoint2 $point
     set label "[ProbeLabels [lindex $point 0] [lindex $point 1] [lindex $point 2]]"
      if {$lastObject != $Segment($label,object)} {
        puts "$i mm: $Segment($label,object)"
	set lastObject $Segment($label,object)
        set skip [lsearch -exact $Ignore $Segment($label,object)]
        if {[file exists ${WHICH_MODELS_IN}$Segment($label,object).vtk] == 1 && $skip == -1} {
          textMapper SetInput "$i mm: [PrettyPrint $Segment($label,object)]"
	  catch {vtkPolyDataReader $Segment($label,object)Reader }
	  $Segment($label,object)Reader SetFileName ${WHICH_MODELS_IN}$Segment($label,object).vtk
	  catch {vtkPolyDataMapper $Segment($label,object)Mapper}	    
          $Segment($label,object)Mapper SetInput [$Segment($label,object)Reader GetOutput]
	  $Segment($label,object)Mapper ImmediateModeRenderingOn
	  $Segment($label,object)Mapper SetLookupTable lut
	  $Segment($label,object)Mapper SetScalarRange 0 [lut GetNumberOfColors]
	  $Segment($label,object)Mapper ScalarVisibilityOff
	  catch {vtkLODActor $Segment($label,object)Actor}	    
          [$Segment($label,object)Actor GetProperty] SetColor 1 .2 .2
	  $Segment($label,object)Actor SetMapper $Segment($label,object)Mapper
	  ren1 RemoveActor $Segment($label,object)Actor
	  ren1 AddActor $Segment($label,object)Actor
eval      textProperty SetColor [lrange [lut GetTableValue $label] 0 2]
eval      textProperty SetColor 1 1 1

          [ren1 GetActiveCamera] SetViewUp 0 0 -1
          [ren1 GetActiveCamera] SetFocalPoint 0 0 0
          [ren1 GetActiveCamera] SetPosition 0 1 0
          ren1 ResetCamera
          [ren1 GetActiveCamera] Dolly 1.8
          ren1 ResetCameraClippingRange
          renWin Render
	  if {$movie != ""} {
		Record Movies/$movie
	  }
	  $Segment($label,object)Mapper ScalarVisibilityOn
        }
      }
    }
}

proc Spin {numFrames {movie ""}} {
  global frameNumber
  set frameNumber 1000
  set ang [expr 360.0 / $numFrames]
  for {set i 0} {$i < $numFrames} {incr i} {
    [ren1 GetActiveCamera] Azimuth $ang
    renWin Render
    if {$movie != ""} {
      Record Movies/$movie
    }
  }
}

#
# Generate a random track
#
proc Shoot {{movie ""}} {
  global sliceMin sliceMax
  set angle [math Random 0 6.28]
  set x [ expr ( cos ( $angle ) * 335 ) + 335 ] 
  set y [ expr ( sin ( $angle ) * 335 ) + 335 ]
  set z [math Random $sliceMin $sliceMax]
  track SetPoint2 400 250 1460
  track SetPoint1 $x $y $z
  bullet SetPoint1 $x $y $z
  track Update
  set l [[track GetOutput] GetLength]
  set resolution [expr int ( $l + .5 ) ]
  track SetResolution $resolution
  track Update
  puts "$x $y $z $resolution"
  ShowLabels $movie
}

#
# Generate a track originating at x,y,z
#
proc Shoot2 {x y z {movie ""}} {
  global sliceMin sliceMax

# since picked point is on skin, extend it a bit
    set dx [expr $x - 400]
    set dy [expr $y - 250]
    set dz [expr $z - 1460]
    set x [expr 400 + 1.20 * $dx ]
    set y [expr 250 + 1.20 * $dy ]
    set z [expr 1460 + 1.20 * $dz ]
    track SetPoint2 400 250 1460
    track SetPoint1 $x $y $z
    bullet SetPoint1 $x $y $z
    track Update
    set l [[track GetOutput] GetLength]
    set resolution [expr int ( $l + .5 ) ]
    track SetResolution $resolution
    track Update
    ShowLabels $movie
}

#
# Use the 'p' key to pick a point to start
#

proc PickShot {} {
    [torsoActor GetProperty] SetOpacity .3
    puts "shot at: [[iren GetPicker] GetPickPosition]"
    eval Shoot2 [[iren GetPicker] GetPickPosition]
}

proc PickStart {} {
    [torsoActor GetProperty] SetOpacity 1
}

iren AddObserver EndPickEvent PickShot
iren AddObserver StartPickEvent PickStart


proc PrettyPrint {name} {
  regsub -all "_" $name " " pretty
  regsub -all " br " $pretty " branch " pretty
  regsub -all " lt " $pretty " left " pretty
  regsub -all " rt " $pretty " right " pretty
  regsub -all " pul " $pretty " pulmonary " pretty
  foreach letter {a b c d e f g h i j k l m n o p q r s t u v w x y z} {
      set uppercaseLetter [string toupper $letter] 
      regsub -all " $letter" $pretty " $uppercaseLetter" pretty
  }
  set firstLetter [string index $pretty 0]
  set uppercaseFirstLetter [string toupper $firstLetter] 
  regsub "[string index $pretty 0]" $pretty "$uppercaseFirstLetter" pretty 
  return $pretty
}

set frameNumber 1000

#
# Record a frame
#
proc Record {prefix} {
    global frameNumber

    catch {vtkWindowToImageFilter captureImage}
      captureImage SetInput renWin
      captureImage Modified

    catch {vtkPNGWriter captureWriter}
      captureWriter SetFileName ${prefix}${frameNumber}.png
      captureWriter SetInput [captureImage GetOutput]
      captureWriter Write

    incr frameNumber
}

			 

