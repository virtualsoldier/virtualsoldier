set prog=c:\millerjv\Watershed\lib\vtk\vtkITK
%prog% MakeALargeSmoothModel.tcl brachial_veins
%prog% MakeALargeSmoothModel.tcl cephalic_vein
%prog% MakeALargeSmoothModel.tcl deltoid_muscle
%prog% MakeALargeSmoothModel.tcl diaphragm
%prog% MakeALargeSmoothModel.tcl eighth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl eighth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl eighth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl eleventh_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl eleventh_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl eleventh_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl external_intercostal_muscles
%prog% MakeALargeSmoothModel.tcl fifth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fifth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fifth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl first_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl first_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl first_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fourth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fourth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fourth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl fourth_posterior_intercostal_artery
%prog% MakeALargeSmoothModel.tcl infraspinatus_muscle
%prog% MakeALargeSmoothModel.tcl innermost_intercostal_muscles
%prog% MakeALargeSmoothModel.tcl internal_intercostal_muscles
%prog% MakeALargeSmoothModel.tcl latissimus_dorsi_muscle
%prog% MakeALargeSmoothModel.tcl left_costal_cartilages
%prog% MakeALargeSmoothModel.tcl left_eighth_rib
%prog% MakeALargeSmoothModel.tcl left_fifth_rib
%prog% MakeALargeSmoothModel.tcl left_fourth_rib
%prog% MakeALargeSmoothModel.tcl left_lower_lobe_of_lung
%prog% MakeALargeSmoothModel.tcl left_lung
%prog% MakeALargeSmoothModel.tcl left_ninth_rib
%prog% MakeALargeSmoothModel.tcl left_second_rib
%prog% MakeALargeSmoothModel.tcl left_seventh_rib
%prog% MakeALargeSmoothModel.tcl left_sixth_rib
%prog% MakeALargeSmoothModel.tcl left_tenth_rib
%prog% MakeALargeSmoothModel.tcl left_third_rib
%prog% MakeALargeSmoothModel.tcl left_twelfth_rib
%prog% MakeALargeSmoothModel.tcl left_upper_lobe_of_lung
%prog% MakeALargeSmoothModel.tcl mediastinum
%prog% MakeALargeSmoothModel.tcl muscle_of_back
%prog% MakeALargeSmoothModel.tcl muscle_of_trunk
%prog% MakeALargeSmoothModel.tcl nineth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl nineth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl nineth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl pectoralis_major_muscle
%prog% MakeALargeSmoothModel.tcl pectoralis_minor_muscle
%prog% MakeALargeSmoothModel.tcl rhomboid_major_muscle
%prog% MakeALargeSmoothModel.tcl rhomboid_minor_muscle
%prog% MakeALargeSmoothModel.tcl right_costal_cartilages
%prog% MakeALargeSmoothModel.tcl right_eighth_rib
%prog% MakeALargeSmoothModel.tcl right_eleventh_rib
%prog% MakeALargeSmoothModel.tcl right_fifth_rib
%prog% MakeALargeSmoothModel.tcl right_first_rib
%prog% MakeALargeSmoothModel.tcl right_fourth_rib
%prog% MakeALargeSmoothModel.tcl right_lower_lobe_of_lung
%prog% MakeALargeSmoothModel.tcl right_ninth_rib
%prog% MakeALargeSmoothModel.tcl right_second_rib
%prog% MakeALargeSmoothModel.tcl right_seventh_rib
%prog% MakeALargeSmoothModel.tcl right_sixth_rib
%prog% MakeALargeSmoothModel.tcl right_tenth_rib
%prog% MakeALargeSmoothModel.tcl right_third_rib
%prog% MakeALargeSmoothModel.tcl right_twelfth_rib
%prog% MakeALargeSmoothModel.tcl right_upper_lobe_of_lung
%prog% MakeALargeSmoothModel.tcl second_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl second_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl second_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl serratus_anterior_muscle
%prog% MakeALargeSmoothModel.tcl seventh_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl seventh_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl seventh_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl sixth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl sixth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl sixth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl sternocleidomastoid_muscle
%prog% MakeALargeSmoothModel.tcl subclavius_muscle
%prog% MakeALargeSmoothModel.tcl supraspinatus_muscle
%prog% MakeALargeSmoothModel.tcl tenth_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl tenth_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl tenth_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl teres_major_muscle
%prog% MakeALargeSmoothModel.tcl teres_minor_muscle
%prog% MakeALargeSmoothModel.tcl third_external_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl third_innermost_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl third_internal_intercostal_muscle
%prog% MakeALargeSmoothModel.tcl tracheal_muscle
%prog% MakeALargeSmoothModel.tcl trapezius_muscle

