# a simple user interface that manipulates window level.
# places in the tcl top window.  Looks for object named viewer

# Take window level parameters from viewer
proc InitializeWindowLevelInterface {} {
   global vhmCrossSectionSliceMin 
   global vhmCrossSectionSliceMax 
   global vhmCrossSectionSliceInc 
   global vhmCTSliceMin
   global vhmCTSliceMax
   global vhmCTSliceInc
   global mutual
   global sliceNumber
   global histBins 
   global mattesSamples
   global violaWellsSamples
   global fixedImStd 
   global movingImStd 
   global learningRate 
   global quaternionRigidIterations 
   global affineIterations
   global minStepLength 
   global maxStepLength 
   global quaternionRigidTranslateScale
   global affineTranslateScale

   global metricFrame
   global transformFrame
   global violaWellsParamsFrame
   global mattesParamsFrame
   global affineParamsFrame
   global quaternionRigidParamsFrame

   # Get parameters from viewer
   set w [wlLut GetWindow]
   set l [wlLut GetLevel]
   set sliceNumber [imageMapperSource GetZSlice]
   set zMin 0
    set zMax [expr [expr {$vhmCrossSectionSliceMax-$vhmCrossSectionSliceMin}] / ${vhmCrossSectionSliceMin}]
    if {[expr [expr {$vhmCTSliceMax-$vhmCTSliceMin}] / ${vhmCTSliceInc}] > ${zMax}} {
        set zMax [expr [expr {$vhmCTSliceMax-$vhmCTSliceMin}] / ${vhmCTSliceInc}]
    }


   frame .slice
   label .slice.label -text "Slice"
   scale .slice.scale -from $zMin -to $zMax -orient horizontal \
     -command SetSlice -variable sliceNumber
   
   frame .wl
   frame .wl.f1
   label .wl.f1.windowLabel -text "Window"
   scale .wl.f1.window -from 1 -to [expr $w * 2]  -orient horizontal \
     -command SetWindow -variable window
   frame .wl.f2
   label .wl.f2.levelLabel -text "Level"
   scale .wl.f2.level -from [expr $l - $w] -to [expr $l + $w] \
     -orient horizontal -command SetLevel
   checkbutton .wl.video -text "Inverse Video" -command SetInverseVideo

#--METRIC----------------------------
   set metric [TitleFrame .wl.metric -text "Metric"]
   set metricFrame [$metric getframe]
   
#--Mattes----------------------------
   radiobutton $metricFrame.mattesRadio -text "Mattes" -command SetMattes -variable metricSelect -value metricMattes
   $metricFrame.mattesRadio select
   set mattesParams [TitleFrame $metricFrame.mattesParams -text "Mattes Parameters"]
   set mattesParamsFrame [$mattesParams getframe]
      
   label $mattesParamsFrame.histBinsLabel -text "Histogram Bins" 
   SpinBox $mattesParamsFrame.histBinsSpin  -range "0 100000 1" -width 6 -command SetHistBins -modifycmd SetHistBins -textvariable histBins 

   label $mattesParamsFrame.spatialSamplesLabel -text "Spatial Samples"
   SpinBox $mattesParamsFrame.spatialSamplesSpin  -range "0 100000 1" -width 6 -command SetMattesSamples -modifycmd SetMattesSamples -textvariable mattesSamples
#--Viola Wells-----------------------
   radiobutton $metricFrame.violaWellsRadio -text "Viola Wells" -command SetViolaWells -variable metricSelect -value metricViolaWells
   $metricFrame.violaWellsRadio deselect
   set violaWellsParams [TitleFrame $metricFrame.violaWellsParams -text "Viola Wells Parameters"]
   set violaWellsParamsFrame [$violaWellsParams getframe]
      
   label $violaWellsParamsFrame.fixedImStdLabel -text "Fixed Image Sigma" -foreground gray
   SpinBox $violaWellsParamsFrame.fixedImStdSpin  -range "0 10 .01" -width 6 -command SetFixedImStd -modifycmd SetFixedImStd -textvariable fixedImStd -state disabled

   label $violaWellsParamsFrame.movingImStdLabel -text "Moving Image Sigma" -foreground gray
   SpinBox $violaWellsParamsFrame.movingImStdSpin  -range "0 10 .01" -width 6 -command SetMovingImStd -modifycmd SetMovingImStd -textvariable movingImStd -state disabled

   label $violaWellsParamsFrame.numberSamplesLabel -text "Spatial Samples" -foreground gray
   SpinBox $violaWellsParamsFrame.numberSamplesSpin  -range "0 100 1" -width 6 -command SetViolaWellsSamples -modifycmd SetViolaWellsSamples -textvariable violaWellsSamples -state disabled
#--TRANSFORM--------------------------
   set transform [TitleFrame .wl.transform -text "Transform"]
   set transformFrame [$transform getframe]
#--Quaternion Rigid-------------------
   radiobutton $transformFrame.quaternionRigidRadio -text "Quaternion Rigid" -command SetQuaternionRigid -variable transformSelect -value transformQuaternionRigid
   $transformFrame.quaternionRigidRadio select
   set quaternionRigidParams [TitleFrame $transformFrame.quaternionRigidParams -text "Quaternion Rigid Optimizer"]
   set quaternionRigidParamsFrame [$quaternionRigidParams getframe]

   label $quaternionRigidParamsFrame.learningRateLabel -text "Learning Rate" 
   SpinBox $quaternionRigidParamsFrame.learningRateSpin -range "0 20 1" -width 6 -command SetLearningRate -modifycmd SetLearningRate -textvariable learningRate 

   label $quaternionRigidParamsFrame.iterationsLabel -text "Iterations" 
   SpinBox $quaternionRigidParamsFrame.iterationsSpin -range "0 500 1" -width 6 -command SetQuaternionRigidIterations -modifycmd SetQuaternionRigidIterations -textvariable quaternionRigidIterations 

   label $quaternionRigidParamsFrame.translateScaleLabel -text "Translate Scale" 
   SpinBox $quaternionRigidParamsFrame.translateScaleSpin -range "0 1000 1" -width 6 -command SetQuaternionRigidTranslateScale -modifycmd SetQuaternionRigidTranslateScale -textvariable quaternionRigidTranslateScale 
#--Affine-----------------------------
   radiobutton $transformFrame.affineRadio -text "Affine" -command SetAffine -variable transformSelect -value transformAffine
   $transformFrame.affineRadio deselect
   set affineParams [TitleFrame $transformFrame.affineParams -text "Regular Step Optimizer"]
   set affineParamsFrame [$affineParams getframe]

   label $affineParamsFrame.minStepLengthLabel -text "Min Step Length" -foreground gray
   SpinBox $affineParamsFrame.minStepLengthSpin -range "0 5 .001" -width 6 -command SetMinStepLength -modifycmd SetMinStepLength -textvariable minStepLength -state disabled

   label $affineParamsFrame.maxStepLengthLabel -text "Max Step Length" -foreground gray
   SpinBox $affineParamsFrame.maxStepLengthSpin -range "0 5 .001" -width 6 -command SetMaxStepLength -modifycmd SetMaxStepLength -textvariable maxStepLength -state disabled

   label $affineParamsFrame.iterationsLabel -text "Iterations" -foreground gray
   SpinBox $affineParamsFrame.iterationsSpin -range "0 500 1" -width 6 -command SetAffineIterations -modifycmd SetAffineIterations -textvariable affineIterations -state disabled

   label $affineParamsFrame.translateScaleLabel -text "Translate Scale" -foreground gray
   SpinBox $affineParamsFrame.translateScaleSpin -range "0 1000 1" -width 6 -command SetAffineTranslateScale -modifycmd SetAffineTranslateScale -textvariable affineTranslateScale -state disabled
#-------------------------------------
   frame .wl.f3
    label .wl.f3.checkerLabel -text "Checkers"
    scale .wl.f3.ch -orient horizontal -from 1 -to 10 -command SetCheckerboard -variable checkerboard
   checkbutton .wl.swapCheck -text "Swap Checkers" -command SwapCheckers 

   frame .wl.f6
    label .wl.f6.subsampleLabel -text "SubSample Rate"
    scale .wl.f6.sub -from 1 -to 16 -orient horizontal -command SetSubSample

    button .wl.go -text "Execute" -command "go"

   # resolutions less than 1.0
   if {$w < 10} {
      set res [expr 0.05 * $w]
      .wl.f1.window configure -resolution $res -from $res -to [expr 2.0 * $w]
      .wl.f2.level configure -resolution $res \
	-from [expr 0.0 + $l - $w] -to [expr 0.0 + $l + $w] 
   }

   .wl.f1.window set $w
   .wl.f2.level set $l
   .wl.f3.ch set 2
   .wl.f6.sub set 1

   frame .ex
   button .ex.exit -text "Exit" -command "exit"

   pack .slice .wl .ex -side top
   pack .slice.label .slice.scale -side left
   pack .wl.f1 .wl.f2 .wl.video -side top
   pack .wl.f1.windowLabel .wl.f1.window -side left
   pack .wl.f2.levelLabel .wl.f2.level -side left
   pack .wl.metric -side top
   grid config $metricFrame.mattesRadio -column 0 -row 0
   grid config $metricFrame.mattesParams -column 1 -row 0
   grid config $metricFrame.violaWellsRadio -column 0 -row 1
   grid config $metricFrame.violaWellsParams -column 1 -row 1
   grid config $mattesParamsFrame.histBinsLabel -column 0 -row 0
   grid config $mattesParamsFrame.histBinsSpin -column 1 -row 0
   grid config $mattesParamsFrame.spatialSamplesLabel -column 0 -row 1
   grid config $mattesParamsFrame.spatialSamplesSpin  -column 1 -row 1
   grid config $violaWellsParamsFrame.fixedImStdLabel -column 0 -row 0
   grid config $violaWellsParamsFrame.fixedImStdSpin -column 1 -row 0
   grid config $violaWellsParamsFrame.movingImStdLabel -column 0 -row 1
   grid config $violaWellsParamsFrame.movingImStdSpin -column 1 -row 1
   grid config $violaWellsParamsFrame.numberSamplesLabel -column 0 -row 2
   grid config $violaWellsParamsFrame.numberSamplesSpin -column 1 -row 2
   pack .wl.transform -side top
   grid config $transformFrame.quaternionRigidRadio -column 0 -row 0
   grid config $transformFrame.quaternionRigidParams -column 1 -row 0
   grid config $transformFrame.affineRadio -column 0 -row 1
   grid config $transformFrame.affineParams -column 1 -row 1
   grid config $quaternionRigidParamsFrame.learningRateLabel -column 0 -row 0
   grid config $quaternionRigidParamsFrame.learningRateSpin -column 1 -row 0
   grid config $quaternionRigidParamsFrame.iterationsLabel -column 0 -row 1
   grid config $quaternionRigidParamsFrame.iterationsSpin -column 1 -row 1                
   grid config $quaternionRigidParamsFrame.translateScaleLabel -column 0 -row 2
   grid config $quaternionRigidParamsFrame.translateScaleSpin -column 1 -row 2               
   grid config $affineParamsFrame.maxStepLengthLabel -column 0 -row 0
   grid config $affineParamsFrame.maxStepLengthSpin -column 1 -row 0
   grid config $affineParamsFrame.minStepLengthLabel -column 0 -row 1
   grid config $affineParamsFrame.minStepLengthSpin -column 1 -row 1
   grid config $affineParamsFrame.iterationsLabel -column 0 -row 2
   grid config $affineParamsFrame.iterationsSpin -column 1 -row 2
   grid config $affineParamsFrame.translateScaleLabel -column 0 -row 3
   grid config $affineParamsFrame.translateScaleSpin -column 1 -row 3

   pack .wl.f3 -side top
   pack .wl.f3.checkerLabel .wl.f3.ch -side left
   pack .wl.swapCheck -side top
   pack .wl.f6 -side top
   pack .wl.f6.subsampleLabel .wl.f6.sub -side left
   pack .wl.go -side top
   pack .ex.exit -side left
}
   
proc SetCheckerboard { val } {
    global checkerboard
    checkers SetNumberOfDivisions $checkerboard $checkerboard 1
    renWin Render
}

proc SwapCheckers {} {
    set temp0 [checkers GetInput 0]
    set temp1 [checkers GetInput 1]

    checkers SetInput 0 $temp1
    checkers SetInput 1 $temp0
    checkers Modified

    renWin Render
}

proc SetSlice { slice } {
   global sliceNumber

   imageMapperSource SetZSlice $slice
   renWin Render
}

proc SetWindow window {
   global video
   if {$video} {
      wlLut SetWindow [expr -$window]
   } else {
      wlLut SetWindow $window
   }
   renWin Render
}

proc SetLevel level {
   wlLut SetLevel $level
   renWin Render
}

proc SetInverseVideo {} {
   global video window
   if {$video} {
      wlLut SetWindow [expr -$window]
   } else {
      wlLut SetWindow $window
   }
   renWin Render
}

#proc SetIterations iter {
#    mutual SetNumberOfIterations $iter
#}

#proc SetLearning learn {
#    mutual SetLearningRate [expr $learn / 10000.0]
#}

proc SetSubSample sub {
    ctCenterShrink SetShrinkFactors $sub $sub 1
    photoCenterShrink SetShrinkFactors $sub $sub 1
}

proc SetHistBins { {value default} } {
    global mutual
    global histBins
    if {$value != "default"} {
        set histBins $value    
    }    
    mutual SetNumberOfHistogramBins $histBins 
}

proc SetAffineTranslateScale { {value default} } {
    global mutual
    global affineTranslateScale
    if {$value != "default"} {
        set affineTranslateScale $value    
    }    
    mutual SetTranslateScale $affineTranslateScale 
}

proc SetQuaternionRigidTranslateScale { {value default} } {
    global mutual
    global quaternionRigidTranslateScale
    if {$value != "default"} {
        set quaternionRigidTranslateScale $value    
    }    
    mutual SetTranslateScale $quaternionRigidTranslateScale 
}

proc SetMattes {}  {
    global mutual
    global metricFrame
    global transformSelect
    global text2Mapper
    global violaWellsParamsFrame
    global mattesParamsFrame

    mutual MattesOn
    SetHistBins
    SetMattesSamples

    if { $transformSelect == "transformQuaternionRigid" } {
        text2Mapper SetInput "Mattes\nRigid"
    } else {
        text2Mapper SetInput "Mattes\nAffine"
    }

    $violaWellsParamsFrame.fixedImStdLabel config -foreground gray
    $violaWellsParamsFrame.movingImStdLabel config -foreground gray
    $violaWellsParamsFrame.numberSamplesLabel config -foreground gray
    $violaWellsParamsFrame.fixedImStdSpin configure -state disabled
    $violaWellsParamsFrame.movingImStdSpin configure -state disabled
    $violaWellsParamsFrame.numberSamplesSpin configure -state disabled

    $mattesParamsFrame.histBinsLabel config -foreground black
    $mattesParamsFrame.spatialSamplesLabel config -foreground black
    $mattesParamsFrame.histBinsSpin configure -state normal
    $mattesParamsFrame.spatialSamplesSpin configure -state normal
}

proc SetMattesSamples { {value default} } {
    global mutual
    global mattesSamples
    if {$value != "default"} {
        set mattesSamples $value    
    }    
    mutual SetNumberOfSamples $mattesSamples
}

proc SetViolaWellsSamples { {value default} } {
    global mutual
    global violaWellsSamples
    if {$value != "default"} {
        set violaWellsSamples $value    
    }    
    mutual SetNumberOfSamples $violaWellsSamples
}

proc SetFixedImStd { {value default} } {
    global mutual
    global fixedImStd
    if {$value != "default"} {
        set fixedImStd $value    
    }    
    mutual SetTargetStandardDeviation $fixedImStd
}

proc SetMovingImStd { {value default} } {
    global mutual
    global movingImStd
    if {$value != "default"} {
        set movingImStd $value    
    }    
    mutual SetSourceStandardDeviation $movingImStd
}

proc SetLearningRate { {value default} } {
    global mutual
    global learningRate
    if {$value != "default"} {
        set learningRate $value    
    }    
    mutual SetLearningRate [expr $learningRate / 10000.0]
}

proc SetQuaternionRigidIterations { {value default} } {
    global mutual
    global quaternionRigidIterations
    if {$value != "default"} {
        set quaternionRigidIterations $value    
    }    
    mutual SetNumberOfIterations $quaternionRigidIterations
}

proc SetAffineIterations { {value default} } {
    global mutual
    global affineIterations
    if {$value != "default"} {
        set affineIterations $value    
    }    
    mutual SetNumberOfIterations $affineIterations
}

proc SetMinStepLength { {value default} } {
    global mutual
    global minStepLength
    if {$value != "default"} {
        set minStepLength $value    
    }    
    mutual SetMinimumStepLength $minStepLength
}

proc SetMaxStepLength { {value default} } {
    global mutual
    global maxStepLength
    if {$value != "default"} {
        set maxStepLength $value    
    }    
    mutual SetMaximumStepLength $maxStepLength
}

proc SetViolaWells {} {
    global mutual
    global metricFrame
    global transformSelect
    global text2Mapper
    global violaWellsParamsFrame
    global mattesParamsFrame

    mutual ViolaWellsOn
    SetFixedImStd
    SetMovingImStd
    SetViolaWellsSamples

    if { $transformSelect == "transformQuaternionRigid" } {
        text2Mapper SetInput "ViolaWells\nRigid"
    } else {
        text2Mapper SetInput "ViolaWells\nAffine"
    }

    $violaWellsParamsFrame.fixedImStdLabel config -foreground black
    $violaWellsParamsFrame.movingImStdLabel config -foreground black
    $violaWellsParamsFrame.numberSamplesLabel config -foreground black
    $violaWellsParamsFrame.fixedImStdSpin configure -state normal
    $violaWellsParamsFrame.movingImStdSpin configure -state normal
    $violaWellsParamsFrame.numberSamplesSpin configure -state normal

    $mattesParamsFrame.histBinsLabel config -foreground gray
    $mattesParamsFrame.spatialSamplesLabel config -foreground gray
    $mattesParamsFrame.histBinsSpin configure -state disabled
    $mattesParamsFrame.spatialSamplesSpin configure -state disabled
}

proc SetQuaternionRigid {} {
    global mutual
    global transformFrame
    global metricSelect
    global text2Mapper
    global affineParamsFrame
    global quaternionRigidParamsFrame

    mutual SetModeToQuaternionRigid
    SetLearningRate
    SetQuaternionRigidIterations
    SetQuaternionRigidTranslateScale

    if { $metricSelect == "metricViolaWells" } {
        text2Mapper SetInput "ViolaWells\nRigid"
    } else {
        text2Mapper SetInput "Mattes\nRigid"
    }

    $affineParamsFrame.maxStepLengthLabel config -foreground gray
    $affineParamsFrame.minStepLengthLabel config -foreground gray
    $affineParamsFrame.iterationsLabel config -foreground gray
    $affineParamsFrame.translateScaleLabel config -foreground gray
    $affineParamsFrame.maxStepLengthSpin configure -state disabled
    $affineParamsFrame.minStepLengthSpin configure -state disabled
    $affineParamsFrame.iterationsSpin configure -state disabled
    $affineParamsFrame.translateScaleSpin configure -state disabled
    
    $quaternionRigidParamsFrame.learningRateLabel config -foreground black
    $quaternionRigidParamsFrame.iterationsLabel config -foreground black
    $quaternionRigidParamsFrame.translateScaleLabel config -foreground black
    $quaternionRigidParamsFrame.learningRateSpin configure -state normal
    $quaternionRigidParamsFrame.iterationsSpin configure -state normal
    $quaternionRigidParamsFrame.translateScaleSpin configure -state normal 
}

proc SetAffine {} {
    global mutual
    global transformFrame
    global metricSelect
    global text2Mapper
    global affineParamsFrame
    global quaternionRigidParamsFrame

    mutual SetModeToAffine    
    SetMinStepLength
    SetMaxStepLength
    SetAffineIterations
    SetAffineTranslateScale

    if { $metricSelect == "metricViolaWells" } {
        text2Mapper SetInput "ViolaWells\nAffine"
    } else {
        text2Mapper SetInput "Mattes\nAffine"
    }

    $affineParamsFrame.maxStepLengthLabel config -foreground black
    $affineParamsFrame.minStepLengthLabel config -foreground black
    $affineParamsFrame.iterationsLabel config -foreground black
    $affineParamsFrame.translateScaleLabel config -foreground black
    $affineParamsFrame.maxStepLengthSpin configure -state normal
    $affineParamsFrame.minStepLengthSpin configure -state normal
    $affineParamsFrame.iterationsSpin configure -state normal
    $affineParamsFrame.translateScaleSpin configure -state normal
    
    $quaternionRigidParamsFrame.learningRateLabel config -foreground gray
    $quaternionRigidParamsFrame.iterationsLabel config -foreground gray 
    $quaternionRigidParamsFrame.translateScaleLabel config -foreground gray
    $quaternionRigidParamsFrame.learningRateSpin configure -state disabled
    $quaternionRigidParamsFrame.iterationsSpin configure -state disabled
    $quaternionRigidParamsFrame.translateScaleSpin configure -state disabled 
}

InitializeWindowLevelInterface