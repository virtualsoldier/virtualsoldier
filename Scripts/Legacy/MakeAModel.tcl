#
# Make vtk files of a given structure
#

if {$argc < 1} {
  puts "usage: vtkITK MakeAModel.tcl model_name"
  exit
}

set structure [lindex $argv 0]

if {$tcl_platform(platform) == "windows"} {
  set MasterAnatomyFile "s:/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "s:/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Relabeled/Relabeled"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Repaired/Repaired"
  set modelPath "s:/Data/Atlas/Processed/Models/Discrete/"
} else {
  set MasterAnatomyFile "/projects/soldier/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "/projects/soldier/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "/projects/soldier/Data/Atlas/Processed/Relabeled/Relabeled"
  set modelPath "/projects/soldier/Data/Atlas/Processed/Models/Discrete"
}
set LabelFilePattern "%s%d.png"
set structure [lindex $argv 0]
#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

# Process one object at a time
# Limit number of input slices to SliceCapacity
set SliceCapacity 200

# pad with slices on top and bottom so that objects are capped
set iMin [lindex $Segment(${structure},bounds) 0]
set iMin [expr $iMin - 1]

set iMax [lindex $Segment(${structure},bounds) 1]
set iMax [expr $iMax + 1]

set jMin [lindex $Segment(${structure},bounds) 2]
set jMin [expr $jMin - 1]

set jMax [lindex $Segment(${structure},bounds) 3]
set jMax [expr $jMax + 1]

set sliceMin [lindex $Segment(${structure},bounds) 6]
set sliceMin [expr $sliceMin - 1]
if {$sliceMin < 1253} { set sliceMin 1253}

set sliceMax [lindex $Segment(${structure},bounds) 7]
set sliceMax [expr $sliceMax + 1]
if {$sliceMax > 1663} { set sliceMax 1663}

set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin $sliceMin"
puts "sliceMax $sliceMax"
puts "slices $slices"
if {$sliceMax < $sliceMin} {exit}
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
    catch {vtkPNGReader reader; reader AddObserver ProgressEvent {puts -nonewline "."}; flush $stdout }
    reader SetDataSpacing .333333 .333333 1
    reader SetFilePattern $LabelFilePattern
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataOrigin 0 0 $sliceMin
    reader SetDataExtent 0 2047 0 1349 $slice $slice

  catch {vtkImageClip clip}
    clip SetInput [reader GetOutput]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr 1349 - $jMax] [expr 1349 - $jMin] $slice $slice
    clip Update

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInput [clip GetOutput]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    [clip GetOutput] SetSource ""
    clip Delete
}
  buildUpImage UpdateWholeExtent
puts "[[buildUpImage GetOutput] Print]"
puts "[[buildUpImage GetOutput] GetScalarRange]"
  buildUpImage ReleaseDataFlagOn
  catch {vtkImageChangeInformation shiftOrigin}
    shiftOrigin SetInput [buildUpImage GetOutput]
    shiftOrigin SetOutputOrigin [expr $iMin * .333333] [expr ( 1349 - $jMax ) * .333333] $sliceMin
  shiftOrigin ReleaseDataFlagOn

  catch {vtkDiscreteMarchingCubes cubes}
    cubes SetInput [shiftOrigin GetOutput]
    cubes SetValue 0 $Segment(${structure},label)
    cubes ComputeNormalsOff
    cubes ComputeScalarsOff
    cubes ComputeGradientsOff

  catch {vtkPolyDataWriter writer}
    writer SetFileName "${modelPath}${structure}.vtk"
puts "${modelPath}${structure}.vtk"
    writer SetInput [cubes GetOutput]
    writer SetFileTypeToBinary
    writer Modified
    writer Write

exit
