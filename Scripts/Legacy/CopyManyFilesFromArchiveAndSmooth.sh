currentUserName=padfield

soldierRoot=/projects/soldier
baseDir="${soldierRoot}/Data/CT/ISR"

for pig in P183 P189 P192
do
    # Copy the datasets from the archive.
    # Since we don't know which ones are there, we just specify the pig name and 
    # copy those files on the archive that have this pig name along with a "up" in the name.
    CopyFilesFromArchive.sh ${pig} $currentUserName
    
    # Now smooth all of the datasets corresponding to each pig.
    # Do do this, we look at all pigs on the soldier drive with this pig name.
    for fullPigDir in `ls -d ${baseDir}/${pig}*`
    do
        pigDir=`basename $fullPigDir`
        # Only smooth the images if they haven't been smoothed already
        if [ ! -d ${baseDir}/${pigDir}/${pigDir}sp ]; then
            SmoothAPig.sh $pigDir
        fi
    done

done

