set prog=c:\millerjv\Watershed\lib\vtk\vtkITK
%prog% ColorAModel.tcl intervertebral_disc_of_eighth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_eleventh_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_fifth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_first_lumbar
%prog% ColorAModel.tcl intervertebral_disc_of_first_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_fourth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_ninth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_second_lumbar
%prog% ColorAModel.tcl intervertebral_disc_of_second_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_seventh_cervical
%prog% ColorAModel.tcl intervertebral_disc_of_seventh_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_sixth_cervical
%prog% ColorAModel.tcl intervertebral_disc_of_sixth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_tenth_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_third_thoracic
%prog% ColorAModel.tcl intervertebral_disc_of_twelfth_thoracic
