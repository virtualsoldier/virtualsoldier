#---This script allows the registration between two CT data sets.  
#   Transforms are quaternion rigid and affine.  
#   Metrics are Mattes MI and Viola Wells MI.
#   Assumption is that both data sets have 512x512 slice dimensions.
if {$argc < 16} {
  puts "usage: vtkITK CTtoCT.tcl movingPrefix movingFileExtension movingMinSlice movingMaxSlice movingSliceInc movingSpacingX movingSpacingY movingSpacingZ fixedPrefix fixedFileExtension fixedMinSlice fixedMaxSlice fixedSliceInc fixedSpacingX fixedSpacingY fixedSpacingZ" 
  exit
}
source [file dirname [info script]]/Paths.tcl
catch {source [file dirname [info script]]/vtkInt.tcl}
catch {package require BWidget}
catch {package require vtk}
catch {package require vtkinteraction}
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1
    renWin SetSize 512 512
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin
#---Global Variables
set movingPrefix [lindex $argv 0]
set movingFileExtension [lindex $argv 1]
set movingMinSlice [lindex $argv 2]
set movingMaxSlice [lindex $argv 3]
set movingSliceInc [lindex $argv 4]
set movingSpacingX [lindex $argv 5]
set movingSpacingY [lindex $argv 6]
set movingSpacingZ [lindex $argv 7]
set fixedPrefix [lindex $argv 8]
set fixedFileExtension [lindex $argv 9]
set fixedMinSlice [lindex $argv 10]
set fixedMaxSlice [lindex $argv 11]
set fixedSliceInc [lindex $argv 12]
set fixedSpacingX [lindex $argv 13]
set fixedSpacingY [lindex $argv 14]
set fixedSpacingZ [lindex $argv 15]
set fixedSliceShrinkFactor 1
set movingSliceShrinkFactor 1
set ctXDim 512
set ctYDim 512
set movingXDim 512
set movingYDim 512
set histBins 50
set mattesSamples 10000
set violaWellsSamples 50
set movingImStd .4
set movingImStd .4
set learningRate 4
set quaternionRigidIterations 200
set affineIterations 200
set minStepLength .005
set maxStepLength 4.00
set affineTranslateScale 500
set quaternionRigidTranslateScale 65
#---Read in Moving data
for { set slicemoving $movingMinSlice } { $slicemoving <= $movingMaxSlice } { incr slicemoving ${movingSliceInc} } {
  catch {vtkPNGReader movingReader; movingReader AddObserver EndEvent {puts "Read [movingReader GetInternalFileName]" }}
#     set formatSlicemoving [format "%0.3d" $slicemoving]
#     movingReader SetFileName ${movingPrefix}${formatSlicemoving}.${movingFileExtension}
    movingReader SetFileName ${movingPrefix}${slicemoving}.${movingFileExtension}
    movingReader SetDataExtent 0 511 0 511 0 0
    movingReader SetDataSpacing $movingSpacingX $movingSpacingY $movingSpacingZ
  catch {vtkImageClip movingReaderClip}
    movingReaderClip SetInput [movingReader GetOutput]
    movingReaderClip SetOutputWholeExtent 0 511 0 511 0 0     
    movingReaderClip ClipDataOn
  catch {vtkImageShrink3D movingReaderShrink}
    movingReaderShrink SetInput [movingReaderClip GetOutput]
    movingReaderShrink SetShrinkFactors $movingSliceShrinkFactor $movingSliceShrinkFactor 1
    movingReaderShrink Update
  catch {vtkImageAppend buildUpMovingImage}
    buildUpMovingImage AddInput [movingReaderShrink GetOutput]
    buildUpMovingImage PreserveExtentsOff
    buildUpMovingImage SetAppendAxis 2    
    [movingReaderShrink GetOutput] SetSource ""
    movingReaderShrink Delete
}
buildUpMovingImage UpdateWholeExtent
buildUpMovingImage ReleaseDataFlagOn
#---Shift the origin of the moving data
vtkImageChangeInformation movingShiftOrigin
  movingShiftOrigin SetInput [buildUpMovingImage GetOutput]
eval movingShiftOrigin SetOutputSpacing [expr $movingSliceShrinkFactor*$movingSpacingX] [expr $movingSliceShrinkFactor*$movingSpacingY] $movingSpacingZ
  movingShiftOrigin CenterImageOn
vtkImageShrink3D movingCenterShrink
  movingCenterShrink SetInput [movingShiftOrigin GetOutput]
  movingCenterShrink SetShrinkFactors 1 1 1
  movingCenterShrink ReleaseDataFlagOn
#---Normalize the Luminance Image
vtkITKNormalizeImageFilter movingNormalize
  movingNormalize CastInputOn
  movingNormalize SetInput [movingCenterShrink GetOutput]
  movingNormalize Update
#---Read in fixed data
for { set slicefixed $fixedMinSlice } { $slicefixed <= $fixedMaxSlice } { incr slicefixed ${fixedSliceInc} } {
  catch {vtkPNGReader fixedReader; fixedReader AddObserver EndEvent {puts "Read [fixedReader GetInternalFileName]" }}
#     set formatSlicefixed [format "%0.3d" $slicefixed]
#     fixedReader SetFileName ${fixedPrefix}${formatSlicefixed}.${fixedFileExtension}
    fixedReader SetFileName ${fixedPrefix}${slicefixed}.${fixedFileExtension}
    if { [file exists [fixedReader GetFileName]] == 0} {
        puts "Could not read [fixedReader GetInternalFileName]"
        continue
    }
    fixedReader SetDataExtent 0 511 0 511 0 0
    #fixedReader SetDataScalarTypeToUnsignedShort
    #fixedReader SetDataByteOrderToLittleEndian
    fixedReader SetDataSpacing $fixedSpacingX $fixedSpacingY $fixedSpacingZ
  catch {vtkImageShrink3D fixedReaderShrink}
    fixedReaderShrink SetInput [fixedReader GetOutput]
    fixedReaderShrink SetShrinkFactors $fixedSliceShrinkFactor $fixedSliceShrinkFactor 1
    fixedReaderShrink Update
  catch {vtkImageAppend buildFixedUpImage}
    buildFixedUpImage AddInput [fixedReaderShrink GetOutput]
    buildFixedUpImage PreserveExtentsOff
    buildFixedUpImage SetAppendAxis 2    
    [fixedReaderShrink GetOutput] SetSource ""
    fixedReaderShrink Delete
}
buildFixedUpImage UpdateWholeExtent
buildFixedUpImage ReleaseDataFlagOn
#---Shift the fixed origin
vtkImageChangeInformation fixedShiftOrigin
  fixedShiftOrigin SetInput [buildFixedUpImage GetOutput]
eval fixedShiftOrigin SetOutputSpacing [expr $fixedSliceShrinkFactor*$fixedSpacingX] [expr $fixedSliceShrinkFactor*$fixedSpacingY] $fixedSpacingZ
  fixedShiftOrigin CenterImageOn
vtkImageShrink3D fixedCenterShrink
    fixedCenterShrink SetInput [fixedShiftOrigin GetOutput]
    fixedCenterShrink SetShrinkFactors 1 1 1
    fixedCenterShrink ReleaseDataFlagOn
    
vtkITKNormalizeImageFilter fixedShiftNormalize
  fixedShiftNormalize CastInputOn
  fixedShiftNormalize SetInput [fixedCenterShrink GetOutput]
  fixedShiftNormalize Update
#---Set up MI registration
vtkITKMutualInformationTransform mutual
    mutual SetSourceImage [movingNormalize GetOutput]
    mutual SetTargetImage [fixedShiftNormalize GetOutput]
    mutual MattesOn
    mutual SetNumberOfSamples $mattesSamples
    mutual SetNumberOfHistogramBins $histBins
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale $quaternionRigidTranslateScale
    mutual SetLearningRate [expr $learningRate / 10000.0]
    mutual SetNumberOfIterations $quaternionRigidIterations
vtkImageReslice newMoving
  newMoving SetInterpolationModeToCubic
  newMoving SetInput [movingShiftOrigin GetOutput]
  newMoving SetResliceTransform mutual
  newMoving SetBackgroundLevel 0
  eval newMoving SetOutputExtent  [[fixedShiftOrigin GetOutput] GetWholeExtent]
  eval newMoving SetOutputSpacing [[fixedShiftOrigin GetOutput] GetSpacing]
set colorLevel 1000
set colorWindow 2000
vtkWindowLevelLookupTable wlLut
  wlLut SetWindow $colorWindow
  wlLut SetLevel $colorLevel
  wlLut Build
vtkImageMapToColors wlCT
  wlCT SetInput [fixedShiftOrigin GetOutput]
  wlCT SetLookupTable wlLut
  wlCT SetOutputFormatToRGB  
vtkImageCheckerboard checkers
  checkers SetInput 1 [newMoving GetOutput]
  checkers SetInput 0 [fixedShiftOrigin GetOutput]
  checkers SetNumberOfDivisions 2 2 1
vtkImageMapper imageMapperSource
    imageMapperSource SetInput [checkers GetOutput]
    imageMapperSource SetZSlice 2
    imageMapperSource SetColorWindow 255
    imageMapperSource SetColorLevel 127.5
vtkActor2D actor1 
    actor1 SetMapper imageMapperSource
vtkTextProperty titleProperty
  titleProperty SetFontSize 32
  titleProperty SetJustificationToCentered
  titleProperty SetVerticalJustificationToTop
  titleProperty ShadowOn
vtkTextMapper titleMapper
  titleMapper SetInput "CT to CT Registration"
  titleMapper SetTextProperty titleProperty
vtkActor2D titleActor
titleActor SetPosition [expr [lindex  [renWin GetSize] 0 ] / 2] [expr [lindex [renWin GetSize] 1 ] - 32]
  titleActor SetMapper titleMapper
vtkTextProperty text1Property
  text1Property SetFontSize 14
  text1Property SetJustificationToLeft
  text1Property SetVerticalJustificationToBottom
  text1Property ShadowOn
vtkTextMapper text1Mapper
  text1Mapper SetInput "Orientation: 0 0 0\nPosition: 0 0 0\nScale: 1 1 1"
  text1Mapper SetTextProperty text1Property
vtkActor2D text1Actor
  text1Actor SetPosition 14 14
  text1Actor SetMapper text1Mapper
vtkTextProperty text2Property
  text2Property SetFontSize 14
  text2Property SetJustificationToRight
  text2Property SetVerticalJustificationToBottom
  text2Property ShadowOn
vtkTextMapper text2Mapper
  text2Mapper SetInput "foo"
  text2Mapper SetTextProperty text2Property
vtkActor2D text2Actor
  text2Actor SetPosition [expr [lindex [renWin GetSize] 0 ] - 14] 14
  text2Actor SetMapper text2Mapper
#---Add the actor to the renderer, set the background and size
ren1 AddActor actor1
ren1 AddActor titleActor
ren1 AddActor text1Actor
ren1 AddActor text2Actor
#---Render the image
iren AddObserver UserEvent {wm deiconify .vtkInteract}
renWin Render
vtkTransform a
vtkTransform b
proc slice {n} {
  imageMapperSource SetZSlice $n
  renWin Render
}
set record "off"
proc recordOn {} {
    global frameNumber record
    set frameNumber 1000
    set record "on"
}
proc recordOff {} {
    global frameNumber record
    set frameNumber 1000
    set record "off"
}
proc go {{repeat 10}} {
    global record
    global fileID
    for {set i 0} { $i < $repeat } {incr i} {
        mutual Modified
        eval a SetMatrix [mutual GetMatrix]
        puts "[a GetOrientation] [a GetPosition] [a GetScale]"
        set rotatex [lindex [a GetOrientation] 0]
        set rotatey [lindex [a GetOrientation] 1]
        set rotatez [lindex [a GetOrientation] 2]
        set transx [lindex [a GetPosition] 0]
        set transy [lindex [a GetPosition] 1]
        set transz [lindex [a GetPosition] 2]
        set scalex [lindex [a GetScale] 0]
        set scaley [lindex [a GetScale] 1]
        set scalez [lindex [a GetScale] 2]
        text1Mapper SetInput "Orientation: $rotatex $rotatey $rotatez\nPosition:        $transx $transy $transz\nScale:            $scalex $scaley $scalez"
        renWin Render
        Record Movies/CTtoCT
    }
}
proc MattesRigid {} {
    mutual MattesOn
    mutual SetNumberOfSamples 10000
    mutual SetNumberOfHistogramBins 50
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale 65
    text2Mapper SetInput "Mattes\nRigid"
}
proc MattesAffine {} {
    mutual MattesOn
    mutual SetNumberOfSamples 10000
    mutual SetNumberOfHistogramBins 50
    mutual SetModeToAffine
    mutual SetTranslateScale 500
    text2Mapper SetInput "Mattes\nAffine"
}
proc ViolaWellsRigid {} {
    mutual ViolaWellsOn
    mutual SetNumberOfSamples 50
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale 65
    text2Mapper SetInput "ViolaWells\nRigid"
}
proc ViolaWellsAffine {} {
    mutual ViolaWellsOn
    mutual SetNumberOfSamples 50
    mutual SetModeToAffine
    mutual SetTranslateScale 500
    text2Mapper SetInput "ViolaWells\nAffine"
}
proc slices {a b} {
    global frameNumber
    if {$a < $b } {
        for {set xx $a} {$xx <= $b} {incr xx} {
            imageMapperSource SetZSlice $xx; renWin Render;
            Record Movies/RGBtoCT
        }
    } else {
        for {set xx $a} {$xx >= $b} {incr xx -1} {
            imageMapperSource SetZSlice $xx; renWin Render;
            Record Movies/RGBtoCT
        }
    }
}
proc MakeMovie {} {
    global frameNumber
    set frameNumber 1000
    text1Mapper SetInput "Orientation: 0 0 0\nPosition:        0 0 0\nScale:            1 1 1"
    slice 50
    mutual Identity
    renWin Render
    MattesRigid
    SetSubSample 16; SetLearning 10; SetIterations 100; go 60
    SetSubSample 8; SetLearning 10; SetIterations 100; go 30
    slices 50 97
    slices 97 0
    slices 0 50
    MattesAffine
    SetSubSample 8; SetLearning 10; SetIterations 100; go 60
    SetSubSample 4; SetLearning 10; SetIterations 100; go 30
    SetSubSample 2; SetLearning 5; SetIterations 100; go 30
    SetSubSample 1; SetLearning 1; SetIterations 100; go 30
    slices 50 97
    slices 97 0
    slices 0 50
}
set frameNumber 1000
proc Record {prefix} {
    global frameNumber record
    if {$record == "on"} {
      catch {vtkRendererSource captureImage}
        captureImage SetInput ren1
        captureImage Modified
      catch {vtkPNGWriter captureWriter}
        captureWriter SetFileName ${prefix}${frameNumber}.png
        captureWriter SetInput [captureImage GetOutput]
        captureWriter Write
      incr frameNumber
    }
}
proc WriteTransformedCTData {} {
    global movingShiftOrigin fixedShiftOrigin 
    global mutual
    catch {vtkImageReslice newCT}
      newCT SetInterpolationModeToLinear
      newCT SetInput [movingShiftOrigin GetOutput]
      newCT SetResliceTransform mutual
      newCT SetBackgroundLevel 0
    #eval newCT SetOutputExtent [[fixedShiftOrigin GetOutput] GetWholeExtent]
    #eval newCT SetOutputSpacing [[fixedShiftOrigin GetOutput] GetSpacing]
    newCT SetOutputExtent 0 511 0 511 0 217
    newCT SetOutputSpacing 0.712891 0.712891 1
    catch {vtkPNGWriter transformedLabelMaskWriter}
      transformedLabelMaskWriter SetFilePrefix  S:/Data/Atlas/Processed/centerVHMCT1mm_to_Ratiu/center_c_vm_to_Ratiu
      transformedLabelMaskWriter SetFilePattern "%s%d.png"
      transformedLabelMaskWriter SetInput [newCT GetOutput]
      transformedLabelMaskWriter Write
}
proc WriteTransformedLabelMask {} {
    global movingMinSlice movingMaxSlice movingSliceShrinkFactor
    global movingSpacingX movingSpacingY movingSpacingZ
    global ctShiftOrigin mutual
    for { set slice $movingMinSlice } { $slice <= $movingMaxSlice } { incr slice 1 } {
        catch {vtkPNGReader labelMaskReader; labelMaskReader AddObserver EndEvent {puts "Read [labelMaskReader GetInternalFileName]" }}
          labelMaskReader SetFileName S:/Data/Atlas/Processed/centerLabelDataTransformed/centerLabelDataTransformed${slice}.png
          labelMaskReader SetDataExtent 0 511 0 511 0 0
          labelMaskReader SetDataSpacing $movingSpacingX $movingSpacingY $movingSpacingZ 
        catch {vtkImageClip labelMaskReaderClip}
          labelMaskReaderClip SetInput [labelMaskReader GetOutput]         
          labelMaskReaderClip SetOutputWholeExtent 0 511 0 511 0 0     
          labelMaskReaderClip ClipDataOn
        
        catch {vtkImageShrink3D labelMaskReaderShrink}
          labelMaskReaderShrink SetInput [labelMaskReaderClip GetOutput]
          labelMaskReaderShrink SetShrinkFactors $movingSliceShrinkFactor $movingSliceShrinkFactor 1
          labelMaskReaderShrink Update
        catch {vtkImageAppend buildUpLabelMaskImage}
          buildUpLabelMaskImage AddInput [labelMaskReaderShrink GetOutput]
          buildUpLabelMaskImage PreserveExtentsOff
          buildUpLabelMaskImage SetAppendAxis 2    
          [labelMaskReaderShrink GetOutput] SetSource ""
          labelMaskReaderShrink Delete
    }
    buildUpLabelMaskImage UpdateWholeExtent
    buildUpLabelMaskImage ReleaseDataFlagOn
    catch {vtkImageChangeInformation labelMaskShiftOrigin}
      labelMaskShiftOrigin SetInput [buildUpLabelMaskImage GetOutput]
    eval labelMaskShiftOrigin SetOutputSpacing [expr $movingSliceShrinkFactor*$movingSpacingX] [expr $movingSliceShrinkFactor*$movingSpacingY] $movingSpacingZ
      labelMaskShiftOrigin CenterImageOn
    catch {vtkImageReslice newLabelMask}
      newLabelMask SetInterpolationModeToNearestNeighbor
      newLabelMask SetInput [labelMaskShiftOrigin GetOutput]
      newLabelMask SetResliceTransform mutual
      newLabelMask SetBackgroundLevel 0
    #eval newLabelMask SetOutputExtent [[fixedShiftOrigin GetOutput] GetWholeExtent]
      newLabelMask SetOutputExtent 0 511 0 511 0 217
    #eval newLabelMask SetOutputSpacing [[fixedShiftOrigin GetOutput] GetSpacing]
      newLabelMask SetOutputSpacing 0.712891 0.712891 1
    catch {vtkPNGWriter transformedLabelMaskWriter}
      transformedLabelMaskWriter SetFilePrefix S:/Data/Atlas/Processed/centerLabelDataTransformed_to_Ratiu/centerLabelDataTransformed_to_Ratiu
      transformedLabelMaskWriter SetFilePattern "%s%d.png"
      transformedLabelMaskWriter SetInput [newLabelMask GetOutput]
      transformedLabelMaskWriter Write
}
proc saveData {prefix suffix dim}\
    {
	set input "newMoving";
	if { 1 }\
	{
		if { [info commands clipdata] == "" } {vtkImageClip clipdata}
		clipdata SetInput [$input GetOutput]
		clipdata SetOutputWholeExtent 0 511 0 511 0 50
		clipdata Update
		set input "clipdata"
	}
	if { [info commands write] == "" } {vtkImageWriter write}
	write SetInput [$input GetOutput]
	if { $dim == 2}\
	 	   {
			set pattern [format "%s%s" "%s%d" ".$suffix"]
			write SetFilePattern $pattern
			write SetFilePrefix $prefix
			write SetFileDimensionality $dim
			write Write
	  	  }\
	elseif { $dim == 3}\
	 	   {
			write SetFileName ${prefix}.${suffix}
			write SetFileDimensionality $dim
			write Write
	  	  }\
	else\
		{
		return;	
		}
    }
MattesAffine
go 1
iren Initialize
#---Make interface
source [file join [file dirname [info script]] WindowLevelCTtoCT.tcl]
