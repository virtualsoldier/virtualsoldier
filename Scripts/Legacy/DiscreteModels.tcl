#
# Make vtk files of a given structure
#

if {$argc < 1} {
  puts "usage: vtkITK DiscreteModels.tcl "
  exit
}
set LabelFilePrefix [lindex $argv 0]

set LabelFilePattern "%s.%03d"
set iMin 0
set iMax 511
set jMin 0
set jMax 511

set sliceMin 1
set sliceMax 308

set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
  catch {vtkImageReader reader}
    reader SetDataSpacing .365234 .365234 .7
    reader SetFilePattern $LabelFilePattern
    reader SetDataScalarTypeToShort
    reader SetDataByteOrderToBigEndian
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataOrigin 0 0 $sliceMin
    reader SetDataExtent 0 511 0 511 $slice $slice

  catch {vtkImageClip clip}
    clip SetInput [reader GetOutput]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr 511 - $jMax] [expr 511 - $jMin] 0 0
    clip Update

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInput [clip GetOutput]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    [clip GetOutput] SetSource ""
    clip Delete
}
  buildUpImage UpdateWholeExtent
puts "[[buildUpImage GetOutput] GetScalarRange]"

  catch {vtkImageChangeInformation shiftOrigin}
    shiftOrigin SetInput [buildUpImage GetOutput]
shiftOrigin SetOutputOrigin [expr $iMin * .365234] [expr ( 511 - $jMax ) * .365234] [expr $sliceMin * .7]

  catch {vtkImageAccumulate histo}
    histo SetInput [shiftOrigin GetOutput]
    histo SetComponentExtent 0 511 0 0 0 0
    histo SetComponentOrigin 0 0 0
    histo SetComponentSpacing 1 1 1
    histo Update

  catch {vtkDiscreteMarchingCubes cubes}
    cubes SetInput [shiftOrigin GetOutput]
    set j 0
    for {set i 1} {$i < 512} { incr i } {
	set freq [[[[histo GetOutput] GetPointData] GetScalars] GetTuple1 $i]
	if { $freq == 0 } continue
puts "---------------- $j $i"
    	cubes SetValue $j $i
	incr j
    }
    cubes ComputeNormalsOff
    cubes ComputeScalarsOn
    cubes ComputeGradientsOff
    cubes ReleaseDataFlagOn
    cubes AddObserver EndEvent "puts \"Cubes complete\""
set iterations 15
set passBand 0.001

catch {vtkWindowedSincPolyDataFilter smoother}
    smoother SetInput [cubes GetOutput]
    smoother SetNumberOfIterations $iterations
    smoother BoundarySmoothingOff
    smoother FeatureEdgeSmoothingOff
    smoother SetPassBand $passBand
    smoother NonManifoldSmoothingOn
#   smoother NormalizeCoordinatesOn
    smoother AddObserver EndEvent "puts \"Smoothing complete\""
#    smoother ReleaseDataFlagOn

catch {vtkAppendPolyData appendPD}

    for {set i 1} {$i < 512} { incr i } {
	set freq [[[[histo GetOutput] GetPointData] GetScalars] GetTuple1 $i]
	if { $freq == 0 } continue
      catch {vtkThreshold thresholder$i}
        thresholder$i SetInput [smoother GetOutput]
        thresholder$i SetAttributeModeToUseCellData
        thresholder$i ThresholdBetween $i $i
        thresholder$i AddObserver EndEvent "puts \"Thresholding complete\""
#        thresholder$i ReleaseDataFlagOn

      catch {vtkGeometryFilter geom$i}
        geom$i SetInput [thresholder$i GetOutput]
        geom$i AddObserver EndEvent "puts \"Geom$i complete\""

      catch {vtkPolyDataWriter writer}
        writer SetInput [geom$i GetOutput]
        writer SetFileName foo$i.vtk
        writer SetFileTypeToBinary
        writer Modified
        writer Write

      catch {vtkWindowedSincPolyDataFilter separateSmoother$i}
        separateSmoother$i SetInput [geom$i GetOutput]
        separateSmoother$i SetNumberOfIterations $iterations
        separateSmoother$i BoundarySmoothingOff
        separateSmoother$i FeatureEdgeSmoothingOff
        separateSmoother$i SetPassBand $passBand
        separateSmoother$i NonManifoldSmoothingOff
        separateSmoother$i AddObserver EndEvent "puts \"Smoothing complete\""

        appendPD AddInput [separateSmoother$i GetOutput]

        writer SetInput [separateSmoother$i GetOutput]
        writer SetFileName fooSeparate$i.vtk
        writer SetFileTypeToBinary
        writer Modified
        writer Write

    }
writer SetInput [smoother GetOutput]
writer SetFileName allsmooth.vtk
writer SetFileTypeToBinary
writer Modified
writer Write

writer SetInput [cubes GetOutput]
writer SetFileName alldiscrete.vtk
writer SetFileTypeToBinary
writer Modified
writer Write

writer SetInput [appendPD GetOutput]
writer SetFileName allseparate.vtk
writer SetFileTypeToBinary
writer Modified
writer Write

exit