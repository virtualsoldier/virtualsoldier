# ParaView State Version 1.0
package provide ParaViewImport 1.0

#
# Build our colormap
#
#
proc makecolors {lut n} {

  catch {vtkMath math}
  $lut SetNumberOfColors $n
  $lut SetTableRange 0 [expr $n-1]
  $lut SetScaleToLinear
  $lut Build
  $lut SetTableValue 0 0 0 0 1
  math RandomSeed 5071
  for {set i 1} {$i < $n } {incr i} {
    $lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
  }
}

# Proc to load a series of files
# 
#
proc ParaViewImport { allFiles } {
  global Application
  global jill
  # Communicate with ParaView
  #
  set jill(PVWindow) [$Application GetMainWindow]
  set jill(PVRenderView) [$jill(PVWindow) GetMainView]
    [$jill(PVRenderView) GetRenderWindow] SetStereoTypeToInterlaced
  set jill(PVColorMap) [$jill(PVWindow) GetPVColorMap {scalars} 1]
  $jill(PVColorMap) SetScalarBarVisibility 0
  $jill(PVColorMap) SetScalarBarPosition1 0.87 0.25
  $jill(PVColorMap) SetScalarBarPosition2 0.13 0.5
    
  catch {vtkLookupTable jillLut}
  makecolors jillLut 512

  catch {vtkLightKit aLightKit}
  aLightKit SetKeyLightIntensity 1

#
  # How many pre-defined props?
  set predefined [[[$jill(PVRenderView) GetRenderer] GetProps] GetNumberOfItems]

  # Read in all our data
  #
  #
  set n 1
  foreach file $allFiles {
    set jill(PVReaderModule$n) [$jill(PVWindow) InitializeReadCustom "legacyreader" $file]
    $jill(PVWindow) ReadFileInformation $jill(PVReaderModule$n) $file
    $jill(PVWindow) FinalizeRead $jill(PVReaderModule$n) $file
    set jill(PVWidget$n) [$jill(PVReaderModule$n) GetPVWidget {Filename}]
    $jill(PVWidget$n) SetValue $file
    $jill(PVReaderModule$n) AcceptCallback
  
    # Set up the camera
    #
    [[$jill(PVRenderView) GetRenderer] GetActiveCamera] SetViewUp 0 0 -1
    [[$jill(PVRenderView) GetRenderer] GetActiveCamera] SetFocalPoint 0 0 0
    [[$jill(PVRenderView) GetRenderer] GetActiveCamera] SetPosition 0 1 0
    [$jill(PVRenderView) GetRenderer] ResetCamera
    [$jill(PVRenderView) GetRenderer] ResetCameraClippingRange

    [[$jill(PVRenderView) GetRenderer] GetLights] RemoveAllItems
    aLightKit AddLightsToRenderer [$jill(PVRenderView) GetRenderer]
     
    set props [[$jill(PVRenderView) GetRenderer] GetProps] 
    $props InitTraversal
    set np 0
    while { [set p [$props GetNextProp]] != ""} {
	incr np
	if {$np <= $predefined } { continue }
	set mapper [$p GetMapper]
	$mapper SetLookupTable jillLut
	eval $mapper SetScalarRange [jillLut GetTableRange]
    }
    incr n

    [$jill(PVRenderView) GetRenderWindow] Render
  }
}