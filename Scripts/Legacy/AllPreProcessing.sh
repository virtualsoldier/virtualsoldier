#!/bin/sh
if [ $# -lt 3 ]; then
    echo "usage: AllPreProcessing operationType imageType series"
    exit
fi


# This script runs any of the levels sets.  It runs 2D and 3D level
# sets.  It also enabled the choice of several different data sets.

echo $OS

# These paths can be changed based on your setup
if (test $OS = "Windows_NT" )
then
    mainImagePath="c:/MYDOCU~1/VirtualSoldier-data"
    pigDirec="ISR"
    execPath="c:/MYDOCU~1/VirtualSoldier/VirtualSoldier-win32/Code/Atlas/RelWithDebInfo"
    execExt=".exe"
elif (test $OS = "2.4" )
then
    mainImagePath="/projects/soldier/Data/CT"
    pigDirec="ISR"
    execPath="/home/padfield/VirtualSoldier/VirtualSoldier-linux/Code/Atlas"
    execExt=""
fi

#operationType=CreateVolume
#operationType=RelabelSlices
#operationType=CalculateBoundingBoxes
#operationType=FindAnatomyLabels
#operationType=CalculateLabelStatistics2D
#operationType=FindAdjacentLabels
#operationType=GenerateLabelMHDFiles
#operationType=GenerateLabelImages
#operationType=FindAllLabels
#operationType=To16Bit
operationType=$1

#imageType=Physiology
#imageType=Transformed_to_Ratiu
#imageType=Transformed_to_Ratiu_Refined
#imageType=ISRPA49a
#imageType=P73DcmPre30
#imageType=MGHPig
#imageType=ISRPA498
#imageType=P82DcmPostIsoWithM
#imageType=P82DcmPostIsoWithout
#imageType=P87DcmPostIsoWithM
#imageType=P87DcmPostIsoWithout
imageType=$2

imageDim=3

# if (test $imageDim -eq 2)
# then slices=1380
# elif (test $imageDim -eq 3)
# #then slices=%d
# then slices=1380
# fi

# PARAMETERS
#displayImageType="SpeedImage"
displayImageType="FeatureImage"

if (test $imageType = ISRPA49a )
then slices="101" #Slice number should have three digits
    numberingOffset=249 
#    anatomyName="left_ventricle_ascending_aorta"
#    anatomyName="pulmonary_trunk"
#    anatomyName="pulm_veins_right_atrium"
    anatomyName="myocardium"
elif (test $imageType = P73DcmPre80 )
then slices="101"
    numberingOffset=69
    percentage=80
elif (test $imageType = MGHPig )
then slices=150
    anatomyName="myocardium"
elif (test $imageType = Transformed_to_Ratiu_Refined )
then slices=100
    anatomyName="aorta_ascending"
else
     slices=1380
     anatomyName="aorta_ascending"
fi


if (test $imageType = Physiology )
then echo Physiology
    imagePath="${mainImagePath}/VHM"
    outPath="${imagePath}/Models"
    featureImage=${imagePath}/70mm_manual_2048_reg/${slices}.png
    labelImage="${imagePath}/Relabeled/Relabeled${slices}.png"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
elif (test $imageType = Transformed )
then echo Transformed
    imagePath="${mainImagePath}/VHM"
    outPath="${imagePath}/ModelsTransformed"
    featureImage="${imagePath}/70mm_manual_2048_reg/${slices}.png"
    labelImage="${imagePath}/LabelDataTransformed/LabelDataTransformed${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxesTransformed.csv"
elif (test $imageType = Transformed_to_Ratiu )
then echo Transformed_to_Ratiu
    imagePath="${mainImagePath}"
    outPath="${imagePath}/ModelsTransformed_to_Ratiu"
    featureImage="${imagePath}/000007.SER_png/%d.png"
    labelImage="${imagePath}/LabelDataTransformed_to_Ratiu/Affine/LabelDataTransformed_to_Ratiu${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed_to_Ratiu.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${mainImagePath}/AnatomyBoundingBoxesTransformed_to_Ratiu.csv"
elif (test $imageType = Transformed_to_Ratiu_Refined )
then echo Transformed_to_Ratiu_Refined
    imagePath="${mainImagePath}/Ratiu"
    outPath="${imagePath}/ModelsTransformed_to_Ratiu_Refined"
    featureImage="${imagePath}/000007.SER_png/${slices}.png"
    labelImage="${imagePath}/LabelDataTransformed_to_Ratiu/RefinedAffine/LabelDataTransformed_to_Ratiu_Refined${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed_to_Ratiu_Refined.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxesTransformed_to_Ratiu_Refined.csv"
elif (test $imageType = ISRPA49a )
then echo ISRPA49a
    imagePath="${mainImagePath}/${pigDirec}/ISRPA49a"
    outPath="${imagePath}/Models"
#    featureImage="${imagePath}/Slices/IM${slices}.png"
    slicesOffset=`expr $slices + $numberingOffset`
    featureImage="${imagePath}/SelectedDicoms/im${slicesOffset}"
#    labelImage="${imagePath}/Repaired/Repaired${slices}.png"
#    labelImage="${imagePath}/Repaired/Repaired%d.png"    
    labelImage="${imagePath}/Labels/combo.${slices}.mhd"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${imagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"

    labelFile="${imagePath}/legend.lbl"
    labelImageRaw="${imagePath}/Labels/combo.${slices}"

elif (test $imageType = MGHPig )
then echo MGHPig
    imagePath="${mainImagePath}/${pigDirec}/MGHPig"
    outPath="${imagePath}/Models"
    featureImage=${imagePath}/greyscale/I.${slices}.mhd
    labelImage="${imagePath}/labels/heartWall.${slices}.mhd"
#    labelImage="${imagePath}/labels/heartWall.%03d.mhd"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${imagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
    
    labelFile="${imagePath}/legend.lbl"

elif (test $imageType = ISRPA498 )
then echo ISRPA498
    series=3
    imagePath="${mainImagePath}/${pigDirec}/P498DcmPost"
    featureImage="${imagePath}/ADSmoothed/DicomSeries${series}/DicomSeries${series}IM0020.dcm"
    labelImage="${imagePath}/Labels/LabelSeries${series}/LabelSeries${series}IM0020.png"
    imageInfoFile="${imagePath}/ImageInfo.csv"    
    masterFile="${imagePath}/MasterAnatomy.csv"    
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
    if [ ! -d ${imagePath}/Labels/LabelSeries${series} ]; then
        mkdir ${imagePath}/Labels/LabelSeries${series}
    fi
    if [ ! -d ${imagePath}/Labels/LabelSeries${series}/TEMP ]; then
        mkdir ${imagePath}/Labels/LabelSeries${series}/TEMP
    fi


else
    echo $imageType
    series=$3
    slicesOffset=`expr $slices + $numberingOffset`
    imagePath="${mainImagePath}/${pigDirec}/${imageType}"
    featureImage="${imagePath}/${imageType}sp/${imageType}spSeries${series}/${imageType}spSeries${series}IM0100.dcm"

    labelImageRaw="${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial/${imageType}lpSeries${series}InitialIM0100"
    labelImage="${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial/${imageType}lpSeries${series}InitialIM0100.png"

    imageInfoFile="dummy.csv" #not used    
    masterFile="${imagePath}/MasterAnatomy.csv"    
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
fi


# Create the necessary label directories
if [ ! -d ${imagePath}/${imageType}lp ]; then
    mkdir ${imagePath}/${imageType}lp
fi
if [ ! -d ${imagePath}/${imageType}lp/${imageType}lpSeries${series} ]; then
    mkdir ${imagePath}/${imageType}lp/${imageType}lpSeries${series}
fi    
if [ ! -d ${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial ]; then
    mkdir ${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial
fi    
if [ ! -d ${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Final ]; then
    mkdir ${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Final
fi    



# Do the work

if (test $operationType = CreateVolume )
then echo CreateVolume
    execFile="${execPath}/CreateVolume${execExt}"
    echo $execFile
    outFeatureFile=${imagePath}/Volumes/${anatomyName}_FeatureVolume.mhd
    outLabelFile=${imagePath}/Volumes/${anatomyName}_LabelVolume.mhd
    ${execFile} ${featureImage} ${imageInfoFile} ${masterFile} ${boundingBoxesFile} ${outFeatureFile} ${anatomyName} 
    ${execFile} ${labelImage} ${imageInfoFile} ${masterFile} ${boundingBoxesFile} ${outLabelFile} ${anatomyName} 

elif (test $operationType = RelabelSlices )
then echo RelabelSlices
    execFile=${execPath}/RelabelSlices${execExt}
    oldLabelImage="${imagePath}/labels/heartWall.%03d.mhd"
    #oldLabelImage="${imagePath}/Repaired/Repaired%d.png"
    labelImage="${imagePath}/Relabeled/Relabeled%03d.png"
    $execFile $labelFile $oldLabelImage $labelImage $masterFile $boundingBoxesFile $featureImage

elif (test $operationType = CalculateBoundingBoxes )
then echo CalculateBoundingBoxes
    execFile="${execPath}/CalculateBoundingBoxes${execExt}"
    echo $execFile
    echo $labelImage
    echo $masterFile
    echo $boundingBoxesFile
    $execFile $labelImage $masterFile $boundingBoxesFile 

elif (test $operationType = FindAnatomyLabels )
then echo FindAnatomyLabels
    execFile="${execPath}/FindAnatomyLabels${execExt}"
    echo $execFile
    labelImage="${imagePath}/labels/heartWall.%03d.mhd"
    $execFile $labelImage 1 308 ${imagePath}/FindAnatomyLabels.csv 99

elif (test $operationType = CalculateLabelStatistics2D )
then echo CalculateLabelStatistics2D
    execFile="${execPath}/CalculateLabelStatistics2D${execExt}"
    echo $execFile
    #                    featureImage="${imagePath}/70mm_manual_2048_reg/%d.png"
    #                    featureImage="${imagePath}/greyscale/I.%03d.mhd"
    #                    labelImage="${imagePath}/labels/heartWall.%03d.mhd"
    #                    labelImage="${imagePath}/Relabeled/Relabeled%03d.png"
    statisticsFile="${imagePath}/StatisticsFile.csv"
    $execFile $featureImage $labelImage $imageInfoFile $masterFile $statisticsFile

elif (test $operationType = FindAdjacentLabels )
then echo FindAdjacentLabels
    execFile="${execPath}/FindAdjacentLabels${execExt}"
    adjacentLabelsFile="${imagePath}/AdjacentLabels.csv"
    $execFile $labelImage $adjacentLabelsFile

elif (test $operationType = GenerateLabelMHDFiles )
then echo GenerateLabelMHDFiles
    execFile="${execPath}/GenerateLabelMHDFiles${execExt}"
    echo $featureImage
    echo $labelImageRaw
    $execFile $featureImage $labelImageRaw 0
   
elif (test $operationType = GenerateLabelImages )
then echo GenerateLabelImages
    execFile="${execPath}/GenerateLabelImages${execExt}"
    echo execFile: $execFile
    threshold=30
    binaryErodeRadius=0
    sliceStart=13
    numberOfSlices=18
    $execFile $featureImage $labelImage $imageInfoFile $threshold $binaryErodeRadius $imageDim

elif (test $operationType = FindAllLabels )
then echo FindAllLabels
    execFile="${execPath}/FindAllLabels${execExt}"
    $execFile $labelImage $masterFile
                
elif (test $operationType = To16Bit )
then echo To16Bit
    execFile="${execPath}/To16Bit${execExt}"
    $execFile $labelImageRaw.mhd png flip

else
    echo ERROR: No operation type specified!

fi
