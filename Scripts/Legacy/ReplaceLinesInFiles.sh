#!/bin/sh
if [ $# -lt 3 ]; then
    echo "usage: ReplaceLinesInFiles.sh replaceDir toBeReplaced replaceWith"
    exit
fi

replaceDir=$1
echo replaceDir: $replaceDir
toBeReplaced=$2
echo toBeReplaced: $toBeReplaced
replaceWith=$3
echo replaceWith: $replaceWith

if [ ! -d  ${replaceDir}/TEMP ]; then
    mkdir ${replaceDir}/TEMP
fi

# Find all files that have the string of interest and save them to a file.
cd $replaceDir
grep -r -l fl_file_chooser.H . > TEMP/filesToChange.txt

# Loop through the files that have the string of interest and replace the strings with the new string.
while read filePathAndName
do
    echo $filePathAndName
    filePath=`dirname $filePathAndName`
    fileName=`basename $filePathAndName`
    sed 's/fl_file_chooser.H/Fl_File_Chooser.H/g' $filePathAndName > ${replaceDir}/TEMP/${fileName}
    mv ${replaceDir}/TEMP/${fileName} ${filePathAndName}
done < TEMP/filesToChange.txt

#rm TEMP/filesToChange.txt
cd ../../
rm -rf ${replaceDir}/TEMP


#for filePathAndName in `find . -type f -print`
#do
#    filePath=`dirname $filePathAndName`
#    fileName=`basename $filePathAndName`
#    sed 's/fl_file_chooser.H/Fl_File_Chooser.H/g' $filePathAndName > ${replaceDir}/TEMP/${fileName}
#    mv ${replaceDir}/TEMP/${fileName} ${filePathAndName}
#done


#find . -type f -exec sed 's/${toBeReplaced}/${replaceWith}/g' '{}' > '{}' \;