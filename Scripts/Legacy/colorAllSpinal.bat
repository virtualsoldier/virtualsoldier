set prog=c:\millerjv\Watershed\lib\vtk\vtkITK
%prog% ColorAModel.tcl cervical_spinal_cord_nerve_root
%prog% ColorAModel.tcl cervical_spinal_ganglia
%prog% ColorAModel.tcl spinal_cord
%prog% ColorAModel.tcl spinal_dura_mater
%prog% ColorAModel.tcl spinal_epidural_space
%prog% ColorAModel.tcl spinal_nerves
%prog% ColorAModel.tcl thoracic_spinal_cord
%prog% ColorAModel.tcl thoracic_spinal_cord_nerve_root
%prog% ColorAModel.tcl thoracic_spinal_dura_mater
%prog% ColorAModel.tcl thoracic_spinal_epidural_space
%prog% ColorAModel.tcl thoracic_spinal_ganglia
