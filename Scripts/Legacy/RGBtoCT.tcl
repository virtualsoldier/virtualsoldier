if {$argc < 8} {
  puts "usage: vtkITK RGBtoCT.tcl vhmCrossSectionalPrefix vhmCrossSectionSliceMin vhmCrossSectionSliceMax vhmCrossSectionSliceInc vhmCTPrefix vhmCTSliceMin vhmCTSliceMax vhmCTSliceInc" 
  exit
}
source [file dirname [info script]]/Paths.tcl
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1
    renWin SetSize 512 512
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin
package require BWidget
package require vtk
package require vtkinteraction
#---Global Variables
set vhmCrossSectionalPrefix [lindex $argv 0]
set vhmCrossSectionSliceMin [lindex $argv 1]
set vhmCrossSectionSliceMax [lindex $argv 2]
set vhmCrossSectionSliceInc [lindex $argv 3]
set vhmCTPrefix [lindex $argv 4]
set vhmCTSliceMin [lindex $argv 5]
set vhmCTSliceMax [lindex $argv 6]
set vhmCTSliceInc [lindex $argv 7]
set photoSliceShrinkFactor 3
set ctSliceShrinkFactor 1
set ctXDim 512
set ctYDim 512
set photoXDim 2048
set photoYDim 1350
set histBins 50
set mattesSamples 10000
set violaWellsSamples 50
set fixedImStd .4
set movingImStd .4
set learningRate 4
set quaternionRigidIterations 200
set affineIterations 200
set minStepLength .005
set maxStepLength 4.00
set affineTranslateScale 500
set quaternionRigidTranslateScale 65
set exponentialMapTranslateScale 300
#---Read in cross-sectional Data
for { set slice [expr $vhmCrossSectionSliceMin] } { $slice <= [expr $vhmCrossSectionSliceMax] } { incr slice $vhmCrossSectionSliceInc } {
  catch {vtkPNGReader photoReader; photoReader AddObserver EndEvent {puts "Read [photoReader GetInternalFileName]" }}
    photoReader SetFileName ${vhmCrossSectionalPrefix}${slice}.png
    photoReader SetDataExtent 0 2047 0 1349 0 0
    photoReader SetDataSpacing .3 .3 [expr 1*${vhmCrossSectionSliceInc}]
  catch {vtkImageClip photoReaderClip}
    photoReaderClip SetInput [photoReader GetOutput]
    photoReaderClip SetOutputWholeExtent 0 2047 0 1100 0 0     
    photoReaderClip ClipDataOn
  catch {vtkImageShrink3D photoReaderShrink}
    photoReaderShrink SetInput [photoReaderClip GetOutput]
    photoReaderShrink SetShrinkFactors $photoSliceShrinkFactor $photoSliceShrinkFactor 1
    photoReaderShrink Update
  catch {vtkImageAppend buildUpPhotoImage}
    buildUpPhotoImage AddInput [photoReaderShrink GetOutput]
    buildUpPhotoImage PreserveExtentsOff
    buildUpPhotoImage SetAppendAxis 2    
    [photoReaderShrink GetOutput] SetSource ""
    photoReaderShrink Delete
}
buildUpPhotoImage UpdateWholeExtent
buildUpPhotoImage ReleaseDataFlagOn
vtkImageChangeInformation photoShiftOrigin
  photoShiftOrigin SetInput [buildUpPhotoImage GetOutput]
eval photoShiftOrigin SetOutputSpacing [expr $photoSliceShrinkFactor*.3] [expr $photoSliceShrinkFactor*.3] [expr 1*${vhmCrossSectionSliceInc}]
  photoShiftOrigin CenterImageOn
#---Convert Color Image to Gray Scale
vtkImageLuminance photoLuminance 
  photoLuminance SetInput [photoShiftOrigin GetOutput]
  photoLuminance ReleaseDataFlagOn
vtkImageShrink3D photoCenterShrink
  photoCenterShrink SetInput [photoLuminance GetOutput]
  photoCenterShrink SetShrinkFactors 4 4 1
  photoCenterShrink ReleaseDataFlagOn
#---Normalize the Luminance Image
vtkITKNormalizeImageFilter photoNormalize
  photoNormalize CastInputOn
  photoNormalize SetInput [photoCenterShrink GetOutput]
  photoNormalize Update
#---Read in VHM CT data
for { set slice [expr $vhmCTSliceMin] } { $slice <= [expr $vhmCTSliceMax] } { incr slice $vhmCTSliceInc } {
  catch {vtkPNGReader ctReader; ctReader AddObserver EndEvent {puts "Read [ctReader GetInternalFileName]" }}
    ctReader SetFileName ${vhmCTPrefix}${slice}.png
    if { [file exists [ctReader GetFileName]] == 0} {
        puts "Could not read [ctReader GetInternalFileName]"
        continue
    }
    ctReader SetDataExtent 0 511 0 511 0 0
    ctReader SetDataSpacing 1 1 [expr 1*$vhmCTSliceInc]
  catch {vtkImageShrink3D ctReaderShrink}
    ctReaderShrink SetInput [ctReader GetOutput]
    ctReaderShrink SetShrinkFactors $ctSliceShrinkFactor $ctSliceShrinkFactor 1
    ctReaderShrink Update
  catch {vtkImageAppend buildUpCTImage}
    buildUpCTImage AddInput [ctReaderShrink GetOutput]
    buildUpCTImage PreserveExtentsOff
    buildUpCTImage SetAppendAxis 2    
    [ctReaderShrink GetOutput] SetSource ""
    ctReaderShrink Delete
}
buildUpCTImage UpdateWholeExtent
buildUpCTImage ReleaseDataFlagOn
vtkImageChangeInformation ctShiftOrigin
  ctShiftOrigin SetInput [buildUpCTImage GetOutput]
eval ctShiftOrigin SetOutputSpacing [expr $ctSliceShrinkFactor*1] [expr $ctSliceShrinkFactor*1] [expr 1*$vhmCTSliceInc]
  ctShiftOrigin CenterImageOn
vtkImageShrink3D ctCenterShrink
    ctCenterShrink SetInput [ctShiftOrigin GetOutput]
    ctCenterShrink SetShrinkFactors 4 4 1
    ctCenterShrink ReleaseDataFlagOn
    
vtkITKNormalizeImageFilter ctShiftNormalize
  ctShiftNormalize CastInputOn
  ctShiftNormalize SetInput [ctCenterShrink GetOutput]
  ctShiftNormalize Update
#---Set up the MI registration
vtkITKMutualInformationTransform mutual
    mutual SetSourceImage [photoNormalize GetOutput]
    mutual SetTargetImage [ctShiftNormalize GetOutput]
    mutual MattesOn
    mutual SetNumberOfSamples $mattesSamples
    mutual SetNumberOfHistogramBins $histBins
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale $quaternionRigidTranslateScale
    mutual SetModeToExponentialMapRigid3D
    mutual SetLearningRate [expr $learningRate / 10000.0]
    mutual SetNumberOfIterations $quaternionRigidIterations
vtkImageReslice newPhoto
  newPhoto SetInterpolationModeToCubic
  newPhoto SetInput [photoShiftOrigin GetOutput]
  newPhoto SetResliceTransform mutual
  newPhoto SetBackgroundLevel 0
eval newPhoto SetOutputExtent [[ctShiftOrigin GetOutput] GetWholeExtent]
eval newPhoto SetOutputSpacing [[ctShiftOrigin GetOutput] GetSpacing]
set colorLevel 1000
set colorWindow 2000
vtkWindowLevelLookupTable wlLut
  wlLut SetWindow $colorWindow
  wlLut SetLevel $colorLevel
  wlLut Build
vtkImageMapToColors wlCT
  wlCT SetInput [ctShiftOrigin GetOutput]
  wlCT SetLookupTable wlLut
  wlCT SetOutputFormatToRGB  
vtkImageCheckerboard checkers
  checkers SetInput 1 [newPhoto GetOutput]
  checkers SetInput 0 [wlCT GetOutput]
  checkers SetNumberOfDivisions 2 2 1
vtkImageMapper imageMapperSource
    imageMapperSource SetInput [checkers GetOutput]
    imageMapperSource SetZSlice 2
    imageMapperSource SetColorWindow 255
    imageMapperSource SetColorLevel 127.5
vtkActor2D actor1 
    actor1 SetMapper imageMapperSource
vtkTextProperty titleProperty
  titleProperty SetFontSize 32
  titleProperty SetJustificationToCentered
  titleProperty SetVerticalJustificationToTop
  titleProperty ShadowOn
vtkTextMapper titleMapper
  titleMapper SetInput "RGB to CT Registration"
  titleMapper SetTextProperty titleProperty
vtkActor2D titleActor
titleActor SetPosition [expr [lindex  [renWin GetSize] 0 ] / 2] [expr [lindex [renWin GetSize] 1 ] - 32]
  titleActor SetMapper titleMapper
vtkTextProperty text1Property
  text1Property SetFontSize 14
  text1Property SetJustificationToLeft
  text1Property SetVerticalJustificationToBottom
  text1Property ShadowOn
vtkTextMapper text1Mapper
  text1Mapper SetInput "Orientation: 0 0 0\nPosition: 0 0 0\nScale: 1 1 1"
  text1Mapper SetTextProperty text1Property
vtkActor2D text1Actor
  text1Actor SetPosition 14 14
  text1Actor SetMapper text1Mapper
vtkTextProperty text2Property
  text2Property SetFontSize 14
  text2Property SetJustificationToRight
  text2Property SetVerticalJustificationToBottom
  text2Property ShadowOn
vtkTextMapper text2Mapper
  text2Mapper SetInput "foo"
  text2Mapper SetTextProperty text2Property
vtkActor2D text2Actor
  text2Actor SetPosition [expr [lindex [renWin GetSize] 0 ] - 14] 14
  text2Actor SetMapper text2Mapper
#---Add the actor to the renderer, set the background and size
ren1 AddActor actor1
ren1 AddActor titleActor
ren1 AddActor text1Actor
ren1 AddActor text2Actor
#---Render the image
iren AddObserver UserEvent {wm deiconify .vtkInteract}
renWin Render
vtkTransform a
vtkTransform b
proc slice {n} {
  imageMapperSource SetZSlice $n
  renWin Render
}
set record "off"
proc recordOn {} {
    global frameNumber record
    set frameNumber 1000
    set record "on"
}
proc recordOff {} {
    global frameNumber record
    set frameNumber 1000
    set record "off"
}
proc go {{repeat 10}} {
    global record
    global fileID
    for {set i 0} { $i < $repeat } {incr i} {
        mutual Modified
        eval a SetMatrix [mutual GetMatrix]
        puts "[a GetOrientation] [a GetPosition] [a GetScale]"
        set rotatex [lindex [a GetOrientation] 0]
        set rotatey [lindex [a GetOrientation] 1]
        set rotatez [lindex [a GetOrientation] 2]
        set transx [lindex [a GetPosition] 0]
        set transy [lindex [a GetPosition] 1]
        set transz [lindex [a GetPosition] 2]
        set scalex [lindex [a GetScale] 0]
        set scaley [lindex [a GetScale] 1]
        set scalez [lindex [a GetScale] 2]
        text1Mapper SetInput "Orientation: $rotatex $rotatey $rotatez\nPosition:        $transx $transy $transz\nScale:            $scalex $scaley $scalez"
        renWin Render
        Record Movies/RGBtoCT
    }
}
proc MattesRigid {} {
    mutual MattesOn
    mutual SetNumberOfSamples 10000
    mutual SetNumberOfHistogramBins 50
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale 65
    text2Mapper SetInput "Mattes\nRigid"
}
proc MattesAffine {} {
    mutual MattesOn
    mutual SetNumberOfSamples 10000
    mutual SetNumberOfHistogramBins 50
    mutual SetModeToAffine
    mutual SetTranslateScale 500
    text2Mapper SetInput "Mattes\nAffine"
}
proc ViolaWellsRigid {} {
    mutual ViolaWellsOn
    mutual SetNumberOfSamples 50
    mutual SetModeToQuaternionRigid
    mutual SetTranslateScale 65
    text2Mapper SetInput "ViolaWells\nRigid"
}
proc ViolaWellsAffine {} {
    mutual ViolaWellsOn
    mutual SetNumberOfSamples 50
    mutual SetModeToAffine
    mutual SetTranslateScale 500
    text2Mapper SetInput "ViolaWells\nAffine"
}
proc slices {a b} {
    global frameNumber
    if {$a < $b } {
        for {set xx $a} {$xx <= $b} {incr xx} {
            imageMapperSource SetZSlice $xx; renWin Render;
            Record Movies/RGBtoCT
        }
    } else {
        for {set xx $a} {$xx >= $b} {incr xx -1} {
            imageMapperSource SetZSlice $xx; renWin Render;
            Record Movies/RGBtoCT
        }
    }
}
proc MakeMovie {} {
    global frameNumber
    set frameNumber 1000
    text1Mapper SetInput "Orientation: 0 0 0\nPosition:        0 0 0\nScale:            1 1 1"
    slice 50
    mutual Identity
    renWin Render
    MattesRigid
    SetSubSample 16; SetLearning 10; SetIterations 100; go 60
    SetSubSample 8; SetLearning 10; SetIterations 100; go 30
    slices 50 97
    slices 97 0
    slices 0 50
    MattesAffine
    SetSubSample 8; SetLearning 10; SetIterations 100; go 60
    SetSubSample 4; SetLearning 10; SetIterations 100; go 30
    SetSubSample 2; SetLearning 5; SetIterations 100; go 30
    SetSubSample 1; SetLearning 1; SetIterations 100; go 30
    slices 50 97
    slices 97 0
    slices 0 50
}
set frameNumber 1000
proc Record {prefix} {
    global frameNumber record
    if {$record == "on"} {
      catch {vtkRendererSource captureImage}
        captureImage SetInput ren1
        captureImage Modified
      catch {vtkPNGWriter captureWriter}
        captureWriter SetFileName ${prefix}${frameNumber}.png
        captureWriter SetInput [captureImage GetOutput]
        captureWriter Write
      incr frameNumber
    }
}
proc WriteTransformedLabelMask {vhmLabelDataPrefix outputPrefix} {
    global sliceMin sliceMax photoSliceShrinkFactor
    global ctShiftOrigin mutual
    for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice 1 } {
        catch {vtkPNGReader labelMaskReader; labelMaskReader AddObserver EndEvent {puts "Read [labelMaskReader GetInternalFileName]" }}
          labelMaskReader SetFileName ${vhmLabelDataPrefix}${slice}.png
          labelMaskReader SetDataExtent 0 2047 0 1349 0 0
          labelMaskReader SetDataSpacing .3 .3 [expr 1*$vhmCrossSectionSliceInc]
        catch {vtkImageClip labelMaskReaderClip}
          labelMaskReaderClip SetInput [labelMaskReader GetOutput]         
          labelMaskReaderClip SetOutputWholeExtent 0 2047 0 1100 0 0     
          labelMaskReaderClip ClipDataOn        
        catch {vtkImageShrink3D labelMaskReaderShrink}
          labelMaskReaderShrink SetInput [labelMaskReaderClip GetOutput]
          labelMaskReaderShrink SetShrinkFactors $photoSliceShrinkFactor $photoSliceShrinkFactor 1
          labelMaskReaderShrink AveragingOff
          labelMaskReaderShrink Update
        catch {vtkImageAppend buildUpLabelMaskImage}
          buildUpLabelMaskImage AddInput [labelMaskReaderShrink GetOutput]
          buildUpLabelMaskImage PreserveExtentsOff
          buildUpLabelMaskImage SetAppendAxis 2    
          [labelMaskReaderShrink GetOutput] SetSource ""
          labelMaskReaderShrink Delete
    }
    buildUpLabelMaskImage UpdateWholeExtent
    buildUpLabelMaskImage ReleaseDataFlagOn
    vtkImageChangeInformation labelMaskShiftOrigin
      labelMaskShiftOrigin SetInput [buildUpLabelMaskImage GetOutput]
    eval labelMaskShiftOrigin SetOutputSpacing [expr $photoSliceShrinkFactor*.3] [expr $photoSliceShrinkFactor*.3] [expr 1*${vhmCrossSectionSliceInc}]
      labelMaskShiftOrigin CenterImageOn
    vtkImageReslice newLabelMask
      newLabelMask SetInterpolationModeToNearestNeighbor
      newLabelMask SetInput [labelMaskShiftOrigin GetOutput]
      newLabelMask SetResliceTransform mutual
      newLabelMask SetBackgroundLevel 0
    eval newLabelMask SetOutputExtent [[ctShiftOrigin GetOutput] GetWholeExtent]
    eval newLabelMask SetOutputSpacing [[ctShiftOrigin GetOutput] GetSpacing]
    vtkPNGWriter transformedLabelMaskWriter
      transformedLabelMaskWriter SetFilePrefix $outputPrefix
      transformedLabelMaskWriter SetFilePattern "%s%d.png"
      transformedLabelMaskWriter SetInput [newLabelMask GetOutput]
      transformedLabelMaskWriter Write
}
MattesRigid
go 1
iren Initialize
#make interface
source [file join [file dirname [info script]] WindowLevelRGB.tcl]
