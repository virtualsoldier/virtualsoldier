#
# Display All the models
#

package require vtk
package require vtktesting
package require vtkinteraction

if {$argc < 1} {
  puts "usage: vtk DecimateModels.tcl *.vtk"
  exit
}

vtkMath math
vtkLookupTable lut

proc makecolors {n} {
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
   }
}

makecolors 512

vtkRenderWindow renWin
vtkRenderer ren1
  renWin AddRenderer ren1
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin

set allFiles [glob [lindex $argv 0]]

set n 1
foreach file $allFiles {
  vtkPolyDataReader ${file}Reader
    ${file}Reader SetFileName $file
    ${file}Reader ReleaseDataFlagOn
    
  catch {vtkAppendPolyData appendPD}
    appendPD AddInputConnection [${file}Reader GetOutputPort]
  incr n
}

vtkCleanPolyData clean
  clean SetInputConnection [appendPD GetOutputPort]

vtkQuadricClustering decimate
  decimate SetNumberOfDivisions 21 21 21
  decimate SetNumberOfDivisions 31 31 31
  decimate SetInputConnection [clean GetOutputPort]

vtkWindowedSincPolyDataFilter smooth
  smooth SetInputConnection [decimate GetOutputPort]
  smooth FeatureEdgeSmoothingOff
  smooth SetFeatureAngle 90
  smooth AddObserver EndEvent {puts "[[smooth GetOutput] Print]"}

vtkPolyDataMapper mapper
  mapper SetInputConnection [smooth GetOutputPort]
  mapper SetLookupTable lut
  mapper SetScalarRange 0 [lut GetNumberOfColors]
  mapper ImmediateModeRenderingOn

vtkLODActor actor
  actor SetMapper mapper
  ren1 AddActor actor

iren AddObserver UserEvent {wm deiconify .vtkInteract}
  iren Initialize

renWin Render
iren Start
