# This script packages the label files in the "Initial" directory that were created by registering P49 with the pig and copying over the label files.

for pig in P87
do
    for percentage in 30 80
    do

    pigDir=${pig}DcmPre${percentage}

    if (test $pigDir = P87DcmPre30 || test $pigDir = P127DcmPre30 )
    then series=2
    else
    series=1
    fi

    echo series: $series

        startImageNo="0001"

        # Copy the master anatomy files from P49 and rename them appropriately.
        baseDir="/projects/soldier/Data/CT/ISR"
        #cp ${baseDir}/P49DcmPre/P49DcmPrelp/P49DcmPrelpSeries1/P49DcmPrefpSeries1MasterAnatomy.csv ${baseDir}/${pigDir}/${pigDir}lp/${pigDir}lpSeries${series}/${pigDir}fpSeries${series}MasterAnatomy.csv
        cp ${baseDir}/P87DcmPre80/P87DcmPre80lp/P87DcmPre80lpSeries1/P87DcmPre80fpSeries1MasterAnatomy.csv ${baseDir}/${pigDir}/${pigDir}lp/${pigDir}lpSeries${series}/${pigDir}fpSeries${series}MasterAnatomy.csv

        # Create bounding boxes file from the label images.
        operationType="CalculateBoundingBoxes"
        ./PigPreProcessing.sh $operationType $pigDir $series $startImageNo dummy

        # Create the adjacency file from the label images.
        operationType="FindAdjacentLabels"
        ./PigPreProcessing.sh $operationType $pigDir $series $startImageNo dummy

        # Create models of all of the structures.
        rm -rf ${baseDir}/${pigDir}/${pigDir}mp
        operationType="Discrete"
        ./RunLevelSetsOnAllStructures.sh $operationType $pigDir $series $startImageNo

        # Tar the labels and models
        #cd ${baseDir}/${pigDir}
        echo tarring labels
        tar cf - ${baseDir}/${pigDir}/${pigDir}lp | gzip -c > "${baseDir}/${pigDir}/${pigDir}Labels.tgz"
        echo tarring models
        tar cf - ${baseDir}/${pigDir}/${pigDir}mp | gzip -c >"${baseDir}/${pigDir}/${pigDir}Models.tgz"
done
done