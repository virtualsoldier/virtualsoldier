# This script runs any of the levels sets.  It runs 2D and 3D level
# sets.  It also enabled the choice of several different data sets.

echo OS: $OS

# These paths can be changed based on your setup
if (test $OS = "Windows_NT" )
then
    mainImagePath="c:/MYDOCU~1/VirtualSoldier-data"
    pigDirec="ISR"
    execPath="c:/MYDOCU~1/VirtualSoldier/VirtualSoldier-win32/Code/Atlas/RelWithDebInfo"
    execExt=".exe"
elif (test $OS = "2.4" )
then
    mainImagePath="/projects/soldier/Data/CT"
    pigDirec="ISR"
    execPath="/home/padfield/VirtualSoldier/VirtualSoldier-linux/Code/Atlas"
    execExt=""
fi

#operationType=ThresholdLevelSet
#operationType=LaplacianLevelSet
#operationType=CannyLevelSet
#operationType=IsolatedConnectedLevelSet
#operationType=Discrete
operationType=$1

#imageType=Physiology
#imageType=Transformed_to_Ratiu
#imageType=Transformed_to_Ratiu_Refined
#imageType=ISRPA49a
#imageType=MGHPig
#imageType=P73DcmPre30
#imageType=ISRPA498
#imageType=P82DcmPostIsoWithM
#imageType=P82DcmPostIsoWithout
#imageType=P87DcmPostIsoWithM
#imageType=P87DcmPostIsoWithout
imageType=$2

imageDim=3

# if (test $imageDim -eq 2)
# then slices=1380
# elif (test $imageDim -eq 3)
# #then slices=%d
# then slices=1380
# fi

# PARAMETERS
#displayImageType="SpeedImage"
displayImageType="FeatureImage"

evolutionImages=""

if (test $imageType = ISRPA49a )
then slices="101" #Slice number should have three digits
    numberingOffset=249 
    anatomyName="aorta"
#    anatomyName="left_ventricle_ascending_aorta"
#    anatomyName="pulmonary_trunk"
#    anatomyName="myocardium"
#    anatomyName="pulm_veins_right_atrium"
#    anatomyName="right_atrium" #not present on slice 101!
elif (test $imageType = P73DcmPre80)
then slices=101
    numberingOffset=69
    percentage=80
    anatomyName="Struct32"
elif (test $imageType = MGHPig )
then slices=150
    anatomyName="myocardium"
elif (test $imageType = Transformed_to_Ratiu_Refined )
then slices=100
    anatomyName="aorta_ascending"
else
    slices="0020"
    anatomyName="heart"
#else
#     slices=1380
#     anatomyName="aorta_ascending"
fi




if (test $imageType = Physiology )
then echo Physiology
    imagePath="${mainImagePath}/VHM"
    outPath="${imagePath}/Models/${operationType}"
    featureImage=${imagePath}/70mm_manual_2048_reg/${slices}.png
    labelImage="${imagePath}/Relabeled/Relabeled${slices}.png"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
elif (test $imageType = Transformed )
then echo Transformed
    imagePath="${mainImagePath}/VHM"
    outPath="${imagePath}/ModelsTransformed/${operationType}"
    featureImage="${imagePath}/70mm_manual_2048_reg/${slices}.png"
    labelImage="${imagePath}/LabelDataTransformed/LabelDataTransformed${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxesTransformed.csv"
elif (test $imageType = Transformed_to_Ratiu )
then echo Transformed_to_Ratiu
    imagePath="${mainImagePath}"
    outPath="${imagePath}/ModelsTransformed_to_Ratiu/${operationType}"
    featureImage="${imagePath}/000007.SER_png/%d.png"
    labelImage="${imagePath}/LabelDataTransformed_to_Ratiu/Affine/LabelDataTransformed_to_Ratiu${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed_to_Ratiu.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${mainImagePath}/AnatomyBoundingBoxesTransformed_to_Ratiu.csv"
elif (test $imageType = Transformed_to_Ratiu_Refined )
then echo Transformed_to_Ratiu_Refined
    imagePath="${mainImagePath}/Ratiu"
    outPath="${imagePath}/ModelsTransformed_to_Ratiu_Refined/${operationType}"
    featureImage="${imagePath}/000007.SER_png/${slices}.png"
    labelImage="${imagePath}/LabelDataTransformed_to_Ratiu/RefinedAffine/LabelDataTransformed_to_Ratiu_Refined${slices}.png"
    imageInfoFile="${imagePath}/ImageInfoTransformed_to_Ratiu_Refined.csv"
    masterFile="${mainImagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxesTransformed_to_Ratiu_Refined.csv"
elif (test $imageType = ISRPA49a )
then echo ISRPA49a
    imagePath="${mainImagePath}/${pigDirec}/ISRPA49a"
    outPath="${imagePath}/Models/${operationType}"
#    featureImage="${imagePath}/Slices/IM${slices}.png"
    slicesOffset=`expr $slices + $numberingOffset`
    featureImage="${imagePath}/SelectedDicoms/im${slicesOffset}"
#    labelImage="${imagePath}/Repaired/Repaired${slices}.png"
#    labelImage="${imagePath}/Repaired/Repaired%d.png"    
    labelImage="${imagePath}/Labels/combo.${slices}.mhd"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${imagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"

    labelFile="${imagePath}/legend.lbl"
    labelImageRaw="${imagePath}/Labels/combo.${slices}"

elif (test $imageType = MGHPig )
then echo MGHPig
    imagePath="${mainImagePath}/${pigDirec}/MGHPig"
    outPath="${imagePath}/Models/${operationType}"
    featureImage=${imagePath}/greyscale/I.${slices}.mhd
    labelImage="${imagePath}/labels/heartWall.${slices}.mhd"
#    labelImage="${imagePath}/labels/heartWall.%03d.mhd"
    imageInfoFile="${imagePath}/ImageInfo.csv"
    masterFile="${imagePath}/MasterAnatomy.csv"
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"
    
    labelFile="${imagePath}/legend.lbl"

else
    echo $imageType
    series=$3
    imageNo="0100"
    imagePath="${mainImagePath}/${pigDirec}/${imageType}"
    slicesOffset=`expr $slices + $numberingOffset`
    featureImage="${imagePath}/${imageType}sp/${imageType}spSeries${series}/${imageType}spSeries${series}IM${imageNo}.dcm"

    labelImageRaw="${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial/${imageType}lpSeries${series}InitialIM${imageNo}"
    initialLabelImage="${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Initial/${imageType}lpSeries${series}InitialIM${imageNo}.png"
    finalLabelImage="${imagePath}/${imageType}lp/${imageType}lpSeries${series}/${imageType}lpSeries${series}Final/${imageType}lpSeries${series}FinalIM${imageNo}.png"

    imageInfoFile="dummy.csv"; #not used
    masterFile="${imagePath}/MasterAnatomy.csv"    
    boundingBoxesFile="${imagePath}/AnatomyBoundingBoxes.csv"

    outPath="${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}${operationType}"
    outSmoothedPath="${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}Smoothed"

fi


# Create the necessary label directories
if [ ! -d ${imagePath}/${imageType}lp ]; then
    mkdir ${imagePath}/${imageType}lp
fi
if [ ! -d ${imagePath}/${imageType}lp/${imageType}lpSeries${series} ]; then
    mkdir ${imagePath}/${imageType}lp/${imageType}lpSeries${series}
fi    

# Create the necessary model directories
if [ ! -d ${imagePath}/${imageType}mp ]; then
    mkdir ${imagePath}/${imageType}mp
fi
if [ ! -d ${imagePath}/${imageType}mp/${imageType}mpSeries${series} ]; then
    mkdir ${imagePath}/${imageType}mp/${imageType}mpSeries${series}
fi
if [ ! -d ${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}${operationType} ]; then
    mkdir ${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}${operationType}
fi

# We also want to create smooth versions of the discrete files.
if ( test $operationType = Discrete )
then
    if [ ! -d ${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}Smoothed ]; then
        mkdir ${imagePath}/${imageType}mp/${imageType}mpSeries${series}/${imageType}mpSeries${series}Smoothed
    fi
fi

#for anatomyName in aorta left_venticle_ascending_aorta myocardium pulm_veins_right_atrium pulmonary_trunk right_atrium to_be_identified_atrium
for anatomyName in $anatomyName
do
    for prop in 1
    do
        for curv in 35
        do
            for iter in 0
            do
                for cond in 2
                do
                    if (test $anatomyName = aorta_ascending )
                    then
                        borderSize=80
                    else
                        borderSize=20
                    fi

                    erode=0
                    levelSetIter=100 #Standard 400
                    rmsError=0.001     #Standard 0.02
                    echo rmsError: $rmsError

                    # Threshold level set parameters
                    mult=0.5
                    weight=0

                    # Canny level set parameters
                    advec=1
                    thresh=20
                    var=0

                    # Isolated level set parameters
                    useAllSeeds=0

                    echo anatomyName: $anatomyName
                    echo prop: $prop
                    echo curv: $curv

                    if (test $operationType = LaplacianLevelSet )
                    then echo LaplacianLevelSet 
                    execFile="${execPath}/LaplacianLevelSet${execExt}"
                    outFile=${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}_curv${curv}_iter${iter}_cond${cond}.vtk
                    ${execFile} ${featureImage} ${labelImage} ${imageInfoFile} ${masterFile} ${boundingBoxesFile} ${outFile} ${anatomyName} ${prop} ${curv} ${erode} ${iter} ${cond} ${borderSize} ${displayImageType} ${imageDim}

                    elif (test $operationType = ThresholdLevelSet )
                    then echo ThresholdLevelSet
                    execFile="${execPath}/ThresholdLevelSet${execExt}"
                    echo execFile: $execFile
                    outFile="${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}_mult${mult}_curv${curv}_erode${erode}_weight${weight}.vtk"
                    echo outFile: $outFile
                    ${execFile} ${featureImage} ${initialLabelImage} ${finalLabelImage} ${masterFile} ${boundingBoxesFile} ${outFile} ${anatomyName} ${mult} ${prop} ${curv} ${erode} ${weight} ${iter} ${cond} ${levelSetIter} ${rmsError} ${borderSize} ${displayImageType} ${imageDim}

                    elif (test $operationType = CannyLevelSet )
                    then echo CannyLevelSet
                    execFile="${execPath}/CannyLevelSet${imageDim}D${execExt}"
                    outFile="${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}_curv${curv}_advec${advec}_thresh${thresh}_var${var}_iter${iter}_cond${cond}.png"
                    ${execFile} ${featureImage} ${labelImage} ${imageInfoFile} ${masterFile} ${boundingBoxesFile} ${outFile} ${anatomyName} ${prop} ${curv} ${advec} ${thresh} ${var} ${iter} ${cond} ${borderSize} ${displayImageType}

                    elif (test $operationType = IsolatedConnectedLevelSet )
                    then echo IsolatedConnectedLevelSet
                    execFile="${execPath}/IsolatedConnectedLevelSet.exe"
                    outFile="${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}_mult${mult}_curv${curv}_erode${erode}_weight${weight}.vtk"
                    time ${execFile} ${featureImage} ${labelImage} ${imageInfoFile} ${masterFile} ${boundingBoxesFile} ${outFile} ${anatomyName} ${mult} ${prop} ${curv} ${erode} ${weight} ${iter} ${cond} ${levelSetIter} ${rmsError} ${borderSize} ${displayImageType} ${imageDim} ${useAllSeeds}

                    elif (test $operationType = IsolatedConnected )
                    then echo IsolatedConnected
                    execFile="${execPath}/IsolatedConnected${imageDim}D.exe"
                    outFile="${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}_curv${curv}_advec${advec}_thresh${thresh}_var${var}_iter${iter}_cond${cond}.vtk"
                    ${execFile} ${featureImage} c:/TEMP/IsolatedConnected.png 288 235 0 326 256                    
                    #${execFile} ${featureImage} c:/TEMP/IsolatedConnected.png 326 265 0 288 235 

                    elif (test $operationType = Discrete)
                    then echo DiscreteModel
                    execFile="${execPath}/CreateAModel${execExt}"
                    outFile="${outPath}/${imageType}mpSeries${series}${operationType}${anatomyName}.vtk"
                    outSmoothedFile="${outSmoothedPath}/${imageType}mpSeries${series}Smoothed${anatomyName}.vtk"
                    echo labelImage: $labelImage
                    echo imageInfoFile: $imageInfoFile
                    echo masterFile: $masterFile
                    echo boundingBoxesFile: $boundingBoxesFile
                    echo outFile: $outFile
                    echo anatomyName: $anatomyName
                    ${execFile} ${initialLabelImage} ${featureImage} ${masterFile} ${boundingBoxesFile} ${outFile} ${anatomyName} ${outSmoothedFile}
                    else
                    echo ERROR: No operation type specified!

                    fi

                done
            done
        done
    done        
done
