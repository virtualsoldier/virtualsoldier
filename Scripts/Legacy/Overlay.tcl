package provide app-watershed 1.0
package require BWidget

# Global state variables
set AnyButtonDown 0
set LeftButtonDown 0

# Global parameters
set scale -1
set axialSlice 10
set sagittalSlice 256
set coronalSlice 256
set sagittalSlice 10
set coronalSlice 10
set progress 0
set progressMessage ""
set positionMessage ""
set window 1600
set level -600
set relabelValue 0
set relabelHexColor [format "\#%02x%02x%02x" 0 0 0]
set changedIndexList ""

set lastIdx -1

# Initial ui used until the algorithm completes the first time
#
#
ProgressDlg .splash -title "Overlay..." -maximum 100 -relief ridge -variable progress -textvariable progressMessage
set progressMessage "Loading VTK+ITK..."
update
wm withdraw .
# Additional packages that take awhile to load
package require vtk
package require vtkinteraction

vtkPNGWriter pngLabelWriter
vtkImageFlip pngFlipped
vtkImageConstantPad pngFlippedPadded
vtkImageTranslateExtent pngFlippedPaddedTranslated

# VTK globals
vtkMath math
vtkLookupTable lut
# Build a lookup table for labels
proc makecolors {n} {
   global progress
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   set ticks 20
   set d [expr $n / $ticks]
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
     if {[expr $i % $d] == 0} {set progress [expr int(100*(double($i)/double($n)))]}
   }
}
set progressMessage "Building labels..."
makecolors 512
# Callback routine for progress methods
#
proc ProgressCommand { obj } {
    global progress
    set progress [expr 100*[$obj GetProgress]]
}

proc StartCommand { msg} {
    global progress progressMessage
    set progress 0
    set progressMessage $msg
}

proc EndCommand {} {
    global progress progressMessage
    set progress 0
    set progressMessage ""
}
#
# Check the output of the relabeler to see it the lut is large enough
# to hold all the colors
proc CheckLUTSizeCommand {} {
    set totalNeeded 500
    if {$totalNeeded > [lut GetNumberOfColors]} {
	makecolors [expr 2 * $totalNeeded]
	catch {modelMapper SetScalarRange 0 [lut GetNumberOfColors]}
    }
}

# grab arguments
if {$argc != 6} {
    puts "ERROR"
    puts "Usage: Overlay.tcl labelPrefix photoPrefix startSliceNumber endSliceNumber masterAnatomyFile relabeledPrefix"
    exit
} else  {
    set labelPrefix [lindex $argv 0]
    set photoPrefix [lindex $argv 1]
    set startSliceNumber [lindex $argv 2]
    set endSliceNumber [lindex $argv 3]
    set masterAnatomyFile [lindex $argv 4]
    set relabeledPrefix [lindex $argv 5]
} 
#labelFileName "C:/Projects/VirtualSoldier/Work/VirtualSoldier/Scripts/LabelMaps/Relabeled0.png"
#photoFileName "C:/Projects/VirtualSoldier/Work/VirtualSoldier/Scripts/PhotoDataPNG/1253.png"

#Read in the MasterAnatomy.csv file and creat a map
#set MasterAnatomyFile "S:/Data/Atlas/Processed/MasterAnatomy.csv"
set MasterAnatomyFile $masterAnatomyFile
set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " br " [lindex $fields 0] " branch " object
  regsub -all " lt " $object " left " object
  regsub -all " rt " $object " right " object
  regsub -all " pul " $object " pulmonary " object
  foreach letter {a b c d e f g h i j k l m n o p q r s t u v w x y z} {
      set uppercaseLetter [string toupper $letter] 
      regsub -all " $letter" $object " $uppercaseLetter" object
  }
  set firstLetter [string index $object 0]
  set uppercaseFirstLetter [string toupper $firstLetter] 
  regsub "[string index $object 0]" $object "$uppercaseFirstLetter" object 
  set label [lindex $fields 1]
  set labelToAnatomy($label) $object
}
close $maFID
vtkPNGReader pngLabelReader
  pngLabelReader SetFilePrefix $labelPrefix
  pngLabelReader SetFilePattern "%s%d.png"
  pngLabelReader SetDataExtent 0 2047 0 1349 $startSliceNumber $endSliceNumber
  pngLabelReader SetDataSpacing .32 .32 1
  pngLabelReader ReleaseDataFlagOn

vtkImageClip labelClip
  labelClip SetInput [pngLabelReader GetOutput]
  labelClip SetOutputWholeExtent 0 2047 0 1023 $startSliceNumber $endSliceNumber
  labelClip ClipDataOn

vtkImageTranslateExtent labelTranslate
  labelTranslate SetInput [labelClip GetOutput]
  labelTranslate SetTranslation 0 0 -$startSliceNumber

vtkPNGReader pngPhotoReader
  pngPhotoReader SetFilePrefix $photoPrefix
  pngPhotoReader SetFilePattern "%s%d.png"
  pngPhotoReader SetDataExtent 0 2047 0 1349 $startSliceNumber $endSliceNumber
  pngPhotoReader SetDataSpacing .32 .32 1
  pngPhotoReader ReleaseDataFlagOn

vtkImageClip photoClip
  photoClip SetInput [pngPhotoReader GetOutput]
  photoClip SetOutputWholeExtent 0 2047 0 1023 $startSliceNumber $endSliceNumber
  photoClip ClipDataOn

vtkImageTranslateExtent photoTranslate
  photoTranslate SetInput [photoClip GetOutput]
  photoTranslate SetTranslation 0 0 -$startSliceNumber

# Pipeline
#

vtkImageFlip labelReader
  labelReader SetInput [labelTranslate GetOutput]
  labelReader SetFilteredAxis 1
  labelReader FlipAboutOriginOff
  labelReader ReleaseDataFlagOff
  labelReader AddObserver StartEvent {StartCommand "Flipping Label Images..."}
  labelReader AddObserver ProgressEvent "ProgressCommand labelReader"
  labelReader AddObserver EndEvent {EndCommand}
  labelReader Update

vtkImageFlip photoReader
  photoReader SetInput [pngPhotoReader GetOutput]
  photoReader SetInput [photoTranslate GetOutput]
  photoReader SetFilteredAxis 1
  photoReader FlipAboutOriginOff
  photoReader ReleaseDataFlagOff
  photoReader AddObserver StartEvent {StartCommand "Flipping Photo Images..."}
  photoReader AddObserver ProgressEvent "ProgressCommand photoReader"
  photoReader AddObserver EndEvent {EndCommand}
  photoReader Update

# Now that the initial segmentation is done. Delete the initial dialog
#
#
destroy .splash

proc UpdateLabelMap {index label {undo 0}} {
    global labelReader axialBlend coronalBlend sagittalBlend
    global changedLabelRecord changedIndexList
    global LeftButtonDown
    if { $LeftButtonDown || $undo} {
      set oldLabel [[[[labelReader GetOutput] GetPointData] GetScalars] GetTuple1 $index]
        if {!$undo} { 
            set alreadyIn 0
            foreach indexInRecord [array names changedLabelRecord] {
                if {$indexInRecord==$index} {
                    set alreadyIn 1
                }
            }
            if {!$alreadyIn} {
                set changedIndexList [lappend changedIndexList $index]  
                set changedLabelRecord($index) $oldLabel
            }
        }
      if { $oldLabel == $label } { return }
      [[[labelReader GetOutput] GetPointData] GetScalars] SetTuple1 $index $label
      set fv [[colorIt GetLookupTable] GetTableValue $label]
      set r [expr int(255 * [lindex $fv 0])]
      set g [expr int(255 * [lindex $fv 1])]
      set b [expr int(255 * [lindex $fv 2])] 
      [[[colorIt GetOutput] GetPointData] GetScalars] SetTuple4 $index $r $g $b 255
      [[[colorIt GetOutput] GetPointData] GetScalars] Modified
      [[colorIt GetOutput] GetPointData] Modified
      [colorIt GetOutput] Modified
      sagittalColor Modified
      coronalColor Modified
      axialBlend Modified
      }
}                             

proc WriteRelabeled {} {
    global labelReader
    global relabeledPrefix
    global pngLabelWriter
    global startSliceNumber endSliceNumber
    global pngFlipped
    global pngFlippedPadded
    global pngFlippedPaddedTranslated

    pngFlipped SetInput [labelReader GetOutput]
    pngFlipped SetFilteredAxis 1
    pngFlipped FlipAboutOriginOff
    pngFlipped ReleaseDataFlagOff

    pngFlippedPadded SetConstant 0
    #pngFlippedPadded SetOutputWholeExtent 0 2047 0 1349 $startSliceNumber $endSliceNumber
    pngFlippedPadded SetOutputWholeExtent 0 2047 0 1349 0 [expr $endSliceNumber-$startSliceNumber]
    #pngFlippedPadded SetInput [pngPaddedLabelWriter GetOutput]
    pngFlippedPadded SetInput [pngFlipped GetOutput]
    
    pngFlippedPaddedTranslated SetInput [pngFlippedPadded GetOutput]
    pngFlippedPaddedTranslated SetTranslation 0 0 $startSliceNumber

    pngLabelWriter SetFilePrefix $relabeledPrefix
    pngLabelWriter SetFilePattern "%s%d.png"
    pngLabelWriter SetInput [pngFlippedPaddedTranslated GetOutput]
    pngLabelWriter Write
}

proc UndoRelabeled {} {
    global changedLabelRecord changedIndexList
    set arrayLength [array size changedLabelRecord]
    if {$arrayLength != 0} {
        set index [lindex $changedIndexList [expr $arrayLength-1]]
        UpdateLabelMap $index $changedLabelRecord($index) 1
        unset changedLabelRecord($index)
        set changedIndexList [lreplace $changedIndexList [expr $arrayLength-1] [expr $arrayLength-1]] 
    }
    Render
}


# Pipeline to color and blend the segmentation
# 
#
vtkImageMapToColors colorIt
  colorIt SetInput [labelReader  GetOutput]
  colorIt SetLookupTable lut
  colorIt UpdateWholeExtent

vtkImageReslice coronalColor
  coronalColor SetInput [colorIt GetOutput]
  coronalColor InterpolateOff
  coronalColor SetResliceAxesDirectionCosines 1 0 0  0 0 1  0 1 0

vtkImageReslice coronal
  coronal SetInput [photoReader GetOutput]
  coronal InterpolateOff
  coronal SetResliceAxesDirectionCosines 1 0 0  0 0 1  0 1 0

vtkImageReslice sagittalColor
  sagittalColor SetInput [colorIt GetOutput]
  sagittalColor InterpolateOff
  sagittalColor SetResliceAxesDirectionCosines 0 1 0  0 0 1  1 0 0

vtkImageReslice sagittal
  sagittal SetInput [photoReader GetOutput]
  sagittal InterpolateOff
  sagittal SetResliceAxesDirectionCosines 0 1 0  0 0 1  1 0 0

vtkImageBlend axialBlend
  axialBlend SetInput 0 [photoReader GetOutput]
  axialBlend SetInput 1 [colorIt GetOutput]
  axialBlend SetOpacity 1 0.3

vtkImageBlend coronalBlend
  coronalBlend SetInput 0 [coronal GetOutput]
  coronalBlend SetInput 1 [coronalColor GetOutput]
  coronalBlend SetOpacity 1 0.3

vtkImageBlend sagittalBlend
  sagittalBlend SetInput 0 [sagittal GetOutput]
  sagittalBlend SetInput 1 [sagittalColor GetOutput]
  sagittalBlend SetOpacity 1 0.3

vtkImageRectilinearWipe axialWipe
axialWipe SetInput1 [photoReader GetOutput]
axialWipe SetInput2 [axialBlend GetOutput]
axialWipe SetWipeToQuad
#axialWipe AddObserver EndEvent { puts "axialWipe " }

vtkImageRectilinearWipe coronalWipe
  coronalWipe SetInput1 [coronal GetOutput]
  coronalWipe SetInput2 [coronalBlend GetOutput]
  coronalWipe SetWipeToQuad

vtkImageRectilinearWipe sagittalWipe
  sagittalWipe SetInput1 [sagittal GetOutput]
  sagittalWipe SetInput2 [sagittalBlend GetOutput]
  sagittalWipe SetWipeToQuad

vtkImageCrossHair2D axialCursor
  axialCursor SetInput [axialBlend GetOutput]
  axialCursor SetCursorColor 1 1 1
  axialCursor BullsEyeOff

vtkImageCrossHair2D coronalCursor
  coronalCursor SetInput [coronalBlend GetOutput]
  coronalCursor SetCursorColor 1 1 1
  coronalCursor BullsEyeOff

vtkImageCrossHair2D sagittalCursor
  sagittalCursor SetInput [sagittalBlend GetOutput]
  sagittalCursor SetCursorColor 1 1 1
  sagittalCursor BullsEyeOff

# model pipeline
#
# 

vtkDiscreteMarchingCubes modelContour
  modelContour SetInput [labelReader GetOutput]
  modelContour SetNumberOfContours 0
  modelContour ComputeScalarsOff
  modelContour ComputeGradientsOff
  modelContour ComputeNormalsOff
  modelContour ReleaseDataFlagOn
  modelContour AddObserver StartEvent {StartCommand "Contouring..." }
  modelContour AddObserver ProgressEvent "ProgressCommand modelContour"
  modelContour AddObserver EndEvent {EndCommand}

vtkWindowedSincPolyDataFilter modelSmoother
  modelSmoother SetInput [modelContour GetOutput]
  modelSmoother ReleaseDataFlagOn
  modelSmoother AddObserver StartEvent {StartCommand "Smoothing..." }
  modelSmoother AddObserver ProgressEvent "ProgressCommand modelSmoother"
  modelSmoother AddObserver EndEvent {EndCommand}

vtkPolyDataNormals modelNormals
  modelNormals SetInput [modelSmoother GetOutput]
  modelNormals SetFeatureAngle 90
  modelNormals ReleaseDataFlagOn
  modelNormals AddObserver StartEvent {StartCommand "Normals..." }
  modelNormals AddObserver ProgressEvent "ProgressCommand modelNormals"
  modelNormals AddObserver EndEvent {EndCommand}

# set up 4 windows
#
#
vtkImageViewer2 axialViewer
  axialViewer SetInput [axialCursor GetOutput]
  axialViewer SetColorWindow 255
  axialViewer SetColorLevel 127.5
  [axialViewer GetImageActor] InterpolateOff
  set axialViewer axialViewer
  set axialRW [axialViewer GetRenderWindow]

vtkImageViewer2 coronalViewer
  coronalViewer SetInput [coronalCursor GetOutput]
  coronalViewer SetColorWindow 255
  coronalViewer SetColorLevel 127.5
  [coronalViewer GetImageActor] InterpolateOff
  set coronalViewer coronalViewer
  set coronalRW [coronalViewer GetRenderWindow]
vtkImageViewer2 sagittalViewer
  sagittalViewer SetInput [sagittalCursor GetOutput]
  sagittalViewer SetColorWindow 255
  sagittalViewer SetColorLevel 127.5
  [sagittalViewer GetImageActor] InterpolateOff
  set sagittalViewer sagittalViewer
  set sagittalRW [sagittalViewer GetRenderWindow]

vtkPolyDataMapper modelMapper
modelMapper SetInput [modelNormals GetOutput]
modelMapper ScalarVisibilityOn
modelMapper SetLookupTable lut
modelMapper ImmediateModeRenderingOn
modelMapper SetScalarRange 0 [lut GetNumberOfColors]

vtkActor modelActor
  modelActor SetMapper modelMapper
  modelActor VisibilityOff
vtkSphereSource sphereModel
  sphereModel SetRadius 1

vtkPolyDataMapper sphereMapper
  sphereMapper SetInput [sphereModel GetOutput]

vtkActor sphereActor
  sphereActor SetMapper sphereMapper
  sphereActor VisibilityOff
  sphereActor PickableOff

vtkRenderer modelRenderer
  modelRenderer AddActor modelActor
  modelRenderer AddActor sphereActor
  modelRenderer SetBackground .1 .2 .3
vtkLightKit modelKit
  modelKit AddLightsToRenderer modelRenderer

vtkRenderWindow modelWindow
  modelWindow AddRenderer modelRenderer
  set modelRW modelWindow

# Construct cameras
vtkCamera axialCamera
axialCamera ParallelProjectionOn
axialCamera SetViewUp 0 -1 0
axialCamera Azimuth 180
[axialViewer GetRenderer] SetActiveCamera axialCamera
vtkCamera sagittalCamera
sagittalCamera ParallelProjectionOn
sagittalCamera SetViewUp 0 -1 0
sagittalCamera Azimuth 180
[sagittalViewer GetRenderer] SetActiveCamera sagittalCamera
vtkCamera coronalCamera
coronalCamera ParallelProjectionOn
coronalCamera SetViewUp 0 -1 0
coronalCamera Azimuth 180
[coronalViewer GetRenderer] SetActiveCamera coronalCamera
# Procs to synchronize the events between the various render windows
#
#
proc SyncMoveEvent {rw} {
    global axialRW coronalRW sagittalRW
    global axialViewer coronalViewer sagittalViewer
    global AnyButtonDown LeftButtonDown
    global positionMessage
    global labelToAnatomy
    global relabelValue
    global lastIdx

    # puts "[[$rw GetInteractor] GetKeyCode]"

    # if the button was down, then render the other windows, if not, then
    # probe the data at the pixel location
    if {$AnyButtonDown == 1 && $LeftButtonDown != 1} {
        # not currently doing anything here

    } elseif { $AnyButtonDown == 0 || ($LeftButtonDown == 1 && [[$rw GetInteractor] GetShiftKey] == 0)}  {
        set iren [$rw GetInteractor]
        set istyle [$iren GetInteractorStyle]
        set picker [$iren GetPicker]

        # get the mouse position in world coordinates
        set pos [$iren GetEventPosition]
        $istyle FindPokedRenderer [lindex $pos 0] [lindex $pos 1]
        $picker Pick [lindex $pos 0] [lindex $pos 1] 0 \
            [$istyle GetCurrentRenderer]
        set pPosition [$picker GetPickPosition]

        # permute the pick position if necessary
        if { $rw == $axialRW } {
            set pickPosition $pPosition
        } elseif { $rw == $coronalRW } {
            set pickPosition [list [lindex $pPosition 0] [lindex $pPosition 2] [lindex $pPosition 1]]
        } elseif { $rw == $sagittalRW} {
            set pickPosition [list [lindex $pPosition 2] [lindex $pPosition 0] [lindex $pPosition 1]]
        }
	
	# convert to an index
	set ijk [XYZToIJK $pickPosition]
	set idx [lindex $ijk 0]
	# if idx is -1, then position was out of bounds, just return
	if {$idx == -1} { return }
	# otherwise, pull out the index
	set i [lindex $ijk 1]
	set j [lindex $ijk 2]
	set k [lindex $ijk 3]

        # probe the label 
        set l [[[[labelReader GetOutput] GetPointData] GetScalars] GetValue $idx]
	# if the left button was pressed, then navigate the 3 views
	if { $LeftButtonDown == 1 } {
	    if { $idx != $lastIdx && [[$rw GetInteractor] GetKeySym] == "Alt_L" } {
		UpdateLabelMap $idx $relabelValue
		set lastIdx $idx
	    }
          SetAxialSlice $k 0
          SetCoronalSlice $j 0
          SetSagittalSlice $i 0
          Render
	}

 #  puts "Anatomy -------- $labelToAnatomy(l)"
	# update the ui
	set positionMessage [format "(%1.2f mm, %1.2f mm, %1.2f mm), (%d px, %d px, %d px) , Label: %d, Anatomy: %s" [lindex $pickPosition 0] [lindex $pickPosition 1] [lindex $pickPosition 2] $i $j $k $l $labelToAnatomy($l) ]
    }

   # clear any key sym.  should be a cleaner way to do this. KeyUpEvent?
   [$rw GetInteractor] SetKeySym ""
}

proc SetRelabelValue { {value default} } {
    global relabelValue
    global relabelHexColor
    global relabelerFrame
    if {$value != "default"} {
        set relabelValue $value    
    }
    set fv [[colorIt GetLookupTable] GetTableValue $relabelValue]
    set r [expr int(255 * [lindex $fv 0])]
    set g [expr int(255 * [lindex $fv 1])]
    set b [expr int(255 * [lindex $fv 2])] 
    set relabelHexColor [format "\#%02x%02x%02x" $r $g $b]

    $relabelerFrame.relabelerBitmap config -background $relabelHexColor -foreground $relabelHexColor -height 1 -width 2
}

proc SyncLeftButtonPressEvent {rw} {
    global AnyButtonDown
    global LeftButtonDown
    set AnyButtonDown 1
    set LeftButtonDown 1

    .status.position configure -text " "

    SyncMoveEvent $rw
}
proc SyncMiddleButtonPressEvent {rw} {
    global AnyButtonDown
    set AnyButtonDown 1

    .status.position configure -text " "

    SyncMoveEvent $rw
}


proc SyncRightButtonPressEvent {rw} {
    global AnyButtonDown
    set AnyButtonDown 1

    .status.position configure -text " "

    SyncMoveEvent $rw
}

proc SyncLeftButtonReleaseEvent {rw} {
    global AnyButtonDown
    global LeftButtonDown

    set AnyButtonDown 0
    set LeftButtonDown 0
    SyncMoveEvent $rw
}

proc SyncMiddleButtonReleaseEvent {rw} {
    global AnyButtonDown

    set AnyButtonDown 0
    SyncMoveEvent $rw
}

proc SyncRightButtonReleaseEvent {rw} {
    global AnyButtonDown

    set AnyButtonDown 0
    SyncMoveEvent $rw
}

proc SyncLeaveEvent {rw} {
    global AnyButtonDown
    #set AnyButtonDown 0
    .status.position configure -text " "
}
proc SyncEnterEvent {rw} {
    global AnyButtonDown
    #set AnyButtonDown 0
}

proc Render {} {
    axialViewer Render
    coronalViewer Render
    sagittalViewer Render
}

proc ResetCamera {rw {all 0}} {
    [$rw GetRenderers] InitTraversal
    set bounds [[photoReader GetOutput] GetBounds]
    set range [expr [lindex $bounds 1] - [lindex $bounds 0]]
    set rangey [expr [lindex $bounds 3] - [lindex $bounds 2]]
    set rangez [expr [lindex $bounds 5] - [lindex $bounds 4]]
    if {$range < $rangey} {set range $rangey}
    if {$range < $rangez} {set range $rangez}
    if {$all == 0} {
        set ren [[$rw GetRenderers] GetNextItem]
        $ren ResetCamera
        [$ren GetActiveCamera] SetParallelScale [expr $range / 2.0]
        $rw Render
    } else {
        [axialViewer GetRenderer] ResetCamera
        axialCamera SetParallelScale [expr $range / 2.0]
        [coronalViewer GetRenderer] ResetCamera
        coronalCamera SetParallelScale [expr $range / 2.0]
        [sagittalViewer GetRenderer] ResetCamera
        sagittalCamera SetParallelScale [expr $range / 2.0]
        Render
    }
}

proc SetAxialSlice { {slice default} {render 1} } {
    global axialSlice
    if {$slice != "default"} {
        set axialSlice $slice
    }
    UpdateCursor
    axialViewer SetZSlice $axialSlice
    if { $render } Render
}

proc SetCoronalSlice { {slice default} {render 1} } {
    global coronalSlice
    if {$slice != "default"} {
        set coronalSlice $slice
    }
    UpdateCursor
    coronalViewer SetZSlice $coronalSlice
    if { $render } Render
}

proc SetSagittalSlice { {slice default} {render 1} } {
    global sagittalSlice
    if {$slice != "default"} {
        set sagittalSlice $slice
    }
    UpdateCursor
    sagittalViewer SetZSlice $sagittalSlice
    if { $render } Render
}

proc UpdateCursor {} {
    global axialSlice sagittalSlice coronalSlice

    # set the cursor position and set the wipe positions
    axialCursor SetCursor $sagittalSlice $coronalSlice 
    axialWipe SetPosition $sagittalSlice $coronalSlice 
    coronalCursor SetCursor $sagittalSlice $axialSlice 
    coronalWipe SetPosition $sagittalSlice $axialSlice 
    sagittalCursor SetCursor $coronalSlice $axialSlice 
    sagittalWipe SetPosition $coronalSlice $axialSlice 
}
proc SyncEvents {rw} {
   set iren [$rw GetInteractor ]
   $iren AddObserver MouseMoveEvent "SyncMoveEvent $rw" 
   $iren AddObserver LeftButtonPressEvent "SyncLeftButtonPressEvent $rw"
   $iren AddObserver MiddleButtonPressEvent "SyncMiddleButtonPressEvent $rw"
   $iren AddObserver RightButtonPressEvent "SyncRightButtonPressEvent $rw"
   $iren AddObserver LeftButtonReleaseEvent "SyncLeftButtonReleaseEvent $rw"
   $iren AddObserver MiddleButtonReleaseEvent "SyncMiddleButtonReleaseEvent $rw"
   $iren AddObserver RightButtonReleaseEvent "SyncRightButtonReleaseEvent $rw"
   $iren AddObserver EnterEvent "SyncEnterEvent $rw"
   $iren AddObserver LeaveEvent "SyncLeaveEvent $rw"
}

proc ProcessKeys {rw} {
    global axialSlice coronalSlice sagittalSlice
    global modelRW
    global axialRW coronalRW sagittalRW

    set iren [$rw GetInteractor]
    if {[$iren GetKeyCode] == "a"} {
	set l [[labelReader GetOutput] GetScalarComponentAsDouble $sagittalSlice $coronalSlice $axialSlice 0]
	set numValues [modelContour GetNumberOfContours]
	modelContour SetValue $numValues $l
	modelActor VisibilityOn
	modelRenderer ResetCamera
	modelWindow Render
    } elseif {[$iren GetKeyCode] == "c"} {
	modelContour SetNumberOfContours 0
        modelActor VisibilityOff
	modelWindow Render
    } elseif {[$iren GetKeyCode] == "g"} {
        set irenGrab [$rw GetInteractor]
        set picker [$irenGrab GetPicker]
        set pPosition [$picker GetPickPosition]
        # permute the pick position if necessary
        if { $rw == $axialRW } {
            set pickPosition $pPosition
        } elseif { $rw == $coronalRW } {
            set pickPosition [list [lindex $pPosition 0] [lindex $pPosition 2] [lindex $pPosition 1]]
        } elseif { $rw == $sagittalRW} {
            set pickPosition [list [lindex $pPosition 2] [lindex $pPosition 0] [lindex $pPosition 1]]
        }	
	set ijk [XYZToIJK $pickPosition]
	set idx [lindex $ijk 0]
        set l [[[[labelReader GetOutput] GetPointData] GetScalars] GetValue $idx]
        SetRelabelValue $l        
        #puts "[format "\#%02x%02x%02x" 200 150 20]"
    } elseif {[$iren GetKeyCode] == "d"} {
	modelContour SetNumberOfContours [expr [modelContour GetNumberOfContours] - 1]
        if {[modelContour GetNumberOfContours] == 0} {
            modelActor VisibilityOff
        }
	modelWindow Render
    } elseif {[$iren GetKeyCode] == "u"} {
	$iren UserCallback
    } elseif {[$iren GetKeyCode] == "R"} {
        ResetCamera $rw 1
    } elseif {[$iren GetKeyCode] == "r"} {
        ResetCamera $rw 0
    } elseif {[$iren GetKeyCode] == "e"} {
           #SpinBox .b2 -command SetAxialSlice -modifycmd SetAxialSlice -range "[axialViewer GetWholeZMin] [axialViewer GetWholeZMax] 1" -textvariable axialSlice -width 5        
#        SpinBox .b2 -range "0 10 1" -textvariable "tEsT" -width 5
        #SpinBox .b2
 #       pack .b2        
    } elseif {[$iren GetKeyCode] == "p"} {
        set istyle [$iren GetInteractorStyle]
        set picker [$iren GetPicker]

        # get the mouse position in world coordinates
        set pos [$iren GetEventPosition]
        $istyle FindPokedRenderer [lindex $pos 0] [lindex $pos 1]
        $picker Pick [lindex $pos 0] [lindex $pos 1] 0 \
            [$istyle GetCurrentRenderer]

        ProcessPick $rw
	$modelRW Render
    } elseif {[$iren GetKeyCode] == "w"} {
	if {[axialCursor GetInput] == [axialBlend GetOutput]} {
	    axialCursor SetInput [axialWipe GetOutput]
	    coronalCursor SetInput [coronalWipe GetOutput]
	    sagittalCursor SetInput [sagittalWipe GetOutput]
	} else {
	    axialCursor SetInput [axialBlend GetOutput]

	    coronalCursor SetInput [coronalBlend GetOutput]
	    sagittalCursor SetInput [sagittalBlend GetOutput]
	}
	Render
    } elseif {[$iren GetKeyCode] == "1"} {
	axialCursor SetInput [axialWipe GetOutput]
	coronalCursor SetInput [coronalWipe GetOutput]
	sagittalCursor SetInput [sagittalWipe GetOutput]
	axialWipe SetWipeToQuad
	coronalWipe SetWipeToQuad
	sagittalWipe SetWipeToQuad
	Render
    } elseif {[$iren GetKeyCode] == "2"} {
	axialCursor SetInput [axialWipe GetOutput]
	coronalCursor SetInput [coronalWipe GetOutput]
	sagittalCursor SetInput [sagittalWipe GetOutput]
	axialWipe SetWipeToHorizontal
	coronalWipe SetWipeToHorizontal
	sagittalWipe SetWipeToHorizontal
	Render
    } elseif {[$iren GetKeyCode] == "3"} {
	axialCursor SetInput [axialWipe GetOutput]
	coronalCursor SetInput [coronalWipe GetOutput]
	sagittalCursor SetInput [sagittalWipe GetOutput]
	axialWipe SetWipeToVertical
	coronalWipe SetWipeToVertical
	sagittalWipe SetWipeToVertical
	Render
    }
}
proc ProcessModelKeys {rw} {
    set iren [$rw GetInteractor]
    set istyle [$iren GetInteractorStyle]
    if {[$iren GetKeyCode] == "u"} {
	$iren UserCallback
    } elseif {[$iren GetKeyCode] == "r"} {
	set ren [$istyle GetCurrentRenderer]
        $ren ResetCamera
	$rw Render
    } elseif {[$iren GetKeyCode] == "p"} {
        set picker [$iren GetPicker]

        # get the mouse position in world coordinates
        set pos [$iren GetEventPosition]
        $istyle FindPokedRenderer [lindex $pos 0] [lindex $pos 1]
        $picker Pick [lindex $pos 0] [lindex $pos 1] 0 \
            [$istyle GetCurrentRenderer]

        ProcessPick $rw
	$rw Render
    } elseif {[$iren GetKeyCode] == "w"} {
    } elseif {[$iren GetKeyCode] == "s"} {
    } elseif {[$iren GetKeyCode] == "t"} {
	$istyle SetCurrentStyleToTrackballCamera
    } elseif {[$iren GetKeyCode] == "j"} {
	$istyle SetCurrentStyleToJoystickCamera
    }
}
proc ProcessPick { rw } {
    global positionMessage 
    global axialRW coronalRW sagittalRW modelRW
    global labelToAnatomy

    set iren [$rw GetInteractor]
    set picker [$iren GetPicker]
    set pPosition [$picker GetPickPosition]

    # permute the pick position if necessary
    if { $rw == $axialRW || $rw == $modelRW} {
	set pickPosition $pPosition
    } elseif { $rw == $coronalRW } {
	set pickPosition [list [lindex $pPosition 0] [lindex $pPosition 2] [lindex $pPosition 1]]
    } elseif { $rw == $sagittalRW} {
	set pickPosition [list [lindex $pPosition 2] [lindex $pPosition 0] [lindex $pPosition 1]]
    }
    
    # set the position of the 3D marker
    sphereActor SetPosition [lindex $pickPosition 0] [lindex $pickPosition 1] [lindex $pickPosition 2]
    sphereActor VisibilityOn

    # convert the position to an image index
    set ijk [XYZToIJK $pickPosition]
    set idx [lindex $ijk 0]
    # if the position is not in the image, idx is set to -1.
    # in this case, skip the rest of the method
    if {$idx == -1} {return}
    set i [lindex $ijk 1]
    set j [lindex $ijk 2]
    set k [lindex $ijk 3]

    # probe the label 
    set l [[[[labelReader GetOutput] GetPointData] GetScalars] GetValue $idx]
    #puts "Anatomy -------- $labelToAnatomy(l)"
    # update the ui
    set positionMessage [format "(%1.2f mm, %1.2f mm, %1.2f mm), (%d px, %d px, %d px), Anatomy: %s" [lindex $pickPosition 0] [lindex $pickPosition 1] [lindex $pickPosition 2] $i $j $k $labelToAnatomy($l)]

    # navigate the three views
    SetAxialSlice $k
    SetCoronalSlice $j
    SetSagittalSlice $i
}

# Convert an XYZ position to a linear index into the scalar array
# as well as separate i, j, k indices.  Returns a list that is 
# {idx i j k}.
#
proc XYZToIJK { xyz } {
    global axialViewer coronalViewer sagittalViewer
    
    # convert the position to an image index
    set idx [[photoReader GetOutput] FindPoint [lindex $xyz 0] [lindex $xyz 1] [lindex $xyz 2]]
    # if the position is not in the image, idx is set to -1.
    # in this case, skip the rest of the method
    if {$idx == -1} {return {-1 0 0 0}}
    
    # get the bounds of the dataset
    set bounds [[photoReader GetOutput] GetBounds]
    
    # convert position to ijk
    set spacing [[photoReader GetOutput] GetSpacing]
    set origin [[photoReader GetOutput] GetOrigin]
    
    set i [expr int(([lindex $xyz 0] - [lindex $origin 0]) / [lindex $spacing 0] + 0.5)]
    set j [expr int(([lindex $xyz 1] - [lindex $origin 1]) / [lindex $spacing 1] + 0.5)]
    set k [expr int(([lindex $xyz 2] - [lindex $origin 2]) / [lindex $spacing 2] + 0.5)]
    
    if { $i < [$sagittalViewer GetWholeZMin] } { 
	set i [$sagittalViewer GetWholeZMin] 
    }
    if { $i > [$sagittalViewer GetWholeZMax] } { 
	set i [$sagittalViewer GetWholeZMax] 
    }
    if { $j < [$coronalViewer GetWholeZMin] } { 
	set j [$coronalViewer GetWholeZMin] 
    }
    if { $j > [$coronalViewer GetWholeZMax] } { 
	set j [$coronalViewer GetWholeZMax] 
    }
    if { $k < [$axialViewer GetWholeZMin] } { 
	set k [$axialViewer GetWholeZMin] 
    }
    if { $k > [$axialViewer GetWholeZMax] } { 
	set k [$axialViewer GetWholeZMax] 
    }

    return [list $idx $i $j $k]
}
# Make interface
#
#
frame .params
frame .renderingGrid
frame .status
#frame .logo

#Logo:
#image create photo logoImage -file "C:/Projects/VirtualSoldier/Work/VirtualSoldier/Scripts/insideGElogo_37.gif"
#label $.logo.logoLabel -image logoImage
#grid config $displayFrame.logoLabel -column 2 -row 0 -sticky e

# Render widgets
set axialWidget [vtkTkRenderWidget .renderingGrid.axial \
   -width 250 \
   -height 250 \
   -rw [axialViewer GetRenderWindow]]
set coronalWidget [vtkTkRenderWidget .renderingGrid.coronal \
   -width 250 \
   -height 250 \
   -rw [coronalViewer GetRenderWindow]]
set sagittalWidget [vtkTkRenderWidget .renderingGrid.sagittal \
   -width 250 \
   -height 250 \
   -rw [sagittalViewer GetRenderWindow]]
set modelWidget [vtkTkRenderWidget .renderingGrid.model \
   -width 250 \
   -height 250 \
   -rw modelWindow]

# Sliders per widget for ZSlice
set zMin [axialViewer GetWholeZMin]
set zMax [axialViewer GetWholeZMax]
set axialSlice [expr ($zMax - $zMin) / 2]
scale .renderingGrid.axialScale  -from $zMin -to $zMax -orient horizontal \
    -command SetAxialSlice -variable axialSlice -res 1
set zMin [coronalViewer GetWholeZMin]
set zMax [coronalViewer GetWholeZMax]
set coronalSlice [expr ($zMax - $zMin) / 2]
scale .renderingGrid.coronalScale  -from $zMin -to $zMax -orient horizontal \
    -command SetCoronalSlice -variable coronalSlice -res 1
set zMin [sagittalViewer GetWholeZMin]
set zMax [sagittalViewer GetWholeZMax]
set sagittalSlice [expr ($zMax - $zMin) / 2]
scale .renderingGrid.sagittalScale  -from $zMin -to $zMax -orient horizontal \
    -command SetSagittalSlice -variable sagittalSlice -res 1

# Layout the rendering widgets
grid config .renderingGrid.axial -column 0 -row 0 -sticky snew
grid config .renderingGrid.axialScale -column 0 -row 1 -sticky ew
grid config .renderingGrid.model -column 1 -row 0 -sticky snew
grid config .renderingGrid.coronal -column 0 -row 2 -sticky snew
grid config .renderingGrid.coronalScale -column 0 -row 3 -sticky ew
grid config .renderingGrid.sagittal -column 1 -row 2 -sticky snew
grid config .renderingGrid.sagittalScale -column 1 -row 3 -sticky ew
# Set these widgets to grow with the window
grid rowconfigure .renderingGrid 0 -weight 1
grid rowconfigure .renderingGrid 2 -weight 1
grid columnconfigure .renderingGrid 0 -weight 1
grid columnconfigure .renderingGrid 1 -weight 1

# Layout the sliders and buttons
set dirname C:/Projects/VirtualSoldier/Work/VirtualSoldier/Scripts
source [file join [file dirname [info script]] OverlayInterface.tcl]
# Put it all together
grid config .renderingGrid -column 0 -row 0 -sticky news
grid config .params -column 1 -row 0 -sticky sn
grid config .status -column 0 -row 1 -columnspan 2 -sticky ew
# Set these widgets to grow with the window
grid columnconfigure . 0 -weight 1
grid rowconfigure . 0 -weight 1
grid rowconfigure . 1 -weight 0
# Setup the interactors
#
#
::vtk::bind_tk_render_widget $axialWidget
::vtk::bind_tk_render_widget $coronalWidget
::vtk::bind_tk_render_widget $sagittalWidget
::vtk::bind_tk_render_widget $modelWidget
# Sync all the necessary events between the windows
SyncEvents $axialRW
SyncEvents $coronalRW
SyncEvents $sagittalRW

# Add an observer to the modelWidget for the key events
[$axialRW GetInteractor] AddObserver KeyPressEvent "ProcessKeys $axialRW"
[$coronalRW GetInteractor] AddObserver KeyPressEvent "ProcessKeys $coronalRW"
[$sagittalRW GetInteractor] AddObserver KeyPressEvent "ProcessKeys $sagittalRW"
# Add an observer to the model window to define our own keys
[$modelRW GetInteractor] AddObserver KeyPressEvent "ProcessModelKeys $modelRW"

# Since we added a key event observer we must also provide the "user event"
[$axialRW GetInteractor] AddObserver UserEvent {wm deiconify .vtkInteract; raise .vtkInteract }
[$coronalRW GetInteractor] AddObserver UserEvent {wm deiconify .vtkInteract; raise .vtkInteract }
[$sagittalRW GetInteractor] AddObserver UserEvent {wm deiconify .vtkInteract; raise .vtkInteract }
[$modelRW GetInteractor] AddObserver UserEvent {wm deiconify .vtkInteract; raise .vtkInteract }
# Set up the images to display zoomed in
[[$axialRW GetInteractor] FindPokedRenderer 0 0] ResetCamera
[[$sagittalRW GetInteractor] FindPokedRenderer 0 0] ResetCamera
[[$coronalRW GetInteractor] FindPokedRenderer 0 0] ResetCamera
ResetCamera $axialRW
ResetCamera $coronalRW 
ResetCamera $sagittalRW
# Swap in an image style interactor (the TkRenderWidgets set up an
# interactor for geometry not images)
vtkInteractorStyleImage axialStyle
eval [$axialRW GetInteractor] SetInteractorStyle axialStyle
vtkInteractorStyleImage coronalStyle
eval [$coronalRW GetInteractor] SetInteractorStyle coronalStyle
vtkInteractorStyleImage sagittalStyle
eval [$sagittalRW GetInteractor] SetInteractorStyle sagittalStyle

vtkInteractorStyleSwitch modelStyle
eval [$modelRW GetInteractor] SetInteractorStyle modelStyle

# Disable the char event processing on the interactor style so that
# the "3" key does NOT cause a stereo render
axialStyle AddObserver CharEvent {}
coronalStyle AddObserver CharEvent {}
sagittalStyle AddObserver CharEvent {}
# Our model style is a "switch" so we need to disable the CharEvent
# on any of the substyles that we are interested in
modelStyle AddObserver CharEvent {}
modelStyle SetCurrentStyleToTrackballCamera
[modelStyle GetCurrentStyle] AddObserver CharEvent {}
modelStyle SetCurrentStyleToJoystickCamera
[modelStyle GetCurrentStyle] AddObserver CharEvent {}
modelStyle SetCurrentStyleToTrackballCamera
#
#
#
wm protocol . WM_DELETE_WINDOW ::vtk::cb_exit
wm deiconify . 
tkwait window .


