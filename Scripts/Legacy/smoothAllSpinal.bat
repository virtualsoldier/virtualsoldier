set prog=c:\millerjv\Watershed\lib\vtk\vtkITK
%prog% MakeASmoothModel.tcl cervical_spinal_cord_nerve_root
%prog% MakeASmoothModel.tcl cervical_spinal_ganglia
%prog% MakeASmoothModel.tcl spinal_cord
%prog% MakeASmoothModel.tcl spinal_dura_mater
%prog% MakeASmoothModel.tcl spinal_epidural_space
%prog% MakeASmoothModel.tcl spinal_nerves
%prog% MakeASmoothModel.tcl thoracic_spinal_cord
%prog% MakeASmoothModel.tcl thoracic_spinal_cord_nerve_root
%prog% MakeASmoothModel.tcl thoracic_spinal_dura_mater
%prog% MakeASmoothModel.tcl thoracic_spinal_epidural_space
%prog% MakeASmoothModel.tcl thoracic_spinal_ganglia
