catch {package require BWidget}
catch {package require vtk}
catch {package require vtkinteraction}

set vtkModelFilename [lindex $argv 0]

vtkPolyDataReader reader
  reader SetFileName ${vtkModelFilename}
  reader Update

vtkMassProperties massProps
  massProps SetInput [reader GetOutput]
  massProps Update
  
set volume [massProps GetVolume]

puts $volume