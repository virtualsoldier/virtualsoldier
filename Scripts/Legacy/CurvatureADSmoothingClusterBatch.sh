# This script smoothes all the pig images using the cluster.
# It should be run on a linux machine.

pigNo=ISRPA58_UnZoomed
mainDirec="/projects/soldier/Data/CT/ISR/${pigNo}"

#mainDirec="S:/Data/CT/ISR/${pigNo}"

if (test $pigNo = ISRPA49a )
   then inputDirec="${mainDirec}/Dicoms"
elif (test $pigNo = ISRPA73_30% )
   then inputDirec="${mainDirec}/P73DicomPreInjury30%"
elif (test $pigNo = ISRPA73_80% )
    then inputDirec="${mainDirec}/P73DicomPreInjury80%"
elif (test $pigNo = ISRPA58_Zoomed )
    then inputDirec="${mainDirec}/P58Pre1DicomUncZoomed"
elif (test $pigNo = ISRPA58_UnZoomed )
    then inputDirec="${mainDirec}/P58Pre1DicomUncUnZoomed"
fi

echo $pigNo
echo $mainDirec

smoothingType="CurvatureADSmoothing"

if (test $smoothingType = CurvatureADSmoothing )
then outputDirec="${mainDirec}/CurvatureADSmoothed2D"
    execFile="/projects/soldier/bin/CurvatureADSmoothing"
fi

for param1 in 20
do
    for param2 in 0.0625
    do
        for param3 in 1.5
        do
            for inputImage in ${inputDirec}/*.dcm
            do
                imageName=`basename $inputImage .dcm`
                echo imageName: $imageName
            
                outputImage="${outputDirec}/${imageName}_${param1}_${param2}_${param3}.dcm"

                # Launch the jobs on the cluster
                /usr/local/lsf/bin/bsub -q backfill -W 1 -n 1 -o stdout.log -e stderr.log $execFile $inputImage $outputImage $param1 $param2 $param3
                #$execFile $inputImage $outputImage $param1 $param2 $param3

            done
        done
    done
done