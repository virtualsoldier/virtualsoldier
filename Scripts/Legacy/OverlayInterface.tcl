# a simple user interface that manipulates window level.
# places in the tcl top window.  Looks for object named viewer

#only use this interface when not doing regression tests
if {[info commands rtExMath] != "rtExMath"} {

# Take window level parameters from viewer
proc InitializeControls {} {
    global blendOpacity
    global progress progressMessage
    global axialSlice coronalSlice sagittalSlice
    global relabelValue
    global relabelHexColor
    global relabelerFrame

    # Setup some fonts
    option add *TitleFrame.l.font {helvetica 10 bold italic}

    
    # parameters to control display
    #
    #
    #GE Logo:
    set gelogo [TitleFrame .params.gelogo -text "Imagination at Work"]
    set gelogoFrame [$gelogo getframe]
    image create photo gelogoImage -file "S:/tmp/GELogo.gif" 
    label $gelogoFrame.gelogoLabel -image gelogoImage 


    #VS Logo:
    set vslogo [TitleFrame .params.vslogo]
    set vslogoFrame [$vslogo getframe]
    image create photo vslogoImage -file "S:/tmp/VSLogo.gif" 
    label $vslogoFrame.vslogoLabel -image vslogoImage 

    #Relabel Settings:
    set relabeler [TitleFrame .params.relabeler -text "Relabeler Settings"]
    set relabelerFrame [$relabeler getframe]
    label $relabelerFrame.relabelerLabel -text "Relabel Value" -anchor ne
    SpinBox $relabelerFrame.relabelerEntry -command SetRelabelValue -modifycmd SetRelabelValue -range "0 512 1" -textvariable relabelValue -width 5
    label $relabelerFrame.relabelerBitmap -background $relabelHexColor -foreground $relabelHexColor -height 1 -width 2 
    button $relabelerFrame.relabelerWrite -text "Write" -command WriteRelabeled
    button $relabelerFrame.relabelerUndo  -text "Undo"  -command UndoRelabeled

    set display [TitleFrame .params.display -text "Display Settings"]
    set displayFrame [$display getframe]

    label $displayFrame.blendLabel -text "Blend" -anchor s
    scale $displayFrame.blendScale -orient horizontal -from 0 -to 1 -res 0.01 -command SetBlend -variable blendOpacity

    label $displayFrame.axialLabel -text "Axial (K)" -anchor ne
    SpinBox $displayFrame.axialEntry -command SetAxialSlice -modifycmd SetAxialSlice -range "[axialViewer GetWholeZMin] [axialViewer GetWholeZMax] 1" -textvariable axialSlice -width 5

    label $displayFrame.coronalLabel -text "Coronal (J)" -anchor ne
    SpinBox $displayFrame.coronalEntry -command SetCoronalSlice -modifycmd SetCoronalSlice -range "[coronalViewer GetWholeZMin] [coronalViewer GetWholeZMax] 1" -textvariable coronalSlice -width 5

    label $displayFrame.sagittalLabel -text "Sagittal (I)" -anchor ne
    SpinBox $displayFrame.sagittalEntry -command SetSagittalSlice -modifycmd SetSagittalSlice -range "[sagittalViewer GetWholeZMin] [sagittalViewer GetWholeZMax] 1" -textvariable sagittalSlice -width 5
    
    button .params.exit -text "Exit" -command "exit"

    #grid config $logoFrame
    grid config $gelogoFrame.gelogoLabel -column 0 -row 0 -sticky news
    grid config $vslogoFrame.vslogoLabel -column 0 -row 0 -sticky news
    grid config $relabelerFrame.relabelerLabel -column 0 -row 0 -sticky e
    grid config $relabelerFrame.relabelerEntry -column 1 -row 0 -sticky ew
    grid config $relabelerFrame.relabelerBitmap -column 4 -row 0 -sticky w
    grid config $relabelerFrame.relabelerUndo -column 0 -row 1 -sticky ew
    grid config $relabelerFrame.relabelerWrite -column 1 -row 1 -sticky ew
    #grid config $patientFrame.nameLabel -column 0 -row 0 -sticky ew
    #grid config $patientFrame.name -column 1 -row 0 -sticky ew
    #grid config $patientFrame.studyLabel -column 0 -row 1 -sticky ew
    #grid config $patientFrame.study -column 1 -row 1 -sticky ew

    grid config $displayFrame.blendLabel -column 0 -row 0 -sticky e
    grid config $displayFrame.blendScale -column 1 -row 0 -sticky news

    grid config $displayFrame.axialLabel -column 0 -row 3 -sticky e
    grid config $displayFrame.axialEntry -column 1 -row 3 -sticky ew
    grid config $displayFrame.coronalLabel -column 0 -row 4 -sticky e
    grid config $displayFrame.coronalEntry -column 1 -row 4 -sticky ew
    grid config $displayFrame.sagittalLabel -column 0 -row 5 -sticky e
    grid config $displayFrame.sagittalEntry -column 1 -row 5 -sticky ew

    grid config $gelogo -column 0 -row 0 -sticky ew
    grid config $vslogo -column 0 -row 1 -sticky ew
    grid config $display -column 0 -row 2 -sticky ew
    grid config $relabeler -column 0 -row 3 -sticky ew
    grid config .params.exit -column 0 -row 4 -sticky e

    # initial values
    $displayFrame.blendScale set 0.3
    
    # additional bindings
#    bind $algorithmFrame.scaleEntry <Return> { SetScale [%W get] }
#    bind $algorithmFrame.thresholdEntry <Return> { SetThreshold [%W get] }

    # status/progress widgets
    #
    #
    Label .status.message -relief ridge -textvariable progressMessage
    Label .status.position -relief ridge -textvariable positionMessage
    ProgressBar .status.progress -maximum 100 -relief ridge -height 19 -variable progress

    # put together the status area
    grid config .status.message -column 0 -row 0 -sticky ew
    grid config .status.position -column 1 -row 0 -sticky ew
    grid config .status.progress -column 2 -row 0 -sticky ew
    # set these widgets to grow 
    grid columnconfig .status 0 -weight 1
    grid columnconfig .status 1 -weight 1
    grid columnconfig .status 2 -weight 1
}
   
proc SetBlend { val } {
    global blendOpacity
    axialBlend SetOpacity 1 $blendOpacity 
    coronalBlend SetOpacity 1 $blendOpacity 
    sagittalBlend SetOpacity 1 $blendOpacity 
    axialViewer Render
    coronalViewer Render
    sagittalViewer Render
}

InitializeControls

} else {
  axialViewer Render
  coronalViewer Render
  sagittalViewer Render
}

