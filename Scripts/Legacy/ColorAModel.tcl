#
# Add vertex colors to a vtk model
#

if {$argc < 1} {
  puts "usage: vtk ColorAModel.tcl model_name"
  exit
}

if {$tcl_platform(platform) == "windows"} {
  set MasterAnatomyFile "s:/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "s:/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Relabeled/Relabeled"
  set RGBFilePrefix "s:/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set SmoothedPath "s:/Data/Atlas/Processed/Models/Smoothed/"
  set DiscretePath "s:/Data/Atlas/Processed/Models/Discrete/"
  set ColoredPath "s:/Data/Atlas/Processed/Models/Colored/"
} else {
  set MasterAnatomyFile "/projects/soldier/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "/projects/soldier/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "/projects/soldier/Data/Atlas/Processed/Relabeled/Relabeled"
  set RGBFilePrefix "/projects/soldier/Data/Atlas/Processed/70mm_manual_2048_reg/"
  set SmoothedPath "/projects/soldier/Data/Atlas/Processed/Models/Smoothed/"
  set DiscretePath "/projects/soldier/Data/Atlas/Processed/Models/Discrete/"
  set ColoredPath "/projects/soldier/Data/Atlas/Processed/Models/Colored/"
}
set LabelFilePattern "%s%d.png"
set RGBFilePattern "%s%d.png"
set structure [lindex $argv 0]


###################################
# Parameters
#
set WHICH_MODELS_IN ${SmoothedPath}
set WHICH_MODELS_OUT ${ColoredPath}
set GLOBAL_ORIGIN "0 0 0"
set GLOBAL_SPACING ".333333 .333333 1"
set MAX_I 2048
set MAX_J 1350
set MIN_SLICE 1253
set MAX_SLICE 1663
set PASS_CELL_DATA 0
#
set MAX_I_EXTENT [expr $MAX_I - 1]
set MAX_J_EXTENT [expr $MAX_J - 1]
###################################

#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

# pad with slices on top and bottom so that objects are capped
set iMin [lindex $Segment(${structure},bounds) 0]
set iMin [expr $iMin - 2]

set iMax [lindex $Segment(${structure},bounds) 1]
set iMax [expr $iMax + 2]

set jMin [lindex $Segment(${structure},bounds) 2]
set jMin [expr $jMin - 2]

set jMax [lindex $Segment(${structure},bounds) 3]
set jMax [expr $jMax + 2]

set sliceMin [lindex $Segment(${structure},bounds) 6]
set sliceMin [expr $sliceMin - 2]
if {$sliceMin < $MIN_SLICE} { set sliceMin $MIN_SLICE}

set sliceMax [lindex $Segment(${structure},bounds) 7]
set sliceMax [expr $sliceMax + 2]
if {$sliceMax > $MAX_SLICE} { set sliceMax $MAX_SLICE}

set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin $sliceMin"
puts "sliceMax $sliceMax"
puts "slices $slices"
if {$sliceMax < $sliceMin} {exit}
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
  catch {vtkPNGReader reader }
    reader SetFilePattern $RGBFilePattern
    reader SetFilePrefix $RGBFilePrefix
    reader SetDataExtent 0 $MAX_I_EXTENT 0 $MAX_J_EXTENT $slice $slice

  catch {vtkImageClip clip}
    clip SetInput [reader GetOutput]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr $MAX_J_EXTENT - $jMax] [expr $MAX_J_EXTENT - $jMin] $slice $slice
    clip Update

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInput [clip GetOutput]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    [clip GetOutput] SetSource ""
    clip Delete
}
buildUpImage UpdateWholeExtent
buildUpImage ReleaseDataFlagOn

vtkImageChangeInformation shiftOrigin
  shiftOrigin SetInput [buildUpImage GetOutput]
eval shiftOrigin SetOutputOrigin ${GLOBAL_ORIGIN}
eval shiftOrigin SetOutputSpacing ${GLOBAL_SPACING}
  shiftOrigin ReleaseDataFlagOn

vtkPolyDataReader readPD
  readPD SetFileName "${WHICH_MODELS_IN}${structure}.vtk"
  readPD Update

vtkProbeFilter probeRGB
  probeRGB SetInput [readPD GetOutput]
  probeRGB SetSource [shiftOrigin GetOutput]

if {$PASS_CELL_DATA} {
  probeRGB Update
  [[probeRGB GetOutput] GetCellData] SetScalars [[[probeRGB GetInput] GetCellData] GetScalars]
}

vtkPolyDataWriter writer
  writer SetFileName "${WHICH_MODELS_OUT}${structure}.vtk"
  writer SetInput [probeRGB GetOutput]
  writer SetFileTypeToBinary
  writer Modified
  puts "${WHICH_MODELS_OUT}${structure}.vtk"
  writer Write

exit
