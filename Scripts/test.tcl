#
# Make vtk files of a given structure
#
package require vtk
package require vtkinteraction

if {$argc < 1} {
  puts "usage: vtk DecimateAModel.tcl model_name"
  exit
}

set structure [lindex $argv 0]

if {$tcl_platform(platform) == "windows"} {
  set MasterAnatomyFile "s:/Data/Atlas/Processed/MasterAnatomy.csv"
  set BBFile "s:/Data/Atlas/Processed/AnatomyBoundingBoxes.csv"
  set LabelFilePrefix "s:/Data/Atlas/Processed/Relabeled/Relabeled"
  set modelPath "s:/Data/Atlas/Processed/Models/Decimated/"
} else {
  set MasterAnatomyFile "../DataFMAAtlas/MasterAnatomyFMA.csv"
  set BBFile "../DataFMAAtlas/AnatomyBoundingBoxesFMA.csv"
  set LabelFilePrefix "../DataFMAAtlas/LabelMaps/Repaired"
  set modelPath "/home/lorensen/tmp/Decimated/"
}
set LabelFilePattern "%s%d.png"
set structure [lindex $argv 0]


vtkMath math
vtkLookupTable lut

proc makecolors {n} {
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
   }
}
makecolors 512
#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
  set SegmentNum($label,object) $object

}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

# Process one object at a time
# Limit number of input slices to SliceCapacity
set SliceCapacity 200

# pad with slices on top and bottom so that objects are capped
set iMin [lindex $Segment(${structure},bounds) 0]
set iMin [expr $iMin - 1]

set iMax [lindex $Segment(${structure},bounds) 1]
set iMax [expr $iMax + 1]

set jMin [lindex $Segment(${structure},bounds) 2]
set jMin [expr $jMin - 1]

set jMax [lindex $Segment(${structure},bounds) 3]
set jMax [expr $jMax + 1]

set sliceMin [lindex $Segment(${structure},bounds) 6]
set sliceMin [expr $sliceMin - 1]
if {$sliceMin < 1253} { set sliceMin 1253}

set sliceMax [lindex $Segment(${structure},bounds) 7]
set sliceMax [expr $sliceMax + 1]
if {$sliceMax > 1663} { set sliceMax 1663}

set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin $sliceMin"
puts "sliceMax $sliceMax"
puts "slices $slices"
if {$sliceMax < $sliceMin} {exit}
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
    catch {vtkPNGReader reader}
    reader SetDataSpacing .333333 .333333 1
    reader SetFilePattern $LabelFilePattern
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataOrigin 0 0 $sliceMin
    reader SetDataExtent 0 2047 0 1349 $slice $slice

  catch {vtkImageClip clip}
    clip SetInputConnection [reader GetOutputPort]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr 1349 - $jMax] [expr 1349 - $jMin] $slice $slice
    clip Update
    set clipOutput [clip GetOutput]
#    $clipOutput Register {}

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInputConnection [clip GetOutputPort]
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    clip Delete
#    $clipOutput UnRegister {}
}
  buildUpImage UpdateWholeExtent
puts "[[[[buildUpImage GetOutput] GetPointData] GetScalars] GetRange]"
  catch {vtkImageChangeInformation shiftOrigin}
    shiftOrigin SetInputConnection [buildUpImage GetOutputPort]
    shiftOrigin SetOutputOrigin [expr $iMin * .333333] [expr ( 1349 - $jMax ) * .333333] $sliceMin

  catch {vtkImageAccumulate histo}
    histo SetInputConnection [shiftOrigin GetOutputPort]
    histo SetComponentExtent 0 511 0 0 0 0
    histo SetComponentOrigin 0 0 0
    histo SetComponentSpacing 1 1 1
    histo Update

puts "Label: $Segment(${structure},label)"
  catch {vtkDiscreteMarchingCubes cubes}
    cubes SetInputConnection [shiftOrigin GetOutputPort]
    cubes SetInputConnection [buildUpImage GetOutputPort]
    cubes SetValue 0 $Segment(${structure},label)
    cubes ComputeNormalsOff
    cubes ComputeScalarsOff
    cubes ComputeGradientsOff
    cubes ReleaseDataFlagOn
    cubes AddObserver EndEvent "puts \"Cubes complete\""
set iterations 15
set passBand 0.001
cubes Update
puts "[[cubes GetOutput] Print]"
catch {vtkWindowedSincPolyDataFilter smoother}
    smoother SetInputConnection [cubes GetOutputPort]
    smoother SetNumberOfIterations $iterations
    smoother BoundarySmoothingOff
    smoother FeatureEdgeSmoothingOff
    smoother SetPassBand $passBand
    smoother NonManifoldSmoothingOn
    smoother NormalizeCoordinatesOn
    smoother AddObserver EndEvent "puts \"Smoothing complete\""
    smoother ReleaseDataFlagOn

set DECIMATE_REDUCTION .95
set DECIMATE_ITERATIONS 5
set DECIMATE_ERROR .0002
set DECIMATE_ERROR_INCREMENT .0002

catch {vtkDecimatePro decimate}
    decimate SetInputConnection [smoother GetOutputPort]
    decimate SetTargetReduction .9
    decimate PreserveTopologyOn
    decimate BoundaryVertexDeletionOff
#    decimate SetFeatureAngle 90
    decimate ReleaseDataFlagOn

catch {vtkPolyDataNormals normals}
    normals SetInputConnection [decimate GetOutputPort]
    normals SetFeatureAngle 90
    normals Update

set label $Segment(${structure},label)
puts "label: $label"
vtkUnsignedCharArray cellArray
  cellArray SetNumberOfComponents 3  
  cellArray SetNumberOfTuples [[normals GetOutput] GetNumberOfCells]
  cellArray FillComponent 0 [expr [lindex [lut GetTableValue $label] 0] * 255 ]
  cellArray FillComponent 1 [expr [lindex [lut GetTableValue $label] 1] * 255 ]
  cellArray FillComponent 2 [expr [lindex [lut GetTableValue $label] 2] * 255 ]

  [[normals GetOutput] GetCellData] SetScalars cellArray

  catch {vtkPolyDataWriter writer}
    writer SetFileName "${modelPath}${structure}.vtk"
    writer SetInputConnection [normals GetOutputPort]
    writer SetFileTypeToBinary
    writer Modified
    writer Write
puts "${modelPath}${structure}.vtk"

exit
