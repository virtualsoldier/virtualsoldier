###############################################################################
#
# Define paths and files for Virtual Soldier scripts
# These paths corresponds to the organization of files at GE Global Research.
# The paths described here can be overridden in the tcl script LocalPaths.tcl.
#

set DATA_ROOT "/home/lorensen/Data/VisibleHuman/DataFMAAtlas/"
set MODELS_ROOT "/tmp/"

set MasterAnatomyFile "${DATA_ROOT}/MasterAnatomyFMA.csv"
set BBFile "${DATA_ROOT}/AnatomyBoundingBoxesFMA.csv"
set RGBFilePrefix "${DATA_ROOT}/70mm_manual_2048_reg/"
set AdjacencyMappingFile "${DATA_ROOT}/AdjacencyMappingFMA.csv"
set LabelFilePrefix "${DATA_ROOT}/LabelMaps/Repaired"

set DiscretePath "${MODELS_ROOT}/Discrete/"
set SmoothedPath "${MODELS_ROOT}/Smoothed/"
set ColoredPath "${MODELS_ROOT}/Colored/"
set DecimatedPath "${MODELS_ROOT}/Decimated/"

#
# If present, the file LocalPaths.tcl can override the paths and files defined
# in this script
#

if {[file exists [file dirname [info script]]/LocalPaths.tcl] } {
    source [file dirname [info script]]/LocalPaths.tcl
}
