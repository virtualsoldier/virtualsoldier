#
# Display Anatomies with adjacent anatomies
#


package require vtk
package require vtktesting
package require vtkinteraction

if {$argc < 1} {
  puts "usage: vtk ShowAdjacentAnatomies.tcl model_name"
  exit
}

set structure [lindex $argv 0]

source [file dirname [info script]]/PathsFMA.tcl

puts "Structure is: $structure"

#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
  set SegmentNum($label,object) $object
}
close $maFID
puts $Segment($structure,label)

#
# Read the Adjacency Mapping file
#
puts $AdjacencyMappingFile
set amFID [open $AdjacencyMappingFile r]
# Skip the first line. It is just descriptive
gets $amFID line
while {[gets $amFID line] >= 0} {
  set fields [split $line ","]
  set am [lrange $fields 1 end]
  set label [lindex $fields 0]
    if { [info exists SegmentNum($label,object)] } {
  set object $SegmentNum($label,object)
  set Segment($object,adjacent) $am
    }
}

close $amFID

###################################
# Parameters
#
set WHICH_MODELS_IN ${DiscretePath}
set WHICH_MODELS_IN ${DecimatedPath}
set modelPath ${WHICH_MODELS_IN}


set VTKFileNames "${WHICH_MODELS_IN}${structure}.vtk"

vtkMath math
vtkLookupTable lut

proc pick {} {
    set actor [lindex [split [lindex [split [[iren GetPicker] GetActor] .] 0] / ] end ]
    set structure [regsub -all _ $actor " "]
    textMapper SetInput $structure
    textActor SetPosition 10 10
    textActor VisibilityOn
    renWin Render
}

proc makecolors {n} {
   lut SetNumberOfColors $n
   lut SetTableRange 0 [expr $n-1]
   lut SetScaleToLinear
   lut Build
   lut SetTableValue 0 0 0 0 1
   math RandomSeed 5071
   for {set i 1} {$i < $n } {incr i} {
     lut  SetTableValue $i [math Random .2 1]  [math Random .2 1]  [math Random .2 1]  1
   }
}

makecolors 512

vtkRenderer ren1
  ren1 SetBackground 0.4667 0.5333 0.6000
vtkRenderWindow renWin
  renWin AddRenderer ren1
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin



puts "label is $label"
puts "Structure $structure has label $Segment(${structure},label)"
puts "Label $Segment($structure,label) corresponds to structure $SegmentNum($Segment($structure,label),object)"
puts "Adjacent anatomies to $structure are $Segment($structure,adjacent)"
puts "Length of $structure list is [llength $Segment($structure,adjacent)]"
puts "VTK structure name is $VTKFileNames"

set allFiles [glob $VTKFileNames]

foreach i $Segment($structure,adjacent) {
  set adjacent $SegmentNum($i,object)
  puts "Adjacent Structure is $adjacent"
  set AdjacentFileName "${WHICH_MODELS_IN}${adjacent}.vtk"
  if {$adjacent == "Empty"} {
    # do nothing
  } elseif {[file isfile "${modelPath}${adjacent}.vtk"] == 0} {
    puts "------------ ${modelPath}${adjacent} is missing"
    # do nothing
  } else {
    #puts "[glob $AdjacentFileName]"
    set allFiles [concat $allFiles [glob $AdjacentFileName] ]
  }
}
puts $allFiles
set n 1
foreach file $allFiles {
    if {[info commands ${file}Reader] != ""} {
      continue
    }
  vtkPolyDataReader ${file}Reader
    ${file}Reader SetFileName $file
    ${file}Reader AddObserver StartEvent "puts -nonewline \"$file\"; flush stdout"
    ${file}Reader AddObserver EndEvent {puts " Complete"}

  vtkPolyDataNormals ${file}Normals
    ${file}Normals SetInputConnection [${file}Reader GetOutputPort]
    ${file}Normals SetFeatureAngle 60

  vtkPolyDataMapper ${file}Mapper
    if {$WHICH_MODELS_IN == $DiscretePath} {
      ${file}Mapper SetInputConnection [${file}Reader GetOutputPort]
    } else {
      ${file}Mapper SetInputConnection [${file}Normals GetOutputPort]
    }
    ${file}Mapper SetLookupTable lut
    ${file}Mapper SetScalarRange 0 [lut GetNumberOfColors]
    ${file}Mapper ImmediateModeRenderingOn

  vtkLODActor ${file}Actor
    ${file}Actor SetMapper ${file}Mapper
    [${file}Actor GetProperty] SetAmbient .3

  ren1 AddActor ${file}Actor

  [ren1 GetActiveCamera] SetViewUp 0 0 -1
  [ren1 GetActiveCamera] SetFocalPoint 0 0 0
  [ren1 GetActiveCamera] SetPosition 0 1 0
  ren1 ResetCamera
  ren1 ResetCameraClippingRange
  renWin Render
  incr n
}

# Create a text mapper and actor to display the results of picking
vtkTextMapper textMapper
set tprop [textMapper GetTextProperty]
    $tprop SetFontFamilyToArial
    $tprop SetFontSize 20
vtkActor2D textActor
    textActor VisibilityOff
    textActor SetMapper textMapper

ren1 AddActor textActor

iren AddObserver UserEvent {wm deiconify .vtkInteract}
iren AddObserver EndPickEvent pick

iren Initialize

renWin Render
wm withdraw .
iren Start

