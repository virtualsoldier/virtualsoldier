#
# Make vtk files of a given structure
#

package require vtk

if {$argc < 1} {
  puts "usage: vtk MakeASmoothModelFMA.tcl model_name"
  exit
}

set structure [lindex $argv 0]

source [file dirname [info script]]/PathsFMA.tcl

set modelPath ${SmoothedPath}
set LabelFilePattern "%s%d.png"
set structure [lindex $argv 0]
#
# Read the Master Anatomy file
#

set maFID [open $MasterAnatomyFile r]
# Skip the first line. It is just descriptive
gets $maFID line
while {[gets $maFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set label [lindex $fields 1]
  set Segment($object,label) $label
  set SegmentNum($label,object) $object

}
close $maFID

#
# Read the Bounding Box file
#
set bbFID [open $BBFile r]
# Skip the first line. It is just descriptive
gets $bbFID line
while {[gets $bbFID line] >= 0} {
  set fields [split $line ","]
  regsub -all " " [lindex $fields 0] "_" object
  set bb [lrange $fields 1 end]
  set Segment($object,bounds) $bb
}
close $bbFID

# Process one object at a time
# Limit number of input slices to SliceCapacity
set SliceCapacity 200

# pad with slices on top and bottom so that objects are capped
set iMin [lindex $Segment(${structure},bounds) 0]
set iMin [expr $iMin - 1]

set iMax [lindex $Segment(${structure},bounds) 1]
set iMax [expr $iMax + 1]

set jMin [lindex $Segment(${structure},bounds) 2]
set jMin [expr $jMin - 1]

set jMax [lindex $Segment(${structure},bounds) 3]
set jMax [expr $jMax + 1]

set sliceMin [lindex $Segment(${structure},bounds) 6]
set sliceMin [expr $sliceMin - 1]
if {$sliceMin < 1253} { set sliceMin 1253}

set sliceMax [lindex $Segment(${structure},bounds) 7]
set sliceMax [expr $sliceMax + 1]
if {$sliceMax > 1663} { set sliceMax 1663}

set slices [expr $sliceMax - $sliceMin + 1]

puts "sliceMin $sliceMin"
puts "sliceMax $sliceMax"
puts "slices $slices"
if {$sliceMax < $sliceMin} {exit}
set s 0
for { set slice $sliceMin } { $slice <= $sliceMax } { incr slice } {
    catch {vtkPNGReader reader}
    reader SetDataSpacing .333333 .333333 1
    reader SetFilePattern $LabelFilePattern
    reader SetFilePrefix $LabelFilePrefix
    reader SetDataOrigin 0 0 $sliceMin
    reader SetDataExtent 0 2047 0 1349 $slice $slice

  catch {vtkImageClip clip}
    clip SetInputConnection [reader GetOutputPort]
    clip ClipDataOn
    clip SetOutputWholeExtent $iMin $iMax [expr 1349 - $jMax] [expr 1349 - $jMin] $slice $slice
    clip Update

  vtkImageData anImage$slice
    anImage$slice ShallowCopy [clip GetOutput]

  catch {vtkImageAppend buildUpImage}
    buildUpImage AddInputData anImage$slice
    buildUpImage PreserveExtentsOff
    buildUpImage SetAppendAxis 2    
    clip Delete
    anImage$slice Delete
}
  buildUpImage UpdateWholeExtent

  catch {vtkImageChangeInformation shiftOrigin}
    shiftOrigin SetInputConnection [buildUpImage GetOutputPort]
    shiftOrigin SetOutputOrigin [expr $iMin * .333333] [expr ( 1349 - $jMax ) * .333333] $sliceMin

  catch {vtkImageAccumulate histo}
    histo SetInputConnection [shiftOrigin GetOutputPort]
    histo SetComponentExtent 0 2999 0 0 0 0
    histo SetComponentOrigin 0 0 0
    histo SetComponentSpacing 1 1 1
    histo Update

  catch {vtkDiscreteMarchingCubes cubes}
    cubes SetInputConnection [shiftOrigin GetOutputPort]
    cubes SetValue 0 $Segment(${structure},label)
    set j 1
    for {set i 1} {$i < 3000} { incr i } {
	set freq [[[[histo GetOutput] GetPointData] GetScalars] GetTuple1 $i]
	if { $freq == 0 } continue
	if { $i == $Segment(${structure},label) } continue
	set neighbor $SegmentNum($i,object)
	puts "Neighboring Structure is $neighbor"
    	cubes SetValue $j $Segment(${neighbor},label)
	incr j
    }

    cubes ComputeGradientsOff
    cubes ReleaseDataFlagOn
    cubes AddObserver EndEvent "puts \"Cubes complete\""
set iterations 15
set passBand 0.001

catch {vtkWindowedSincPolyDataFilter smoother}
    smoother SetInputConnection [cubes GetOutputPort]
    smoother SetNumberOfIterations $iterations
    smoother BoundarySmoothingOff
    smoother FeatureEdgeSmoothingOff
    smoother SetPassBand $passBand
    smoother NonManifoldSmoothingOn
    smoother NormalizeCoordinatesOn
    smoother AddObserver EndEvent "puts \"Smoothing complete\""
    smoother ReleaseDataFlagOn
smoother Update
puts "[[[[smoother GetOutput] GetCellData] GetScalars] GetRange]"

catch {vtkThreshold thresholder}
    thresholder SetInputConnection [smoother GetOutputPort]
#    thresholder SetAttributeModeToUseCellData
    thresholder SetInputArrayToProcess 0 0 0 vtkDataObject::FIELD_ASSOCIATION_CELLS vtkDataSetAttributes::SCALARS
    thresholder ThresholdBetween $Segment(${structure},label) $Segment(${structure},label)
    thresholder AddObserver EndEvent "puts \"Thresholding complete\""
    thresholder ReleaseDataFlagOn

catch {vtkGeometryFilter geom}
    geom SetInputConnection [thresholder GetOutputPort]
    geom AddObserver EndEvent "puts \"Geom complete\""

  catch {vtkPolyDataWriter writer}
    writer SetFileName "${modelPath}${structure}.vtk"
puts "${modelPath}${structure}.vtk"
    writer SetInputConnection [geom GetOutputPort]
    writer SetFileTypeToBinary
    writer Modified
    writer Write

exit
